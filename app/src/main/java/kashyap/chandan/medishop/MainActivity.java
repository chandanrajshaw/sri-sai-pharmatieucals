package kashyap.chandan.medishop;



import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import kashyap.chandan.medishop.admin.DashBoard;
import kashyap.chandan.medishop.admin.SharedPreferencesAdmin;
import kashyap.chandan.medishop.agentPannel.UserDashBoard;
import kashyap.chandan.medishop.customer.CustomerRegistration;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    boolean ondoublebackpresed=false;
    TextInputEditText ettemail,ettpassword;
    ConnectionDetector connectionDetector;
    SharedPreferencesAdmin sharedPreferencesAdmin;
    TextView login,forgetpassword,customerRegistration;
    private static final int ALL_PERMISSIONS_RESULT=100;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
setContentView(R.layout.activity_main);
        if ((ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED)
                && (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED)
                && (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED)) {
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA, Manifest.permission.ACCESS_COARSE_LOCATION
                            , Manifest.permission.ACCESS_FINE_LOCATION},
                    ALL_PERMISSIONS_RESULT);
        }
        connectionDetector=new ConnectionDetector(MainActivity.this);
        customerRegistration=findViewById(R.id.customerRegistration);

        ettemail=findViewById(R.id.ettemail);
        ettpassword=findViewById(R.id.ettpassword);
        login=findViewById(R.id.login);
        forgetpassword=findViewById(R.id.forgetpassword);
       sharedPreferencesAdmin =new SharedPreferencesAdmin(MainActivity.this);
        customerRegistration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
Intent intent=new Intent(MainActivity.this, CustomerRegistration.class);
startActivity(intent);
            }
        });
login.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        final String uname=ettemail.getText().toString().trim();
        final String pass=ettpassword.getText().toString().trim();

        if (uname.isEmpty()&& pass.isEmpty())
        {
            Toast.makeText(MainActivity.this, "Enter All the fields", Toast.LENGTH_SHORT).show();

        }
       else if (uname.isEmpty())
        {
            Toast.makeText(MainActivity.this, "Email Field cant be Empty", Toast.LENGTH_SHORT).show();

        }
       else if (pass.isEmpty())
        {
            Toast.makeText(MainActivity.this, "Password Field cant be Empty", Toast.LENGTH_SHORT).show();
        }
        else
            {
                if (!connectionDetector.isConnectingToInternet()){
                    Toast.makeText(MainActivity.this, "Please Connect to internet and try again", Toast.LENGTH_SHORT).show();
                }
                else
                    {
                        final Dialog progressDialog = new Dialog(MainActivity.this);
                        progressDialog.setContentView(R.layout.customdialog);
                        progressDialog.setCancelable(false);
                        progressDialog.show();
                        UserInterface userInterface=ApiClient.getClient().create(UserInterface.class);
                        Call<LoginResponse> loginResponseCall=userInterface.login(uname,pass);
                        loginResponseCall.enqueue(new Callback<LoginResponse>() {
                            @Override
                            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                                if(response.code()==200)
                                {

                                        progressDialog.dismiss();
                                       List<LoginResponse.DataBean> data=response.body().getData();
                                    int datasize=data.size();
                                    String phone=null,id=null,email=null,name=null;
                                    for (LoginResponse.DataBean data1:data)
                                    {
                                         name=data1.getName();
                                         email=data1.getEmail();

                                        phone=data1.getMobile();
                                         id=data1.getUser_id();
                                    }
                                    sharedPreferencesAdmin.getSharedPreference(name,pass,phone,id,uname);
                                    if (id.equalsIgnoreCase("1"))
                                    {
                                        Intent intent=new Intent(MainActivity.this, DashBoard.class);
//                                        intent.putExtra("list", (Serializable) data);
                                        startActivity(intent);
                                        finish();
                                    }
                                    else if (!id.equalsIgnoreCase("1"))
                                    {
                                        Intent intent=new Intent(MainActivity.this, UserDashBoard.class);
//                                        intent.putExtra("list", (Serializable) data);
                                        startActivity(intent);
                                        finish();
                                    }

                                    }

                                    else if (response.code()!=200){
                                        progressDialog.dismiss();
                                        Toast.makeText(MainActivity.this, "Login Failed", Toast.LENGTH_SHORT).show();

                                    }

                                }

                            @Override
                            public void onFailure(Call<LoginResponse> call, Throwable t)
                            {
                                progressDialog.dismiss();
                                Toast.makeText(MainActivity.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
            }
    }
});
forgetpassword.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        Intent intent=new Intent(MainActivity.this,ForgetPassword.class);
        startActivity(intent);
    }
});
    }

    @Override
    public void onBackPressed() {
        if (ondoublebackpresed) {
            super.onBackPressed();
            return;
        }
this.ondoublebackpresed=true;
        Snackbar.make(this.getWindow().getDecorView().findViewById(android.R.id.content), "Please click BACK again to exit", Snackbar.LENGTH_SHORT).show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
              ondoublebackpresed=false;
            }
        },2000);

    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode)
        {
            case ALL_PERMISSIONS_RESULT :

                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Granted. Start getting the location information
                }
                break;


        }

    }
}

package kashyap.chandan.medishop;

import androidx.appcompat.app.AppCompatActivity;


import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;

import kashyap.chandan.medishop.admin.ValidateOTPResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ValidateOTP extends AppCompatActivity {
    ImageView validateotpgobackarrow;
    TextView recoverpassword;
    TextInputEditText recoveryemail,ettrecoverpassword;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
     setContentView(R.layout.activity_validate_otp);
        recoverpassword=findViewById(R.id.recoverpassword);
        validateotpgobackarrow=findViewById(R.id.validateotpgobackarrow);
        recoveryemail=findViewById(R.id.recoveryemail);
        ettrecoverpassword=findViewById(R.id.ettrecoverpassword);
     validateotpgobackarrow.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               overridePendingTransition(R.anim.transitionrighttoleft, R.anim.transitionrighttoleft);
               finish();
           }
       });
     recoverpassword.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View view) {
              String email=recoveryemail.getText().toString().trim();
              String otp=ettrecoverpassword.getText().toString().trim();
              if (email.isEmpty()&& otp.isEmpty()){
                  Toast.makeText(ValidateOTP.this,"Enter Both field",Toast.LENGTH_SHORT).show();
              }
              else if (email.isEmpty()){
                  Toast.makeText(ValidateOTP.this,"Enter Registered Email",Toast.LENGTH_SHORT).show();
              }
              else if (otp.isEmpty()){
                  Toast.makeText(ValidateOTP.this,"Enter Otp got in the Registered Mail",Toast.LENGTH_SHORT).show();

              }
              else {
                  final Dialog progressDialog=new Dialog(ValidateOTP.this);
                  progressDialog.setContentView(R.layout.customdialog);
                  progressDialog.setCancelable(false);
                  progressDialog.show();
                  UserInterface userInterface=ApiClient.getClient().create(UserInterface.class);
                  Call<ValidateOTPResponse>call=userInterface.validateOTP(email,otp);
                  call.enqueue(new Callback<ValidateOTPResponse>() {
                      @Override
                      public void onResponse(Call<ValidateOTPResponse> call, Response<ValidateOTPResponse> response) {
                          if (response.code()==200)
                          {
                              progressDialog.dismiss();
                              Intent intent=new Intent(ValidateOTP.this,ChangePasswordwithOTP.class);
                              startActivity(intent);
                          }
                      }

                      @Override
                      public void onFailure(Call<ValidateOTPResponse> call, Throwable t) {
                          Toast.makeText(ValidateOTP.this,""+t.getMessage(),Toast.LENGTH_SHORT).show();

                      }
                  });

              }
          }
      });
    }
}

package kashyap.chandan.medishop.payment;

import android.app.Dialog;
import android.content.Context;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.List;

import kashyap.chandan.medishop.R;
import kashyap.chandan.medishop.client.ClientDetails;

public class PaymentAdapter extends RecyclerView.Adapter<PaymentAdapter.MyViewHolder> {
    Context context;
    List<PaymentResponse.DataBean>allpayment;
    public PaymentAdapter(Context context,  List<PaymentResponse.DataBean> allpayment) {
        this.context=context;
        this.allpayment=allpayment;
    }

    @NonNull
    @Override
    public PaymentAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.recpayment,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final PaymentAdapter.MyViewHolder holder, int position) {
holder.billno.setText(allpayment.get(position).getBill_no());
holder.status.setText(allpayment.get(position).getStatus());
String type=allpayment.get(position).getPayment_type();
if (type.equalsIgnoreCase("1")) holder.ptype.setText("Cash");
      else if (type.equalsIgnoreCase("2")) holder.ptype.setText("Online");

             else holder.ptype.setText("Cheque");
holder.paidamt.setText(allpayment.get(position).getPaid_amount());
holder.shopname.setText(allpayment.get(position).getShop_name());
holder.date.setText(allpayment.get(position).getDate_time());
        Picasso.get().load("http://www.sbpharma.in/admin_assets/uploads/payments/"+allpayment.get(position).getImage()).error(R.drawable.cross).placeholder(R.drawable.loading).into(holder.billImg);
holder.billImg.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        int pos=holder.getAdapterPosition();
        PaymentResponse.DataBean bill=allpayment.get(pos);
        Dialog imagedialog=new Dialog(context);
        imagedialog.setContentView(R.layout.imagedialog);
        DisplayMetrics metrics=context.getResources().getDisplayMetrics();
        int width=metrics.widthPixels;
        imagedialog.getWindow().setLayout(width, LinearLayout.LayoutParams.MATCH_PARENT);
        ImageView billimg=imagedialog.findViewById(R.id.viewimage);
        Picasso.get().load("http://igranddev.xyz/sb-pharma/admin_assets/uploads/payments/"+bill.getImage()).error(R.drawable.cross)
                .placeholder(R.drawable.loading).into(billimg);
        imagedialog.show();
    }
});
    }

    @Override
    public int getItemCount() {
        return allpayment.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView paidamt,ptype,status,billno,des,shopname,date;
        ImageView billImg;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            paidamt=itemView.findViewById(R.id.pamt);
            ptype=itemView.findViewById(R.id.ptype);
            status=itemView.findViewById(R.id.pstatus);
            billno=itemView.findViewById(R.id.billno);
            des=itemView.findViewById(R.id.desc);
            billImg=itemView.findViewById(R.id.billimg);
            shopname=itemView.findViewById(R.id.pshopname);
            date=itemView.findViewById(R.id.datetime);



        }
    }
}

package kashyap.chandan.medishop.payment;

import java.io.Serializable;

public class AddPaymentResponse implements Serializable {


    /**
     * status : {"code":200,"message":"Admin added Product Details Successfully"}
     * data : {"image":"decl.jpeg","shop_id":"1","paid_amount":"420","payment_type":"1","description":"gjhk","bill_no":"123456"}
     */

    private StatusBean status;
    private DataBean data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class StatusBean implements Serializable{
        /**
         * code : 200
         * message : Admin added Product Details Successfully
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean implements Serializable {
        /**
         * image : decl.jpeg
         * shop_id : 1
         * paid_amount : 420
         * payment_type : 1
         * description : gjhk
         * bill_no : 123456
         */

        private String image;
        private String shop_id;
        private String paid_amount;
        private String payment_type;
        private String description;
        private String bill_no;

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getShop_id() {
            return shop_id;
        }

        public void setShop_id(String shop_id) {
            this.shop_id = shop_id;
        }

        public String getPaid_amount() {
            return paid_amount;
        }

        public void setPaid_amount(String paid_amount) {
            this.paid_amount = paid_amount;
        }

        public String getPayment_type() {
            return payment_type;
        }

        public void setPayment_type(String payment_type) {
            this.payment_type = payment_type;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getBill_no() {
            return bill_no;
        }

        public void setBill_no(String bill_no) {
            this.bill_no = bill_no;
        }
    }
}

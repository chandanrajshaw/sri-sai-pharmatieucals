package kashyap.chandan.medishop.payment;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import kashyap.chandan.medishop.R;

public class PtypeAdapter extends RecyclerView.Adapter<PtypeAdapter.MyViewHolder> {
    Context context;
    String[] payment;
    TextView ptype,pid;
    Dialog paydialog;
    private int SelectedItem = -1;
    String selectedptype,selectedid;
    public PtypeAdapter(Context context, String[] payment, TextView ptype, TextView pid, Dialog paydialog) {
        this.paydialog=paydialog;
        this.context=context;
        this.payment=payment;
        this.ptype=ptype;
        this.pid=pid;

    }

    @NonNull
    @Override
    public PtypeAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.itemlayout,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PtypeAdapter.MyViewHolder holder, int position) {
        holder.radioselect.setChecked(position == SelectedItem);
        holder.textptype.setText(payment[position]);
        TextView head=paydialog.findViewById(R.id.dialoghead);
        head.setText("Select Payment Type");
        TextView ok=paydialog.findViewById(R.id.ok);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ptype.setText(selectedptype);
                if (ptype.getText().toString().isEmpty()){
                    Toast.makeText(context,"Select Payment Type",Toast.LENGTH_SHORT).show();
                }
                else {
                    if (ptype.getText().toString().equalsIgnoreCase("Cash")){ pid.setText("1");paydialog.dismiss(); }
                    else if (ptype.getText().toString().equalsIgnoreCase("Online")){ pid.setText("2");paydialog.dismiss(); }
                    if (ptype.getText().toString().equalsIgnoreCase("Cheque")){ pid.setText("3");paydialog.dismiss(); }
                }

            }
        });
    }

    @Override
    public int getItemCount() {
        return payment.length;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView textptype;
        RadioButton radioselect;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            textptype=itemView.findViewById(R.id.items);
            radioselect=itemView.findViewById(R.id.selectedItem);
            View.OnClickListener clickListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SelectedItem =getAdapterPosition();
                    notifyDataSetChanged();
                    selectedptype=payment[SelectedItem];
                }
            };
            itemView.setOnClickListener(clickListener);
            radioselect.setOnClickListener(clickListener);
        }
    }
}

package kashyap.chandan.medishop.payment;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.material.snackbar.Snackbar;

import java.util.List;

import kashyap.chandan.medishop.ApiClient;
import kashyap.chandan.medishop.ConnectionDetector;
import kashyap.chandan.medishop.R;
import kashyap.chandan.medishop.UserInterface;
import kashyap.chandan.medishop.client.Clients;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Payment extends AppCompatActivity {
ImageView goback;
TextView toolheader;
ConnectionDetector connectionDetector;
ImageView addpay;
    ShimmerFrameLayout shimmerFrameLayout;

    RecyclerView paymentrec;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_payment);
        connectionDetector=new ConnectionDetector(Payment.this);
        shimmerFrameLayout=findViewById(R.id.shimmerview);

        toolheader=findViewById(R.id.customtoolheader);
        toolheader.setText("Payment");
        goback=findViewById(R.id.ordergobackarrow);
        goback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                overridePendingTransition(R.anim.transitionrighttoleft,R.anim.transitionrighttoleft);
                finish();
            }
        });

        paymentrec=findViewById(R.id.paymentlist);
        if (!connectionDetector.isConnectingToInternet()){
            shimmerFrameLayout.stopShimmer();
            shimmerFrameLayout.setVisibility(View.GONE);
            paymentrec.setVisibility(View.GONE);
            Snackbar.make(Payment.this.getWindow().getDecorView().findViewById(android.R.id.content),"Plzz Connect to internet And Try Again",Snackbar.LENGTH_LONG).show();
        }
        else if (connectionDetector.isConnectingToInternet()){
//            final Dialog progress=new Dialog(Payment.this);
//            progress.setContentView(R.layout.customdialog);
//            progress.setCancelable(false);
//            progress.show();
            UserInterface userInterface= ApiClient.getClient().create(UserInterface.class);
            Call<PaymentResponse>call=userInterface.getPayment();
            call.enqueue(new Callback<PaymentResponse>() {
                @Override
                public void onResponse(Call<PaymentResponse> call, Response<PaymentResponse> response) {
                    if (response.code()==200)
                    {
                        shimmerFrameLayout.stopShimmer();
                        shimmerFrameLayout.setVisibility(View.GONE);
                        List<PaymentResponse.DataBean>allpayment=response.body().getData();
                        paymentrec.setLayoutManager(new LinearLayoutManager(Payment.this,LinearLayoutManager.VERTICAL,false));
                        paymentrec.setAdapter(new PaymentAdapter(Payment.this,allpayment));
                       paymentrec.setVisibility(View.VISIBLE);
                    }
                    else if (response.code()!=200)
                    {
                        shimmerFrameLayout.stopShimmer();
                        shimmerFrameLayout.setVisibility(View.GONE);
                        Snackbar.make(Payment.this.getWindow().getDecorView().findViewById(android.R.id.content),"Error Occurs!! No Payments",Snackbar.LENGTH_LONG).show();

                    }
                }

                @Override
                public void onFailure(Call<PaymentResponse> call, Throwable t) {
                    Snackbar.make(Payment.this.getWindow().getDecorView().findViewById(android.R.id.content),""+t.getMessage(),Snackbar.LENGTH_LONG).show();

                }
            });
        }

addpay=findViewById(R.id.addfab);
addpay.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        Intent intent=new Intent(Payment.this,AddPayment.class);
        startActivity(intent);
    }
});
    }
    @Override
    protected void onResume() {
        super.onResume();
        shimmerFrameLayout.startShimmer();
    }

    @Override
    protected void onPause() {
        super.onPause();
        shimmerFrameLayout.stopShimmer();
    }
}

package kashyap.chandan.medishop.vendor;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import kashyap.chandan.medishop.R;

class VendorAdapter extends RecyclerView.Adapter<VendorAdapter.MyViewHolder> {
    Context context;
    List<AllVendorResponse.DataBean> vendors=new ArrayList<AllVendorResponse.DataBean>();
    public VendorAdapter(Context context, List<AllVendorResponse.DataBean> vendors) {
        this.vendors=vendors;
        this.context=context;
    }

    @NonNull
    @Override
    public VendorAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.recvendorlist,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {
holder.ownername.setText(vendors.get(position).getPerson_name());
holder.email.setText(vendors.get(position).getEmail());
holder.address.setText(vendors.get(position).getAddress());
holder.companyname.setText(vendors.get(position).getCompany_name());
holder.city.setText(vendors.get(position).getCity_name());
holder.comphone.setText(vendors.get(position).getMobile());
        Picasso.get().load("http://www.sbpharma.in/admin_assets/uploads/vendors/"+vendors.get(position).getImage()).error(R.drawable.cross).placeholder(R.drawable.loading).into(holder.image);
holder.call.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        int pos=holder.getAdapterPosition();
        String mob=vendors.get(pos).getMobile();
        Uri u = Uri.fromParts("tel",mob,null);
        Intent i = new Intent(Intent.ACTION_DIAL, u);
       context.startActivity(i);
    }
});
holder.detail.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        int pos=holder.getAdapterPosition();
        AllVendorResponse.DataBean vendoredit=vendors.get(pos);
        Intent intent=new Intent(context,VendorDetails.class);
        Bundle bundle=new Bundle();
        bundle.putSerializable("vendor",vendoredit);

        intent.putExtra("bundle",bundle);
        context.startActivity(intent);

    }
});
holder.edit.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        int pos=holder.getAdapterPosition();
        AllVendorResponse.DataBean vendoredit=vendors.get(pos);
        Intent intent=new Intent(context,EditCompanyProfile.class);
        Bundle bundle=new Bundle();
        bundle.putSerializable("vendor",vendoredit);
        intent.putExtra("bundle",bundle);
        context.startActivity(intent);
    }
});
        holder.whatsapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int pos=holder.getAdapterPosition();
                String mob=vendors.get(pos).getMobile();
                // use country code with your phone number
                String url = "https://api.whatsapp.com/send?phone=+91"+mob;
                try {
                    PackageManager pm = context.getPackageManager();
                    pm.getPackageInfo("com.whatsapp", PackageManager.GET_ACTIVITIES);
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(url));
                    context.startActivity(i);
                } catch (PackageManager.NameNotFoundException e) {
                    Toast.makeText(context, "Whatsapp app not installed in your phone", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return vendors.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView ownername,email,address,city,companyname,comphone;
        ImageView image,call,edit,whatsapp,location;
        LinearLayout detail;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ownername=itemView.findViewById(R.id.personname);
            companyname=itemView.findViewById(R.id.companyname);
            email=itemView.findViewById(R.id.comemail);
            address=itemView.findViewById(R.id.comaddr);
            city=itemView.findViewById(R.id.vendorcity);
            image=itemView.findViewById(R.id.companyImage);
            detail=itemView.findViewById(R.id.detail);
            call=itemView.findViewById(R.id.call);
            edit=itemView.findViewById(R.id.edit);
            location=itemView.findViewById(R.id.location);
            location.setVisibility(View.GONE);
            comphone=itemView.findViewById(R.id.comphone);
            whatsapp=itemView.findViewById(R.id.whatsapp);
        }
    }
}

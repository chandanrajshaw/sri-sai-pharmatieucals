package kashyap.chandan.medishop.vendor;

import java.io.Serializable;
import java.util.List;

public class AllVendorResponse implements Serializable {


    /**
     * status : {"code":200,"message":"Vendors List"}
     * data : [{"id":"2","company_name":"abcd","image":"crop.png","person_name":"abcd","email":"abcd.com","mobile":"1234567890","city_id":"2","address":"Kokar","gst_no":"123456","gst_certificate":"donation.png","pan_no":"321654","pan_card":"womenempower.png","date_time":"2020-03-07 17:48:16","status":"1","city_name":"Vijayawada"}]
     */

    private StatusBean status;
    private List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean implements Serializable{
        /**
         * code : 200
         * message : Vendors List
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean implements Serializable{
        /**
         * id : 2
         * company_name : abcd
         * image : crop.png
         * person_name : abcd
         * email : abcd.com
         * mobile : 1234567890
         * city_id : 2
         * address : Kokar
         * gst_no : 123456
         * gst_certificate : donation.png
         * pan_no : 321654
         * pan_card : womenempower.png
         * date_time : 2020-03-07 17:48:16
         * status : 1
         * city_name : Vijayawada
         */

        private String id;
        private String company_name;
        private String image;
        private String person_name;
        private String email;
        private String mobile;
        private String city_id;
        private String address;
        private String gst_no;
        private String gst_certificate;
        private String pan_no;
        private String pan_card;
        private String date_time;
        private String status;
        private String city_name;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getCompany_name() {
            return company_name;
        }

        public void setCompany_name(String company_name) {
            this.company_name = company_name;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getPerson_name() {
            return person_name;
        }

        public void setPerson_name(String person_name) {
            this.person_name = person_name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getCity_id() {
            return city_id;
        }

        public void setCity_id(String city_id) {
            this.city_id = city_id;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getGst_no() {
            return gst_no;
        }

        public void setGst_no(String gst_no) {
            this.gst_no = gst_no;
        }

        public String getGst_certificate() {
            return gst_certificate;
        }

        public void setGst_certificate(String gst_certificate) {
            this.gst_certificate = gst_certificate;
        }

        public String getPan_no() {
            return pan_no;
        }

        public void setPan_no(String pan_no) {
            this.pan_no = pan_no;
        }

        public String getPan_card() {
            return pan_card;
        }

        public void setPan_card(String pan_card) {
            this.pan_card = pan_card;
        }

        public String getDate_time() {
            return date_time;
        }

        public void setDate_time(String date_time) {
            this.date_time = date_time;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getCity_name() {
            return city_name;
        }

        public void setCity_name(String city_name) {
            this.city_name = city_name;
        }
    }
}

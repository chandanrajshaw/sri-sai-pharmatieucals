package kashyap.chandan.medishop.vendor;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.graphics.Typeface;
import android.os.Bundle;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.util.List;

import kashyap.chandan.medishop.Order.OrderResponse;
import kashyap.chandan.medishop.R;

public class VendorOrder extends AppCompatActivity {
TextView toolheader;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vendor_order);
        toolheader=findViewById(R.id.customtoolheader);
        toolheader.setText("Vendor Order");
    }
    public void addHeaders() {
        TableLayout tl = findViewById(R.id.table);
        TableRow tr = new TableRow(this);
        tr.setLayoutParams(getLayoutParams());

        tr.addView(getTextView(0, "Sl.No", ContextCompat.getColor(this, R.color.textcolor), Typeface.BOLD, ContextCompat.getColor(this, R.color.background_color)));
        tr.addView(getTextView(0, "Category",  ContextCompat.getColor(this, R.color.textcolor), Typeface.BOLD,ContextCompat.getColor(this, R.color.background_color)));
        tr.addView(getTextView(0, "Product",  ContextCompat.getColor(this, R.color.textcolor), Typeface.BOLD,ContextCompat.getColor(this, R.color.background_color)));
        tr.addView(getTextView(0, "Quantity", ContextCompat.getColor(this, R.color.textcolor), Typeface.BOLD,ContextCompat.getColor(this, R.color.background_color)));
        tr.addView(getTextView(0, "Unit Price", ContextCompat.getColor(this, R.color.textcolor), Typeface.BOLD,ContextCompat.getColor(this, R.color.background_color)));
        tr.addView(getTextView(0, "Amount", ContextCompat.getColor(this, R.color.textcolor), Typeface.BOLD,ContextCompat.getColor(this, R.color.background_color)));
        tl.addView(tr, getTblLayoutParams());
    }
    public void addData(List<OrderResponse.DataBean.DetailsBean> order) {
        // int numCompanies = companies.length;
        TableLayout tl = findViewById(R.id.table);
        List<OrderResponse.DataBean.DetailsBean> orders=order;
        for (int i=0;i<orders.size();i++)
        {
            OrderResponse.DataBean.DetailsBean detail=orders.get(i);
            TableRow tr = new TableRow(this);
            tr.setLayoutParams(getLayoutParams());
            tr.addView(getTextView( 1,""+(i+1), ContextCompat.getColor(this, R.color.background_color), Typeface.NORMAL,  ContextCompat.getColor(this, R.color.textcolor)));
            tr.addView(getTextView(1, detail.getPc_name(),ContextCompat.getColor(this, R.color.background_color), Typeface.NORMAL,  ContextCompat.getColor(this, R.color.textcolor)));
            tr.addView(getTextView(1,detail.getProduct_name(), ContextCompat.getColor(this, R.color.background_color), Typeface.NORMAL,  ContextCompat.getColor(this, R.color.textcolor)));
            tr.addView(getTextView(1, detail.getQuantity(), ContextCompat.getColor(this, R.color.background_color), Typeface.NORMAL, ContextCompat.getColor(this, R.color.textcolor)));
            tr.addView(getTextView(1, detail.getPrice(), ContextCompat.getColor(this, R.color.background_color), Typeface.NORMAL, ContextCompat.getColor(this, R.color.textcolor)));
            tr.addView(getTextView(1,detail.getTotal(), ContextCompat.getColor(this, R.color.background_color), Typeface.NORMAL,  ContextCompat.getColor(this, R.color.textcolor)));
            tl.addView(tr, getTblLayoutParams());
        }

    }
    private TextView getTextView (int id, String title, int color, int typeface, int bgColor)
    {
        TextView tv = new TextView(this);
        tv.setId(id);
        tv.setText(title.toUpperCase());
        tv.setTextColor(color);
        tv.setPadding(5, 5, 5, 5);
        tv.setTypeface(Typeface.DEFAULT, typeface);
        tv.setBackgroundColor(bgColor);
        tv.setLayoutParams(getLayoutParams());

        return tv;
    }
    @NonNull
    private TableLayout.LayoutParams getTblLayoutParams () {
        return new TableLayout.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT);
    }
    private TableRow.LayoutParams getLayoutParams ()
    {
        TableRow.LayoutParams params = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT);
        params.setMargins(2, 0, 0, 2);
        return params;
    }
}

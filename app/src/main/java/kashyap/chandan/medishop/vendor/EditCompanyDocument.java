package kashyap.chandan.medishop.vendor;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.snackbar.Snackbar;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import kashyap.chandan.medishop.ApiClient;
import kashyap.chandan.medishop.ConnectionDetector;
import kashyap.chandan.medishop.R;
import kashyap.chandan.medishop.UserInterface;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditCompanyDocument extends AppCompatActivity {
    TextView submit,skipdoc,toolheader;
    EditText gst,pan;
    Dialog dialog;
    ImageView goback,panImg,gstImg;
    Bitmap gstbitmap,panbitmap,gstconveretdImage,panconvertedImage;
    Uri picUri;
    File companyimage=null,gstCertificate=null,panCertificate=null;
    String ownerName,comapanyName,mobile,email,address,city;
    String gstpicturePath,panpictutePath,gstimagePic,panimagePic;
    ConnectionDetector connectionDetector;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_company_document);
        toolheader=findViewById(R.id.customtoolheader);
        toolheader.setText("Edit Company Document");
        connectionDetector=new ConnectionDetector(EditCompanyDocument.this);
       /*---------------File pictureFile = (File)getIntent.getExtras().get("picture");



       ------------------*/
        Intent i=getIntent();
        Bundle bundle=i.getBundleExtra("formdata");
        final AllVendorResponse.DataBean vendor= (AllVendorResponse.DataBean) bundle.getSerializable("vendor");
        companyimage=(File) bundle.getSerializable("file");
        ownerName=bundle.getString("ownername");
        comapanyName=bundle.getString("company");
        mobile=bundle.getString("phone");
        email=bundle.getString("email");

        /*--------------------------------change the pojo for update vendor--------------------------*/
        city=bundle.getString("city");
        address=bundle.getString("address");
        //editText
        gst=findViewById(R.id.gstno);
        gst.setText(vendor.getGst_no());
        pan=findViewById(R.id.panno);
        pan.setText(vendor.getPan_no());
        //Image view
        gstImg=findViewById(R.id.gstcertificate);
        Picasso.get().load("http://www.sbpharma.in/admin_assets/uploads/vendors/"+vendor.getGst_certificate()).error(R.drawable.cross).placeholder(R.drawable.loading).into(gstImg);

        panImg=findViewById(R.id.panimg);
        Picasso.get().load("http://www.sbpharma.in/admin_assets/uploads/vendors/"+vendor.getPan_card()).error(R.drawable.cross).placeholder(R.drawable.loading).into(panImg);

        gstImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LinearLayout camera, folder;
                dialog = new Dialog(EditCompanyDocument.this);
                dialog.setContentView(R.layout.dialogboxcamera);
                DisplayMetrics metrics=getResources().getDisplayMetrics();
                int width=metrics.widthPixels;
                dialog.getWindow().setLayout(width, LinearLayout.LayoutParams.MATCH_PARENT);
                dialog.show();
                dialog.setCancelable(true);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                Window window = dialog.getWindow();
                window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
                camera = dialog.findViewById(R.id.camera);
                folder = dialog.findViewById(R.id.folder);
                folder.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        /*checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                STORAGE_PERMISSION_CODE);*/
                        Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(i, 100);
                        dialog.dismiss();

                    }
                });
                camera.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        //checkPermission(Manifest.permission.CAMERA, CAMERA_PERMISSION_CODE);
                        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(cameraIntent, 101);
                        dialog.dismiss();


                    }
                });
            }
        });
        panImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LinearLayout camera, folder;
                dialog = new Dialog(EditCompanyDocument.this);
                dialog.setContentView(R.layout.dialogboxcamera);
                DisplayMetrics metrics=getResources().getDisplayMetrics();
                int width=metrics.widthPixels;
                dialog.getWindow().setLayout(width, LinearLayout.LayoutParams.MATCH_PARENT);
                dialog.show();
                dialog.setCancelable(true);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                Window window = dialog.getWindow();
                window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
                camera = dialog.findViewById(R.id.camera);
                folder = dialog.findViewById(R.id.folder);
                folder.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        /*checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                STORAGE_PERMISSION_CODE);*/
                        Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(i, 102);
                        dialog.dismiss();

                    }
                });
                camera.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        //checkPermission(Manifest.permission.CAMERA, CAMERA_PERMISSION_CODE);
                        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(cameraIntent, 103);
                        dialog.dismiss();


                    }
                });
            }
        });


        if (gstpicturePath != null && !gstpicturePath.isEmpty() && !gstpicturePath.equals("null")) {

            Picasso.get().load(gstpicturePath).into(gstImg);

            gstbitmap = ((BitmapDrawable) gstImg.getDrawable().getCurrent()).getBitmap();
//            Log.e("bitmap", "" + bitmap);
            gstconveretdImage = getResizedBitmap(gstbitmap, 500);

        } else { }
        if (panpictutePath != null && !panpictutePath.isEmpty() && !panpictutePath.equals("null")) {

            Picasso.get().load(panpictutePath).into(panImg);

            panbitmap = ((BitmapDrawable) panImg.getDrawable().getCurrent()).getBitmap();
//            Log.e("bitmap", "" + bitmap);
            panconvertedImage = getResizedBitmap(panbitmap, 500);

        } else { }

submit=findViewById(R.id.submitdoc);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String gstno=gst.getText().toString().trim();
                String panno=pan.getText().toString().trim();
                if (gstno.isEmpty()&&panno.isEmpty())
                    Snackbar.make(EditCompanyDocument.this.getWindow().getDecorView().findViewById(android.R.id.content),"Enter all the fields",Snackbar.LENGTH_SHORT).show();
                else if (gstno.isEmpty())
                    Snackbar.make(EditCompanyDocument.this.getWindow().getDecorView().findViewById(android.R.id.content),"Enter GST No.",Snackbar.LENGTH_SHORT).show();
                else if (panno.isEmpty())
                    Snackbar.make(EditCompanyDocument.this.getWindow().getDecorView().findViewById(android.R.id.content),"Enter Pan No.",Snackbar.LENGTH_SHORT).show();
                else
                {
                    if (!connectionDetector.isConnectingToInternet())
                        Snackbar.make(EditCompanyDocument.this.getWindow().getDecorView().findViewById(android.R.id.content),"Everything is fine",Snackbar.LENGTH_SHORT).show();
               else if (connectionDetector.isConnectingToInternet())
                    {
                        MultipartBody.Part cimg=null,gstimg=null,panimg=null;
if (companyimage!=null && gstCertificate!=null && panCertificate!=null ){
    RequestBody img1 = RequestBody.create(MediaType.parse("multipart/form-data"), companyimage);
    cimg = MultipartBody.Part.createFormData("image", companyimage.getName(), img1);
    RequestBody img2 = RequestBody.create(MediaType.parse("multipart/form-data"), gstCertificate);
    gstimg = MultipartBody.Part.createFormData("gst_certificate", gstCertificate.getName(), img2);
    RequestBody img3 = RequestBody.create(MediaType.parse("multipart/form-data"), panCertificate);
    panimg = MultipartBody.Part.createFormData("pan_card", panCertificate.getName(), img3);
}


                        RequestBody vid = RequestBody.create(MediaType.parse("multipart/form-data"), vendor.getId());
                        RequestBody cname = RequestBody.create(MediaType.parse("multipart/form-data"), comapanyName);
                        RequestBody  owner= RequestBody.create(MediaType.parse("multipart/form-data"),ownerName );
                        RequestBody cemail = RequestBody.create(MediaType.parse("multipart/form-data"),email );
                        RequestBody cmob = RequestBody.create(MediaType.parse("multipart/form-data"),mobile );
                        System.out.println(city);
                        RequestBody ccity = RequestBody.create(MediaType.parse("multipart/form-data"),city);
                        RequestBody  caddr= RequestBody.create(MediaType.parse("multipart/form-data"),address );
                        RequestBody no_gst = RequestBody.create(MediaType.parse("multipart/form-data"), gstno);
                        RequestBody no_pan = RequestBody.create(MediaType.parse("multipart/form-data"),panno );
                        RequestBody stat = RequestBody.create(MediaType.parse("multipart/form-data"),vendor.getStatus() );

                        final Dialog progress=new Dialog(EditCompanyDocument.this);
                        progress.setContentView(R.layout.customdialog);
                        progress.setCancelable(false);
                        progress.show();
                        UserInterface userInterface= ApiClient.getClient().create(UserInterface.class);
                        Call<UpdateVendorResponse>call=userInterface.updateVendors(vid,cname,owner,cemail,cmob,ccity,caddr,no_gst,no_pan,cimg,gstimg,panimg,stat);
                        call.enqueue(new Callback<UpdateVendorResponse>() {
                            @Override
                            public void onResponse(Call<UpdateVendorResponse> call, Response<UpdateVendorResponse> response) {
                                if (response.code()==200)
                                {
                                    progress.dismiss();
                                    Toast.makeText(EditCompanyDocument.this, "Vendor Updated Successfully", Toast.LENGTH_SHORT).show();
                               Intent intent=new Intent(EditCompanyDocument.this,Vendors.class);
                               intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                               overridePendingTransition(R.anim.transitionrighttoleft,R.anim.transitionrighttoleft);
                               startActivity(intent);
                               finish();
                                }
                                else if (response.code()!=200)
                                {
                                    progress.dismiss();
                                    Toast.makeText(EditCompanyDocument.this, "Vendor Not Updated Try Again", Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onFailure(Call<UpdateVendorResponse> call, Throwable t) {
                                Snackbar.make(EditCompanyDocument.this.getWindow().getDecorView().findViewById(android.R.id.content),""+t.getMessage(),Snackbar.LENGTH_SHORT).show();
                                progress.dismiss();

                            }
                        });
                    }
                }


            }
        });


    }
    private Bitmap getResizedBitmap(Bitmap image, int maxSize) {

        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //  if (requestCode == 100 && resultCode == RESULT_OK && data != null) {

        //     imagepath=selectedImage.getPath();

        if (requestCode == 100 && resultCode == RESULT_OK && data != null) {

//the image URI
            Uri selectedImage = data.getData();

            //     imagepath=selectedImage.getPath();


            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            gstpicturePath = cursor.getString(columnIndex);
            cursor.close();


            if (gstpicturePath != null && !gstpicturePath.equals("")) {
                gstCertificate = new File(gstpicturePath);
            }

            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);
                gstconveretdImage = getResizedBitmap(bitmap, 500);
                gstImg.setImageBitmap(gstconveretdImage);
                gstImg.setVisibility(View.VISIBLE);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else if (requestCode == 101 && resultCode == Activity.RESULT_OK) {


            Bitmap converetdImage = (Bitmap) data.getExtras().get("data");
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            converetdImage.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
            gstImg.setImageBitmap(converetdImage);
            gstImg.setVisibility(View.VISIBLE);
            gstCertificate = new File(Environment.getExternalStorageDirectory(), "gst.jpg");
            FileOutputStream fo;
            try {
                fo = new FileOutputStream(gstCertificate);
                fo.write(bytes.toByteArray());
                fo.close();
            } catch (IOException e) {
                e.printStackTrace();
            }


        }
      else  if (requestCode == 102 && resultCode == RESULT_OK && data != null) {

//the image URI
            Uri selectedImage = data.getData();

            //     imagepath=selectedImage.getPath();


            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            panpictutePath = cursor.getString(columnIndex);
            cursor.close();


            if (panpictutePath != null && !panpictutePath.equals("")) {
                panCertificate = new File(panpictutePath);
            }

            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);
                panconvertedImage = getResizedBitmap(bitmap, 500);
                panImg.setImageBitmap(panconvertedImage);
                panImg.setVisibility(View.VISIBLE);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else if (requestCode == 103 && resultCode == Activity.RESULT_OK) {


            Bitmap converetdImage = (Bitmap) data.getExtras().get("data");
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            converetdImage.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
            panImg.setImageBitmap(converetdImage);
            panImg.setVisibility(View.VISIBLE);
            panCertificate = new File(Environment.getExternalStorageDirectory(), "pan.jpg");
            FileOutputStream fo;
            try {
                fo = new FileOutputStream(panCertificate);
                fo.write(bytes.toByteArray());
                fo.close();
            } catch (IOException e) {
                e.printStackTrace();
            }


        }

    }
}

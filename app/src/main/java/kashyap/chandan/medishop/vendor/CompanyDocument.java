package kashyap.chandan.medishop.vendor;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import kashyap.chandan.medishop.ApiClient;
import kashyap.chandan.medishop.ConnectionDetector;
import kashyap.chandan.medishop.R;
import kashyap.chandan.medishop.UserInterface;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CompanyDocument extends AppCompatActivity {
    TextView submit,skipdoc,toolheader;
    EditText gst,pan,dlNo1,dlNo2;
    Dialog dialog;
    ImageView goback,panImg,gstImg,dlImg;
    Bitmap dlBitMap,gstbitmap,panbitmap,gstconveretdImage,panconvertedImage,dlconvertedImage;
    Uri picUri;
    File companyimage=null,gstCertificate=null,panCertificate=null,dlCertificate=null;
    String ownerName,comapanyName,mobile,email,address,city,stateId,districtId;
    String gstpicturePath, panpicturePath,gstimagePic,panimagePic,dlPicturePath;
    ConnectionDetector connectionDetector;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_company_document);
        toolheader=findViewById(R.id.customtoolheader);
        toolheader.setText("Company Document");
        connectionDetector=new ConnectionDetector(CompanyDocument.this);
       /*---------------File pictureFile = (File)getIntent.getExtras().get("picture");



       ------------------*/
        Intent i=getIntent();
        Bundle bundle=i.getBundleExtra("formdata");
        companyimage=(File) bundle.getSerializable("file");
        ownerName=bundle.getString("ownername");
        comapanyName=bundle.getString("company");
        mobile=bundle.getString("phone");
        email=bundle.getString("email");
        city=bundle.getString("city");
        address=bundle.getString("address");
        stateId=bundle.getString("state");
        districtId=bundle.getString("district");
        //editText
        gst=findViewById(R.id.gstno);
        pan=findViewById(R.id.panno);
        dlNo1=findViewById(R.id.dlNo1);
        dlNo2=findViewById(R.id.dlNo2);
        //Image view
        dlImg=findViewById(R.id.dlImg);
        gstImg=findViewById(R.id.gstcertificate);
        panImg=findViewById(R.id.panimg);
        gstImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LinearLayout camera, folder;
                dialog = new Dialog(CompanyDocument.this);
                dialog.setContentView(R.layout.dialogboxcamera);
                DisplayMetrics metrics=getResources().getDisplayMetrics();
                int width=metrics.widthPixels;
                dialog.getWindow().setLayout(width, LinearLayout.LayoutParams.MATCH_PARENT);
                dialog.show();
                dialog.setCancelable(true);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                Window window = dialog.getWindow();
                window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
                camera = dialog.findViewById(R.id.camera);
                folder = dialog.findViewById(R.id.folder);
                folder.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        /*checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                STORAGE_PERMISSION_CODE);*/
                        Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(i, 100);
                        dialog.dismiss();

                    }
                });
                camera.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        //checkPermission(Manifest.permission.CAMERA, CAMERA_PERMISSION_CODE);
                        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(cameraIntent, 101);
                        dialog.dismiss();


                    }
                });
            }
        });
        panImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LinearLayout camera, folder;
                dialog = new Dialog(CompanyDocument.this);
                dialog.setContentView(R.layout.dialogboxcamera);
                DisplayMetrics metrics=getResources().getDisplayMetrics();
                int width=metrics.widthPixels;
                dialog.getWindow().setLayout(width, LinearLayout.LayoutParams.MATCH_PARENT);
                dialog.show();
                dialog.setCancelable(true);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                Window window = dialog.getWindow();
                window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
                camera = dialog.findViewById(R.id.camera);
                folder = dialog.findViewById(R.id.folder);
                folder.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        /*checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                STORAGE_PERMISSION_CODE);*/
                        Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(i, 102);
                        dialog.dismiss();

                    }
                });
                camera.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        //checkPermission(Manifest.permission.CAMERA, CAMERA_PERMISSION_CODE);
                        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(cameraIntent, 103);
                        dialog.dismiss();


                    }
                });
            }
        });

        dlImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LinearLayout camera, folder;
                dialog = new Dialog(CompanyDocument.this);
                dialog.setContentView(R.layout.dialogboxcamera);
                DisplayMetrics metrics=getResources().getDisplayMetrics();
                int width=metrics.widthPixels;
                dialog.getWindow().setLayout(width, LinearLayout.LayoutParams.MATCH_PARENT);
                dialog.show();
                dialog.setCancelable(true);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                Window window = dialog.getWindow();
                window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
                camera = dialog.findViewById(R.id.camera);
                folder = dialog.findViewById(R.id.folder);
                folder.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        /*checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                STORAGE_PERMISSION_CODE);*/
                        Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(i, 104);
                        dialog.dismiss();

                    }
                });
                camera.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        //checkPermission(Manifest.permission.CAMERA, CAMERA_PERMISSION_CODE);
                        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(cameraIntent, 105);
                        dialog.dismiss();


                    }
                });
            }
        });

        if (gstpicturePath != null && !gstpicturePath.isEmpty() && !gstpicturePath.equals("null")) {

            Picasso.get().load(gstpicturePath).into(gstImg);

            gstbitmap = ((BitmapDrawable) gstImg.getDrawable().getCurrent()).getBitmap();
//            Log.e("bitmap", "" + bitmap);
            gstconveretdImage = getResizedBitmap(gstbitmap, 500);

        } else { }
        if (panpicturePath != null && !panpicturePath.isEmpty() && !panpicturePath.equals("null")) {
            Picasso.get().load(panpicturePath).into(panImg);
            panbitmap = ((BitmapDrawable) panImg.getDrawable().getCurrent()).getBitmap();
            panconvertedImage = getResizedBitmap(panbitmap, 500);
        } else { }
        if (dlPicturePath != null && !dlPicturePath.isEmpty() && !dlPicturePath.equals("null")) {
            Picasso.get().load(dlPicturePath).into(dlImg);
            dlBitMap = ((BitmapDrawable) dlImg.getDrawable().getCurrent()).getBitmap();
            dlconvertedImage = getResizedBitmap(dlBitMap, 500);
        } else { }

submit=findViewById(R.id.submitdoc);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String gstno=gst.getText().toString().trim();
                String panno=pan.getText().toString().trim();
                String DlNo1=dlNo1.getText().toString().trim();
                String DlNo2=dlNo2.getText().toString().trim();
                if (companyimage==null&&gstCertificate==null&&panCertificate==null&&gstno.isEmpty()&&panno.isEmpty()&&DlNo1.isEmpty()&&DlNo2.isEmpty())
                    Snackbar.make(CompanyDocument.this.getWindow().getDecorView().findViewById(android.R.id.content),"Enter all the fields",Snackbar.LENGTH_SHORT).show();
                else if (companyimage==null)
                    Snackbar.make(CompanyDocument.this.getWindow().getDecorView().findViewById(android.R.id.content),"Company Image is not available",Snackbar.LENGTH_SHORT).show();
                else if (gstCertificate==null)
                    Snackbar.make(CompanyDocument.this.getWindow().getDecorView().findViewById(android.R.id.content),"Please Upload GST Certificate",Snackbar.LENGTH_SHORT).show();
                else if (panCertificate==null)
                    Snackbar.make(CompanyDocument.this.getWindow().getDecorView().findViewById(android.R.id.content),"Please Upload Pan Card Image",Snackbar.LENGTH_SHORT).show();
                else if (gstno.isEmpty())
                    Snackbar.make(CompanyDocument.this.getWindow().getDecorView().findViewById(android.R.id.content),"Enter GST No.",Snackbar.LENGTH_SHORT).show();
                else if (panno.isEmpty())
                    Snackbar.make(CompanyDocument.this.getWindow().getDecorView().findViewById(android.R.id.content),"Enter Pan No.",Snackbar.LENGTH_SHORT).show();
                else if (DlNo1.isEmpty())
                    Snackbar.make(CompanyDocument.this.getWindow().getDecorView().findViewById(android.R.id.content),"Enter DL No. 1",Snackbar.LENGTH_SHORT).show();
                else if (DlNo2.isEmpty())
                    Snackbar.make(CompanyDocument.this.getWindow().getDecorView().findViewById(android.R.id.content),"Enter DL No. 2",Snackbar.LENGTH_SHORT).show();
                else
                {
                    if (!connectionDetector.isConnectingToInternet())
                        Snackbar.make(CompanyDocument.this.getWindow().getDecorView().findViewById(android.R.id.content),"Everything is fine",Snackbar.LENGTH_SHORT).show();
               else if (connectionDetector.isConnectingToInternet())
                    {
                        MultipartBody.Part cimg=null,gstimg=null,panimg=null,dlimg;
                        if (companyimage != null && gstCertificate!=null && panCertificate!=null) {
                            RequestBody img1 = RequestBody.create(MediaType.parse("multipart/form-data"), companyimage);
                            cimg = MultipartBody.Part.createFormData("image", companyimage.getName(), img1);
                            RequestBody img2 = RequestBody.create(MediaType.parse("multipart/form-data"), gstCertificate);
                            gstimg = MultipartBody.Part.createFormData("gst_certificate", gstCertificate.getName(), img2);
                            RequestBody img3 = RequestBody.create(MediaType.parse("multipart/form-data"), panCertificate);
                            panimg = MultipartBody.Part.createFormData("pan_card", panCertificate.getName(), img3);
                            RequestBody img4 = RequestBody.create(MediaType.parse("multipart/form-data"), dlCertificate);
                            dlimg = MultipartBody.Part.createFormData("pan_card", dlCertificate.getName(), img4);
                        }
                        RequestBody cname = RequestBody.create(MediaType.parse("multipart/form-data"), comapanyName);
                        RequestBody  owner= RequestBody.create(MediaType.parse("multipart/form-data"),ownerName );
                        RequestBody cemail = RequestBody.create(MediaType.parse("multipart/form-data"),email );
                        RequestBody cmob = RequestBody.create(MediaType.parse("multipart/form-data"),mobile );
                        RequestBody ccity = RequestBody.create(MediaType.parse("multipart/form-data"),city);
                        RequestBody cstate = RequestBody.create(MediaType.parse("multipart/form-data"),stateId);
                        RequestBody cdistrict = RequestBody.create(MediaType.parse("multipart/form-data"),districtId);
                        RequestBody  caddr= RequestBody.create(MediaType.parse("multipart/form-data"),address );
                        RequestBody no_gst = RequestBody.create(MediaType.parse("multipart/form-data"), gstno);
                        RequestBody no_pan = RequestBody.create(MediaType.parse("multipart/form-data"),panno );
                        RequestBody no1_dl = RequestBody.create(MediaType.parse("multipart/form-data"), DlNo1);
                        RequestBody no2_dl = RequestBody.create(MediaType.parse("multipart/form-data"),DlNo2 );

                        final Dialog progress=new Dialog(CompanyDocument.this);
                        progress.setContentView(R.layout.customdialog);
                        progress.setCancelable(false);
                        progress.show();

                        UserInterface userInterface= ApiClient.getClient().create(UserInterface.class);
                        Call<AddVendorResponse>call=userInterface.addVendors(cname,owner,cemail,cmob,ccity,caddr,no_gst,no_pan,cimg,gstimg,panimg);
                        call.enqueue(new Callback<AddVendorResponse>() {
                            @Override
                            public void onResponse(Call<AddVendorResponse> call, Response<AddVendorResponse> response) {
                                if (response.code()==200)
                                {
                                    progress.dismiss();
                                    Toast.makeText(CompanyDocument.this, "Vendor Added Successfully", Toast.LENGTH_SHORT).show();
                               Intent intent=new Intent(CompanyDocument.this,Vendors.class);
                               intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                               overridePendingTransition(R.anim.transitionrighttoleft,R.anim.transitionrighttoleft);
                               startActivity(intent);
                               finish();
                                }
                                else if (response.code()!=200)
                                {
                                    progress.dismiss();
                                    Toast.makeText(CompanyDocument.this, "Vendor Not Added Try Again", Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onFailure(Call<AddVendorResponse> call, Throwable t) {
                                Snackbar.make(CompanyDocument.this.getWindow().getDecorView().findViewById(android.R.id.content),""+t.getMessage(),Snackbar.LENGTH_SHORT).show();
                                progress.dismiss();

                            }
                        });
                    }
                }


            }
        });


    }
    private Bitmap getResizedBitmap(Bitmap image, int maxSize) {

        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //  if (requestCode == 100 && resultCode == RESULT_OK && data != null) {

        //     imagepath=selectedImage.getPath();

        if (requestCode == 100 && resultCode == RESULT_OK && data != null) {

//the image URI
            Uri selectedImage = data.getData();

            //     imagepath=selectedImage.getPath();


            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            gstpicturePath = cursor.getString(columnIndex);
            cursor.close();


            if (gstpicturePath != null && !gstpicturePath.equals("")) {
                gstCertificate = new File(gstpicturePath);
            }

            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);
                gstconveretdImage = getResizedBitmap(bitmap, 500);
                gstImg.setImageBitmap(gstconveretdImage);
                gstImg.setVisibility(View.VISIBLE);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else if (requestCode == 101 && resultCode == Activity.RESULT_OK) {


            Bitmap converetdImage = (Bitmap) data.getExtras().get("data");
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            converetdImage.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
            gstImg.setImageBitmap(converetdImage);
            gstImg.setVisibility(View.VISIBLE);
            gstCertificate = new File(Environment.getExternalStorageDirectory(), "gst.jpg");
            FileOutputStream fo;
            try {
                fo = new FileOutputStream(gstCertificate);
                fo.write(bytes.toByteArray());
                fo.close();
            } catch (IOException e) {
                e.printStackTrace();
            }


        }
      else  if (requestCode == 102 && resultCode == RESULT_OK && data != null) {

//the image URI
            Uri selectedImage = data.getData();

            //     imagepath=selectedImage.getPath();


            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            panpicturePath = cursor.getString(columnIndex);
            cursor.close();


            if (panpicturePath != null && !panpicturePath.equals("")) {
                panCertificate = new File(panpicturePath);
            }

            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);
                panconvertedImage = getResizedBitmap(bitmap, 500);
                panImg.setImageBitmap(panconvertedImage);
                panImg.setVisibility(View.VISIBLE);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else if (requestCode == 103 && resultCode == Activity.RESULT_OK) {
            Bitmap converetdImage = (Bitmap) data.getExtras().get("data");
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            converetdImage.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
            panImg.setImageBitmap(converetdImage);
            panImg.setVisibility(View.VISIBLE);
            panCertificate = new File(Environment.getExternalStorageDirectory(), "pan.jpg");
            FileOutputStream fo;
            try {
                fo = new FileOutputStream(panCertificate);
                fo.write(bytes.toByteArray());
                fo.close();
            } catch (IOException e) {
                e.printStackTrace();
            }


        }
        else if (requestCode == 105 && resultCode == Activity.RESULT_OK) {


            Bitmap converetdImage = (Bitmap) data.getExtras().get("data");
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            converetdImage.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
            dlImg.setImageBitmap(converetdImage);
            dlImg.setVisibility(View.VISIBLE);
            dlCertificate = new File(Environment.getExternalStorageDirectory(), "dlImage.jpg");
            FileOutputStream fo;
            try {
                fo = new FileOutputStream(dlCertificate);
                fo.write(bytes.toByteArray());
                fo.close();
            } catch (IOException e) {
                e.printStackTrace();
            }


        }
        else  if (requestCode == 104 && resultCode == RESULT_OK && data != null) {
//the image URI
            Uri selectedImage = data.getData();
            //     imagepath=selectedImage.getPath();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            dlPicturePath = cursor.getString(columnIndex);
            cursor.close();


            if (dlPicturePath != null && !dlPicturePath.equals("")) {
                dlCertificate = new File(dlPicturePath);
            }

            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);
                dlconvertedImage = getResizedBitmap(bitmap, 500);
                dlImg.setImageBitmap(dlconvertedImage);
                dlImg.setVisibility(View.VISIBLE);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }
}

package kashyap.chandan.medishop.vendor;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.List;

import kashyap.chandan.medishop.ApiClient;
import kashyap.chandan.medishop.ConnectionDetector;
import kashyap.chandan.medishop.R;
import kashyap.chandan.medishop.UserInterface;
import kashyap.chandan.medishop.product.AddProduct;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Vendors extends AppCompatActivity {
TextView toolheader;
RecyclerView vendorslist;
ImageView goback,fab,comImg;
ConnectionDetector connectionDetector;
    ShimmerFrameLayout shimmerFrameLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_vendors);
        connectionDetector=new ConnectionDetector(Vendors.this);
        goback=findViewById(R.id.ordergobackarrow);
        fab=findViewById(R.id.fab);
        toolheader=findViewById(R.id.customtoolheader);
        shimmerFrameLayout=findViewById(R.id.shimmerview);

        vendorslist=findViewById(R.id.vendorlist);
        toolheader.setText("Vendors");
        comImg=findViewById(R.id.companyimg);
        goback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                overridePendingTransition(R.anim.transitionrighttoleft,R.anim.transitionrighttoleft);
                finish();
            }
        });
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                                Intent intent=new Intent(Vendors.this,CompanyProfile.class);
                startActivity(intent);
            }
        });
//      final Dialog progress=new Dialog(Vendors.this);
//      progress.setContentView(R.layout.customdialog);
//      progress.setCancelable(false);
//      progress.show();
      if (!connectionDetector.isConnectingToInternet()){
          shimmerFrameLayout.stopShimmer();
          shimmerFrameLayout.setVisibility(View.GONE);
          Snackbar.make(Vendors.this.getWindow().getDecorView().findViewById(android.R.id.content),"Internet Not available",Snackbar.LENGTH_LONG).show();
vendorslist.setVisibility(View.GONE);
      }
      else if (connectionDetector.isConnectingToInternet()){
          UserInterface userInterface= ApiClient.getClient().create(UserInterface.class);
          Call<AllVendorResponse>call=userInterface.getAllVendors();
          call.enqueue(new Callback<AllVendorResponse>() {
              @Override
              public void onResponse(Call<AllVendorResponse> call, Response<AllVendorResponse> response) {
                  if (response.code()==200){
                      shimmerFrameLayout.stopShimmer();
                      shimmerFrameLayout.setVisibility(View.GONE);
                      List<AllVendorResponse.DataBean> vendors=new ArrayList<AllVendorResponse.DataBean>();
                      vendors=response.body().getData();
                      vendorslist.setLayoutManager(new LinearLayoutManager(Vendors.this,LinearLayoutManager.VERTICAL,false));
                      vendorslist.setAdapter(new VendorAdapter(Vendors.this,vendors));
                     vendorslist.setVisibility(View.VISIBLE);
                  }
                  else if (response.code()!=200){
                      shimmerFrameLayout.stopShimmer();
                      shimmerFrameLayout.setVisibility(View.GONE);
                      Snackbar.make(Vendors.this.getWindow().getDecorView().findViewById(android.R.id.content),"Error Occurs!! No Vendors Available",Snackbar.LENGTH_LONG).show();
                  }
              }

              @Override
              public void onFailure(Call<AllVendorResponse> call, Throwable t) {
                  Snackbar.make(Vendors.this.getWindow().getDecorView().findViewById(android.R.id.content),""+t.getMessage(),Snackbar.LENGTH_LONG).show();
              }
          });

      }
    }
    @Override
    protected void onResume() {
        super.onResume();
        shimmerFrameLayout.startShimmer();
    }

    @Override
    protected void onPause() {
        super.onPause();
        shimmerFrameLayout.stopShimmer();
    }
}

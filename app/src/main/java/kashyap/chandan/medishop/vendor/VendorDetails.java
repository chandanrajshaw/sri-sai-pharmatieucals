package kashyap.chandan.medishop.vendor;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import kashyap.chandan.medishop.R;

public class VendorDetails extends AppCompatActivity {
TextView toolheader,owner,email,addr,city,gst,pan,phone;
ImageView image,gstimage,panimage,goback,call,whatsapp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_vendor_details);
        Intent i=getIntent();
        Bundle b=i.getBundleExtra("bundle");
        final AllVendorResponse.DataBean vendordetail= (AllVendorResponse.DataBean) b.getSerializable("vendor");
        toolheader=findViewById(R.id.customtoolheader);
        toolheader.setText(vendordetail.getCompany_name());
        owner=findViewById(R.id.ownername);
        owner.setText(vendordetail.getPerson_name());
        email=findViewById(R.id.email);
        email.setText(vendordetail.getEmail());
        addr=findViewById(R.id.address);
        addr.setText(vendordetail.getAddress());
        city=findViewById(R.id.city);
        city.setText(vendordetail.getCity_name());
        gst=findViewById(R.id.gstno);
        gst.setText(vendordetail.getGst_no());
        pan=findViewById(R.id.panno);
        image=findViewById(R.id.comcumshopimage);
        gstimage=findViewById(R.id.gstimage);
        panimage=findViewById(R.id.panimage);
        whatsapp=findViewById(R.id.whatsapp);
        pan.setText(vendordetail.getPan_no());
        phone=findViewById(R.id.phone);
        phone.setText(vendordetail.getMobile());
        Picasso.get().load("http://www.sbpharma.in/admin_assets/uploads/vendors/"+vendordetail.getImage()).error(R.drawable.cross)
                .placeholder(R.drawable.loading).into(image);
        Picasso.get().load("http://www.sbpharma.in/admin_assets/uploads/vendors/"+vendordetail.getGst_certificate()).error(R.drawable.cross)
                .placeholder(R.drawable.loading).into(gstimage);
        Picasso.get().load("http://www.sbpharma.in/admin_assets/uploads/vendors/"+vendordetail.getPan_card()).error(R.drawable.cross)
                .placeholder(R.drawable.loading).into(panimage);
        call=findViewById(R.id.call);
        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String mob=vendordetail.getMobile();
                Uri u = Uri.fromParts("tel",mob,null);
                Intent i = new Intent(Intent.ACTION_DIAL, u);
                startActivity(i);
            }
        });
        whatsapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String url = "https://api.whatsapp.com/send?phone=+91"+vendordetail.getMobile();
                try {
                    PackageManager pm = getPackageManager();
                    pm.getPackageInfo("com.whatsapp", PackageManager.GET_ACTIVITIES);
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(url));
                    startActivity(i);
                } catch (PackageManager.NameNotFoundException e) {
                    Toast.makeText(VendorDetails.this, "Whatsapp app not installed in your phone", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }
        });

goback=findViewById(R.id.ordergobackarrow);
goback.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        overridePendingTransition(R.anim.transitionrighttoleft,R.anim.transitionrighttoleft);
        finish();
    }
});
panimage.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        final Dialog imagedialog=new Dialog(VendorDetails.this);
        imagedialog.setContentView(R.layout.imagedialog);
        DisplayMetrics metrics=getResources().getDisplayMetrics();
        int width=metrics.widthPixels;
        imagedialog.getWindow().setLayout(width, LinearLayout.LayoutParams.MATCH_PARENT);
         ImageView pancard=imagedialog.findViewById(R.id.viewimage);
        ImageView close=imagedialog.findViewById(R.id.closebtn);
        Picasso.get().load("http://www.sbpharma.in/admin_assets/uploads/vendors/"+vendordetail.getPan_card()).error(R.drawable.cross)
                .placeholder(R.drawable.loading).into(pancard);
        imagedialog.show();
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imagedialog.dismiss();
            }
        });
    }
});
gstimage.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        final Dialog imagedialog=new Dialog(VendorDetails.this);
        imagedialog.setContentView(R.layout.imagedialog);
        DisplayMetrics metrics=getResources().getDisplayMetrics();
        int width=metrics.widthPixels;
        imagedialog.getWindow().setLayout(width, LinearLayout.LayoutParams.MATCH_PARENT);
        ImageView gstImage=imagedialog.findViewById(R.id.viewimage);
        ImageView close=imagedialog.findViewById(R.id.closebtn);
        Picasso.get().load("http://www.sbpharma.in/admin_assets/uploads/vendors/"+vendordetail.getGst_certificate()).error(R.drawable.cross)
                .placeholder(R.drawable.loading).into(gstImage);
        imagedialog.show();
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imagedialog.dismiss();
            }
        });
    }
});


    }
}

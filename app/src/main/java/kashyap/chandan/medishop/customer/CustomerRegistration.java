package kashyap.chandan.medishop.customer;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import kashyap.chandan.medishop.ApiClient;
import kashyap.chandan.medishop.CustomItemClickListener;
import kashyap.chandan.medishop.R;
import kashyap.chandan.medishop.UserInterface;
import kashyap.chandan.medishop.admin.AddDistrict;
import kashyap.chandan.medishop.admin.DistrictListAdapter;
import kashyap.chandan.medishop.admin.DistrictResponse;
import kashyap.chandan.medishop.admin.StateListAdapter;
import kashyap.chandan.medishop.admin.StateResponse;
import kashyap.chandan.medishop.agentPannel.AddShopActivity1;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CustomerRegistration extends AppCompatActivity {
TextInputEditText etName,etShopName,etEmail,etPhone,etaddress;
RelativeLayout stateList,districtList,profileImagelayout;
CircleImageView profileImage;
TextView district,state;
private String stateId,districtId;
RecyclerView stateRecycler,districtRecycler;
    private List<StateResponse.DataBean> allStateList=new ArrayList<>();
    List<DistrictResponse.DataBean>allDistrict=new ArrayList<>();
    private Dialog progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_registration);
        init();
        stateList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Statelist();
            }
        });
        districtList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (stateId==null||stateId.isEmpty())
                    Toast.makeText(CustomerRegistration.this, "Select State", Toast.LENGTH_SHORT).show();
                else
                districtlist();
            }
        });
    }

    private void init() {
        district=findViewById(R.id.district);
        state=findViewById(R.id.state);
        profileImage=findViewById(R.id.profileImage);
        stateList=findViewById(R.id.stateList);
        districtList=findViewById(R.id.districtList);
        profileImagelayout=findViewById(R.id.profileImagelayout);
        etaddress=findViewById(R.id.etaddress);
        etName=findViewById(R.id.etName);
        etShopName=findViewById(R.id.etShopName);
        etEmail=findViewById(R.id.etEmail);
        etPhone=findViewById(R.id.etPhone);
    }
    private void Statelist()
    {
        stateId ="";
        progress = new Dialog(CustomerRegistration.this);
        progress.setContentView(R.layout.customdialog);
        progress.setCancelable(false);
        progress.show();
        UserInterface userInterface= ApiClient.getClient().create(UserInterface.class);
        /*--------------------------------------*/
        Call<StateResponse> call=userInterface.stateList();
        call.enqueue(new Callback<StateResponse>() {
            @Override
            public void onResponse(Call<StateResponse> call, Response<StateResponse> response) {
                if (response.code()==200)
                {
                    allStateList=response.body().getData();
                    final  Dialog stateDialog=new Dialog(CustomerRegistration.this);
                    stateDialog.setContentView(R.layout.recyclerdialog);
                    DisplayMetrics metrics=getResources().getDisplayMetrics();
                    int width=metrics.widthPixels;
                    stateDialog.getWindow().setLayout(width, LinearLayout.LayoutParams.WRAP_CONTENT);
                    stateRecycler=stateDialog.findViewById(R.id.dialogRecycler);
                    stateRecycler.setLayoutManager(new LinearLayoutManager(CustomerRegistration.this,LinearLayoutManager.VERTICAL,false));
                    stateRecycler.setAdapter(new StateListAdapter(CustomerRegistration.this,allStateList,stateDialog, new CustomItemClickListener() {
                        @Override
                        public void onItemClick(View v, int position, String value) {
                            state.setText(value);
                            stateId =String.valueOf(position);
                        }
                    }));
                    progress.dismiss();
                    stateDialog.show();
                }
                else
                {
                    progress.dismiss();
                    Toast.makeText(CustomerRegistration.this, "Something Goes Wrong", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<StateResponse> call, Throwable t) {
                progress.dismiss();
                Toast.makeText(CustomerRegistration.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }
    private void districtlist()
    {
        districtId="";
        progress = new Dialog(CustomerRegistration.this);
        progress.setContentView(R.layout.customdialog);
        progress.setCancelable(false);
        progress.show();
        UserInterface userInterface= ApiClient.getClient().create(UserInterface.class);
        Call<DistrictResponse>call=userInterface.getDistrict(stateId);
        call.enqueue(new Callback<DistrictResponse>() {
            @Override
            public void onResponse(Call<DistrictResponse> call, Response<DistrictResponse> response) {
                if (response.code()==200)
                {
                    allDistrict=response.body().getData();
                    final  Dialog districtDialog=new Dialog(CustomerRegistration.this);
                    districtDialog.setContentView(R.layout.recyclerdialog);
                    DisplayMetrics metrics=getResources().getDisplayMetrics();
                    int width=metrics.widthPixels;
                    districtDialog.getWindow().setLayout(width, LinearLayout.LayoutParams.WRAP_CONTENT);
                    districtRecycler=districtDialog.findViewById(R.id.dialogRecycler);
                    districtRecycler.setLayoutManager(new LinearLayoutManager(CustomerRegistration.this,LinearLayoutManager.VERTICAL,false));
                    districtRecycler.setAdapter(new DistrictListAdapter(CustomerRegistration.this,allDistrict,districtDialog, new CustomItemClickListener() {
                        @Override
                        public void onItemClick(View v, int position, String value) {
                            district.setText(value);
                            districtId=String.valueOf(position);
                        }
                    }));
                    progress.dismiss();
                    districtDialog.show();
                }
                else {
                    progress.dismiss();
                    Toast.makeText(CustomerRegistration.this, "Something Goes Wrong", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<DistrictResponse> call, Throwable t) {
                progress.dismiss();
                Toast.makeText(CustomerRegistration.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
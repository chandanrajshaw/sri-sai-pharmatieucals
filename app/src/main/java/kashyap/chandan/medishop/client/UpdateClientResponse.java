package kashyap.chandan.medishop.client;

import java.io.Serializable;

public class UpdateClientResponse implements Serializable {


    /**
     * status : {"code":200,"message":"clients Data Updated Successfully"}
     */

    private StatusBean status;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public static class StatusBean implements  Serializable{
        /**
         * code : 200
         * message : clients Data Updated Successfully
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }
}

package kashyap.chandan.medishop.client;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.List;

import kashyap.chandan.medishop.ApiClient;
import kashyap.chandan.medishop.ConnectionDetector;
import kashyap.chandan.medishop.R;
import kashyap.chandan.medishop.UserInterface;
import kashyap.chandan.medishop.vendor.CompanyDocument;
import kashyap.chandan.medishop.vendor.CompanyProfile;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Clients extends AppCompatActivity {
TextView toolheader;
RecyclerView clientlist;
ImageView goback,fab,comImg;
ConnectionDetector connectionDetector;
    ShimmerFrameLayout shimmerFrameLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_vendors);
        connectionDetector=new ConnectionDetector(Clients.this);
        goback=findViewById(R.id.ordergobackarrow);
        shimmerFrameLayout=findViewById(R.id.shimmerview);

        fab=findViewById(R.id.fab);
        toolheader=findViewById(R.id.customtoolheader);
        clientlist=findViewById(R.id.vendorlist);
        toolheader.setText("Clients");
        comImg=findViewById(R.id.companyimg);
        goback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                overridePendingTransition(R.anim.transitionrighttoleft,R.anim.transitionrighttoleft);
                finish();
            }
        });
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                                Intent intent=new Intent(Clients.this,ClientProfile.class);
                startActivity(intent);
            }
        });

if (!connectionDetector.isConnectingToInternet()){
    shimmerFrameLayout.stopShimmer();
    shimmerFrameLayout.setVisibility(View.GONE);
    Snackbar.make(Clients.this.getWindow().getDecorView().findViewById(android.R.id.content),"Please Connect to Internet ant try again",Snackbar.LENGTH_SHORT).show();
    clientlist.setVisibility(View.GONE);
}

else if (connectionDetector.isConnectingToInternet()){
//    final Dialog progress=new Dialog(Clients.this);
//    progress.setContentView(R.layout.customdialog);
//    progress.setCancelable(false);
//    progress.show();
    UserInterface userInterface= ApiClient.getClient().create(UserInterface.class);
    Call<AllClientsResponse>call=userInterface.getAllClients();
    call.enqueue(new Callback<AllClientsResponse>() {
        @Override
        public void onResponse(Call<AllClientsResponse> call, Response<AllClientsResponse> response) {
            if (response.code()==200){
                shimmerFrameLayout.stopShimmer();
                shimmerFrameLayout.setVisibility(View.GONE);
                List<AllClientsResponse.DataBean>allClients=new ArrayList<AllClientsResponse.DataBean>();
                        allClients=response.body().getData();
                clientlist.setLayoutManager(new LinearLayoutManager(Clients.this,LinearLayoutManager.VERTICAL,false));
                clientlist.setAdapter(new ClientAdapter(Clients.this,allClients));
                clientlist.setVisibility(View.VISIBLE);
//           progress.dismiss();
            }
            else if (response.code()!=200){
                shimmerFrameLayout.stopShimmer();
                shimmerFrameLayout.setVisibility(View.GONE);
//                progress.dismiss();
                Snackbar.make(Clients.this.getWindow().getDecorView().findViewById(android.R.id.content),"Error Ocurs!! No Clients Available",Snackbar.LENGTH_SHORT).show();

            }
        }

        @Override
        public void onFailure(Call<AllClientsResponse> call, Throwable t) {
            Snackbar.make(Clients.this.getWindow().getDecorView().findViewById(android.R.id.content),""+t.getMessage(),Snackbar.LENGTH_SHORT).show();

        }
    });



}







    }
    @Override
    protected void onResume() {
        super.onResume();
        shimmerFrameLayout.startShimmer();
    }

    @Override
    protected void onPause() {
        super.onPause();
        shimmerFrameLayout.stopShimmer();
    }
}

package kashyap.chandan.medishop.client;

import java.io.Serializable;
import java.util.List;

public class AllClientsResponse implements Serializable {


    /**
     * status : {"code":200,"message":"Clients List"}
     * data : [{"id":"3","shop_name":"igrand medicals","image":"shop6.jpg","person_name":"ram","email":"jfjfjf@fjjfjd.com","mobile":"9030955577","state_id":"3","district_id":"5","city_id":"1","area_id":"23","address":"hdjfjf","gst_no":"hdjdjd","gst_certificate":"gst2.jpg","pan_no":"udhdjr","pan_card":"pan2.jpg","latitude":"17.4371115","longitude":"78.4515324","date_time":"2020-07-11 13:28:35","status":"1","state_name":" Telangana","district_name":"Hyderabad","city_name":"Hyderabad","area_name":"S R NAGAR"},{"id":"2","shop_name":"SUP RAJA MEDICAL HALL","image":"shop5.jpg","person_name":"DR SRIIRAM","email":"drsreeam59@gmail.com","mobile":"9849333855","state_id":"3","district_id":"5","city_id":"1","area_id":"11","address":"jawaharnagar","gst_no":"AP/16/04/2013-109921","gst_certificate":"gst1.jpg","pan_no":"AP/16/04/2013-19922","pan_card":"pan1.jpg","latitude":"17.4419622","longitude":"78.4284826","date_time":"2020-04-25 11:51:44","status":"1","state_name":" Telangana","district_name":"Hyderabad","city_name":"Hyderabad","area_name":"JAWAHARNAGAR"},{"id":"1","shop_name":"SINDUJA PHARMACY","image":"shop4.jpg","person_name":"ANJANAYULU","email":"prajaclinicanjanayulu@gmail.com","mobile":"9951286965","state_id":"3","district_id":"5","city_id":"1","area_id":"10","address":"BACHUPALLY FOUR ROAD JUNCTION","gst_no":"TS/MDL/2018-33560","gst_certificate":"gst.jpg","pan_no":"TS/MDL/2018-33560","pan_card":"pan.jpg","latitude":"17.5406294","longitude":"78.3639031","date_time":"2020-04-24 10:35:19","status":"1","state_name":" Telangana","district_name":"Hyderabad","city_name":"Hyderabad","area_name":"BACHUPALLY "}]
     */

    private StatusBean status;
    private List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean implements Serializable {
        /**
         * code : 200
         * message : Clients List
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean implements Serializable {
        /**
         * id : 3
         * shop_name : igrand medicals
         * image : shop6.jpg
         * person_name : ram
         * email : jfjfjf@fjjfjd.com
         * mobile : 9030955577
         * state_id : 3
         * district_id : 5
         * city_id : 1
         * area_id : 23
         * address : hdjfjf
         * gst_no : hdjdjd
         * gst_certificate : gst2.jpg
         * pan_no : udhdjr
         * pan_card : pan2.jpg
         * latitude : 17.4371115
         * longitude : 78.4515324
         * date_time : 2020-07-11 13:28:35
         * status : 1
         * state_name :  Telangana
         * district_name : Hyderabad
         * city_name : Hyderabad
         * area_name : S R NAGAR
         */

        private String id;
        private String shop_name;
        private String image;
        private String person_name;
        private String email;
        private String mobile;
        private String state_id;
        private String district_id;
        private String city_id;
        private String area_id;
        private String address;
        private String gst_no;
        private String gst_certificate;
        private String pan_no;
        private String pan_card;
        private String latitude;
        private String longitude;
        private String date_time;
        private String status;
        private String state_name;
        private String district_name;
        private String city_name;
        private String area_name;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getShop_name() {
            return shop_name;
        }

        public void setShop_name(String shop_name) {
            this.shop_name = shop_name;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getPerson_name() {
            return person_name;
        }

        public void setPerson_name(String person_name) {
            this.person_name = person_name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getState_id() {
            return state_id;
        }

        public void setState_id(String state_id) {
            this.state_id = state_id;
        }

        public String getDistrict_id() {
            return district_id;
        }

        public void setDistrict_id(String district_id) {
            this.district_id = district_id;
        }

        public String getCity_id() {
            return city_id;
        }

        public void setCity_id(String city_id) {
            this.city_id = city_id;
        }

        public String getArea_id() {
            return area_id;
        }

        public void setArea_id(String area_id) {
            this.area_id = area_id;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getGst_no() {
            return gst_no;
        }

        public void setGst_no(String gst_no) {
            this.gst_no = gst_no;
        }

        public String getGst_certificate() {
            return gst_certificate;
        }

        public void setGst_certificate(String gst_certificate) {
            this.gst_certificate = gst_certificate;
        }

        public String getPan_no() {
            return pan_no;
        }

        public void setPan_no(String pan_no) {
            this.pan_no = pan_no;
        }

        public String getPan_card() {
            return pan_card;
        }

        public void setPan_card(String pan_card) {
            this.pan_card = pan_card;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getDate_time() {
            return date_time;
        }

        public void setDate_time(String date_time) {
            this.date_time = date_time;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getState_name() {
            return state_name;
        }

        public void setState_name(String state_name) {
            this.state_name = state_name;
        }

        public String getDistrict_name() {
            return district_name;
        }

        public void setDistrict_name(String district_name) {
            this.district_name = district_name;
        }

        public String getCity_name() {
            return city_name;
        }

        public void setCity_name(String city_name) {
            this.city_name = city_name;
        }

        public String getArea_name() {
            return area_name;
        }

        public void setArea_name(String area_name) {
            this.area_name = area_name;
        }
    }
}

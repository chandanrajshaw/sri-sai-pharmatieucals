package kashyap.chandan.medishop.client;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import kashyap.chandan.medishop.ApiClient;
import kashyap.chandan.medishop.CustomItemClickListener;
import kashyap.chandan.medishop.R;
import kashyap.chandan.medishop.UserInterface;
import kashyap.chandan.medishop.admin.AdminAllAreaResponse;
import kashyap.chandan.medishop.admin.AdminCityListAdapter;
import kashyap.chandan.medishop.admin.DistrictListAdapter;
import kashyap.chandan.medishop.admin.DistrictResponse;
import kashyap.chandan.medishop.admin.StateListAdapter;
import kashyap.chandan.medishop.admin.StateResponse;
import kashyap.chandan.medishop.agentPannel.AreaListAdapter;
import kashyap.chandan.medishop.pojoclasses.AdminCityList;
import kashyap.chandan.medishop.pojoclasses.GetAreaResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditClientProfile extends AppCompatActivity {
    TextView nextdoc,skipdoc,toolheader,cityname,cityid,areaid,areaname,district,state;
    EditText name,shopname,mob,email,addr,web;
    Dialog dialog;
    ImageView goback,shopImg;

    Bitmap bitmap,converetdImage;
    Uri picUri;
    File image=null;
    RelativeLayout citydrop,areadrop,districtDrop,stateDrop;
    RecyclerView cityRecycler,areaRecycler,stateRecycler,districtRecycler;
    String picturePath,imagePic,stateId,districtId;
    List<AdminCityList.DataBean> allCityList=new ArrayList<AdminCityList.DataBean>();
    private Dialog progress;
    private List<DistrictResponse.DataBean> allDistrict=new ArrayList<>();
    private List<StateResponse.DataBean> allStateList=new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_client_profile);
        Intent i=getIntent();
        Bundle b=i.getBundleExtra("bundle");
        final AllClientsResponse.DataBean client= (AllClientsResponse.DataBean) b.getSerializable("client");
        state=findViewById(R.id.state);
        district=findViewById(R.id.district);
        districtDrop=findViewById(R.id.districtDrop);
        stateDrop=findViewById(R.id.stateDrop);
        cityname=findViewById(R.id.areacity);
        cityid=findViewById(R.id.areacityid);
        toolheader=findViewById(R.id.customtoolheader);
        goback=findViewById(R.id.ordergobackarrow);
        toolheader.setText("Edit Client Profile");
        nextdoc=findViewById(R.id.nextdoc);
        skipdoc=findViewById(R.id.skipdoc);
        name=findViewById(R.id.sowner);
        name.setText(client.getPerson_name());
        citydrop=findViewById(R.id.citydrop);
        cityname.setText(client.getCity_name());
        areadrop=findViewById(R.id.areadrop);
        areaid=findViewById(R.id.areaid);
        areaid.setText(client.getArea_id());
        areaname=findViewById(R.id.area);
        areaname.setText(client.getArea_name());
        cityid.setText(client.getCity_id());
        mob=findViewById(R.id.contactno);
        mob.setText(client.getMobile());
        shopname=findViewById(R.id.sname);
        shopname.setText(client.getShop_name());
        shopname.setHint("ShopName");
        email=findViewById(R.id.semail);
        email.setText(client.getEmail());
        addr=findViewById(R.id.saddress);
        addr.setText(client.getAddress());
        stateId=client.getState_id();
        districtId=client.getDistrict_id();
        web=findViewById(R.id.sweb);
        web.setVisibility(View.GONE);
//        web.setText(client.);
        goback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                overridePendingTransition(R.anim.transitionrighttoleft,R.anim.transitionrighttoleft);
                finish();
            }
        });
        skipdoc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                overridePendingTransition(R.anim.transitionrighttoleft,R.anim.transitionrighttoleft);
                finish();
            }
        });
        stateDrop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                statelist();
            }
        });
        districtDrop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               if (stateId==null||stateId.isEmpty())
                   Toast.makeText(EditClientProfile.this, "Select State", Toast.LENGTH_SHORT).show();
               else
               {
                   districtlist();
               }
            }
        });
        citydrop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               if (districtId==null||districtId.isEmpty())
                   Toast.makeText(EditClientProfile.this, "Select District", Toast.LENGTH_SHORT).show();
               else
               {
                   final Dialog progressDialog = new Dialog(EditClientProfile.this);
                   progressDialog.setContentView(R.layout.customdialog);
                   progressDialog.setCancelable(false);
                   progressDialog.show();
                   UserInterface userInterface= ApiClient.getClient().create(UserInterface.class);
                   Call<AdminCityList> call=userInterface.getCityList(districtId);
                   call.enqueue(new Callback<AdminCityList>() {
                       @Override
                       public void onResponse(Call<AdminCityList> call, Response<AdminCityList> response) {
                           if (response.code()==200)
                           {
                               allCityList=response.body().getData();
                               final  Dialog citydialog=new Dialog(EditClientProfile.this);
                               citydialog.setContentView(R.layout.recyclerdialog);
                               DisplayMetrics metrics=getResources().getDisplayMetrics();
                               int width=metrics.widthPixels;
                               citydialog.getWindow().setLayout(width, LinearLayout.LayoutParams.WRAP_CONTENT);
                               cityRecycler=citydialog.findViewById(R.id.dialogRecycler);
                               cityRecycler.setLayoutManager(new LinearLayoutManager(EditClientProfile.this,LinearLayoutManager.VERTICAL,false));
                               cityRecycler.setAdapter(new AdminCityListAdapter(EditClientProfile.this,allCityList,cityname,citydialog,cityid));
                               progressDialog.dismiss();
                               citydialog.show();

                           }
                           else   if (response.code()!=200){
                               progressDialog.dismiss();
                               Toast.makeText(EditClientProfile.this, "Something Goes Wrong", Toast.LENGTH_SHORT).show();
                           }

                       }

                       @Override
                       public void onFailure(Call<AdminCityList> call, Throwable t) {
                           Snackbar.make(EditClientProfile.this.getWindow().getDecorView().findViewById(android.R.id.content), ""+t.getMessage(), Snackbar.LENGTH_SHORT).show();

                       }
                   });
               }
            }
        });
        areadrop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String cid=cityid.getText().toString().trim();
                if (cid.isEmpty())
                {
                    Toast.makeText(EditClientProfile.this, "Select City First", Toast.LENGTH_SHORT).show();
                }
                else {
                    final Dialog progressDialog = new Dialog(EditClientProfile.this);
                    progressDialog.setContentView(R.layout.customdialog);
                    progressDialog.setCancelable(false);
                    progressDialog.show();
                    UserInterface userInterface= ApiClient.getClient().create(UserInterface.class);
                    Call<AdminAllAreaResponse> call=userInterface.getAllAreaAdmin(cid);
                    call.enqueue(new Callback<AdminAllAreaResponse>() {
                        @Override
                        public void onResponse(Call<AdminAllAreaResponse> call, Response<AdminAllAreaResponse> response) {
                            if (response.code()==200)
                            {
                                List<AdminAllAreaResponse.DataBean> areaList=response.body().getData();
                                final  Dialog areadialog=new Dialog(EditClientProfile.this);
                                areadialog.setContentView(R.layout.recyclerdialog);
                                DisplayMetrics metrics=getResources().getDisplayMetrics();
                                int width=metrics.widthPixels;
                                areadialog.getWindow().setLayout(width, LinearLayout.LayoutParams.WRAP_CONTENT);
                                areaRecycler=areadialog.findViewById(R.id.dialogRecycler);
                                areaRecycler.setLayoutManager(new LinearLayoutManager(EditClientProfile.this,LinearLayoutManager.VERTICAL,false));
                                areaRecycler.setAdapter(new AreaListAdapter(EditClientProfile.this,areaList,areaname,areadialog,areaid));
                                progressDialog.dismiss();
                                areadialog.show();

                            }
                            else if (response.code()!=200)
                            {
                                progressDialog.dismiss();
                                Toast.makeText(EditClientProfile.this, "Area Not Found", Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<AdminAllAreaResponse> call, Throwable t) {

                        }
                    });
                }
            }
        });
        shopImg=findViewById(R.id.shopimg);
        Picasso.get().load("http://www.sbpharma.in/admin_assets/uploads/clients/"+client.getImage()).error(R.drawable.cross)
                .placeholder(R.drawable.loading).into(shopImg);

        shopImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LinearLayout camera, folder;
                dialog = new Dialog(EditClientProfile.this);
                dialog.setContentView(R.layout.dialogboxcamera);
                DisplayMetrics metrics=getResources().getDisplayMetrics();
                int width=metrics.widthPixels;
                dialog.getWindow().setLayout(width, LinearLayout.LayoutParams.MATCH_PARENT);
                dialog.show();
                dialog.setCancelable(true);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                Window window = dialog.getWindow();
                window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
                camera = dialog.findViewById(R.id.camera);
                folder = dialog.findViewById(R.id.folder);
                folder.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        /*checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                STORAGE_PERMISSION_CODE);*/
                        Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(i, 100);
                        dialog.dismiss();



                    }
                });
                camera.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        //checkPermission(Manifest.permission.CAMERA, CAMERA_PERMISSION_CODE);
                        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(cameraIntent, 101);
                        dialog.dismiss();
                        if (picturePath != null && !picturePath.isEmpty() && !picturePath.equals("null")) {

                            Picasso.get().load(imagePic).into(shopImg);

                            bitmap = ((BitmapDrawable) shopImg.getDrawable().getCurrent()).getBitmap();
                            Log.e("bitmap", "" + bitmap);
                            converetdImage = getResizedBitmap(bitmap, 500);

                        } else { }

                    }
                });
            }
        });
      nextdoc.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View view) {
              String ownername=name.getText().toString().trim();
              String cname=shopname.getText().toString().trim();
              String contact=mob.getText().toString().trim();
              String cemail=email.getText().toString().trim();
              String ccity=cityid.getText().toString().trim();
              String caddress=addr.getText().toString().trim();
              String carea=areaid.getText().toString().trim();
//              String webaddress=web.getText().toString().trim();
              if (ownername.isEmpty()&&cname.isEmpty()&&contact.isEmpty()&&cemail.isEmpty()&&ccity.isEmpty()&&caddress.isEmpty())
                  Snackbar.make(EditClientProfile.this.getWindow().getDecorView().findViewById(android.R.id.content),"Enter all the fields",Snackbar.LENGTH_SHORT).show();
                else if (ownername.isEmpty())
                  Snackbar.make(EditClientProfile.this.getWindow().getDecorView().findViewById(android.R.id.content),"Enter Owner Name",Snackbar.LENGTH_SHORT).show();
              else if (cname.isEmpty())
                  Snackbar.make(EditClientProfile.this.getWindow().getDecorView().findViewById(android.R.id.content),"Enter Shop Name",Snackbar.LENGTH_SHORT).show();
              else if (contact.isEmpty()|| contact.length()!=10)
                  Snackbar.make(EditClientProfile.this.getWindow().getDecorView().findViewById(android.R.id.content),"Enter Valid Mobile No.",Snackbar.LENGTH_SHORT).show();
              else if (cemail.isEmpty())
                  Snackbar.make(EditClientProfile.this.getWindow().getDecorView().findViewById(android.R.id.content),"Enter Email",Snackbar.LENGTH_SHORT).show();
              else if (ccity.isEmpty())
                  Snackbar.make(EditClientProfile.this.getWindow().getDecorView().findViewById(android.R.id.content),"Enter City",Snackbar.LENGTH_SHORT).show();
              else if (caddress.isEmpty())
                  Snackbar.make(EditClientProfile.this.getWindow().getDecorView().findViewById(android.R.id.content),"Enter Shop Address",Snackbar.LENGTH_SHORT).show();

              else {
                  Intent intent=new Intent(EditClientProfile.this, EditShopDocument.class);
                  Bundle bundle=new Bundle();
                  bundle.putSerializable("client",client);
                  bundle.putSerializable("file",image);
                  bundle.putString("ownername",ownername);
                  bundle.putString("phone",contact);
                  bundle.putString("company",cname);
                  bundle.putString("email",cemail);
                  bundle.putString("city",ccity);
                  bundle.putString("address",caddress);
                  bundle.putString("area",carea);
                  bundle.putString("state",stateId);
                  bundle.putString("district",districtId);
                  intent.putExtra("formdata",bundle);
                  overridePendingTransition(R.anim.transitionrighttoleft, R.anim.transitionrighttoleft);
                  startActivity(intent);
              }
          }
      });
    }
    private Bitmap getResizedBitmap(Bitmap image, int maxSize) {

        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //  if (requestCode == 100 && resultCode == RESULT_OK && data != null) {

        //     imagepath=selectedImage.getPath();




        if (requestCode == 100 && resultCode == RESULT_OK && data != null) {

//the image URI
            Uri selectedImage = data.getData();

            //     imagepath=selectedImage.getPath();


            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            picturePath = cursor.getString(columnIndex);
            cursor.close();


            if (picturePath != null && !picturePath.equals("")) {
                image = new File(picturePath);
            }

            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);
                converetdImage = getResizedBitmap(bitmap, 500);
                shopImg.setImageBitmap(converetdImage);
                shopImg.setVisibility(View.VISIBLE);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else if (requestCode == 101 && resultCode == Activity.RESULT_OK) {


            Bitmap converetdImage = (Bitmap) data.getExtras().get("data");
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            converetdImage.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
            shopImg.setImageBitmap(converetdImage);
            shopImg.setVisibility(View.VISIBLE);
            image = new File(Environment.getExternalStorageDirectory(), "shop.jpg");
            FileOutputStream fo;
            try {
                fo = new FileOutputStream(image);
                fo.write(bytes.toByteArray());
                fo.close();
            } catch (IOException e) {
                e.printStackTrace();
            }


        }
    }
    private void statelist()
    {
        stateId="";
        progress = new Dialog(EditClientProfile.this);
        progress.setContentView(R.layout.customdialog);
        progress.setCancelable(false);
        progress.show();
        UserInterface userInterface= ApiClient.getClient().create(UserInterface.class);
        /*--------------------------------------*/
        Call<StateResponse>call=userInterface.stateList();
        call.enqueue(new Callback<StateResponse>() {
            @Override
            public void onResponse(Call<StateResponse> call, Response<StateResponse> response) {
                if (response.code()==200)
                {
                    allStateList=response.body().getData();
                    final  Dialog stateDialog=new Dialog(EditClientProfile.this);
                    stateDialog.setContentView(R.layout.recyclerdialog);
                    DisplayMetrics metrics=getResources().getDisplayMetrics();
                    int width=metrics.widthPixels;
                    stateDialog.getWindow().setLayout(width, LinearLayout.LayoutParams.WRAP_CONTENT);
                    stateRecycler=stateDialog.findViewById(R.id.dialogRecycler);
                    stateRecycler.setLayoutManager(new LinearLayoutManager(EditClientProfile.this,LinearLayoutManager.VERTICAL,false));
                    stateRecycler.setAdapter(new StateListAdapter(EditClientProfile.this,allStateList,stateDialog, new CustomItemClickListener() {
                        @Override
                        public void onItemClick(View v, int position, String value) {
                            state.setText(value);
                            stateId=String.valueOf(position);
                        }
                    }));
                    progress.dismiss();
                    stateDialog.show();
                }
                else
                {
                    progress.dismiss();
                    Toast.makeText(EditClientProfile.this, "Something Goes Wrong", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<StateResponse> call, Throwable t) {
                progress.dismiss();
                Toast.makeText(EditClientProfile.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }
    private void districtlist()
    {
        districtId="";
        progress = new Dialog(EditClientProfile.this);
        progress.setContentView(R.layout.customdialog);
        progress.setCancelable(false);
        progress.show();
        UserInterface userInterface= ApiClient.getClient().create(UserInterface.class);
        Call<DistrictResponse>call=userInterface.getDistrict(stateId);
        call.enqueue(new Callback<DistrictResponse>() {
            @Override
            public void onResponse(Call<DistrictResponse> call, Response<DistrictResponse> response) {
                if (response.code()==200)
                {
                    allDistrict=response.body().getData();
                    final  Dialog districtDialog=new Dialog(EditClientProfile.this);
                    districtDialog.setContentView(R.layout.recyclerdialog);
                    DisplayMetrics metrics=getResources().getDisplayMetrics();
                    int width=metrics.widthPixels;
                    districtDialog.getWindow().setLayout(width, LinearLayout.LayoutParams.WRAP_CONTENT);
                    districtRecycler=districtDialog.findViewById(R.id.dialogRecycler);
                    districtRecycler.setLayoutManager(new LinearLayoutManager(EditClientProfile.this,LinearLayoutManager.VERTICAL,false));
                    districtRecycler.setAdapter(new DistrictListAdapter(EditClientProfile.this,allDistrict,districtDialog, new CustomItemClickListener() {
                        @Override
                        public void onItemClick(View v, int position, String value) {
                            district.setText(value);
                            districtId=String.valueOf(position);
                        }
                    }));
                    progress.dismiss();
                    districtDialog.show();
                }
                else {
                    progress.dismiss();
                    Toast.makeText(EditClientProfile.this, "Something Goes Wrong", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<DistrictResponse> call, Throwable t) {
                progress.dismiss();
                Toast.makeText(EditClientProfile.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });




    }
}

package kashyap.chandan.medishop.client;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import kashyap.chandan.medishop.R;


class ClientAdapter extends RecyclerView.Adapter<ClientAdapter.MyViewHolder> {
    Context context;
    List<AllClientsResponse.DataBean> allClients=new ArrayList<AllClientsResponse.DataBean>();

    public ClientAdapter(Context context,  List<AllClientsResponse.DataBean> allClients) {
        this.context=context;
        this.allClients=allClients;
    }

    @NonNull
    @Override
    public ClientAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.recvendorlist,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ClientAdapter.MyViewHolder holder, int position) {
holder.owner.setText(allClients.get(position).getPerson_name());
holder.city.setText(allClients.get(position).getCity_name());
//if (allClients.get(position).get)
holder.adress.setText(allClients.get(position).getAddress());
holder.tvshop.setText("Shop Name :");
holder.shopname.setText(allClients.get(position).getShop_name());
holder.email.setText(allClients.get(position).getEmail());
holder.comphone.setText(allClients.get(position).getMobile());
        Picasso.get().load("http://www.sbpharma.in/admin_assets/uploads/clients/"+allClients.get(position).getImage()).error(R.drawable.cross).placeholder(R.drawable.loading).into(holder.shopimage);

holder.call.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        int pos=holder.getAdapterPosition();
        String mob=allClients.get(pos).getMobile();
        Uri u = Uri.fromParts("tel",mob,null);
        Intent i = new Intent(Intent.ACTION_DIAL, u);
        context.startActivity(i);
    }
});
holder.edit.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        int
                pos=holder.getAdapterPosition();
        AllClientsResponse.DataBean client=allClients.get(pos);
        Intent intent=new Intent(context,EditClientProfile.class);
        Bundle bundle=new Bundle();
        bundle.putSerializable("client",client);
        intent.putExtra("bundle",bundle);
        context.startActivity(intent);
    }
});
holder.detail.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        int
                pos=holder.getAdapterPosition();
        AllClientsResponse.DataBean client=allClients.get(pos);
        Intent intent=new Intent(context,ClientDetails.class);
        Bundle bundle=new Bundle();
        bundle.putSerializable("client",client);
        intent.putExtra("bundle",bundle);
        context.startActivity(intent);
    }
});
        holder.whatsapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int pos=holder.getAdapterPosition();
                String mob=allClients.get(pos).getMobile();
                // use country code with your phone number
                String url = "https://api.whatsapp.com/send?phone=+91"+mob;
                try {
                    PackageManager pm = context.getPackageManager();
                    pm.getPackageInfo("com.whatsapp", PackageManager.GET_ACTIVITIES);
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(url));
                    context.startActivity(i);
                } catch (PackageManager.NameNotFoundException e) {
                    Toast.makeText(context, "Whatsapp app not installed in your phone", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }
        });
        holder.location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int pos=holder.getAdapterPosition();
                String geoUri = "http://maps.google.com/maps?q=loc:" + Double.valueOf(allClients.get(pos).getLatitude()) + "," + Double.valueOf(allClients.get(pos).getLongitude()) + " Client";
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(geoUri));
                context.startActivity(intent);
            }
        });

    }
    @Override
    public int getItemCount() {
        return allClients.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvshop,shopname,owner,email,adress,city,comphone;
        ImageView call,shopimage,edit,whatsapp,location;
        LinearLayout detail;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            tvshop=itemView.findViewById(R.id.tvpname);
            shopname=itemView.findViewById(R.id.companyname);
            owner=itemView.findViewById(R.id.personname);
            email=itemView.findViewById(R.id.comemail);
            adress=itemView.findViewById(R.id.comaddr);
            city=itemView.findViewById(R.id.vendorcity);
            call=itemView.findViewById(R.id.call);
            edit=itemView.findViewById(R.id.edit);
            shopimage=itemView.findViewById(R.id.companyImage);
            detail=itemView.findViewById(R.id.detail);
            comphone=itemView.findViewById(R.id.comphone);
            location=itemView.findViewById(R.id.location);
            whatsapp=itemView.findViewById(R.id.whatsapp);

        }
    }
}

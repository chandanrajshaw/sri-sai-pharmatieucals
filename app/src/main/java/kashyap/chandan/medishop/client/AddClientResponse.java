package kashyap.chandan.medishop.client;

public class AddClientResponse {


    /**
     * status : {"code":200,"message":"Admin added Client Details Successfully"}
     * data : {"shop_name":"abcd","person_name":"abcd","email":"vdsav","mobile":"1224567890","state_id":"2","district_id":"1","city_id":"1","area_id":"1","address":"asfdgsdgs","latitude":"123","longitude":"1234","image":"demoImage6.jpeg","gst_no":"agsdfgfd","gst_certificate":"logo.jpg","pan_no":"fbdshds","pan_card":"banner2.jpg"}
     */

    private StatusBean status;
    private DataBean data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : Admin added Client Details Successfully
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean {
        /**
         * shop_name : abcd
         * person_name : abcd
         * email : vdsav
         * mobile : 1224567890
         * state_id : 2
         * district_id : 1
         * city_id : 1
         * area_id : 1
         * address : asfdgsdgs
         * latitude : 123
         * longitude : 1234
         * image : demoImage6.jpeg
         * gst_no : agsdfgfd
         * gst_certificate : logo.jpg
         * pan_no : fbdshds
         * pan_card : banner2.jpg
         */

        private String shop_name;
        private String person_name;
        private String email;
        private String mobile;
        private String state_id;
        private String district_id;
        private String city_id;
        private String area_id;
        private String address;
        private String latitude;
        private String longitude;
        private String image;
        private String gst_no;
        private String gst_certificate;
        private String pan_no;
        private String pan_card;

        public String getShop_name() {
            return shop_name;
        }

        public void setShop_name(String shop_name) {
            this.shop_name = shop_name;
        }

        public String getPerson_name() {
            return person_name;
        }

        public void setPerson_name(String person_name) {
            this.person_name = person_name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getState_id() {
            return state_id;
        }

        public void setState_id(String state_id) {
            this.state_id = state_id;
        }

        public String getDistrict_id() {
            return district_id;
        }

        public void setDistrict_id(String district_id) {
            this.district_id = district_id;
        }

        public String getCity_id() {
            return city_id;
        }

        public void setCity_id(String city_id) {
            this.city_id = city_id;
        }

        public String getArea_id() {
            return area_id;
        }

        public void setArea_id(String area_id) {
            this.area_id = area_id;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getGst_no() {
            return gst_no;
        }

        public void setGst_no(String gst_no) {
            this.gst_no = gst_no;
        }

        public String getGst_certificate() {
            return gst_certificate;
        }

        public void setGst_certificate(String gst_certificate) {
            this.gst_certificate = gst_certificate;
        }

        public String getPan_no() {
            return pan_no;
        }

        public void setPan_no(String pan_no) {
            this.pan_no = pan_no;
        }

        public String getPan_card() {
            return pan_card;
        }

        public void setPan_card(String pan_card) {
            this.pan_card = pan_card;
        }
    }
}

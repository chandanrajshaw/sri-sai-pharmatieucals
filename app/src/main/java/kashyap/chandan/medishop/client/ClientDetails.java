package kashyap.chandan.medishop.client;

import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;
import kashyap.chandan.medishop.R;
import kashyap.chandan.medishop.vendor.AllVendorResponse;
import kashyap.chandan.medishop.vendor.VendorDetails;

public class ClientDetails extends AppCompatActivity {
TextView toolheader,owner,email,addr,city,gst,pan,phone;
ImageView image,gstimage,panimage,goback,call,whatsapp;
CircleImageView order,payment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_vendor_details);
        Intent i=getIntent();
        Bundle b=i.getBundleExtra("bundle");
        final AllClientsResponse.DataBean clientdetail= (AllClientsResponse.DataBean) b.getSerializable("client");
        toolheader=findViewById(R.id.customtoolheader);
        toolheader.setText(clientdetail.getShop_name());
        order=findViewById(R.id.order);
        order.setVisibility(View.VISIBLE);
        payment=findViewById(R.id.payment);
        payment.setVisibility(View.VISIBLE);
        owner=findViewById(R.id.ownername);
        owner.setText(clientdetail.getPerson_name());
        email=findViewById(R.id.email);
        city=findViewById(R.id.city);
        city.setText(clientdetail.getCity_name());
        email.setText(clientdetail.getEmail());
        addr=findViewById(R.id.address);
        addr.setText(clientdetail.getAddress());
        gst=findViewById(R.id.gstno);
        gst.setText(clientdetail.getGst_no());
        pan=findViewById(R.id.panno);
        pan.setText(clientdetail.getPan_no());
        image=findViewById(R.id.comcumshopimage);
        gstimage=findViewById(R.id.gstimage);
        panimage=findViewById(R.id.panimage);
        phone=findViewById(R.id.phone);
        whatsapp=findViewById(R.id.whatsapp);
        phone.setText(clientdetail.getMobile());
        Picasso.get().load("http://www.sbpharma.in/admin_assets/uploads/clients/"+clientdetail.getImage()).error(R.drawable.cross)
                .placeholder(R.drawable.loading).into(image);
        Picasso.get().load("http://www.sbpharma.in/admin_assets/uploads/clients/"+clientdetail.getGst_certificate()).error(R.drawable.cross)
                .placeholder(R.drawable.loading).into(gstimage);
        Picasso.get().load("http://www.sbpharma.in/admin_assets/uploads/clients/"+clientdetail.getPan_card()).error(R.drawable.cross)
                .placeholder(R.drawable.loading).into(panimage);
        call=findViewById(R.id.call);
        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String mob=clientdetail.getMobile();
                Uri u = Uri.fromParts("tel",mob,null);
                Intent i = new Intent(Intent.ACTION_DIAL, u);
                startActivity(i);
            }
        });
        whatsapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String url = "https://api.whatsapp.com/send?phone=+91"+clientdetail.getMobile();
                try {
                    PackageManager pm = getPackageManager();
                    pm.getPackageInfo("com.whatsapp", PackageManager.GET_ACTIVITIES);
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(url));
                    startActivity(i);
                } catch (PackageManager.NameNotFoundException e) {
                    Toast.makeText(ClientDetails.this, "Whatsapp app not installed in your phone", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }
        });
        goback=findViewById(R.id.ordergobackarrow);
        goback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                overridePendingTransition(R.anim.transitionrighttoleft,R.anim.transitionrighttoleft);
                finish();
            }
        });

        panimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog imagedialog=new Dialog(ClientDetails.this);
                imagedialog.setContentView(R.layout.imagedialog);
                DisplayMetrics metrics=getResources().getDisplayMetrics();
                int width=metrics.widthPixels;
                imagedialog.getWindow().setLayout(width, LinearLayout.LayoutParams.MATCH_PARENT);
                ImageView pancard=imagedialog.findViewById(R.id.viewimage);
                ImageView close=imagedialog.findViewById(R.id.closebtn);
                Picasso.get().load("http://www.sbpharma.in/admin_assets/uploads/clients/"+clientdetail.getPan_card()).error(R.drawable.cross)
                        .placeholder(R.drawable.loading).into(pancard);
                imagedialog.show();
                close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        imagedialog.dismiss();
                    }
                });
            }
        });
        gstimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog imagedialog=new Dialog(ClientDetails.this);
                imagedialog.setContentView(R.layout.imagedialog);
                DisplayMetrics metrics=getResources().getDisplayMetrics();
                int width=metrics.widthPixels;
                imagedialog.getWindow().setLayout(width, LinearLayout.LayoutParams.MATCH_PARENT);
                ImageView gstImage=imagedialog.findViewById(R.id.viewimage);
                ImageView close=imagedialog.findViewById(R.id.closebtn);
                Picasso.get().load("http://www.sbpharma.in/admin_assets/uploads/clients/"+clientdetail.getGst_certificate()).error(R.drawable.cross)
                        .placeholder(R.drawable.loading).into(gstImage);
                imagedialog.show();
                close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        imagedialog.dismiss();
                    }
                });
            }
        });

    }
}

package kashyap.chandan.medishop.pojoclasses;

import java.util.List;

public class GetCityResponseUser {

    /**
     * status : {"code":200,"message":"List of Cities"}
     * data : [{"id":"5","city_name":"Rajamundry","date_time":"2020-02-05 15:08:05","status":"1"},{"id":"4","city_name":"Bhimavaram","date_time":"2020-02-05 15:07:49","status":"1"},{"id":"3","city_name":"Vizag","date_time":"2020-02-05 15:07:38","status":"1"},{"id":"2","city_name":"Vijayawada","date_time":"2020-02-05 15:07:31","status":"1"},{"id":"1","city_name":"Hyderabad","date_time":"2020-02-05 15:07:24","status":"1"}]
     */

    private StatusBean status;
    private List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : List of Cities
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean {
        /**
         * id : 5
         * city_name : Rajamundry
         * date_time : 2020-02-05 15:08:05
         * status : 1
         */

        private String id;
        private String city_name;
        private String date_time;
        private String status;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getCity_name() {
            return city_name;
        }

        public void setCity_name(String city_name) {
            this.city_name = city_name;
        }

        public String getDate_time() {
            return date_time;
        }

        public void setDate_time(String date_time) {
            this.date_time = date_time;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
    }
}

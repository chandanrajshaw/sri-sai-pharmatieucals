package kashyap.chandan.medishop.pojoclasses;

import java.io.Serializable;

public class AddUserResponse implements Serializable {


    /**
     * status : {"code":200,"message":"Admin added Agent Successfully"}
     * data : {"image":"adoption.png","name":"Rajeev","email":"rajeev.sharma@gmail.com","mobile":"789654123","wa_mobile":"8789160444","password":"81dc9bdb52d04dc20036dbd8313ed055"}
     */

    private StatusBean status;
    private DataBean data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class StatusBean implements Serializable{
        /**
         * code : 200
         * message : Admin added Agent Successfully
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean implements Serializable {
        /**
         * image : adoption.png
         * name : Rajeev
         * email : rajeev.sharma@gmail.com
         * mobile : 789654123
         * wa_mobile : 8789160444
         * password : 81dc9bdb52d04dc20036dbd8313ed055
         */

        private String image;
        private String name;
        private String email;
        private String mobile;
        private String wa_mobile;
        private String password;

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getWa_mobile() {
            return wa_mobile;
        }

        public void setWa_mobile(String wa_mobile) {
            this.wa_mobile = wa_mobile;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }
    }
}

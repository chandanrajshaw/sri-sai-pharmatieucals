package kashyap.chandan.medishop.pojoclasses;

public class AdminDashboardResponse {

    /**
     * status : {"code":200,"message":"Admin Dashboard"}
     * data : {"agents":7,"stores":4}
     */

    public StatusBean status;
    public DataBean data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : Admin Dashboard
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean {
        /**
         * agents : 7
         * stores : 4
         */

        private int agents;
        private int stores;

        public int getAgents() {
            return agents;
        }

        public void setAgents(int agents) {
            this.agents = agents;
        }

        public int getStores() {
            return stores;
        }

        public void setStores(int stores) {
            this.stores = stores;
        }
    }
}

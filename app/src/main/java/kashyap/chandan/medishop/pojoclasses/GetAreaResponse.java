package kashyap.chandan.medishop.pojoclasses;

import java.util.List;

public class GetAreaResponse {

    /**
     * status : {"code":200,"message":"List of Areas"}
     * data : [{"id":"6","city_id":"1","area_name":"Begumpet","date_time":"2020-02-05 15:09:51","status":"1"},{"id":"1","city_id":"1","area_name":"Ameerpet","date_time":"2020-02-05 15:08:28","status":"1"}]
     */

    private StatusBean status;
    private List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : List of Areas
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean {
        /**
         * id : 6
         * city_id : 1
         * area_name : Begumpet
         * date_time : 2020-02-05 15:09:51
         * status : 1
         */

        private String id;
        private String city_id;
        private String area_name;
        private String date_time;
        private String status;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getCity_id() {
            return city_id;
        }

        public void setCity_id(String city_id) {
            this.city_id = city_id;
        }

        public String getArea_name() {
            return area_name;
        }

        public void setArea_name(String area_name) {
            this.area_name = area_name;
        }

        public String getDate_time() {
            return date_time;
        }

        public void setDate_time(String date_time) {
            this.date_time = date_time;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
    }
}

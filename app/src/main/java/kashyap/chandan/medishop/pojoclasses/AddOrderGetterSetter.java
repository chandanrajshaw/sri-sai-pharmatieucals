package kashyap.chandan.medishop.pojoclasses;

import java.io.Serializable;

public class AddOrderGetterSetter implements Serializable {
    String pName,pCate,pId,cId,sName,sId,qty;

    public AddOrderGetterSetter(String pName, String pCate, String pId, String cId, String sName, String sId, String qty) {
        this.pName = pName;
        this.pCate = pCate;
        this.pId = pId;
        this.cId = cId;
        this.sName = sName;
        this.sId = sId;
        this.qty = qty;
    }

    public String getpName() {
        return pName;
    }

    public void setpName(String pName) {
        this.pName = pName;
    }

    public String getpCate() {
        return pCate;
    }

    public void setpCate(String pCate) {
        this.pCate = pCate;
    }

    public String getpId() {
        return pId;
    }

    public void setpId(String pId) {
        this.pId = pId;
    }

    public String getcId() {
        return cId;
    }

    public void setcId(String cId) {
        this.cId = cId;
    }

    public String getsName() {
        return sName;
    }

    public void setsName(String sName) {
        this.sName = sName;
    }

    public String getsId() {
        return sId;
    }

    public void setsId(String sId) {
        this.sId = sId;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }
}

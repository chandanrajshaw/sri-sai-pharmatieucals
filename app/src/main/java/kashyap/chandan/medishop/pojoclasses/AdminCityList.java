package kashyap.chandan.medishop.pojoclasses;

import java.io.Serializable;
import java.util.List;

public class AdminCityList {


    /**
     * status : {"code":200,"message":"City List"}
     * data : [{"id":"2","district_id":"1","city_name":"Ameerpet","date_time":"2020-08-24 16:30:47","status":"1","district_name":"East Godavari"}]
     */

    private StatusBean status;
    private List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean implements Serializable {
        /**
         * code : 200
         * message : City List
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean implements Serializable {
        /**
         * id : 2
         * district_id : 1
         * city_name : Ameerpet
         * date_time : 2020-08-24 16:30:47
         * status : 1
         * district_name : East Godavari
         */

        private String id;
        private String district_id;
        private String city_name;
        private String date_time;
        private String status;
        private String district_name;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getDistrict_id() {
            return district_id;
        }

        public void setDistrict_id(String district_id) {
            this.district_id = district_id;
        }

        public String getCity_name() {
            return city_name;
        }

        public void setCity_name(String city_name) {
            this.city_name = city_name;
        }

        public String getDate_time() {
            return date_time;
        }

        public void setDate_time(String date_time) {
            this.date_time = date_time;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getDistrict_name() {
            return district_name;
        }

        public void setDistrict_name(String district_name) {
            this.district_name = district_name;
        }
    }
}

package kashyap.chandan.medishop.pojoclasses;

public class AddShopResponse {

    /**
     * status : {"code":200,"message":"Admin added Store Details Successfully"}
     * data : {"agent_id":"2","store_name":"ambika","person_name":"ambika","email":"asdf","mobile":"1234567890","city":"Hyderabad","area":"balkampwt","latitude":"17.5555","longitude":"98.00.22","photo":"220px-Vinoba_Bhave_University_logo.jpg"}
     */

    private StatusBean status;
    private DataBean data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : Admin added Store Details Successfully
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean {
        /**
         * agent_id : 2
         * store_name : ambika
         * person_name : ambika
         * email : asdf
         * mobile : 1234567890
         * city : Hyderabad
         * area : balkampwt
         * latitude : 17.5555
         * longitude : 98.00.22
         * photo : 220px-Vinoba_Bhave_University_logo.jpg
         */

        private String agent_id;
        private String store_name;
        private String person_name;
        private String email;
        private String mobile;
        private String city;
        private String area;
        private String latitude;
        private String longitude;
        private String photo;

        public String getAgent_id() {
            return agent_id;
        }

        public void setAgent_id(String agent_id) {
            this.agent_id = agent_id;
        }

        public String getStore_name() {
            return store_name;
        }

        public void setStore_name(String store_name) {
            this.store_name = store_name;
        }

        public String getPerson_name() {
            return person_name;
        }

        public void setPerson_name(String person_name) {
            this.person_name = person_name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getArea() {
            return area;
        }

        public void setArea(String area) {
            this.area = area;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getPhoto() {
            return photo;
        }

        public void setPhoto(String photo) {
            this.photo = photo;
        }
    }
}

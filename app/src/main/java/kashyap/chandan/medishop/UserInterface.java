package kashyap.chandan.medishop;

import kashyap.chandan.medishop.Order.AddOrderResponse;
import kashyap.chandan.medishop.Order.OrderCategoryListResponse;
import kashyap.chandan.medishop.Order.OrderProductListResponse;
import kashyap.chandan.medishop.Order.OrderResponse;
import kashyap.chandan.medishop.Order.OrderShopListResponse;
import kashyap.chandan.medishop.admin.AddDistrictResponse;
import kashyap.chandan.medishop.admin.AdminAllAreaResponse;
import kashyap.chandan.medishop.admin.AdminAllCityResponse;
import kashyap.chandan.medishop.admin.AdminChangePassResponse;
import kashyap.chandan.medishop.admin.AdminUpdateCityResponse;
import kashyap.chandan.medishop.admin.AgentListResponse;
import kashyap.chandan.medishop.admin.AllShopsResponse;
import kashyap.chandan.medishop.admin.DeleteAgentResponse;
import kashyap.chandan.medishop.admin.DeleteAreaResponse;
import kashyap.chandan.medishop.admin.DeleteCityResponse;
import kashyap.chandan.medishop.admin.DeleteDistrictResponse;
import kashyap.chandan.medishop.admin.DistrictResponse;
import kashyap.chandan.medishop.admin.EditAreaResponse;
import kashyap.chandan.medishop.admin.StateResponse;
import kashyap.chandan.medishop.admin.UpdateAgentResponse;
import kashyap.chandan.medishop.admin.UpdateDistrictResponse;
import kashyap.chandan.medishop.admin.ValidateOTPResponse;
import kashyap.chandan.medishop.agentPannel.AgentAllShopResponse;
import kashyap.chandan.medishop.agentPannel.AgentChangePwdResponse;
import kashyap.chandan.medishop.agentPannel.CityForAreaResponse;
import kashyap.chandan.medishop.agentPannel.DeleteStoreResponse;
import kashyap.chandan.medishop.agentPannel.UpdateShopResponse;
import kashyap.chandan.medishop.agentPannel.agentorder.AgentAddOrderResponse;
import kashyap.chandan.medishop.agentPannel.agentorder.AgentOrderListResponse;
import kashyap.chandan.medishop.agentPannel.agentpayment.AgentAddPaymentResponse;
import kashyap.chandan.medishop.agentPannel.agentpayment.AgentPaymentListResponse;
import kashyap.chandan.medishop.client.AddClientResponse;
import kashyap.chandan.medishop.client.AllClientsResponse;
import kashyap.chandan.medishop.client.UpdateClientResponse;
import kashyap.chandan.medishop.inventory.AddInventoryResponse;
import kashyap.chandan.medishop.inventory.InventoryProductListResponse;
import kashyap.chandan.medishop.inventory.InventoryResponse;
import kashyap.chandan.medishop.inventory.InventoryVendorListResponse;
import kashyap.chandan.medishop.inventory.UpdateImventoryResponse;
import kashyap.chandan.medishop.makeclient.MakeAsClientResponse;
import kashyap.chandan.medishop.makeclient.ShopAsClientResponse;
import kashyap.chandan.medishop.payment.AddPaymentResponse;
import kashyap.chandan.medishop.payment.PaymentResponse;
import kashyap.chandan.medishop.pojoclasses.AddCityResponse;
import kashyap.chandan.medishop.pojoclasses.AddShopResponse;
import kashyap.chandan.medishop.pojoclasses.AdminAddAreaResponse;
import kashyap.chandan.medishop.pojoclasses.AdminCityList;
import kashyap.chandan.medishop.pojoclasses.ForgetPasswordResponse;
import kashyap.chandan.medishop.pojoclasses.GetAreaResponse;
import kashyap.chandan.medishop.pojoclasses.AddUserResponse;
import kashyap.chandan.medishop.pojoclasses.AdminDashboardResponse;
import kashyap.chandan.medishop.pojoclasses.GetCityResponseUser;
import kashyap.chandan.medishop.product.AddProductResponse;
import kashyap.chandan.medishop.product.AllProductCategoryResponse;
import kashyap.chandan.medishop.product.AllProductResponse;
import kashyap.chandan.medishop.product.ProductCategoryResponse;
import kashyap.chandan.medishop.product.UpdateCategoryResponse;
import kashyap.chandan.medishop.product.UpdateProductResponse;
import kashyap.chandan.medishop.vendor.AddVendorResponse;
import kashyap.chandan.medishop.vendor.AllVendorResponse;
import kashyap.chandan.medishop.vendor.UpdateVendorResponse;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface UserInterface {
//    @FormUrlEncoded
//    @Headers("x-api-key:msla@123")
//    @POST("login")
//    Call<LoginResponse> login(JsonObject object);


    @FormUrlEncoded
    @Headers("X-API-KEY:msla@123")
    @POST("homeservice/login")
    Call<LoginResponse>login(@Field("email") String email, @Field("password") String password);

    @Multipart
    @Headers("X-API-KEY:msla@123")
    @POST("adminservice/add_agents")
    Call<AddUserResponse>userAdd(@Part("name") RequestBody name,
                                 @Part("email") RequestBody email,
                                 @Part("mobile") RequestBody phone,
                                 @Part("wa_mobile") RequestBody whatsapp,
                                 @Part("password") RequestBody password,
                                 @Part MultipartBody.Part file);


    @Multipart
    @Headers("X-API-KEY:msla@123")
    @POST("adminservice/update_agents")
    Call<UpdateAgentResponse>updateAgent(
            @Part("agent_id") RequestBody id,
          @Part("name") RequestBody name,
          @Part("email") RequestBody email,
          @Part("mobile") RequestBody phone,
          @Part("wa_mobile") RequestBody whatsapp,
          @Part("status") RequestBody status,
          @Part MultipartBody.Part file);



    @Headers("X-API-KEY:msla@123")
    @GET("adminservice/dashboard")
    Call<AdminDashboardResponse>adminDashboard();

    @Headers("x-api-key:msla@123")
    @GET("adminservice/state")
    Call<StateResponse>stateList();

    @FormUrlEncoded
    @Headers("X-API-KEY:msla@123")
    @POST("adminservice/district")
    Call<DistrictResponse>getDistrict(@Field("state_id") String stateId);

    @FormUrlEncoded
    @Headers("X-API-KEY:msla@123")
    @POST("adminservice/deletedistrict")
    Call<DeleteDistrictResponse>deleteDistrict(@Field("district_id") String district_id);



    @FormUrlEncoded
    @Headers("X-API-KEY:msla@123")
    @POST("adminservice/add_district")
    Call<AddDistrictResponse>addDistrict(@Field("state_id") String stateId, @Field("district_name") String name);

    @FormUrlEncoded
    @Headers("X-API-KEY:msla@123")
    @POST("adminservice/update_district")
    Call<UpdateDistrictResponse>updateDistrict(@Field("state_id") String stateId,@Field("district_id") String district_id,
                                               @Field("district_name") String name,@Field("status") String status);


    @Headers("X-API-KEY:msla@123")
    @GET("agentservice/city_in_add_store")
    Call<GetCityResponseUser>getCity();

    @FormUrlEncoded
    @Headers("X-API-KEY:msla@123")
    @POST("agentservice/area_in_add_store")
    Call<GetAreaResponse>getArea(@Field("city_id") String cityid);

    @FormUrlEncoded
    @Headers("X-API-KEY:msla@123")
    @POST("adminservice/add_city")
    Call<AddCityResponse>addCity(@Field("district_id") String id,@Field("city_name") String cityname);

    @FormUrlEncoded
    @Headers("X-API-KEY:msla@123")
    @POST("adminservice/city")
    Call<AdminCityList>getCityList(@Field("district_id") String id);

//    @Headers("X-API-KEY:msla@123")
//    @GET("adminservice/city")
//    Call<AdminAllCityResponse>getAllAdminCity();

    @FormUrlEncoded
    @Headers("X-API-KEY:msla@123")
    @POST("adminservice/add_area")
    Call<AdminAddAreaResponse>addArea(@Field("city_id") String id, @Field("area_name") String areaname);

    @Headers("X-API-KEY:msla@123")
    @GET("adminservice/stores")
    Call<AllShopsResponse>getAllShopAdmin();

    @Headers("X-API-KEY:msla@123")
    @GET("adminservice/agents")
    Call<AgentListResponse>getAllAgentAdmin();

    @FormUrlEncoded
    @Headers("X-API-KEY:msla@123")
    @POST("adminservice/change_password")
    Call<AdminChangePassResponse>adminChangePassword(@Field("email") String email, @Field("old_password") String oldPass, @Field("new_password") String newPass);



    @Multipart
    @Headers("X-API-KEY:msla@123")
    @POST("agentservice/add_stores")
    Call<AddShopResponse>addShop(@Part("agent_id") RequestBody id,
                                 @Part("store_name") RequestBody sname,
                                 @Part("person_name") RequestBody pname,
                                 @Part("email") RequestBody email,
                                 @Part("mobile") RequestBody mob,
                                 @Part("wa_number") RequestBody wtsp,
                                 @Part("city") RequestBody city,
                                 @Part("area") RequestBody area,
                                 @Part("latitude") RequestBody lat,
                                 @Part("longitude") RequestBody lng,
                                 @Part MultipartBody.Part file,
                                 @Part("state") RequestBody state,
                                 @Part("district") RequestBody district  );
    @FormUrlEncoded
    @Headers("X-API-KEY:msla@123")
    @POST("adminservice/area")
    Call<AdminAllAreaResponse>getAllAreaAdmin(@Field("city_id") String city_id);

    @FormUrlEncoded
    @Headers("X-API-KEY:msla@123")
    @POST("adminservice/update_city")
    Call<AdminUpdateCityResponse>updateCity(@Field("district_id") String districtId,@Field("city_id") String id, @Field("city_name") String cityname, @Field("status") String status);

    @FormUrlEncoded
    @Headers("X-API-KEY:msla@123")
    @POST("adminservice/update_area")
    Call<EditAreaResponse>updateArea(@Field("area_id") String areaid, @Field("city_id") String cityId,
                                     @Field("area_name") String areaname, @Field("status") String status);

    @FormUrlEncoded
    @Headers("X-API-KEY:msla@123")
    @POST("adminservice/deleteagent")
    Call<DeleteAgentResponse>deleteAgent(@Field("agent_id") String agentId);

    @FormUrlEncoded
    @Headers("X-API-KEY:msla@123")
    @POST("adminservice/deletearea")
    Call<DeleteAreaResponse>deletearea(@Field("area_id") String areaId);

    @FormUrlEncoded
    @Headers("X-API-KEY:msla@123")
    @POST("adminservice/deletecity")
    Call<DeleteCityResponse>deletecity(@Field("city_id") String cityId);

    @FormUrlEncoded
    @Headers("X-API-KEY:msla@123")
    @POST("agentservice/stores")
    Call<AgentAllShopResponse>getAllAgentShop(@Field("agent_id") String agentId);

    @Multipart
    @Headers("X-API-KEY:msla@123")
    @POST("agentservice/update_stores")
    Call<UpdateShopResponse>updateShop(@Part("store_id") RequestBody id,
                                 @Part("store_name") RequestBody sname,
                                 @Part("person_name") RequestBody pname,
                                 @Part("email") RequestBody email,
                                 @Part("mobile") RequestBody mob,
                                       @Part("wa_number") RequestBody wtsp,
                                 @Part("city") RequestBody city,
                                 @Part("area") RequestBody area,
                                 @Part("latitude") RequestBody lat,
                                 @Part("longitude") RequestBody lng,
                                       @Part("status") RequestBody status,
                                 @Part MultipartBody.Part file);

    @FormUrlEncoded
    @Headers("X-API-KEY:msla@123")
    @POST("agentservice/deletestores")
    Call<DeleteStoreResponse>deletetShop(@Field("store_id") String storeid);

    @FormUrlEncoded
    @Headers("X-API-KEY:msla@123")
    @POST("agentservice/change_password")
    Call<AgentChangePwdResponse>agentChangePassword(@Field("email") String email, @Field("old_password") String oldPass, @Field("new_password") String newPass);

    @FormUrlEncoded
    @Headers("X-API-KEY:msla@123")
    @POST("homeservice/forgetpassword")
    Call<ForgetPasswordResponse>getOtp(@Field("email") String email);

    @FormUrlEncoded
    @Headers("X-API-KEY:msla@123")
    @POST("homeservice/insertotp")
    Call<ValidateOTPResponse>validateOTP(@Field("email") String email, @Field("otp") String otp);

    @FormUrlEncoded
    @Headers("X-API-KEY:msla@123")
    @POST("homeservice/changepassword")
    Call<ChangePwdFinalResponse>changepwd(@Field("email") String email, @Field("password") String pass,@Field("conf_password") String conpass);

    @FormUrlEncoded
    @Headers("X-API-KEY:msla@123")
    @POST("adminservice/add_product_catagories")
    Call<ProductCategoryResponse>addProductCategory(@Field("pc_name") String pname);

    @Headers("X-API-KEY:msla@123")
    @GET("adminservice/product_catagories")
    Call<AllProductCategoryResponse>getProductCategory();

    @FormUrlEncoded
    @Headers("X-API-KEY:msla@123")
    @POST("adminservice/update_product_catagories")
    Call<UpdateCategoryResponse>updateProductCategory(@Field("product_catagory_id") String id,@Field("product_catagory_name") String name,@Field("status") String status);

    @Multipart
    @Headers("X-API-KEY:msla@123")
    @POST("adminservice/add_products")
    Call<AddProductResponse>addProduct(@Part("product_catagory_id") RequestBody id,
                                       @Part("product_name") RequestBody pname,
                                       @Part("mkt_price") RequestBody price,
                                       @Part("ptr_price") RequestBody newprice,
                                       @Part MultipartBody.Part file);

    @Headers("X-API-KEY:msla@123")
    @GET("adminservice/products")
    Call<AllProductResponse>getAllProducts();

    @Multipart
    @Headers("X-API-KEY:msla@123")
    @POST("adminservice/update_products")
    Call<UpdateProductResponse>updateProduct(  @Part("product_id") RequestBody pid,
                                               @Part("product_catagory_id") RequestBody cid,
                                             @Part("product_name") RequestBody pname,
                                             @Part("mkt_price") RequestBody price,
                                             @Part("ptr_price") RequestBody newprice,
                                               @Part("status") RequestBody status,
                                             @Part MultipartBody.Part file);
    @Headers("X-API-KEY:msla@123")
    @GET("adminservice/vendors")
    Call<AllVendorResponse>getAllVendors();


    @Multipart
    @Headers("X-API-KEY:msla@123")
    @POST("adminservice/add_vendors")
    Call<AddVendorResponse>addVendors(@Part("company_name") RequestBody companyname,
                                      @Part("person_name") RequestBody ownername,
                                      @Part("email") RequestBody email,
                                      @Part("mobile") RequestBody mobile,
                                      @Part("city_id") RequestBody city,
                                      @Part("address") RequestBody addr,
                                      @Part("gst_no") RequestBody gstno,
                                      @Part("pan_no") RequestBody panno,
                                      @Part MultipartBody.Part file, @Part MultipartBody.Part gstfile, @Part MultipartBody.Part panfile
//                                      @Part("gst_no") RequestBody gstno,
//                                      @Part("pan_no") RequestBody panno,
                                   );


    @Headers("X-API-KEY:msla@123")
    @GET("adminservice/clients")
    Call<AllClientsResponse>getAllClients();


    @Multipart
    @Headers("X-API-KEY:msla@123")
    @POST("adminservice/add_clients")
    Call<AddClientResponse>addClient(@Part("shop_name") RequestBody companyname,
                                     @Part("person_name") RequestBody ownername,
                                     @Part("email") RequestBody email,
                                     @Part("mobile") RequestBody mobile,
                                     @Part("city_id") RequestBody city,
                                     @Part("area_id") RequestBody area,
                                     @Part("address") RequestBody addr,
                                     @Part("latitude") RequestBody latitude,
                                     @Part("longitude") RequestBody longitude,
                                     @Part("gst_no") RequestBody gstno,
                                     @Part("pan_no") RequestBody panno,
                                     @Part("state_id") RequestBody stateId,
                                     @Part("district_id") RequestBody districtId,
                                     @Part MultipartBody.Part file, @Part MultipartBody.Part gstfile, @Part MultipartBody.Part panfile);

    @Multipart
    @Headers("X-API-KEY:msla@123")
    @POST("adminservice/addclients_to_client")
    Call<ShopAsClientResponse>addStoreClient(
            @Part("store_id") RequestBody id,
            @Part("shop_name") RequestBody companyname,
                                             @Part("person_name") RequestBody ownername,
                                             @Part("email") RequestBody email,
                                             @Part("mobile") RequestBody mobile,
                                             @Part("city_id") RequestBody city,
                                             @Part("area_id") RequestBody area,
                                             @Part("address") RequestBody addr,
                                             @Part("latitude") RequestBody latitude,
                                             @Part("longitude") RequestBody longitude,
                                             @Part("gst_no") RequestBody gstno,
                                             @Part("pan_no") RequestBody panno,
                                             @Part MultipartBody.Part file, @Part MultipartBody.Part gstfile,
            @Part MultipartBody.Part panfile,
            @Part("state_id") RequestBody stateId,
            @Part("district_id") RequestBody districtId);






    @Headers("X-API-KEY:msla@123")
    @GET("adminservice/inventory")
    Call<InventoryResponse>getInventory();

    @FormUrlEncoded
    @Headers("X-API-KEY:msla@123")
    @POST("adminservice/add_inventory")
    Call<AddInventoryResponse>addInventory(@Field("vendor_id") String vendorid,
                                           @Field("date_of_delivery") String delDate,
                                           @Field("product_catagories[]") String[] pc,
                                           @Field("product_id[]") String[] pid,
                                           @Field("quantity[]") String[] qty,
                                           @Field("purchase_price[]") String[] pprice,
                                           @Field("mrp[]") String[] mrp,
                                           @Field("expiry_date[]") String[] exp,
                                           @Field("batch_no[]") String[] batch
                                           )

            ;

    @FormUrlEncoded
    @Headers("X-API-KEY:msla@123")
    @POST("adminservice/add_inventory")
    Call<UpdateImventoryResponse>updateInventory(@Field("inventory_id") String inventoryid,@Field("product_id") String pid, @Field("vendor_id") String vid, @Field("quantity") String qty, @Field("date_of_delivery") String deliver, @Field("batch_no") String batch,@Field("status") String status);




    @Multipart
    @Headers("X-API-KEY:msla@123")
    @POST("adminservice/update_vendors")
    Call<UpdateVendorResponse>updateVendors(@Part("vendors_id") RequestBody vendorid,
                                            @Part("company_name") RequestBody companyname,
                                            @Part("person_name") RequestBody ownername,
                                            @Part("email") RequestBody email,
                                            @Part("mobile") RequestBody mobile,
                                            @Part("city_id") RequestBody city,
                                            @Part("address") RequestBody addr,
                                            @Part("gst_no") RequestBody gstno,
                                            @Part("pan_no") RequestBody panno,
                                            @Part MultipartBody.Part file, @Part MultipartBody.Part gstfile, @Part MultipartBody.Part panfile
       ,@Part("status") RequestBody status);



    @Multipart
    @Headers("X-API-KEY:msla@123")
    @POST("adminservice/update_clients")
    Call<UpdateClientResponse>updateClient(@Part("clients_id") RequestBody clientid,
                                           @Part("shop_name") RequestBody shopname,
                                           @Part("person_name") RequestBody ownername,
                                           @Part("email") RequestBody email,
                                           @Part("mobile") RequestBody mobile,
                                           @Part("city_id") RequestBody city,
                                           @Part("area_id") RequestBody area,
                                           @Part("address") RequestBody addr,
                                           @Part("latitude") RequestBody lat,
                                           @Part("longitude") RequestBody lng,
                                           @Part("gst_no") RequestBody gstno,
                                           @Part("pan_no") RequestBody panno,
                                           @Part MultipartBody.Part file, @Part MultipartBody.Part gstfile, @Part MultipartBody.Part panfile
                                             ,@Part("status") RequestBody status,
                                                 @Part("state_id") RequestBody state_id,
                                                    @Part("district_id") RequestBody district_id);


    @Headers("X-API-KEY:msla@123")
    @GET("adminservice/products_list_for_inventory")
    Call<InventoryProductListResponse>getInventoryProductList();


    @Headers("X-API-KEY:msla@123")
    @GET("adminservice/vendors_list_for_inventory")
    Call<InventoryVendorListResponse>getInventoryVendorList();


    @Headers("X-API-KEY:msla@123")
    @GET("adminservice/shop_list_for_orders")
    Call<OrderShopListResponse>getOrderShopList();

    @Headers("X-API-KEY:msla@123")
    @GET("adminservice/product_catagories_list_for_orders")
    Call<OrderCategoryListResponse>getOrderCategoryList();


    @FormUrlEncoded
    @Headers("x-api-key:msla@123")
    @POST("adminservice/product_name_list_for_orders")
    Call<OrderProductListResponse>orderProductList(@Field("product_catagory_id") String catid);


    @FormUrlEncoded
    @Headers("x-api-key:msla@123")
    @POST("agentservice/add_orders")
    Call<AgentAddOrderResponse>addAgentOrder(@Field("shop_id") String sid,@Field("agent_id") String agentid, @Field("product_catagories[]") String[] cid, @Field("product_id[]") String[] pid, @Field("quantity[]") String[] qty, @Field("price[]") String[] price, @Field("total[]") String[] total);

    @FormUrlEncoded
    @Headers("x-api-key:msla@123")
    @POST("agentservice/orders")
    Call<AgentOrderListResponse>getAgentOrders(@Field("agent_id")String id);

    @FormUrlEncoded
    @Headers("x-api-key:msla@123")
    @POST("adminservice/add_orders")
    Call<AddOrderResponse>addOrder(@Field("shop_id") String sid,@Field("product_catagories[]") String[] cid,@Field("product_id[]") String[] pid,@Field("quantity[]") String[] qty,@Field("price[]") String[] price,@Field("total[]") String[] total);

    @Multipart
    @Headers("x-api-key:msla@123")
    @POST("agentservice/add_payments")
    Call<AgentAddPaymentResponse>addAgentPayment(@Part MultipartBody.Part file,
                                                 @Part("agent_id") RequestBody aid,
                                            @Part("shop_id") RequestBody sid,
                                            @Part("paid_amount") RequestBody amount,
                                            @Part("payment_type") RequestBody ptype,
                                            @Part("description") RequestBody desc,
                                            @Part("bill_no") RequestBody billno);


    @Headers("x-api-key:msla@123")
    @GET("adminservice/orders")
    Call<OrderResponse>getAllOrder();


    @Headers("x-api-key:msla@123")
    @GET("adminservice/payments")
    Call<PaymentResponse>getPayment();

    @FormUrlEncoded
    @Headers("x-api-key:msla@123")
    @POST("agentservice/payments")
    Call<AgentPaymentListResponse>getAgentPayment(@Field("agent_id")String id);

    @Multipart
    @Headers("x-api-key:msla@123")
    @POST("adminservice/add_payments")
    Call<AddPaymentResponse>addPayment(  @Part MultipartBody.Part file,
                                         @Part("shop_id") RequestBody sid,
                                          @Part("paid_amount") RequestBody amount,
                                          @Part("payment_type") RequestBody ptype,
                                          @Part("description") RequestBody desc,
                                          @Part("bill_no") RequestBody billno
                                        );

    @Headers("x-api-key:msla@123")
    @GET("adminservice/city_for_area")
    Call<CityForAreaResponse>cityForArea();

    @FormUrlEncoded
    @Headers("x-api-key:msla@123")
    @POST("adminservice/stores_to_addclient")
    Call<MakeAsClientResponse>makeClient(@Field("store_id")String id);


}

package kashyap.chandan.medishop.Order;

import java.io.Serializable;
import java.util.List;

public class OrderResponse implements Serializable {


    /**
     * status : {"code":200,"message":"Orders List(Here product_catagories belongs to pc_name)"}
     * data : [{"unique_id":"SBORD5","total":"6000","date_time":"2020-04-01 10:07:45","shop_name":"Shankar Medical Shop","no_of_items":3,"details":[{"id":"10","unique_id":"SBORD5","shop_id":"1","product_catagories":"1","product_id":"1","quantity":"250","price":"10","total":"2500","date_time":"2020-04-01 10:07:45","status":"1","product_name":"Paracetmol","pc_name":"A Catagory"},{"id":"9","unique_id":"SBORD5","shop_id":"1","product_catagories":"2","product_id":"2","quantity":"10","price":"250","total":"2500","date_time":"2020-04-01 10:07:45","status":"1","product_name":"Tylenol Cold","pc_name":"B Catagory"},{"id":"8","unique_id":"SBORD5","shop_id":"1","product_catagories":"1","product_id":"1","quantity":"100","price":"10","total":"1000","date_time":"2020-04-01 10:07:45","status":"1","product_name":"Paracetmol","pc_name":"A Catagory"}]},{"unique_id":"SBORD4","total":"3653715","date_time":"2020-03-24 11:55:23","shop_name":"Shakti Medical","no_of_items":2,"details":[{"id":"7","unique_id":"SBORD4","shop_id":"3","product_catagories":"2","product_id":"2","quantity":"25","price":"123","total":"3075","date_time":"2020-03-24 11:55:23","status":"1","product_name":"Tylenol Cold","pc_name":"B Catagory"},{"id":"6","unique_id":"SBORD4","shop_id":"3","product_catagories":"2","product_id":"2","quantity":"656","price":"5565","total":"3650640","date_time":"2020-03-24 11:55:23","status":"1","product_name":"Tylenol Cold","pc_name":"B Catagory"}]},{"unique_id":"SBORD3","total":"5000","date_time":"2020-03-15 02:06:16","shop_name":"Shankar Medical Shop","no_of_items":1,"details":[{"id":"5","unique_id":"SBORD3","shop_id":"1","product_catagories":"1","product_id":"1","quantity":"100","price":"50","total":"5000.0","date_time":"2020-03-15 02:06:16","status":"1","product_name":"Paracetmol","pc_name":"A Catagory"}]},{"unique_id":"SBORD2","total":"83","date_time":"2020-03-13 17:30:43","shop_name":"Shankar Medical Shop","no_of_items":2,"details":[{"id":"4","unique_id":"SBORD2","shop_id":"1","product_catagories":"1","product_id":"1","quantity":"8","price":"6","total":"48","date_time":"2020-03-13 17:30:43","status":"1","product_name":"Paracetmol","pc_name":"A Catagory"},{"id":"3","unique_id":"SBORD2","shop_id":"1","product_catagories":"2","product_id":"2","quantity":"5","price":"7","total":"35","date_time":"2020-03-13 17:30:43","status":"1","product_name":"Tylenol Cold","pc_name":"B Catagory"}]},{"unique_id":"SBORD1","total":"11","date_time":"2020-03-13 17:30:16","shop_name":"Shankar Medical Shop","no_of_items":2,"details":[{"id":"2","unique_id":"SBORD1","shop_id":"1","product_catagories":"2","product_id":"2","quantity":"2","price":"4","total":"8","date_time":"2020-03-13 17:30:16","status":"1","product_name":"Tylenol Cold","pc_name":"B Catagory"},{"id":"1","unique_id":"SBORD1","shop_id":"1","product_catagories":"1","product_id":"1","quantity":"1","price":"3","total":"3","date_time":"2020-03-13 17:30:16","status":"1","product_name":"Paracetmol","pc_name":"A Catagory"}]}]
     */

    private StatusBean status;
    private List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean implements Serializable {
        /**
         * code : 200
         * message : Orders List(Here product_catagories belongs to pc_name)
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean implements Serializable {
        /**
         * unique_id : SBORD5
         * total : 6000
         * date_time : 2020-04-01 10:07:45
         * shop_name : Shankar Medical Shop
         * no_of_items : 3
         * details : [{"id":"10","unique_id":"SBORD5","shop_id":"1","product_catagories":"1","product_id":"1","quantity":"250","price":"10","total":"2500","date_time":"2020-04-01 10:07:45","status":"1","product_name":"Paracetmol","pc_name":"A Catagory"},{"id":"9","unique_id":"SBORD5","shop_id":"1","product_catagories":"2","product_id":"2","quantity":"10","price":"250","total":"2500","date_time":"2020-04-01 10:07:45","status":"1","product_name":"Tylenol Cold","pc_name":"B Catagory"},{"id":"8","unique_id":"SBORD5","shop_id":"1","product_catagories":"1","product_id":"1","quantity":"100","price":"10","total":"1000","date_time":"2020-04-01 10:07:45","status":"1","product_name":"Paracetmol","pc_name":"A Catagory"}]
         */

        private String unique_id;
        private String total;
        private String date_time;
        private String shop_name;
        private int no_of_items;
        private List<DetailsBean> details;

        public String getUnique_id() {
            return unique_id;
        }

        public void setUnique_id(String unique_id) {
            this.unique_id = unique_id;
        }

        public String getTotal() {
            return total;
        }

        public void setTotal(String total) {
            this.total = total;
        }

        public String getDate_time() {
            return date_time;
        }

        public void setDate_time(String date_time) {
            this.date_time = date_time;
        }

        public String getShop_name() {
            return shop_name;
        }

        public void setShop_name(String shop_name) {
            this.shop_name = shop_name;
        }

        public int getNo_of_items() {
            return no_of_items;
        }

        public void setNo_of_items(int no_of_items) {
            this.no_of_items = no_of_items;
        }

        public List<DetailsBean> getDetails() {
            return details;
        }

        public void setDetails(List<DetailsBean> details) {
            this.details = details;
        }

        public static class DetailsBean implements Serializable {
            /**
             * id : 10
             * unique_id : SBORD5
             * shop_id : 1
             * product_catagories : 1
             * product_id : 1
             * quantity : 250
             * price : 10
             * total : 2500
             * date_time : 2020-04-01 10:07:45
             * status : 1
             * product_name : Paracetmol
             * pc_name : A Catagory
             */

            private String id;
            private String unique_id;
            private String shop_id;
            private String product_catagories;
            private String product_id;
            private String quantity;
            private String price;
            private String total;
            private String date_time;
            private String status;
            private String product_name;
            private String pc_name;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getUnique_id() {
                return unique_id;
            }

            public void setUnique_id(String unique_id) {
                this.unique_id = unique_id;
            }

            public String getShop_id() {
                return shop_id;
            }

            public void setShop_id(String shop_id) {
                this.shop_id = shop_id;
            }

            public String getProduct_catagories() {
                return product_catagories;
            }

            public void setProduct_catagories(String product_catagories) {
                this.product_catagories = product_catagories;
            }

            public String getProduct_id() {
                return product_id;
            }

            public void setProduct_id(String product_id) {
                this.product_id = product_id;
            }

            public String getQuantity() {
                return quantity;
            }

            public void setQuantity(String quantity) {
                this.quantity = quantity;
            }

            public String getPrice() {
                return price;
            }

            public void setPrice(String price) {
                this.price = price;
            }

            public String getTotal() {
                return total;
            }

            public void setTotal(String total) {
                this.total = total;
            }

            public String getDate_time() {
                return date_time;
            }

            public void setDate_time(String date_time) {
                this.date_time = date_time;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public String getProduct_name() {
                return product_name;
            }

            public void setProduct_name(String product_name) {
                this.product_name = product_name;
            }

            public String getPc_name() {
                return pc_name;
            }

            public void setPc_name(String pc_name) {
                this.pc_name = pc_name;
            }
        }
    }
}

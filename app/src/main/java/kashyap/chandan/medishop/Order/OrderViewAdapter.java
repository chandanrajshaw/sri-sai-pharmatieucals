package kashyap.chandan.medishop.Order;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import kashyap.chandan.medishop.R;

class OrderViewAdapter extends RecyclerView.Adapter<OrderViewAdapter.MyViewHOlder> {
    int i;
    Context context;
    String[] tot, productnam, qunty;
    public OrderViewAdapter(Context context, String[] tot, String[] productnam, String[] qunty) {
        this.context=context;
        this.tot=tot;
        this.productnam=productnam;
        this.qunty=qunty;
    }

    @NonNull
    @Override
    public MyViewHOlder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.recaddorder,parent,false);
        return new MyViewHOlder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHOlder holder, int position) {
holder.amt.setText(tot[position]);
holder.qty.setText(qunty[position]);
holder.pname.setText(productnam[position]);
    }
 private    void getArrayLength()
    {
         i=0;
      while(productnam[i]!=null)  {
          i++;
      }
    }

    @Override
    public int getItemCount() {
        getArrayLength();
        return i;
    }

    public class MyViewHOlder extends RecyclerView.ViewHolder {
        TextView pname,qty,amt;
        public MyViewHOlder(@NonNull View itemView) {
            super(itemView);
            pname=itemView.findViewById(R.id.pname);
            qty=itemView.findViewById(R.id.quantity);
            amt=itemView.findViewById(R.id.amount);
        }
    }
}

package kashyap.chandan.medishop.Order;

import java.io.Serializable;
import java.util.List;

public class OrderProductListResponse implements Serializable {


    /**
     * status : {"code":200,"message":"List of Products of Particular Product Catagory"}
     * data : [{"id":"1","product_catagory_id":"1","product_name":"Paracetmol","image":"paracetamol_tablets_500mg.jpg","mkt_price":"200","ptr_price":"150","date_time":"2020-02-25 13:57:46","status":"1"}]
     */

    private StatusBean status;
    private List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean implements Serializable {
        /**
         * code : 200
         * message : List of Products of Particular Product Catagory
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean implements Serializable {
        /**
         * id : 1
         * product_catagory_id : 1
         * product_name : Paracetmol
         * image : paracetamol_tablets_500mg.jpg
         * mkt_price : 200
         * ptr_price : 150
         * date_time : 2020-02-25 13:57:46
         * status : 1
         */

        private String id;
        private String product_catagory_id;
        private String product_name;
        private String image;
        private String mkt_price;
        private String ptr_price;
        private String date_time;
        private String status;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getProduct_catagory_id() {
            return product_catagory_id;
        }

        public void setProduct_catagory_id(String product_catagory_id) {
            this.product_catagory_id = product_catagory_id;
        }

        public String getProduct_name() {
            return product_name;
        }

        public void setProduct_name(String product_name) {
            this.product_name = product_name;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getMkt_price() {
            return mkt_price;
        }

        public void setMkt_price(String mkt_price) {
            this.mkt_price = mkt_price;
        }

        public String getPtr_price() {
            return ptr_price;
        }

        public void setPtr_price(String ptr_price) {
            this.ptr_price = ptr_price;
        }

        public String getDate_time() {
            return date_time;
        }

        public void setDate_time(String date_time) {
            this.date_time = date_time;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
    }
}

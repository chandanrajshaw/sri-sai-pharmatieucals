package kashyap.chandan.medishop.Order;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import kashyap.chandan.medishop.R;

public class OrderAllProductAdapter extends RecyclerView.Adapter<OrderAllProductAdapter.MyViewHolder> {
    Context context;
    List<OrderProductListResponse.DataBean> allproducts=new ArrayList<OrderProductListResponse.DataBean>();
    TextView productname, productid;
    Dialog productdialog;
    private int SelectedItem = -1;
    String selectedproduct,selectedid;
    public OrderAllProductAdapter(Context context, List<OrderProductListResponse.DataBean> allproducts, TextView productname, TextView productid, Dialog productdialog) {
   this.context=context;
   this.productname=productname;
   this.productid=productid;
   this.productdialog=productdialog;
   this.allproducts=allproducts;
    }

    @NonNull
    @Override
    public OrderAllProductAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.itemlayout,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull OrderAllProductAdapter.MyViewHolder holder, int position) {
        holder.radioselect.setChecked(position == SelectedItem);
        holder.product.setText(allproducts.get(position).getProduct_name());
        TextView head=productdialog.findViewById(R.id.dialoghead);
        head.setText("Select Product");
        TextView ok=productdialog.findViewById(R.id.ok);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                productname.setText(selectedproduct);
                if (productname.getText().toString().isEmpty()){
                    Toast.makeText(context,"Select Product",Toast.LENGTH_SHORT).show();
                }
                else {

                    productid.setText(selectedid);
                    productdialog.dismiss();
                }

            }
        });
    }

    @Override
    public int getItemCount() {
        return allproducts.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView product;
        RadioButton radioselect;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            product=itemView.findViewById(R.id.items);
            radioselect=itemView.findViewById(R.id.selectedItem);
            View.OnClickListener clickListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SelectedItem =getAdapterPosition();
                    notifyDataSetChanged();
                    selectedproduct=allproducts.get(SelectedItem).getProduct_name();
                    selectedid=allproducts.get(SelectedItem).getId();
                }

            };
            itemView.setOnClickListener(clickListener);
            radioselect.setOnClickListener(clickListener);
        }
    }
}

package kashyap.chandan.medishop.Order;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.List;

import kashyap.chandan.medishop.ApiClient;
import kashyap.chandan.medishop.ConnectionDetector;
import kashyap.chandan.medishop.R;
import kashyap.chandan.medishop.UserInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Orders extends AppCompatActivity {
RecyclerView orderrecycler;
ShimmerFrameLayout shimmerFrameLayout;
//TextView tv;
ImageView addFab,gobackarrow;
ConnectionDetector connectionDetector;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_orders);
orderrecycler=findViewById(R.id.orderlist);
shimmerFrameLayout=findViewById(R.id.shimmerview);
addFab=findViewById(R.id.addfab);
//tv=findViewById(R.id.customtoolheader);
//tv.setText("No Order");
        connectionDetector=new ConnectionDetector(Orders.this);
gobackarrow=findViewById(R.id.ordergobackarrow);
addFab.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        Intent intent=new Intent(Orders.this,AddOrders.class);
        overridePendingTransition(R.anim.transitionrighttoleft,R.anim.transitionrighttoleft);
        startActivity(intent);
    }
});
if (!connectionDetector.isConnectingToInternet()){
    Snackbar.make(Orders.this.getWindow().getDecorView().findViewById(android.R.id.content),"Plzz Connect to internet And Tryu Again",Snackbar.LENGTH_SHORT).show();

}
else if (connectionDetector.isConnectingToInternet()){
//    final Dialog progress=new Dialog(Orders.this);
//    progress.setContentView(R.layout.customdialog);
//    progress.setCancelable(false);
//    progress.show();
    UserInterface userInterface= ApiClient.getClient().create(UserInterface.class);
    Call<OrderResponse>call=userInterface.getAllOrder();
    call.enqueue(new Callback<OrderResponse>() {
        @Override
        public void onResponse(Call<OrderResponse> call, Response<OrderResponse> response) {
            if (response.code()==200){
                OrderResponse allorders=new OrderResponse();
                allorders=response.body();
                orderrecycler.setLayoutManager(new LinearLayoutManager(Orders.this,LinearLayoutManager.VERTICAL,false));
                orderrecycler.setAdapter(new OrderListAdapter(Orders.this,allorders));
                shimmerFrameLayout.stopShimmer();
                shimmerFrameLayout.setVisibility(View.GONE);
                orderrecycler.setVisibility(View.VISIBLE);
//                progress.dismiss();
            }
            else if (response.code()!=200)
            {
//                progress.dismiss();
                shimmerFrameLayout.stopShimmer();
                shimmerFrameLayout.setVisibility(View.GONE);
                Snackbar.make(Orders.this.getWindow().getDecorView().findViewById(android.R.id.content),"Error Occurs!!No Orders",Snackbar.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onFailure(Call<OrderResponse> call, Throwable t) {
//            progress.dismiss();
            Snackbar.make(Orders.this.getWindow().getDecorView().findViewById(android.R.id.content),""+t.getMessage(),Snackbar.LENGTH_SHORT).show();

        }
    });
}


gobackarrow.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        overridePendingTransition(R.anim.transitionrighttoleft,R.anim.transitionrighttoleft);
        finish();
    }
});

    }

    @Override
    protected void onResume() {
        super.onResume();
        shimmerFrameLayout.startShimmer();
    }

    @Override
    protected void onPause() {
        super.onPause();
        shimmerFrameLayout.stopShimmer();
    }
}

package kashyap.chandan.medishop.Order;

import java.io.Serializable;
import java.util.List;

public class OrderShopListResponse implements Serializable {

    /**
     * status : {"code":200,"message":"List of Shop Names from Clients"}
     * data : [{"id":"1","shop_name":"igrand","image":"screen-0.jpg","person_name":"vinay","email":"vinaynarendrapeeta@gmail.com","mobile":"7013332509","city":"bhimavaram","address":"bhimavaram","gst_no":"13g","gst_certificate":"Angular_Course_Content.docx","pan_no":"asdas12","pan_card":"Python_Course_Content.docx","date_time":"2020-02-26 12:16:29","status":"1"},{"id":"2","shop_name":"kjh","image":"crop.png","person_name":"lkjh","email":"lkkhkh.com","mobile":"123456789","city":"abcd","address":"skjbjasbjkbfknkasnknknn","gst_no":"CDSK00000","gst_certificate":"vilahge.png","pan_no":"PAN0000","pan_card":"survey.png","date_time":"2020-03-02 18:48:28","status":"1"},{"id":"3","shop_name":"gdjsks","image":"shop.jpg","person_name":"ghjj","email":"xbjx so","mobile":"1234576989","city":"vxnznz","address":"yehsj","gst_no":"fhh","gst_certificate":"gst.jpg","pan_no":"gh","pan_card":"pan.jpg","date_time":"2020-03-02 19:03:56","status":"1"}]
     */

    private StatusBean status;
    private List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : List of Shop Names from Clients
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean {
        /**
         * id : 1
         * shop_name : igrand
         * image : screen-0.jpg
         * person_name : vinay
         * email : vinaynarendrapeeta@gmail.com
         * mobile : 7013332509
         * city : bhimavaram
         * address : bhimavaram
         * gst_no : 13g
         * gst_certificate : Angular_Course_Content.docx
         * pan_no : asdas12
         * pan_card : Python_Course_Content.docx
         * date_time : 2020-02-26 12:16:29
         * status : 1
         */

        private String id;
        private String shop_name;
        private String image;
        private String person_name;
        private String email;
        private String mobile;
        private String city;
        private String address;
        private String gst_no;
        private String gst_certificate;
        private String pan_no;
        private String pan_card;
        private String date_time;
        private String status;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getShop_name() {
            return shop_name;
        }

        public void setShop_name(String shop_name) {
            this.shop_name = shop_name;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getPerson_name() {
            return person_name;
        }

        public void setPerson_name(String person_name) {
            this.person_name = person_name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getGst_no() {
            return gst_no;
        }

        public void setGst_no(String gst_no) {
            this.gst_no = gst_no;
        }

        public String getGst_certificate() {
            return gst_certificate;
        }

        public void setGst_certificate(String gst_certificate) {
            this.gst_certificate = gst_certificate;
        }

        public String getPan_no() {
            return pan_no;
        }

        public void setPan_no(String pan_no) {
            this.pan_no = pan_no;
        }

        public String getPan_card() {
            return pan_card;
        }

        public void setPan_card(String pan_card) {
            this.pan_card = pan_card;
        }

        public String getDate_time() {
            return date_time;
        }

        public void setDate_time(String date_time) {
            this.date_time = date_time;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
    }
}

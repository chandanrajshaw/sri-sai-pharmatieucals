package kashyap.chandan.medishop.Order;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import kashyap.chandan.medishop.R;

public class OrderAllCategoryAdapter extends RecyclerView.Adapter<OrderAllCategoryAdapter.MyViewHolder> {
    Context context;
    List<OrderCategoryListResponse.DataBean> allCategory=new ArrayList<OrderCategoryListResponse.DataBean>();
    TextView catname,catid;
      Dialog catdialog;
    private int SelectedItem = -1;
    String selectedcat,selectedid;
    public OrderAllCategoryAdapter(Context context, List<OrderCategoryListResponse.DataBean> allCategory, TextView catname, TextView catid, Dialog catdialog) {
   this.context=context;
   this.allCategory=allCategory;
   this.catid=catid;
   this.catname=catname;
   this.catdialog=catdialog;
    }

    @NonNull
    @Override
    public OrderAllCategoryAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.itemlayout,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull OrderAllCategoryAdapter.MyViewHolder holder, int position) {
        holder.radioselect.setChecked(position == SelectedItem);
        holder.category.setText(allCategory.get(position).getPc_name());
        TextView head=catdialog.findViewById(R.id.dialoghead);
        head.setText("Select Category");
        TextView ok=catdialog.findViewById(R.id.ok);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                catname.setText(selectedcat);
                if (catname.getText().toString().isEmpty()){
                    Toast.makeText(context,"Select Category",Toast.LENGTH_SHORT).show();
                }
                else {

                    catid.setText(selectedid);
                    catdialog.dismiss();
                }

            }
        });
    }

    @Override
    public int getItemCount() {
        return allCategory.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView category;
        RadioButton radioselect;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            category=itemView.findViewById(R.id.items);
            radioselect=itemView.findViewById(R.id.selectedItem);
            View.OnClickListener clickListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SelectedItem =getAdapterPosition();
                    notifyDataSetChanged();
                    selectedcat=allCategory.get(SelectedItem).getPc_name();
                    selectedid=allCategory.get(SelectedItem).getId();
                }

            };
            itemView.setOnClickListener(clickListener);
            radioselect.setOnClickListener(clickListener);
        }
    }
}

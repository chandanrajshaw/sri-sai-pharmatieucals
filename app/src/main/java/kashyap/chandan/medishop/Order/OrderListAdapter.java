package kashyap.chandan.medishop.Order;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import kashyap.chandan.medishop.R;

public class OrderListAdapter extends RecyclerView.Adapter<OrderListAdapter.MyViewHolder> {
    Context context;
    OrderResponse allorders;
    List<OrderResponse.DataBean> orders=new ArrayList<OrderResponse.DataBean>();
    public OrderListAdapter(Context context,OrderResponse allorders) {
        this.context=context;
        this.allorders=allorders;
        this.orders=allorders.getData();
    }

    @NonNull
    @Override
    public OrderListAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.rec_order_list,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final OrderListAdapter.MyViewHolder holder, int position) {
        holder.orderno.setText(orders.get(position).getUnique_id());
        holder.date.setText(orders.get(position).getDate_time());
        holder.qty.setText(String.valueOf(orders.get(position).getNo_of_items()));
        holder.status.setVisibility(View.GONE);
        holder.shopname.setText(orders.get(position).getShop_name());
        holder.amt.setText(orders.get(position).getTotal());
        holder.detail.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
       int pos=holder.getAdapterPosition();
        Intent intent=new Intent(context,OrderDetail.class);
        Bundle bundle=new Bundle();
        List<OrderResponse.DataBean.DetailsBean> detail=new ArrayList<>();
        detail=orders.get(pos).getDetails();
        bundle.putSerializable("orders", (Serializable) detail);
        intent.putExtra("bundle",bundle);
        intent.putExtra("orderid",orders.get(pos).getUnique_id());
        intent.putExtra("items",orders.get(pos).getNo_of_items());
        intent.putExtra("shopname",orders.get(pos).getShop_name());
        intent.putExtra("date",orders.get(pos).getDate_time());
        intent.putExtra("total",orders.get(pos).getTotal());
        context.startActivity(intent);
    }
});
    }

    @Override
    public int getItemCount() {
        return orders.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        LinearLayout detail,statuslay;
        TextView orderno,qty,amt,date,shopname,status;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            detail=itemView.findViewById(R.id.detail);
            orderno=itemView.findViewById(R.id.orderno);
            qty=itemView.findViewById(R.id.quantity);
            amt=itemView.findViewById(R.id.amount);
            date=itemView.findViewById(R.id.date);
            shopname=itemView.findViewById(R.id.shopname);
            status=itemView.findViewById(R.id.status);
            statuslay=itemView.findViewById(R.id.statuslay);
            statuslay.setVisibility(View.GONE);
        }
    }
}

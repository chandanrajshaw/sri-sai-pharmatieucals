package kashyap.chandan.medishop.Order;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import kashyap.chandan.medishop.R;

public class OrderAllShopAdapter extends RecyclerView.Adapter<OrderAllShopAdapter.MyViewHolder> {
    Context context;
    List<OrderShopListResponse.DataBean> allshops;
    TextView shopname, shopid;
    Dialog shopdialog;
    private int SelectedItem = -1;
    String selectedshop,selectedid;
    public OrderAllShopAdapter(Context context, List<OrderShopListResponse.DataBean> allshops,TextView shopname,TextView shopid, Dialog shopdialog) {
    this.context=context;
    this.allshops=allshops;
    this.shopdialog=shopdialog;
    this.shopid=shopid;
    this.shopname=shopname;
    }

    @NonNull
    @Override
    public OrderAllShopAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.itemlayout,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull OrderAllShopAdapter.MyViewHolder holder, int position) {
        holder.radioselect.setChecked(position == SelectedItem);
        holder.shop.setText(allshops.get(position).getShop_name());
        TextView head=shopdialog.findViewById(R.id.dialoghead);
        head.setText("Select Shop");
        TextView ok=shopdialog.findViewById(R.id.ok);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                shopname.setText(selectedshop);
                if (shopname.getText().toString().isEmpty()){
                    Toast.makeText(context,"Select Shop",Toast.LENGTH_SHORT).show();
                }
                else {

                    shopid.setText(selectedid);
                    shopdialog.dismiss();
                }

            }
        });
    }

    @Override
    public int getItemCount() {
        return allshops.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView shop;
        RadioButton radioselect;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            shop=itemView.findViewById(R.id.items);
            radioselect=itemView.findViewById(R.id.selectedItem);
            View.OnClickListener clickListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SelectedItem =getAdapterPosition();
                    notifyDataSetChanged();
                    selectedshop=allshops.get(SelectedItem).getShop_name();
                    selectedid=allshops.get(SelectedItem).getId();
                }

            };
            itemView.setOnClickListener(clickListener);
            radioselect.setOnClickListener(clickListener);
        }
    }
}

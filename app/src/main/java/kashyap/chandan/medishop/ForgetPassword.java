package kashyap.chandan.medishop;

import androidx.appcompat.app.AppCompatActivity;


import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;


import kashyap.chandan.medishop.pojoclasses.ForgetPasswordResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgetPassword extends AppCompatActivity {
ConnectionDetector detector;
ImageView fgtpwgobackarrow;
TextView getOtp;
TextInputEditText ettemailforget;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        detector=new ConnectionDetector(ForgetPassword.this);
setContentView(R.layout.activity_forget_password);
        fgtpwgobackarrow=findViewById(R.id.fgtpwgobackarrow);
        fgtpwgobackarrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                overridePendingTransition(R.anim.transitionrighttoleft, R.anim.transitionrighttoleft);
                finish();
            }
        });
        ettemailforget=findViewById(R.id.ettemailforget);
        getOtp=findViewById(R.id.getOtp);
        getOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!detector.isConnectingToInternet()){
                    Snackbar.make(ForgetPassword.this.getWindow().getDecorView().findViewById(android.R.id.content), "Make Sure you are connected to Internet", Snackbar.LENGTH_SHORT).show();

                }
                else {
                    if (ettemailforget.getText().toString().trim().isEmpty())
                    {
                        Snackbar.make(ForgetPassword.this.getWindow().getDecorView().findViewById(android.R.id.content), "Please Fill Username Field", Snackbar.LENGTH_SHORT).show();
                    }
                    else {
                        final Dialog progressDialog=new Dialog(ForgetPassword.this);
                        progressDialog.setContentView(R.layout.customdialog);
                        progressDialog.setCancelable(false);
                        progressDialog.show();
                       UserInterface userInterface=ApiClient.getClient().create(UserInterface.class);
                        Call<ForgetPasswordResponse>call=userInterface.getOtp(ettemailforget.getText().toString());
                        call.enqueue(new Callback<ForgetPasswordResponse>() {
                            @Override
                            public void onResponse(Call<ForgetPasswordResponse> call, Response<ForgetPasswordResponse> response) {
                                if (response.code()==200)
                                {
                                    progressDialog.dismiss();
                                   Intent intent=new Intent(ForgetPassword.this,ValidateOTP.class);
                                   startActivity(intent);
                                }
                                else if (response.code()!=200){
                                    progressDialog.dismiss();
                                    Snackbar.make(ForgetPassword.this.getWindow().getDecorView().findViewById(android.R.id.content), "This Email is not Registered", Snackbar.LENGTH_SHORT).show();

                                }
                            }

                            @Override
                            public void onFailure(Call<ForgetPasswordResponse> call, Throwable t) {
progressDialog.dismiss();
                                Snackbar.make(ForgetPassword.this.getWindow().getDecorView().findViewById(android.R.id.content), ""+t.getMessage(), Snackbar.LENGTH_SHORT).show();

                            }
                        });
                    }
                }
            }
        });

    }
}

package kashyap.chandan.medishop;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;
import android.view.WindowManager;

import kashyap.chandan.medishop.admin.DashBoard;
import kashyap.chandan.medishop.admin.SharedPreferencesAdmin;
import kashyap.chandan.medishop.agentPannel.AddShopActivity1;
import kashyap.chandan.medishop.agentPannel.UserDashBoard;

public class SplashScreen extends AppCompatActivity {
    SharedPreferencesAdmin sharedPreferencesAdmin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash_screen);
        sharedPreferencesAdmin=new SharedPreferencesAdmin(SplashScreen.this);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
String username=sharedPreferencesAdmin.getAdminNamePreferences();
String pass=sharedPreferencesAdmin.getAdminPassPreferences();
String id=sharedPreferencesAdmin.getAdminIdPreferences();
if (username!=null && pass!=null && id.equals("1") )
{
    Intent intent=new Intent(SplashScreen.this, DashBoard.class);
    startActivity(intent);
    finish();
}
 if (username!=null && pass!=null && !id.equals("1")){
    Intent intent=new Intent(SplashScreen.this, UserDashBoard.class);
    startActivity(intent);
    finish();
}
 if (username==null && pass==null && id==null)
{
    Intent intent=new Intent(SplashScreen.this, MainActivity.class);
    startActivity(intent);
    finish();
}
            }
        },2000);
    }


}

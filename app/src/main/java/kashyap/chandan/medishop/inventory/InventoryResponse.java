package kashyap.chandan.medishop.inventory;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class InventoryResponse implements Serializable {


    /**
     * status : {"code":200,"message":"Inventory List(Note: Here product_id belongs to product_name, vendor_id belongs to person_name)"}
     * data : [{"unique_id":"SBINV2","total purchase price":"50","date_time":"2020-04-07 15:16:13","vendor_name":"Shankar Medishop","no_of_items":2,"details":[{"id":"4","unique_id":"SBINV2","product_catagories":"1","product_id":"1","vendor_id":"1","date_of_delivery":"30-04-2020","quantity":"15","purchase_price":"25","mrp":"30","expiry_date":"31-05-2020","batch_no":"qwer","status":"1","date_time":"2020-04-07 15:16:13","product_name":"Paracetmol","pc_name":"A Catagory"},{"id":"3","unique_id":"SBINV2","product_catagories":"5","product_id":"3","vendor_id":"1","date_of_delivery":"30-04-2020","quantity":"15","purchase_price":"25","mrp":"25","expiry_date":"31-05-2020","batch_no":"1234","status":"1","date_time":"2020-04-07 15:16:13","product_name":"mps syrup","pc_name":"syrups"}]},{"unique_id":"SBINV1","total purchase price":"50","date_time":"2020-04-07 15:16:01","vendor_name":"Shankar Medishop","no_of_items":2,"details":[{"id":"2","unique_id":"SBINV1","product_catagories":"1","product_id":"1","vendor_id":"1","date_of_delivery":"30-04-2020","quantity":"15","purchase_price":"25","mrp":"30","expiry_date":"31-05-2020","batch_no":"qwer","status":"1","date_time":"2020-04-07 15:16:01","product_name":"Paracetmol","pc_name":"A Catagory"},{"id":"1","unique_id":"SBINV1","product_catagories":"5","product_id":"3","vendor_id":"1","date_of_delivery":"30-04-2020","quantity":"15","purchase_price":"25","mrp":"25","expiry_date":"31-05-2020","batch_no":"1234","status":"1","date_time":"2020-04-07 15:16:00","product_name":"mps syrup","pc_name":"syrups"}]}]
     */

    private StatusBean status;
    private List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean implements Serializable {
        /**
         * code : 200
         * message : Inventory List(Note: Here product_id belongs to product_name, vendor_id belongs to person_name)
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean implements Serializable {
        /**
         * unique_id : SBINV2
         * total purchase price : 50
         * date_time : 2020-04-07 15:16:13
         * vendor_name : Shankar Medishop
         * no_of_items : 2
         * details : [{"id":"4","unique_id":"SBINV2","product_catagories":"1","product_id":"1","vendor_id":"1","date_of_delivery":"30-04-2020","quantity":"15","purchase_price":"25","mrp":"30","expiry_date":"31-05-2020","batch_no":"qwer","status":"1","date_time":"2020-04-07 15:16:13","product_name":"Paracetmol","pc_name":"A Catagory"},{"id":"3","unique_id":"SBINV2","product_catagories":"5","product_id":"3","vendor_id":"1","date_of_delivery":"30-04-2020","quantity":"15","purchase_price":"25","mrp":"25","expiry_date":"31-05-2020","batch_no":"1234","status":"1","date_time":"2020-04-07 15:16:13","product_name":"mps syrup","pc_name":"syrups"}]
         */

        private String unique_id;
        @SerializedName("total purchase price")
        private String _$TotalPurchasePrice23; // FIXME check this code
        private String date_time;
        private String vendor_name;
        private int no_of_items;
        private List<DetailsBean> details;

        public String getUnique_id() {
            return unique_id;
        }

        public void setUnique_id(String unique_id) {
            this.unique_id = unique_id;
        }

        public String get_$TotalPurchasePrice23() {
            return _$TotalPurchasePrice23;
        }

        public void set_$TotalPurchasePrice23(String _$TotalPurchasePrice23) {
            this._$TotalPurchasePrice23 = _$TotalPurchasePrice23;
        }

        public String getDate_time() {
            return date_time;
        }

        public void setDate_time(String date_time) {
            this.date_time = date_time;
        }

        public String getVendor_name() {
            return vendor_name;
        }

        public void setVendor_name(String vendor_name) {
            this.vendor_name = vendor_name;
        }

        public int getNo_of_items() {
            return no_of_items;
        }

        public void setNo_of_items(int no_of_items) {
            this.no_of_items = no_of_items;
        }

        public List<DetailsBean> getDetails() {
            return details;
        }

        public void setDetails(List<DetailsBean> details) {
            this.details = details;
        }

        public static class DetailsBean implements Serializable{
            /**
             * id : 4
             * unique_id : SBINV2
             * product_catagories : 1
             * product_id : 1
             * vendor_id : 1
             * date_of_delivery : 30-04-2020
             * quantity : 15
             * purchase_price : 25
             * mrp : 30
             * expiry_date : 31-05-2020
             * batch_no : qwer
             * status : 1
             * date_time : 2020-04-07 15:16:13
             * product_name : Paracetmol
             * pc_name : A Catagory
             */

            private String id;
            private String unique_id;
            private String product_catagories;
            private String product_id;
            private String vendor_id;
            private String date_of_delivery;
            private String quantity;
            private String purchase_price;
            private String mrp;
            private String expiry_date;
            private String batch_no;
            private String status;
            private String date_time;
            private String product_name;
            private String pc_name;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getUnique_id() {
                return unique_id;
            }

            public void setUnique_id(String unique_id) {
                this.unique_id = unique_id;
            }

            public String getProduct_catagories() {
                return product_catagories;
            }

            public void setProduct_catagories(String product_catagories) {
                this.product_catagories = product_catagories;
            }

            public String getProduct_id() {
                return product_id;
            }

            public void setProduct_id(String product_id) {
                this.product_id = product_id;
            }

            public String getVendor_id() {
                return vendor_id;
            }

            public void setVendor_id(String vendor_id) {
                this.vendor_id = vendor_id;
            }

            public String getDate_of_delivery() {
                return date_of_delivery;
            }

            public void setDate_of_delivery(String date_of_delivery) {
                this.date_of_delivery = date_of_delivery;
            }

            public String getQuantity() {
                return quantity;
            }

            public void setQuantity(String quantity) {
                this.quantity = quantity;
            }

            public String getPurchase_price() {
                return purchase_price;
            }

            public void setPurchase_price(String purchase_price) {
                this.purchase_price = purchase_price;
            }

            public String getMrp() {
                return mrp;
            }

            public void setMrp(String mrp) {
                this.mrp = mrp;
            }

            public String getExpiry_date() {
                return expiry_date;
            }

            public void setExpiry_date(String expiry_date) {
                this.expiry_date = expiry_date;
            }

            public String getBatch_no() {
                return batch_no;
            }

            public void setBatch_no(String batch_no) {
                this.batch_no = batch_no;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public String getDate_time() {
                return date_time;
            }

            public void setDate_time(String date_time) {
                this.date_time = date_time;
            }

            public String getProduct_name() {
                return product_name;
            }

            public void setProduct_name(String product_name) {
                this.product_name = product_name;
            }

            public String getPc_name() {
                return pc_name;
            }

            public void setPc_name(String pc_name) {
                this.pc_name = pc_name;
            }
        }
    }
}

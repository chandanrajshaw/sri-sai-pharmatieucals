package kashyap.chandan.medishop.inventory;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import kashyap.chandan.medishop.ConnectionDetector;
import kashyap.chandan.medishop.R;
import kashyap.chandan.medishop.product.AllProductCategoryResponse;

public class AddInventories extends AppCompatActivity {
    TextView toolheader;
    RelativeLayout productdrop,vendorlay, dateofdelivery, expdatelay, categorydrop;
    TextView productname, prodictid, vendorname, vendorid, submit, addmore, delDate, expDate, catName, catId;
    ConnectionDetector connectionDetector;
    ImageView goback;
    RecyclerView productrec, vendorrec, categoryRecycler;
    EditText qty, batch;
    int itemCount ;
    String[] cat = new String[10];
    String[] product = new String[10];
    String[] qunty = new String[10];
    String[] bno = new String[10];
    String[] mprice = new String[10];
    String[] pprice = new String[10];
    String[] ddate = new String[10];
    String[] edate = new String[10];
    TextInputEditText qnty, purchase, mrp, batchNo;
    Calendar calendar = Calendar.getInstance();
    List<AllProductCategoryResponse.DataBean> allcategory = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_add_inventories);
        toolheader = findViewById(R.id.customtoolheader);
        toolheader.setText("Add Inventory");
        vendorlay = findViewById(R.id.vendor_lay);
        vendorlay.setEnabled(true);
        itemCount=0;
        submit = findViewById(R.id.Submit);
        connectionDetector = new ConnectionDetector(AddInventories.this);
        addmore = findViewById(R.id.moreItem);
        vendorname = findViewById(R.id.vendorname);
        vendorid = findViewById(R.id.vendorid);
//        prodictid = findViewById(R.id.productid);
        goback = findViewById(R.id.ordergobackarrow);
        goback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                overridePendingTransition(R.anim.transitionrighttoleft, R.anim.transitionrighttoleft);
                finish();
            }
        });
        vendorlay=findViewById(R.id.vendor_lay);
       vendorlay.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               if (!connectionDetector.isConnectingToInternet()){
                   Toast.makeText(AddInventories.this, "Internet Not Available", Toast.LENGTH_SHORT).show();
               }
               else if (connectionDetector.isConnectingToInternet()){
                   Toast.makeText(AddInventories.this, "Vendor DropDown Clicked", Toast.LENGTH_SHORT).show();
               }
           }
       });
    }
}

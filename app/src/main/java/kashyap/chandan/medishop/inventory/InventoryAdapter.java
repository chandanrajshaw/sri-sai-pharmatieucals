package kashyap.chandan.medishop.inventory;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import kashyap.chandan.medishop.R;

class InventoryAdapter extends RecyclerView.Adapter<InventoryAdapter.MyViewHolder> {
    Context context;
    InventoryResponse inventory;
    List<InventoryResponse.DataBean> inventorylist=new ArrayList<InventoryResponse.DataBean>() ;
    public InventoryAdapter(Context context,InventoryResponse inventory) {
        this.context=context;
       this.inventory=inventory;
       this.inventorylist=inventory.getData();
    }

    @NonNull
    @Override
    public InventoryAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.recinventory,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final InventoryAdapter.MyViewHolder holder, int position) {
holder.pname.setText(inventorylist.get(position).getUnique_id());
holder.vname.setText(inventorylist.get(position).getVendor_name());
holder.total.setText(inventorylist.get(position).get_$TotalPurchasePrice23());
holder.dateofdel.setText(inventorylist.get(position).getDate_time());
holder.quantity.setText(String.valueOf(inventorylist.get(position).getNo_of_items()));
holder.edit.setVisibility(View.GONE);
holder.detail.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        int pos=holder.getAdapterPosition();
        List<InventoryResponse.DataBean.DetailsBean> inventory=new ArrayList<>();
        inventory=inventorylist.get(pos).getDetails();
        Intent intent=new Intent(context,InventoryDetails.class);
        Bundle bundle=new Bundle();
        bundle.putSerializable("inventory", (Serializable) inventory);
        intent.putExtra("bundle",bundle);
        intent.putExtra("id",inventorylist.get(pos).getUnique_id());
        intent.putExtra("date",inventorylist.get(pos).getDate_time());
        intent.putExtra("total",inventorylist.get(pos).get_$TotalPurchasePrice23());
        intent.putExtra("qnty",String.valueOf(inventorylist.get(pos).getNo_of_items()));
        intent.putExtra("vname",inventorylist.get(pos).getVendor_name());
        context.startActivity(intent);
    }
});
    }

    @Override
    public int getItemCount() {
        return inventorylist.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView pname,vname,quantity,total,dateofdel;
        ImageView edit;
        LinearLayout detail;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            pname=itemView.findViewById(R.id.iproductname);
            vname=itemView.findViewById(R.id.ivendorname);
            quantity=itemView.findViewById(R.id.iquantity);
        total=itemView.findViewById(R.id.itotal);
        dateofdel=itemView.findViewById(R.id.datetime);
        edit=itemView.findViewById(R.id.edit);
        detail=itemView.findViewById(R.id.detail);

        }
    }
}

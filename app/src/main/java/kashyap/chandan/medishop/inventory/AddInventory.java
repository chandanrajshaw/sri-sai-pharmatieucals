package kashyap.chandan.medishop.inventory;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import kashyap.chandan.medishop.ApiClient;
import kashyap.chandan.medishop.ConnectionDetector;
import kashyap.chandan.medishop.Order.OrderAllCategoryAdapter;
import kashyap.chandan.medishop.Order.OrderAllProductAdapter;
import kashyap.chandan.medishop.Order.OrderCategoryListResponse;
import kashyap.chandan.medishop.Order.OrderProductListResponse;
import kashyap.chandan.medishop.R;
import kashyap.chandan.medishop.UserInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddInventory extends AppCompatActivity {
    TextView toolheader;
    RelativeLayout productdrop,vendorlay, dateofdelivery, expdatelay, categorydrop;
    TextView productname, prodictid, vendorname,vendorid, submit, addmore, delDate, expDate, catName, catId;
    ConnectionDetector connectionDetector;
    ImageView goback;
    RecyclerView productrec, vendorrec, categoryRecycler;
    EditText qty, batch;
    int itemCount=0 ;
    String[] cat = new String[10];
    String[] product = new String[10];
    String[] qunty = new String[10];
    String[] bno = new String[10];
    String[] mprice = new String[10];
    String[] pprice = new String[10];
    String[] ddate = new String[10];
    String[] edate = new String[10];
    TextInputEditText qnty, purchase, mrp, batchNo;
    Calendar calendar = Calendar.getInstance();
    List<OrderCategoryListResponse.DataBean> allcategory = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_add_inventory);
        toolheader = findViewById(R.id.customtoolheader);
        toolheader.setText("Add Inventory");
        vendorlay = findViewById(R.id.vendor_lay);
        vendorlay.setEnabled(true);

        submit = findViewById(R.id.Submit);
        connectionDetector = new ConnectionDetector(AddInventory.this);
        addmore = findViewById(R.id.moreItem);
//        prodictid = findViewById(R.id.productid);
        goback = findViewById(R.id.ordergobackarrow);
        goback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                overridePendingTransition(R.anim.transitionrighttoleft, R.anim.transitionrighttoleft);
                finish();
            }
        });

        vendorname = findViewById(R.id.vendorname);
        vendorid = findViewById(R.id.vendorid);
        vendorlay.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View view) {
              Toast.makeText(AddInventory.this, "Vendors", Toast.LENGTH_SHORT).show();
                  final Dialog progress=new Dialog(AddInventory.this);
                  progress.setContentView(R.layout.customdialog);
                  progress.setCancelable(false);
                  progress.show();
                  UserInterface userInterface=ApiClient.getClient().create(UserInterface.class);
                  Call<InventoryVendorListResponse>call=userInterface.getInventoryVendorList();
                  call.enqueue(new Callback<InventoryVendorListResponse>() {
                      @Override
                      public void onResponse(Call<InventoryVendorListResponse> call, Response<InventoryVendorListResponse> response) {
                          if (response.code() == 200) {
                              progress.dismiss();
                              List<InventoryVendorListResponse.DataBean> allvendors = new ArrayList<InventoryVendorListResponse.DataBean>();
                              allvendors = response.body().getData();
                              Dialog vendorsdialog = new Dialog(AddInventory.this);
                              vendorsdialog.setContentView(R.layout.recyclerdialog);
                              vendorsdialog.setCancelable(false);
                              DisplayMetrics metrics = getResources().getDisplayMetrics();
                              int width = metrics.widthPixels;
                              vendorsdialog.getWindow().setLayout(width, LinearLayout.LayoutParams.MATCH_PARENT);
                              vendorrec = vendorsdialog.findViewById(R.id.dialogRecycler);
                              vendorrec.setLayoutManager(new LinearLayoutManager(AddInventory.this, LinearLayoutManager.VERTICAL, false));
                              vendorrec.setAdapter(new VendorDropDownAdapter(AddInventory.this, allvendors, vendorname, vendorid, vendorsdialog));
                              vendorsdialog.show();

                          }
                          else if (response.code() != 200) {
                              progress.dismiss();
                              Snackbar.make(AddInventory.this.getWindow().getDecorView().findViewById(android.R.id.content), "Some Error Occurs!!try again", Snackbar.LENGTH_SHORT).show();

                          }
                      }

                      @Override
                      public void onFailure(Call<InventoryVendorListResponse> call, Throwable t) {
                          Snackbar.make(AddInventory.this.getWindow().getDecorView().findViewById(android.R.id.content), "Some Error Occurs!!try again", Snackbar.LENGTH_SHORT).show();

                      }
                  });

          }
      });

        addmore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (vendorname.getText().toString().equalsIgnoreCase("Vendor")) {
                    Toast.makeText(AddInventory.this, "Select Vendor First", Toast.LENGTH_SHORT).show();
                }
                else {

                    final Dialog addInventory = new Dialog(AddInventory.this);
                    addInventory.setContentView(R.layout.inventorydialog);
                    DisplayMetrics metrics = getResources().getDisplayMetrics();
                    int width = metrics.widthPixels;
                    addInventory.getWindow().setLayout(width, LinearLayout.LayoutParams.MATCH_PARENT);
                    addInventory.show();
                    catName = addInventory.findViewById(R.id.categoryname);
                    catId = addInventory.findViewById(R.id.catid);
                    categorydrop = addInventory.findViewById(R.id.categorydrop);
                    productdrop = addInventory.findViewById(R.id.productdrop);
                    productname = addInventory.findViewById(R.id.productname);
                    prodictid = addInventory.findViewById(R.id.productid);
                    categorydrop.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            final Dialog progressDialog = new Dialog(AddInventory.this);
                            progressDialog.setContentView(R.layout.customdialog);
                            progressDialog.setCancelable(false);
                            progressDialog.show();
                            UserInterface userInterface = ApiClient.getClient().create(UserInterface.class);
                            Call<OrderCategoryListResponse> call = userInterface.getOrderCategoryList();
                            call.enqueue(new Callback<OrderCategoryListResponse>() {
                                @Override
                                public void onResponse(Call<OrderCategoryListResponse> call, Response<OrderCategoryListResponse> response) {

                                    if (response.code() == 200) {
                                        allcategory = response.body().getData();
                                        final Dialog categorydialog = new Dialog(AddInventory.this);
                                        categorydialog.setContentView(R.layout.recyclerdialog);
                                        DisplayMetrics metrics = getResources().getDisplayMetrics();
                                        int width = metrics.widthPixels;
                                        categorydialog.getWindow().setLayout(width, LinearLayout.LayoutParams.WRAP_CONTENT);
                                        categoryRecycler = categorydialog.findViewById(R.id.dialogRecycler);
                                        categoryRecycler.setLayoutManager(new LinearLayoutManager(AddInventory.this, LinearLayoutManager.VERTICAL, false));
                                        categoryRecycler.setAdapter(new OrderAllCategoryAdapter(AddInventory.this, allcategory, catName,catId,categorydialog));
                                        progressDialog.dismiss();
                                        categorydialog.show();
                                    } else if (response.code() != 200) {
                                        progressDialog.dismiss();
                                        Toast.makeText(AddInventory.this, "Product Category Not Found", Toast.LENGTH_SHORT).show();
                                    }

                                }

                                @Override
                                public void onFailure(Call<OrderCategoryListResponse> call, Throwable t) {

                                }
                            });
                        }
                    });
                    productdrop.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (!connectionDetector.isConnectingToInternet()) {
                                Toast.makeText(AddInventory.this, "Pleasr Connect to Internet", Toast.LENGTH_SHORT).show();
                            } else if (connectionDetector.isConnectingToInternet()) {
                                final Dialog progress = new Dialog(AddInventory.this);
                                progress.setContentView(R.layout.customdialog);
                                progress.setCancelable(false);
                                progress.show();
                                UserInterface userInterface = ApiClient.getClient().create(UserInterface.class);
                                Call<OrderProductListResponse> call = userInterface.orderProductList(String.valueOf(catId.getText().toString().trim()));
                                call.enqueue(new Callback<OrderProductListResponse>() {
                                    @Override
                                    public void onResponse(Call<OrderProductListResponse> call, Response<OrderProductListResponse> response) {
                                        if (response.code() == 200) {
                                            progress.dismiss();
                                            List<OrderProductListResponse.DataBean> allProduct = new ArrayList<OrderProductListResponse.DataBean>();
                                            allProduct = response.body().getData();
                                            Dialog productdialog = new Dialog(AddInventory.this);
                                            productdialog.setContentView(R.layout.recyclerdialog);
                                            productdialog.setCancelable(false);
                                            DisplayMetrics metrics = getResources().getDisplayMetrics();
                                            int width = metrics.widthPixels;
                                            productdialog.getWindow().setLayout(width, LinearLayout.LayoutParams.MATCH_PARENT);
                                            productrec = productdialog.findViewById(R.id.dialogRecycler);
                                            productrec.setLayoutManager(new LinearLayoutManager(AddInventory.this, LinearLayoutManager.VERTICAL, false));
                                            productrec.setAdapter(new OrderAllProductAdapter(AddInventory.this, allProduct, productname, prodictid, productdialog));
                                            productdialog.show();
                                        } else if (response.code() != 200) {
                                            progress.dismiss();
                                            Toast.makeText(AddInventory.this, "No Products Of this Category", Toast.LENGTH_SHORT).show();
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<OrderProductListResponse> call, Throwable t) {

                                    }
                                });
                            }
                        }
                    });
                    qty = addInventory.findViewById(R.id.etqty);
                    qty.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                        @Override
                        public void onFocusChange(View view, boolean b) {
                            if (b)
                                qty.setHint("");
                            else qty.setHint("Quantity");
                        }
                    });


                    expdatelay = addInventory.findViewById(R.id.expdatelay);
                    expDate = addInventory.findViewById(R.id.expDate);
                    final DatePickerDialog.OnDateSetListener date1 = new DatePickerDialog.OnDateSetListener() {

                        @Override
                        public void onDateSet(DatePicker view, int year, int monthOfYear,
                                              int dayOfMonth) {
                            // TODO Auto-generated method stub
                            calendar.set(Calendar.YEAR, year);
                            calendar.set(Calendar.MONTH, monthOfYear);
                            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                            String myFormat = "dd-MM-yyyy"; //In which you need put here
                            SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                            expDate.setText(sdf.format(calendar.getTime()));
                        }
                    };
                    expdatelay.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            new DatePickerDialog(AddInventory.this, R.style.TimePickerTheme, date1, calendar
                                    .get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                                    calendar.get(Calendar.DAY_OF_MONTH)).show();
                        }
                    });
                    final TextView add = addInventory.findViewById(R.id.Submit);
                    purchase = addInventory.findViewById(R.id.etpurchase);
                    batchNo = addInventory.findViewById(R.id.etbatch);
mrp=addInventory.findViewById(R.id.etmrp);
purchase=addInventory.findViewById(R.id.etpurchase);

                    add.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            String catid = catId.getText().toString().trim();
                            String productid = prodictid.getText().toString().trim();
                            String qnty = qty.getText().toString().trim();
                            String batchno=batchNo.getText().toString().trim();
                            String purchaseprice = purchase.getText().toString().trim();
                            String mrpprice = mrp.getText().toString().trim();
                            String exp = expDate.getText().toString().trim();
                            if (catid.equalsIgnoreCase("") && productid.equalsIgnoreCase("") && qnty.isEmpty() && purchaseprice.isEmpty() && mrpprice.isEmpty()&&batchno.isEmpty()
                                   && exp.isEmpty())
                                Toast.makeText(AddInventory.this, "Enter All the Fields", Toast.LENGTH_SHORT).show();
                            else if (catid.equalsIgnoreCase(""))
                                Toast.makeText(AddInventory.this, "Select Category", Toast.LENGTH_SHORT).show();
                            else if (productid.equalsIgnoreCase(""))
                                Toast.makeText(AddInventory.this, "Select Product", Toast.LENGTH_SHORT).show();
                            else if (qnty.isEmpty())
                                Toast.makeText(AddInventory.this, "Enter Quantity", Toast.LENGTH_SHORT).show();
                            else if (purchaseprice.isEmpty())
                                Toast.makeText(AddInventory.this, "Enter Purchase Price", Toast.LENGTH_SHORT).show();
                            else if (mrpprice.isEmpty())
                                Toast.makeText(AddInventory.this, "Enter MRP", Toast.LENGTH_SHORT).show();
                            else if (batchno.isEmpty())
                                Toast.makeText(AddInventory.this, "Enter Batch No", Toast.LENGTH_SHORT).show();
                            else if (exp.isEmpty())
                                Toast.makeText(AddInventory.this, "Select Expiry Date", Toast.LENGTH_SHORT).show();
                            else {
                                itemCount++;
                                if (itemCount!=0) {
                                    submit.setVisibility(View.VISIBLE);
                                    vendorlay.setEnabled(false);
                                }

                                if (itemCount == 1) addHeaders();
                                if (itemCount == 10) {
                                    addmore.setVisibility(View.GONE);
                                }
                                cat[itemCount - 1] = catid;
                                product[itemCount - 1] = productid;
                                qunty[itemCount - 1] = qnty;
                                pprice[itemCount - 1] = purchaseprice;
                                mprice[itemCount - 1] = mrpprice;
                                edate[itemCount - 1] = exp;
                                bno[itemCount-1]=batchno;
                                addData(String.valueOf(itemCount), catName.getText().toString(), productname.getText().toString(), qnty, purchaseprice, mrpprice, exp,batchno);
                           addInventory.dismiss();
                            }
                        }
                    });
                }
            }
        });


        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String date=delDate.getText().toString().trim();
                if (date.equalsIgnoreCase("Date Of Delivery"))
                {
                    Toast.makeText(AddInventory.this, "Select Delevery Date", Toast.LENGTH_SHORT).show();
                }
//               String vendor_id=vendorid.getText().toString().trim();
//                if ( vendor_id.isEmpty()&& cat.length==0 && product.length==0 && qunty.length==0 && bno.length==0&&pprice.length==0&&mprice.length==0&&edate.length==0)
//                    Snackbar.make(AddInventory.this.getWindow().getDecorView().findViewById(android.R.id.content), "You have No Order", Snackbar.LENGTH_SHORT).show();
//                else if (pid.isEmpty())
//                    Snackbar.make(AddInventory.this.getWindow().getDecorView().findViewById(android.R.id.content), "Select Product", Snackbar.LENGTH_SHORT).show();
//                else if (vid.isEmpty())
//                    Snackbar.make(AddInventory.this.getWindow().getDecorView().findViewById(android.R.id.content), "Select Vendor", Snackbar.LENGTH_SHORT).show();
//                else if (qnty.isEmpty())
//                    Snackbar.make(AddInventory.this.getWindow().getDecorView().findViewById(android.R.id.content), "Enter Quantity", Snackbar.LENGTH_SHORT).show();
//                else if (batchno.isEmpty())
//                    Snackbar.make(AddInventory.this.getWindow().getDecorView().findViewById(android.R.id.content), "Enter Batch No.", Snackbar.LENGTH_SHORT).show();
//
//                else if (deliver.isEmpty() || deliver.equalsIgnoreCase("Date Of Delivery"))
//                    Snackbar.make(AddInventory.this.getWindow().getDecorView().findViewById(android.R.id.content), "Select Delivery Date", Snackbar.LENGTH_SHORT).show();
//
              else
                {
                    if (!connectionDetector.isConnectingToInternet())
                        Snackbar.make(AddInventory.this.getWindow().getDecorView().findViewById(android.R.id.content), "Please Connect to internet and Try Again", Snackbar.LENGTH_SHORT).show();
                    else if (connectionDetector.isConnectingToInternet()) {

                        final Dialog progress = new Dialog(AddInventory.this);
                        progress.setContentView(R.layout.customdialog);
                        progress.setCancelable(false);
                        progress.show();
                        UserInterface userInterface = ApiClient.getClient().create(UserInterface.class);
                        Call<AddInventoryResponse> call = userInterface.addInventory(vendorid.getText().toString().trim(),delDate.getText().toString(),cat,product,qunty, pprice,mprice,edate,bno);
                        call.enqueue(new Callback<AddInventoryResponse>() {
                            @Override
                            public void onResponse(Call<AddInventoryResponse> call, Response<AddInventoryResponse> response) {
                                if (response.code() == 200) {
                                    progress.dismiss();
                                    Toast.makeText(AddInventory.this, "Inventory Added", Toast.LENGTH_SHORT).show();
                                    Intent intent = new Intent(AddInventory.this, Inventory.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    overridePendingTransition(R.anim.transitionrighttoleft, R.anim.transitionrighttoleft);
                                    startActivity(intent);
                                    finish();

                                }
                                if (response.code() != 200) {
                                    progress.dismiss();
                                    Snackbar.make(AddInventory.this.getWindow().getDecorView().findViewById(android.R.id.content), "Inventory Not Added", Snackbar.LENGTH_SHORT).show();

                                }
                            }

                            @Override
                            public void onFailure(Call<AddInventoryResponse> call, Throwable t) {
                                progress.dismiss();
                                Snackbar.make(AddInventory.this.getWindow().getDecorView().findViewById(android.R.id.content), "" + t.getMessage(), Snackbar.LENGTH_SHORT).show();
                            }
                        });

                    }
                }


            }
        });
        dateofdelivery = findViewById(R.id.datepicker);
        delDate = findViewById(R.id.delDate);
        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, monthOfYear);
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                String myFormat = "dd-MM-yyyy"; //In which you need put here
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                delDate.setText(sdf.format(calendar.getTime()));
            }
        };
        dateofdelivery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(AddInventory.this, R.style.TimePickerTheme, date, calendar
                        .get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

    }
    private TextView getTextView ( int id, String title,int color, int typeface, int bgColor)
    {
        TextView tv = new TextView(AddInventory.this);
        tv.setId(id);
        tv.setText(title.toUpperCase());
        tv.setTextColor(color);
        tv.setPadding(5, 5, 5, 5);
        tv.setTypeface(Typeface.DEFAULT, typeface);
        tv.setBackgroundColor(bgColor);
        tv.setLayoutParams(getLayoutParams());

        return tv;
    }
    @NonNull
    private TableLayout.LayoutParams getTblLayoutParams () {
        return new TableLayout.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT);
    }
    private TableRow.LayoutParams getLayoutParams ()
    {
        TableRow.LayoutParams params = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT);
        params.setMargins(2, 0, 0, 2);
        return params;
    }
    public void addData(String sno,String category,String product,String quantity,String purchase,String mrp,String expiry,String batch) {
        // int numCompanies = companies.length;
        TableLayout tl = findViewById(R.id.table);
        TableRow tr = new TableRow(this);
        tr.setLayoutParams(getLayoutParams());

        tr.addView(getTextView( 1,String.valueOf(sno), ContextCompat.getColor(this, R.color.background_color), Typeface.NORMAL,  ContextCompat.getColor(this, R.color.textcolor)));
        tr.addView(getTextView(1, category,ContextCompat.getColor(this, R.color.background_color), Typeface.NORMAL,  ContextCompat.getColor(this, R.color.textcolor)));
        tr.addView(getTextView(1,product, ContextCompat.getColor(this, R.color.background_color), Typeface.NORMAL,  ContextCompat.getColor(this, R.color.textcolor)));
        tr.addView(getTextView(1, quantity, ContextCompat.getColor(this, R.color.background_color), Typeface.NORMAL, ContextCompat.getColor(this, R.color.textcolor)));
        tr.addView(getTextView(1, purchase, ContextCompat.getColor(this, R.color.background_color), Typeface.NORMAL, ContextCompat.getColor(this, R.color.textcolor)));
        tr.addView(getTextView(1, mrp, ContextCompat.getColor(this, R.color.background_color), Typeface.NORMAL, ContextCompat.getColor(this, R.color.textcolor)));
        tr.addView(getTextView(1, batch, ContextCompat.getColor(this, R.color.background_color), Typeface.NORMAL, ContextCompat.getColor(this, R.color.textcolor)));
        tr.addView(getTextView(1, expiry, ContextCompat.getColor(this, R.color.background_color), Typeface.NORMAL, ContextCompat.getColor(this, R.color.textcolor)));

        tl.addView(tr, getTblLayoutParams());


    }
    public void addHeaders() {
        TableLayout tl = findViewById(R.id.table);
        TableRow tr = new TableRow(this);
        tr.setLayoutParams(getLayoutParams());
        tr.addView(getTextView(0, "Sl.No", ContextCompat.getColor(this, R.color.textcolor), Typeface.BOLD, ContextCompat.getColor(this, R.color.background_color)));
        tr.addView(getTextView(0, "Category",  ContextCompat.getColor(this, R.color.textcolor), Typeface.BOLD,ContextCompat.getColor(this, R.color.background_color)));
        tr.addView(getTextView(0, "Product", ContextCompat.getColor(this, R.color.textcolor), Typeface.BOLD,ContextCompat.getColor(this, R.color.background_color)));
        tr.addView(getTextView(0, "Quantity", ContextCompat.getColor(this, R.color.textcolor), Typeface.BOLD,ContextCompat.getColor(this, R.color.background_color)));
        tr.addView(getTextView(0, "Purchase", ContextCompat.getColor(this, R.color.textcolor), Typeface.BOLD,ContextCompat.getColor(this, R.color.background_color)));
        tr.addView(getTextView(0, "MRP", ContextCompat.getColor(this, R.color.textcolor), Typeface.BOLD,ContextCompat.getColor(this, R.color.background_color)));
        tr.addView(getTextView(0, "Batch No", ContextCompat.getColor(this, R.color.textcolor), Typeface.BOLD,ContextCompat.getColor(this, R.color.background_color)));
        tr.addView(getTextView(0, "Expiry", ContextCompat.getColor(this, R.color.textcolor), Typeface.BOLD,ContextCompat.getColor(this, R.color.background_color)));

        tl.addView(tr, getTblLayoutParams());
    }
}

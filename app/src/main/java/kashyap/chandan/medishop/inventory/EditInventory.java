package kashyap.chandan.medishop.inventory;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import kashyap.chandan.medishop.ApiClient;
import kashyap.chandan.medishop.ConnectionDetector;
import kashyap.chandan.medishop.R;
import kashyap.chandan.medishop.UserInterface;
import kashyap.chandan.medishop.product.AllProductResponse;
import kashyap.chandan.medishop.vendor.AllVendorResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditInventory extends AppCompatActivity {
TextView toolheader,deliveryDate;
RelativeLayout productdrop,vendordrop,dateofdelivery;
TextView productname,prodictid,vendorname,vendorid,submit;
ConnectionDetector connectionDetector;
ImageView goback;
RecyclerView productrec,vendorrec;
EditText qty,batch;
    Calendar calendar=Calendar.getInstance();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_add_inventory);
//        Intent i=getIntent();
//        Bundle bundle=i.getBundleExtra("bundle");
//        final InventoryResponse.DataBean inventory= (InventoryResponse.DataBean) bundle.getSerializable("inventory");
//        toolheader=findViewById(R.id.customtoolheader);
//        toolheader.setText("Edit Inventory");
//        productdrop=findViewById(R.id.productdrop);
//        connectionDetector=new ConnectionDetector(EditInventory.this);
//        productname=findViewById(R.id.selproduct);
//        productname.setText(inventory.getProduct_name());
//        prodictid=findViewById(R.id.productid);
//        prodictid.setText(inventory.getProduct_id());
//        goback=findViewById(R.id.ordergobackarrow);
//        goback.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                overridePendingTransition(R.anim.transitionrighttoleft,R.anim.transitionrighttoleft);
//                finish();
//            }
//        });
//
//batch.setText(inventory.getBatch_no());
//        productdrop.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//               if (!connectionDetector.isConnectingToInternet())
//                   Snackbar.make(EditInventory.this.getWindow().getDecorView().findViewById(android.R.id.content),"Plzz Connect to internet and try again",Snackbar.LENGTH_SHORT).show();
//else if (connectionDetector.isConnectingToInternet())
//               {
//                   final Dialog progress=new Dialog(EditInventory.this);
//                   progress.setContentView(R.layout.customdialog);
//                   progress.setCancelable(false);
//                   progress.show();
//                   UserInterface userInterface= ApiClient.getClient().create(UserInterface.class);
//                   Call<InventoryProductListResponse> call=userInterface.getInventoryProductList();
//                   call.enqueue(new Callback<InventoryProductListResponse>() {
//                       @Override
//                       public void onResponse(Call<InventoryProductListResponse> call, Response<InventoryProductListResponse> response) {
//                           if (response.code()==200)
//                           {
//                               progress.dismiss();
//                               List<InventoryProductListResponse.DataBean> allProduct=new ArrayList<InventoryProductListResponse.DataBean>();
//                               allProduct=response.body().getData();
//                               Dialog productdialog=new Dialog(EditInventory.this);
//                               productdialog.setContentView(R.layout.recyclerdialog);
//                               productdialog.setCancelable(false);
//                               DisplayMetrics metrics=getResources().getDisplayMetrics();
//                               int width=metrics.widthPixels;
//                               productdialog.getWindow().setLayout(width, LinearLayout.LayoutParams.MATCH_PARENT);
//                               productrec=productdialog.findViewById(R.id.cityRecycler);
//                               productrec.setLayoutManager(new LinearLayoutManager(EditInventory.this,LinearLayoutManager.VERTICAL,false));
//                               productrec.setAdapter(new ProductDropDownAdapter(EditInventory.this,allProduct,productname,prodictid,productdialog));
//                               productdialog.show();
//
//                           }
//                           else if (response.code()!=200)
//                           {
//                               Snackbar.make(EditInventory.this.getWindow().getDecorView().findViewById(android.R.id.content),"No Products available at that time",Snackbar.LENGTH_SHORT).show();
//
//                           }
//                       }
//
//                       @Override
//                       public void onFailure(Call<InventoryProductListResponse> call, Throwable t) {
//                           Snackbar.make(EditInventory.this.getWindow().getDecorView().findViewById(android.R.id.content),""+t.getMessage(),Snackbar.LENGTH_SHORT).show();
//
//                       }
//                   });
//               }
//            }
//        });
//        vendordrop=findViewById(R.id.vendor);
//        vendorname=findViewById(R.id.vendorname);
//        vendorname.setText(inventory.getPerson_name());
//        vendorid=findViewById(R.id.vendorid);
//        vendorid.setText(inventory.getVendor_id());
//        vendordrop.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (!connectionDetector.isConnectingToInternet()){
//                    Snackbar.make(EditInventory.this.getWindow().getDecorView().findViewById(android.R.id.content),"Plzz Connect to internet and try again",Snackbar.LENGTH_SHORT).show();
//
//                }
//                else if (connectionDetector.isConnectingToInternet()){
//                    final Dialog progress=new Dialog(EditInventory.this);
//                    progress.setContentView(R.layout.customdialog);
//                    progress.setCancelable(false);
//                    progress.show();
//                    UserInterface userInterface= ApiClient.getClient().create(UserInterface.class);
//                    Call<InventoryVendorListResponse>allVendorResponseCall=userInterface.getInventoryVendorList();
//                    allVendorResponseCall.enqueue(new Callback<InventoryVendorListResponse>() {
//                        @Override
//                        public void onResponse(Call<InventoryVendorListResponse> call, Response<InventoryVendorListResponse> response) {
//                            if (response.code()==200)
//                            {
//                                progress.dismiss();
//                                List<InventoryVendorListResponse.DataBean> allvendors=new ArrayList<InventoryVendorListResponse.DataBean>();
//                                allvendors=response.body().getData();
//                                Dialog vendorsdialog=new Dialog(EditInventory.this);
//                                vendorsdialog.setContentView(R.layout.recyclerdialog);
//                                vendorsdialog.setCancelable(false);
//                                DisplayMetrics metrics=getResources().getDisplayMetrics();
//                                int width=metrics.widthPixels;
//                                vendorsdialog.getWindow().setLayout(width, LinearLayout.LayoutParams.MATCH_PARENT);
//                                vendorrec=vendorsdialog.findViewById(R.id.cityRecycler);
//                                vendorrec.setLayoutManager(new LinearLayoutManager(EditInventory.this,LinearLayoutManager.VERTICAL,false));
//                                vendorrec.setAdapter(new VendorDropDownAdapter(EditInventory.this,allvendors,vendorname,vendorid,vendorsdialog));
//                                vendorsdialog.show();
//
//                            }
//                            else if (response.code()!=200)
//                            {
//                                Snackbar.make(EditInventory.this.getWindow().getDecorView().findViewById(android.R.id.content),"Some Error Occurs!!try again",Snackbar.LENGTH_SHORT).show();
//
//                            }
//                        }
//
//                        @Override
//                        public void onFailure(Call<InventoryVendorListResponse> call, Throwable t) {
//                            Snackbar.make(EditInventory.this.getWindow().getDecorView().findViewById(android.R.id.content),""+t.getMessage(),Snackbar.LENGTH_SHORT).show();
//
//                        }
//                    });
//                }
//
//            }
//        });
//        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
//
//            @Override
//            public void onDateSet(DatePicker view, int year, int monthOfYear,
//                                  int dayOfMonth) {
//                // TODO Auto-generated method stub
//                calendar.set(Calendar.YEAR, year);
//                calendar.set(Calendar.MONTH, monthOfYear);
//                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
//                updateLabel();
//            }
//        };
//        qty=findViewById(R.id.etqty);
//        qty.setText(inventory.getQuantity());
//        dateofdelivery=findViewById(R.id.date);
//        deliveryDate=findViewById(R.id.delDate);
//        dateofdelivery.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                new DatePickerDialog(EditInventory.this,R.style.TimePickerTheme,date, calendar
//                        .get(Calendar.YEAR), calendar.get(Calendar.MONTH),
//                        calendar.get(Calendar.DAY_OF_MONTH)).show();
//            }
//        });
//        submit=findViewById(R.id.Submit);
//        submit.setText("Update Inventory");
//        submit.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                String pid=prodictid.getText().toString().trim();
//                String vid=vendorid.getText().toString().trim();
//                String qnty=qty.getText().toString().trim();
//                String deliver=deliveryDate.getText().toString().trim();
//                String batchno=batch.getText().toString().trim();
//                String inventoryid=inventory.getId();
//                String stat=inventory.getStatus();
//                if (batchno.isEmpty()&&pid.isEmpty()&&vid.isEmpty()&&qnty.isEmpty()&&deliver.isEmpty())
//                    Snackbar.make(EditInventory.this.getWindow().getDecorView().findViewById(android.R.id.content),"Enter all The fields",Snackbar.LENGTH_SHORT).show();
//else if (pid.isEmpty())
//                    Snackbar.make(EditInventory.this.getWindow().getDecorView().findViewById(android.R.id.content),"Select Product",Snackbar.LENGTH_SHORT).show();
//else if (vid.isEmpty())
//                    Snackbar.make(EditInventory.this.getWindow().getDecorView().findViewById(android.R.id.content),"Select Vendor",Snackbar.LENGTH_SHORT).show();
//else if (qnty.isEmpty())
//                    Snackbar.make(EditInventory.this.getWindow().getDecorView().findViewById(android.R.id.content),"Enter Quantity",Snackbar.LENGTH_SHORT).show();
//                else if (batchno.isEmpty())
//                    Snackbar.make(EditInventory.this.getWindow().getDecorView().findViewById(android.R.id.content),"Enter Batch No.",Snackbar.LENGTH_SHORT).show();
//
//else if (deliver.isEmpty()||deliver.equalsIgnoreCase("Date Of Delivery"))
//                    Snackbar.make(EditInventory.this.getWindow().getDecorView().findViewById(android.R.id.content),"Select Delivery Date",Snackbar.LENGTH_SHORT).show();
//else {
//                    if (!connectionDetector.isConnectingToInternet())
//                        Snackbar.make(EditInventory.this.getWindow().getDecorView().findViewById(android.R.id.content),"Please Connect to internet and Try Again",Snackbar.LENGTH_SHORT).show();
//                    else if (connectionDetector.isConnectingToInternet()){
//                        final Dialog progress=new Dialog(EditInventory.this);
//                        progress.setContentView(R.layout.customdialog);
//                        progress.setCancelable(false);
//                        progress.show();
//UserInterface userInterface=ApiClient.getClient().create(UserInterface.class);
//Call<UpdateImventoryResponse>call=userInterface.updateInventory(inventoryid,pid,vid,qnty,deliver,batchno,stat);
//call.enqueue(new Callback<UpdateImventoryResponse>() {
//    @Override
//    public void onResponse(Call<UpdateImventoryResponse> call, Response<UpdateImventoryResponse> response) {
//        if (response.code()==200)
//        {
//            progress.dismiss();
//            Toast.makeText(EditInventory.this, "Inventory Added", Toast.LENGTH_SHORT).show();
//            Intent intent=new Intent(EditInventory.this,Inventory.class);
//            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//            overridePendingTransition(R.anim.transitionrighttoleft,R.anim.transitionrighttoleft);
//            startActivity(intent);
//            finish();
//
//        }
//        if (response.code()!=200){
//            progress.dismiss();
//            Snackbar.make(EditInventory.this.getWindow().getDecorView().findViewById(android.R.id.content),"Inventory Not Added",Snackbar.LENGTH_SHORT).show();
//
//        }
//    }
//
//    @Override
//    public void onFailure(Call<UpdateImventoryResponse> call, Throwable t) {
//        progress.dismiss();
//        Snackbar.make(EditInventory.this.getWindow().getDecorView().findViewById(android.R.id.content),""+t.getMessage(),Snackbar.LENGTH_SHORT).show();
//    }
//});
//
//                    }
//                }
//
//            }
//        });
//
//    }
//    private void updateLabel() {
//
//        String myFormat = "MM/dd/yyyy"; //In which you need put here
//        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
//        deliveryDate.setText(sdf.format(calendar.getTime()));
//    }
    }
}

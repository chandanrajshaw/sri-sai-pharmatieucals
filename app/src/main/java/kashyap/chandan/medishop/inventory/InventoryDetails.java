package kashyap.chandan.medishop.inventory;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.util.List;

import kashyap.chandan.medishop.R;

public class InventoryDetails extends AppCompatActivity {
TextView uniqueid,vendor,deliverydate,total,qty,toolheader;
ImageView goback;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_inventory_details);
        uniqueid=findViewById(R.id.uniqueid);
        vendor=findViewById(R.id.vendorname);
        deliverydate=findViewById(R.id.deliverdate);
        total=findViewById(R.id.total);
        qty=findViewById(R.id.qty);
        toolheader=findViewById(R.id.customtoolheader);
        toolheader.setText("Details");
        Intent i=getIntent();
        Bundle b=i.getBundleExtra("bundle");
        List<InventoryResponse.DataBean.DetailsBean> inventory= (List<InventoryResponse.DataBean.DetailsBean>) b.getSerializable("inventory");
        uniqueid.setText(i.getStringExtra("id"));
        vendor.setText(i.getStringExtra("vname"));
        deliverydate.setText(i.getStringExtra("date"));
        total.setText(i.getStringExtra("total"));
        qty.setText(String.valueOf(i.getStringExtra("qnty")));
        goback=findViewById(R.id.ordergobackarrow);
        goback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                overridePendingTransition(R.anim.transitionrighttoleft,R.anim.transitionrighttoleft);
                finish();
            }
        });
        addHeaders();
        addData(inventory);
    }
    private TextView getTextView ( int id, String title,int color, int typeface, int bgColor)
    {
        TextView tv = new TextView(InventoryDetails.this);
        tv.setId(id);
        tv.setText(title.toUpperCase());
        tv.setTextColor(color);
        tv.setPadding(5, 5, 5, 5);
        tv.setTypeface(Typeface.DEFAULT, typeface);
        tv.setBackgroundColor(bgColor);
        tv.setLayoutParams(getLayoutParams());

        return tv;
    }
    @NonNull
    private TableLayout.LayoutParams getTblLayoutParams () {
        return new TableLayout.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT);
    }
    private TableRow.LayoutParams getLayoutParams ()
    {
        TableRow.LayoutParams params = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT);
        params.setMargins(2, 0, 0, 2);
        return params;
    }
    public void addData( List<InventoryResponse.DataBean.DetailsBean> inventory) {
        // int numCompanies = companies.length;
        TableLayout tl = findViewById(R.id.table);
for (int i=0;i<inventory.size();i++){
    InventoryResponse.DataBean.DetailsBean detail=inventory.get(i);
    TableRow tr = new TableRow(this);
    tr.setLayoutParams(getLayoutParams());
    tr.addView(getTextView( 1,""+(i+1), ContextCompat.getColor(this, R.color.background_color), Typeface.NORMAL,  ContextCompat.getColor(this, R.color.textcolor)));
    tr.addView(getTextView(1, detail.getPc_name(),ContextCompat.getColor(this, R.color.background_color), Typeface.NORMAL,  ContextCompat.getColor(this, R.color.textcolor)));
    tr.addView(getTextView(1,detail.getProduct_name(), ContextCompat.getColor(this, R.color.background_color), Typeface.NORMAL,  ContextCompat.getColor(this, R.color.textcolor)));
    tr.addView(getTextView(1, detail.getQuantity(), ContextCompat.getColor(this, R.color.background_color), Typeface.NORMAL, ContextCompat.getColor(this, R.color.textcolor)));
    tr.addView(getTextView(1, detail.getPurchase_price(), ContextCompat.getColor(this, R.color.background_color), Typeface.NORMAL, ContextCompat.getColor(this, R.color.textcolor)));
    tr.addView(getTextView(1, detail.getMrp(), ContextCompat.getColor(this, R.color.background_color), Typeface.NORMAL, ContextCompat.getColor(this, R.color.textcolor)));
    tr.addView(getTextView(1, detail.getBatch_no(), ContextCompat.getColor(this, R.color.background_color), Typeface.NORMAL, ContextCompat.getColor(this, R.color.textcolor)));
    tr.addView(getTextView(1, detail.getDate_of_delivery(), ContextCompat.getColor(this, R.color.background_color), Typeface.NORMAL, ContextCompat.getColor(this, R.color.textcolor)));
    tr.addView(getTextView(1, detail.getExpiry_date(), ContextCompat.getColor(this, R.color.background_color), Typeface.NORMAL, ContextCompat.getColor(this, R.color.textcolor)));
    tl.addView(tr, getTblLayoutParams());
}
    }
    public void addHeaders() {
        TableLayout tl = findViewById(R.id.table);
        TableRow tr = new TableRow(this);
        tr.setLayoutParams(getLayoutParams());
        tr.addView(getTextView(0, "Sl.No", ContextCompat.getColor(this, R.color.textcolor), Typeface.BOLD, ContextCompat.getColor(this, R.color.background_color)));
        tr.addView(getTextView(0, "Category",  ContextCompat.getColor(this, R.color.textcolor), Typeface.BOLD,ContextCompat.getColor(this, R.color.background_color)));
        tr.addView(getTextView(0, "Product", ContextCompat.getColor(this, R.color.textcolor), Typeface.BOLD,ContextCompat.getColor(this, R.color.background_color)));
        tr.addView(getTextView(0, "Quantity", ContextCompat.getColor(this, R.color.textcolor), Typeface.BOLD,ContextCompat.getColor(this, R.color.background_color)));
        tr.addView(getTextView(0, "Purchase", ContextCompat.getColor(this, R.color.textcolor), Typeface.BOLD,ContextCompat.getColor(this, R.color.background_color)));
        tr.addView(getTextView(0, "MRP", ContextCompat.getColor(this, R.color.textcolor), Typeface.BOLD,ContextCompat.getColor(this, R.color.background_color)));
        tr.addView(getTextView(0, "Batch No", ContextCompat.getColor(this, R.color.textcolor), Typeface.BOLD,ContextCompat.getColor(this, R.color.background_color)));
        tr.addView(getTextView(0, "Delivery", ContextCompat.getColor(this, R.color.textcolor), Typeface.BOLD,ContextCompat.getColor(this, R.color.background_color)));
        tr.addView(getTextView(0, "Expiry", ContextCompat.getColor(this, R.color.textcolor), Typeface.BOLD,ContextCompat.getColor(this, R.color.background_color)));

        tl.addView(tr, getTblLayoutParams());
    }
}

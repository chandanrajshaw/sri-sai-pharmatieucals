package kashyap.chandan.medishop.inventory;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.List;

import kashyap.chandan.medishop.ApiClient;
import kashyap.chandan.medishop.ConnectionDetector;
import kashyap.chandan.medishop.R;
import kashyap.chandan.medishop.UserInterface;
import kashyap.chandan.medishop.client.ShopDocument;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Inventory extends AppCompatActivity {
TextView toolheader;
ImageView fabadd,goback;
RecyclerView inventoryrec;
ConnectionDetector connectionDetector;
    ShimmerFrameLayout shimmerFrameLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_inventory);
        toolheader=findViewById(R.id.customtoolheader);
        connectionDetector=new ConnectionDetector(Inventory.this);
        toolheader.setText("Inventory");
        shimmerFrameLayout=findViewById(R.id.shimmerview);

        goback=findViewById(R.id.ordergobackarrow);
        goback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                overridePendingTransition(R.anim.transitionrighttoleft,R.anim.transitionrighttoleft);
                finish();
            }
        });
        fabadd=findViewById(R.id.addfab);
        fabadd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(Inventory.this,AddInventory.class);
                overridePendingTransition(R.anim.transitionrighttoleft,R.anim.transitionrighttoleft);
                startActivity(intent);
            }
        });
inventoryrec=findViewById(R.id.inventorylist);
if (!connectionDetector.isConnectingToInternet()){
    inventoryrec.setVisibility(View.GONE);
    shimmerFrameLayout.stopShimmer();
    shimmerFrameLayout.setVisibility(View.GONE);
    Snackbar.make(Inventory.this.getWindow().getDecorView().findViewById(android.R.id.content),"Please Connect to internet and try again",Snackbar.LENGTH_SHORT).show();

}else if (connectionDetector.isConnectingToInternet()){
//    final Dialog progress=new Dialog(Inventory.this);
//    progress.setContentView(R.layout.customdialog);
//    progress.setCancelable(false);
//    progress.show();
    UserInterface userInterface= ApiClient.getClient().create(UserInterface.class);
    Call<InventoryResponse>call=userInterface.getInventory();
    call.enqueue(new Callback<InventoryResponse>() {
        @Override
        public void onResponse(Call<InventoryResponse> call, Response<InventoryResponse> response) {
            if (response.code()==200)
            {
                shimmerFrameLayout.stopShimmer();
                shimmerFrameLayout.setVisibility(View.GONE);
                InventoryResponse inventorylist=new InventoryResponse() ;
                inventorylist=response.body();
                inventoryrec.setLayoutManager(new LinearLayoutManager(Inventory.this,LinearLayoutManager.VERTICAL,false));
                inventoryrec.setAdapter(new InventoryAdapter(Inventory.this,inventorylist));
              inventoryrec.setVisibility(View.VISIBLE);

            }
            else if (response.code()!=200)
            {
                shimmerFrameLayout.stopShimmer();
                shimmerFrameLayout.setVisibility(View.GONE);
                Snackbar.make(Inventory.this.getWindow().getDecorView().findViewById(android.R.id.content),"Error Occurs!! No Inventory",Snackbar.LENGTH_SHORT).show();

            }
        }

        @Override
        public void onFailure(Call<InventoryResponse> call, Throwable t) {
            Snackbar.make(Inventory.this.getWindow().getDecorView().findViewById(android.R.id.content),""+t.getMessage(),Snackbar.LENGTH_SHORT).show();
        }
    });
}

    }
    @Override
    protected void onResume() {
        super.onResume();
        shimmerFrameLayout.startShimmer();
    }

    @Override
    protected void onPause() {
        super.onPause();
        shimmerFrameLayout.stopShimmer();
    }
}

package kashyap.chandan.medishop.inventory;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import kashyap.chandan.medishop.R;

class ProductDropDownAdapter extends RecyclerView.Adapter<ProductDropDownAdapter.MyViewHolder> {
    Context context;
    List<InventoryProductListResponse.DataBean> allProduct=new ArrayList<InventoryProductListResponse.DataBean>();
    TextView productname, prodictid;
    Dialog productdialog;
    private int SelectedItem = -1;
    String selectedproduct,selectedid;

    public ProductDropDownAdapter(Context context, List<InventoryProductListResponse.DataBean> allProduct, TextView productname, TextView prodictid, Dialog productdialog) {
   this.context=context;
   this.allProduct=allProduct;
   this.prodictid=prodictid;
   this.productname=productname;
   this.productdialog=productdialog;
    }

    @NonNull
    @Override
    public ProductDropDownAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.itemlayout,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductDropDownAdapter.MyViewHolder holder, int position) {
        holder.radioselect.setChecked(position == SelectedItem);
        holder.productitem.setText(allProduct.get(position).getProduct_name());
        TextView head=productdialog.findViewById(R.id.dialoghead);
        head.setText("Select Product");
        TextView ok=productdialog.findViewById(R.id.ok);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                productname.setText(selectedproduct);
                if (productname.getText().toString().isEmpty()){
                    Toast.makeText(context,"Select Product",Toast.LENGTH_SHORT).show();
                }
                else {

                    prodictid.setText(selectedid);
                    productdialog.dismiss();
                }

            }
        });
    }

    @Override
    public int getItemCount() {
        return allProduct.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView productitem;
        RadioButton radioselect;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            productitem=itemView.findViewById(R.id.items);
            radioselect=itemView.findViewById(R.id.selectedItem);
            View.OnClickListener clickListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SelectedItem =getAdapterPosition();
                    notifyDataSetChanged();
                    selectedproduct=allProduct.get(SelectedItem).getProduct_name();
                    selectedid=allProduct.get(SelectedItem).getId();
                }
            };
            itemView.setOnClickListener(clickListener);
            radioselect.setOnClickListener(clickListener);
        }
    }
}

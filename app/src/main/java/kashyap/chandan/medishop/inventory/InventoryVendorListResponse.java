package kashyap.chandan.medishop.inventory;

import java.util.List;

public class InventoryVendorListResponse {


    /**
     * status : {"code":200,"message":"Vendors List"}
     * data : [{"id":"1","company_name":"hgchch","image":"apollo.jpg","person_name":"lkdsnfnslkn","email":"sdga@gmail","mobile":"1234567890","city_id":"1","address":"sgggggsd","gst_no":"agdf","gst_certificate":"Angular_Course_Content.docx","pan_no":"adgfgg","pan_card":"Python_Course_Content.docx","date_time":"2020-03-07 15:47:35","status":"1"},{"id":"2","company_name":"hgchch","image":"temp.jpg","person_name":"lkdsnfncdsk","email":"sdggg@gmail","mobile":"1234567890","city_id":"1","address":"sgggggsd","gst_no":"agdf","gst_certificate":"gst.jpg","pan_no":"adgfgg","pan_card":"pan.jpg","date_time":"2020-03-16 09:55:56","status":"1"},{"id":"3","company_name":"hgchch","image":"temp1.jpg","person_name":"lkdsnfncd","email":"sdga@gmail","mobile":"1234567890","city_id":"","address":"sgggggsd","gst_no":"agdf","gst_certificate":"gst1.jpg","pan_no":"adgfgg","pan_card":"pan1.jpg","date_time":"2020-03-16 10:12:47","status":"1"},{"id":"4","company_name":"abcd","image":"crop.png","person_name":"abcd","email":"abcd.com","mobile":"1234567789","city_id":"2","address":"Kokar","gst_no":"123456","gst_certificate":"donation.png","pan_no":"321654","pan_card":"womenempower.png","date_time":"2020-03-19 09:59:18","status":"1"},{"id":"5","company_name":"maxwel  Laboratories","image":"20200401_075948.jpg","person_name":"maxwel Laboratories","email":"maxwel@gmail.com","mobile":"1234562322","city_id":"1","address":"harathvanam colony uppal","gst_no":"36ADXFS6 ha80087","gst_certificate":"20200401_075928.jpg","pan_no":"apaps13799u","pan_card":"20200401_075913.jpg","date_time":"2020-04-01 09:55:23","status":"1"}]
     */

    private StatusBean status;
    private List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : Vendors List
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean {
        /**
         * id : 1
         * company_name : hgchch
         * image : apollo.jpg
         * person_name : lkdsnfnslkn
         * email : sdga@gmail
         * mobile : 1234567890
         * city_id : 1
         * address : sgggggsd
         * gst_no : agdf
         * gst_certificate : Angular_Course_Content.docx
         * pan_no : adgfgg
         * pan_card : Python_Course_Content.docx
         * date_time : 2020-03-07 15:47:35
         * status : 1
         */

        private String id;
        private String company_name;
        private String image;
        private String person_name;
        private String email;
        private String mobile;
        private String city_id;
        private String address;
        private String gst_no;
        private String gst_certificate;
        private String pan_no;
        private String pan_card;
        private String date_time;
        private String status;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getCompany_name() {
            return company_name;
        }

        public void setCompany_name(String company_name) {
            this.company_name = company_name;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getPerson_name() {
            return person_name;
        }

        public void setPerson_name(String person_name) {
            this.person_name = person_name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getCity_id() {
            return city_id;
        }

        public void setCity_id(String city_id) {
            this.city_id = city_id;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getGst_no() {
            return gst_no;
        }

        public void setGst_no(String gst_no) {
            this.gst_no = gst_no;
        }

        public String getGst_certificate() {
            return gst_certificate;
        }

        public void setGst_certificate(String gst_certificate) {
            this.gst_certificate = gst_certificate;
        }

        public String getPan_no() {
            return pan_no;
        }

        public void setPan_no(String pan_no) {
            this.pan_no = pan_no;
        }

        public String getPan_card() {
            return pan_card;
        }

        public void setPan_card(String pan_card) {
            this.pan_card = pan_card;
        }

        public String getDate_time() {
            return date_time;
        }

        public void setDate_time(String date_time) {
            this.date_time = date_time;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
    }
}

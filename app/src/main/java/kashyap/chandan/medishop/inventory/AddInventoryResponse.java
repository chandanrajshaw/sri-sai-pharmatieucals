package kashyap.chandan.medishop.inventory;

public class AddInventoryResponse {


    /**
     * status : {"code":200,"message":"Admin added Inventory Successfully"}
     * data : {"vendor_id":"1","unique_id":"SBINV2","product_catagories":"2,3","product_id":"1,2","quantity":"25,25","purchase_price":"15,15","mrp":"20,20","expiry_date":"12-11-2020,12-11-2020","batch_no":"15222,15222"}
     */

    private StatusBean status;
    private DataBean data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : Admin added Inventory Successfully
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean {
        /**
         * vendor_id : 1
         * unique_id : SBINV2
         * product_catagories : 2,3
         * product_id : 1,2
         * quantity : 25,25
         * purchase_price : 15,15
         * mrp : 20,20
         * expiry_date : 12-11-2020,12-11-2020
         * batch_no : 15222,15222
         */

        private String vendor_id;
        private String unique_id;
        private String product_catagories;
        private String product_id;
        private String quantity;
        private String purchase_price;
        private String mrp;
        private String expiry_date;
        private String batch_no;

        public String getVendor_id() {
            return vendor_id;
        }

        public void setVendor_id(String vendor_id) {
            this.vendor_id = vendor_id;
        }

        public String getUnique_id() {
            return unique_id;
        }

        public void setUnique_id(String unique_id) {
            this.unique_id = unique_id;
        }

        public String getProduct_catagories() {
            return product_catagories;
        }

        public void setProduct_catagories(String product_catagories) {
            this.product_catagories = product_catagories;
        }

        public String getProduct_id() {
            return product_id;
        }

        public void setProduct_id(String product_id) {
            this.product_id = product_id;
        }

        public String getQuantity() {
            return quantity;
        }

        public void setQuantity(String quantity) {
            this.quantity = quantity;
        }

        public String getPurchase_price() {
            return purchase_price;
        }

        public void setPurchase_price(String purchase_price) {
            this.purchase_price = purchase_price;
        }

        public String getMrp() {
            return mrp;
        }

        public void setMrp(String mrp) {
            this.mrp = mrp;
        }

        public String getExpiry_date() {
            return expiry_date;
        }

        public void setExpiry_date(String expiry_date) {
            this.expiry_date = expiry_date;
        }

        public String getBatch_no() {
            return batch_no;
        }

        public void setBatch_no(String batch_no) {
            this.batch_no = batch_no;
        }
    }
}

package kashyap.chandan.medishop.inventory;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import kashyap.chandan.medishop.R;

class VendorDropDownAdapter extends RecyclerView.Adapter<VendorDropDownAdapter.MyViewHolder> {
    Context context;
    List<InventoryVendorListResponse.DataBean> allvendors;
    TextView vendorname, vendorid;
    Dialog vendorsdialog;
    private int SelectedItem = -1;
    String selectedvendor,selectedid;
    public VendorDropDownAdapter(Context context, List<InventoryVendorListResponse.DataBean> allvendors, TextView vendorname, TextView vendorid, Dialog vendorsdialog) {
   this.context=context;
   this.allvendors=allvendors;
   this.vendorid=vendorid;
   this.vendorname=vendorname;
   this.vendorsdialog=vendorsdialog;
    }

    @NonNull
    @Override
    public VendorDropDownAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.itemlayout,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull VendorDropDownAdapter.MyViewHolder holder, int position) {
        holder.radioselect.setChecked(position == SelectedItem);
        holder.vendor.setText(allvendors.get(position).getPerson_name());
        TextView head=vendorsdialog.findViewById(R.id.dialoghead);
        head.setText("Select Vendor");
        TextView ok=vendorsdialog.findViewById(R.id.ok);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                vendorname.setText(selectedvendor);
                if (vendorname.getText().toString().isEmpty()){
                    Toast.makeText(context,"Select Product",Toast.LENGTH_SHORT).show();
                }
                else {

                    vendorid.setText(selectedid);
                    vendorsdialog.dismiss();
                }

            }
        });
    }

    @Override
    public int getItemCount() {
        return allvendors.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView vendor,dialogheader;
        RadioButton radioselect;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            vendor=itemView.findViewById(R.id.items);
            radioselect=itemView.findViewById(R.id.selectedItem);
            View.OnClickListener clickListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SelectedItem =getAdapterPosition();
                    notifyDataSetChanged();
                    selectedvendor=allvendors.get(SelectedItem).getPerson_name();
                    selectedid=allvendors.get(SelectedItem).getId();
                }
            };
            itemView.setOnClickListener(clickListener);
            radioselect.setOnClickListener(clickListener);
        }
    }
}

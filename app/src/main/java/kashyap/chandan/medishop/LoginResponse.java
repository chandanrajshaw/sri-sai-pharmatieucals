package kashyap.chandan.medishop;

import java.util.List;

public class LoginResponse {


    /**
     * status : {"code":200,"message":"Admin Login"}
     * data : [{"user_id":"1","name":"admin","email":"admin@admin.com","mobile":"7013332509","password":"4de93544234adffbb681ed60ffcfb941"}]
     */

    private StatusBean status;
    private List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : Admin Login
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean {
        /**
         * user_id : 1
         * name : admin
         * email : admin@admin.com
         * mobile : 7013332509
         * password : 4de93544234adffbb681ed60ffcfb941
         */

        private String user_id;
        private String name;
        private String email;
        private String mobile;
        private String password;

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }
    }
}




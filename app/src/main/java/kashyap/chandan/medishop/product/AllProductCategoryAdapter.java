package kashyap.chandan.medishop.product;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import kashyap.chandan.medishop.ApiClient;
import kashyap.chandan.medishop.R;
import kashyap.chandan.medishop.UserInterface;
import kashyap.chandan.medishop.admin.AdminAllCityList;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

class AllProductCategoryAdapter extends RecyclerView.Adapter<AllProductCategoryAdapter.MyViewHolder> {
    Context context;
    List<AllProductCategoryResponse.DataBean> allCategory=new ArrayList<AllProductCategoryResponse.DataBean>();
    public AllProductCategoryAdapter(Context context, List<AllProductCategoryResponse.DataBean> allCategory) {
        this.context=context;
        this.allCategory=allCategory;
    }

    @NonNull
    @Override
    public AllProductCategoryAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.recallproductcategory,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final AllProductCategoryAdapter.MyViewHolder holder, int position) {
holder.cateName.setText(allCategory.get(position).getPc_name());
holder.editcategory.setVisibility(View.VISIBLE);
holder.editcategory.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        int pos=holder.getAdapterPosition();
        final Dialog dialog=new Dialog(context);
        dialog.setContentView(R.layout.dialogcityedit);
        DisplayMetrics metrics=context.getResources().getDisplayMetrics();
        int width = metrics.widthPixels;
        int height = metrics.heightPixels;
        dialog.getWindow().setLayout(width, LinearLayout.LayoutParams.WRAP_CONTENT);
        final EditText categoryedit= dialog.findViewById(R.id.dcityedit);
        categoryedit.setText(allCategory.get(pos).getPc_name());
        final String id=allCategory.get(pos).getId();
        final String status=allCategory.get(pos).getStatus();
        ImageView close=dialog.findViewById(R.id.closebtn);
        TextView update=dialog.findViewById(R.id.btnupdate);
        dialog.show();
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String categoryname=categoryedit.getText().toString();
                if (categoryname.isEmpty())
                    Toast.makeText(context,"Enter City Name",Toast.LENGTH_SHORT).show();
                else {
                    final Dialog progress=new Dialog(context);
                    progress.setContentView(R.layout.customdialog);
                    progress.setCancelable(false);
                    progress.show();
                    UserInterface userInterface= ApiClient.getClient().create(UserInterface.class);
                    Call<UpdateCategoryResponse>call=userInterface.updateProductCategory(id,categoryname,status);
                    call.enqueue(new Callback<UpdateCategoryResponse>() {
                        @Override
                        public void onResponse(Call<UpdateCategoryResponse> call, Response<UpdateCategoryResponse> response) {
                            if (response.code()==200){
                                progress.dismiss();
                                dialog.dismiss();
                                Toast.makeText(context, "Product Category Updated", Toast.LENGTH_SHORT).show();
                                Intent intent=new Intent(context, ProductCategory.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                context.startActivity(intent);
                                ((ProductCategory) context).finish();
                            }
                            else if (response.code()!=200)
                            {
                                progress.dismiss();
                                Toast.makeText(context, "Product Category Not Updated", Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<UpdateCategoryResponse> call, Throwable t) {
                            Toast.makeText(context, ""+t.getMessage(), Toast.LENGTH_SHORT).show();

                        }
                    });
                }
            }
        });
    }
});
    }

    @Override
    public int getItemCount() {
        return allCategory.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView cateName;
        ImageView editcategory;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            cateName=itemView.findViewById(R.id.categoryname);
            editcategory=itemView.findViewById(R.id.editcategory);
        }
    }
}

package kashyap.chandan.medishop.product;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.util.List;

import kashyap.chandan.medishop.Order.OrderProductListResponse;
import kashyap.chandan.medishop.R;

class ProductByCategoryAdapter extends RecyclerView.Adapter<ProductByCategoryAdapter.MyViewHolder> {
    Context context;
    List<OrderProductListResponse.DataBean> allproducts;
    public ProductByCategoryAdapter(Context context, List<OrderProductListResponse.DataBean> allproducts) {
        this.allproducts=allproducts;
        this.context=context;
    }

    @NonNull
    @Override
    public ProductByCategoryAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.recproductdetail,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ProductByCategoryAdapter.MyViewHolder holder, int position) {
        holder.category.setVisibility(View.GONE);
        holder.name.setText(allproducts.get(position).getProduct_name());
        holder.mkt.setText(allproducts.get(position).getMkt_price());
        holder.ptr.setText(allproducts.get(position).getPtr_price());
        holder.tvcat.setVisibility(View.GONE);
        Picasso.get().load("http://www.sbpharma.in/admin_assets/uploads/products/"+allproducts.get(position).getImage()).placeholder(R.drawable.loading).error(R.drawable.cross).into(holder.img);
        holder.detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int pos=holder.getAdapterPosition();
                OrderProductListResponse.DataBean data=allproducts.get(pos);
                Intent intent=new Intent(context, ProductDetailbyCategory.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                Bundle bundle=new Bundle();
                bundle.putSerializable("productdetail", (Serializable) data);
                intent.putExtra("productbycategorybundle",bundle);
                context.startActivity(intent);
            }
        });
        holder.edit.setVisibility(View.GONE);
        holder.edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int pos=holder.getAdapterPosition();
                OrderProductListResponse.DataBean data=allproducts.get(pos);
                Intent intent=new Intent(context, EditProduct.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                Bundle bundle=new Bundle();
                bundle.putSerializable("productdetail", (Serializable) data);
                intent.putExtra("productbycategorybundle",bundle);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return allproducts.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView category,name,mkt,ptr,tvcat;
        ImageView img,edit;
        LinearLayout detail;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            category=itemView.findViewById(R.id.category);
            name=itemView.findViewById(R.id.productname);
            mkt=itemView.findViewById(R.id.mktprice);
            ptr=itemView.findViewById(R.id.ptrprice);
            img=itemView.findViewById(R.id.productImage);
            detail=itemView.findViewById(R.id.detail);
            edit=itemView.findViewById(R.id.edit);
            tvcat=itemView.findViewById(R.id.tvcat);
        }
    }
}

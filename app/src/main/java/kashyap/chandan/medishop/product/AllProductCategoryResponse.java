package kashyap.chandan.medishop.product;

import java.util.List;

public class AllProductCategoryResponse {

    /**
     * status : {"code":200,"message":"Product Catagories List"}
     * data : [{"id":"7","pc_name":"General Cold","date_time":"2020-02-27 16:23:49","status":"1"},{"id":"6","pc_name":"Life Saving Drugs","date_time":"2020-02-27 15:53:11","status":"1"},{"id":"4","pc_name":"D Catagory","date_time":"2020-02-25 13:56:17","status":"1"},{"id":"3","pc_name":"C Catagory","date_time":"2020-02-25 13:56:07","status":"1"},{"id":"2","pc_name":"B Catagory","date_time":"2020-02-25 13:55:32","status":"1"},{"id":"1","pc_name":"A Catagory","date_time":"2020-02-25 13:55:22","status":"1"}]
     */

    private StatusBean status;
    private List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : Product Catagories List
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean {
        /**
         * id : 7
         * pc_name : General Cold
         * date_time : 2020-02-27 16:23:49
         * status : 1
         */

        private String id;
        private String pc_name;
        private String date_time;
        private String status;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getPc_name() {
            return pc_name;
        }

        public void setPc_name(String pc_name) {
            this.pc_name = pc_name;
        }

        public String getDate_time() {
            return date_time;
        }

        public void setDate_time(String date_time) {
            this.date_time = date_time;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
    }
}

package kashyap.chandan.medishop.product;

public class AddProductResponse {


    /**
     * status : {"code":200,"message":"Admin added Product Details Successfully"}
     * data : {"product_catagory_id":"7","product_name":"Paracetamol","mkt_price":"50","ptr_price":"43","image":"logo.jpg"}
     */

    private StatusBean status;
    private DataBean data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : Admin added Product Details Successfully
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean {
        /**
         * product_catagory_id : 7
         * product_name : Paracetamol
         * mkt_price : 50
         * ptr_price : 43
         * image : logo.jpg
         */

        private String product_catagory_id;
        private String product_name;
        private String mkt_price;
        private String ptr_price;
        private String image;

        public String getProduct_catagory_id() {
            return product_catagory_id;
        }

        public void setProduct_catagory_id(String product_catagory_id) {
            this.product_catagory_id = product_catagory_id;
        }

        public String getProduct_name() {
            return product_name;
        }

        public void setProduct_name(String product_name) {
            this.product_name = product_name;
        }

        public String getMkt_price() {
            return mkt_price;
        }

        public void setMkt_price(String mkt_price) {
            this.mkt_price = mkt_price;
        }

        public String getPtr_price() {
            return ptr_price;
        }

        public void setPtr_price(String ptr_price) {
            this.ptr_price = ptr_price;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }
    }
}

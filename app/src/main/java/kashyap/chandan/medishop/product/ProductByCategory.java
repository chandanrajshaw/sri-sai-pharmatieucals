package kashyap.chandan.medishop.product;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.List;

import kashyap.chandan.medishop.ApiClient;
import kashyap.chandan.medishop.ConnectionDetector;
import kashyap.chandan.medishop.Order.AddOrders;
import kashyap.chandan.medishop.Order.OrderProductListResponse;
import kashyap.chandan.medishop.R;
import kashyap.chandan.medishop.UserInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductByCategory extends AppCompatActivity {
RecyclerView productrecycler;
TextView toolheader;
ImageView goback;
ConnectionDetector connectionDetector;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_product_by_category);
        Intent i=getIntent();
        String category=i.getStringExtra("category");
        toolheader=findViewById(R.id.customtoolheader);
        toolheader.setText("Products");
        goback=findViewById(R.id.ordergobackarrow);
        connectionDetector=new ConnectionDetector(ProductByCategory.this);

        goback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                overridePendingTransition(R.anim.transitionrighttoleft,R.anim.transitionrighttoleft);
                finish();
            }
        });
        final Dialog progress=new Dialog(ProductByCategory.this);
        progress.setContentView(R.layout.customdialog);
        progress.setCancelable(false);
        progress.show();
        productrecycler=findViewById(R.id.productbycategory);
        if (!connectionDetector.isConnectingToInternet()) {
            progress.dismiss();
            Snackbar.make(ProductByCategory.this.getWindow().getDecorView().findViewById(android.R.id.content), "Please Connect to internet and try again", Snackbar.LENGTH_LONG).show();
            productrecycler.setVisibility(View.GONE);
        }
        else if (connectionDetector.isConnectingToInternet())
        {
            UserInterface userInterface= ApiClient.getClient().create(UserInterface.class);
            Call<OrderProductListResponse> call=userInterface.orderProductList(category);
            call.enqueue(new Callback<OrderProductListResponse>() {
                @Override
                public void onResponse(Call<OrderProductListResponse> call, Response<OrderProductListResponse> response) {
                    if (response.code()==200)
                    {
                        List<OrderProductListResponse.DataBean> allproducts=new ArrayList<OrderProductListResponse.DataBean>();
                        allproducts=response.body().getData();
                        productrecycler.setLayoutManager(new LinearLayoutManager(ProductByCategory.this,LinearLayoutManager.VERTICAL,false));
                        productrecycler.setAdapter(new ProductByCategoryAdapter(ProductByCategory.this,allproducts));
                        progress.dismiss();
                    }
                    else if (response.code()!=200)
                    {
                        progress.dismiss();
                        Snackbar.make(ProductByCategory.this.getWindow().getDecorView().findViewById(android.R.id.content), "Error Ocurs!!No Products of this Category", Snackbar.LENGTH_LONG).show();

                    }
                }

                @Override
                public void onFailure(Call<OrderProductListResponse> call, Throwable t) {
                    progress.dismiss();
                    Snackbar.make(ProductByCategory.this.getWindow().getDecorView().findViewById(android.R.id.content), ""+t.getMessage(), Snackbar.LENGTH_LONG).show();

                }
            });

        }


    }
}

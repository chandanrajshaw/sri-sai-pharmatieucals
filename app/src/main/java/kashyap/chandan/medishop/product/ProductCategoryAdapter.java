package kashyap.chandan.medishop.product;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.List;

import kashyap.chandan.medishop.ApiClient;
import kashyap.chandan.medishop.Order.OrderProductListResponse;
import kashyap.chandan.medishop.R;
import kashyap.chandan.medishop.UserInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

class ProductCategoryAdapter extends RecyclerView.Adapter<ProductCategoryAdapter.MyViewHolder> {
    Context context;
    List<AllProductCategoryResponse.DataBean> allCategory=new ArrayList<AllProductCategoryResponse.DataBean>();
    RecyclerView all_products;
    TextView categoryname;
    public ProductCategoryAdapter(Context context, List<AllProductCategoryResponse.DataBean> allCategory, RecyclerView all_products, TextView categoryname) {
        this.context=context;
        this.allCategory=allCategory;
        this.all_products=all_products;
        this.categoryname=categoryname;
    }

    @NonNull
    @Override
    public ProductCategoryAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.recproductcategory,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ProductCategoryAdapter.MyViewHolder holder, int position) {
holder.cateName.setText(allCategory.get(position).getPc_name());
holder.cateName.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        int pos=holder.getAdapterPosition();
        String catid=allCategory.get(pos).getId();
        categoryname.setText(allCategory.get(pos).getPc_name());
        final String catname=allCategory.get(pos).getPc_name();
        all_products.removeAllViews();
        UserInterface userInterface= ApiClient.getClient().create(UserInterface.class);
        Call<OrderProductListResponse> call=userInterface.orderProductList(catid);
        final Dialog progress=new Dialog(context);
        progress.setContentView(R.layout.customdialog);
        progress.setCancelable(false);
        progress.show();
        call.enqueue(new Callback<OrderProductListResponse>() {
            @Override
            public void onResponse(Call<OrderProductListResponse> call, Response<OrderProductListResponse> response) {
                if (response.code()==200)
                {
                    List<OrderProductListResponse.DataBean> allproducts=new ArrayList<OrderProductListResponse.DataBean>();
                    allproducts=response.body().getData();
                    all_products.setVisibility(View.VISIBLE);
                    all_products.setLayoutManager(new LinearLayoutManager(context,LinearLayoutManager.VERTICAL,false));
                    all_products.setAdapter(new ProductAdapter(context,allproducts,"category",catname));
                    progress.dismiss();
                }
                else if (response.code()!=200)
                {
                    all_products.removeAllViews();
                    all_products.setVisibility(View.GONE);
                    progress.dismiss();
                    Toast.makeText(context, "Error Ocurs!!No Products of this Category", Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<OrderProductListResponse> call, Throwable t) {
                progress.dismiss();
                all_products.setVisibility(View.GONE);
                Toast.makeText(context, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
});
    }

    @Override
    public int getItemCount() {

        return allCategory.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView cateName;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            cateName=itemView.findViewById(R.id.categoryname);
        }
    }
}

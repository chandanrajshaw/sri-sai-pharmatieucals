package kashyap.chandan.medishop.product;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.List;

import kashyap.chandan.medishop.ApiClient;
import kashyap.chandan.medishop.ConnectionDetector;
import kashyap.chandan.medishop.R;
import kashyap.chandan.medishop.UserInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductCategory extends AppCompatActivity {
ImageView fab;
TextView toolheader;
RecyclerView productcategory;
ConnectionDetector connectionDetector;
ImageView goback;
ShimmerFrameLayout shimmerFrameLayout;
    List<AllProductCategoryResponse.DataBean> allCategory=new ArrayList<AllProductCategoryResponse.DataBean>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_product_category);
        toolheader=findViewById(R.id.customtoolheader);
        shimmerFrameLayout=findViewById(R.id.shimmerview);
        toolheader.setText("Product Category");
        connectionDetector=new ConnectionDetector(ProductCategory.this);
        goback=findViewById(R.id.ordergobackarrow);
        productcategory=findViewById(R.id.categoryrecycler);

        goback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                overridePendingTransition(R.anim.transitionrighttoleft,R.anim.transitionrighttoleft);
                finish();
            }
        });
        if(!connectionDetector.isConnectingToInternet())
        {
          shimmerFrameLayout.stopShimmer();
          shimmerFrameLayout.setVisibility(View.GONE);
            productcategory.setVisibility(View.GONE);
            Snackbar.make(ProductCategory.this.getWindow().getDecorView().findViewById(android.R.id.content),"Internet Not Available",Snackbar.LENGTH_LONG).show();
        }
        else if (connectionDetector.isConnectingToInternet())
        {
            UserInterface userInterface= ApiClient.getClient().create(UserInterface.class);
            Call<AllProductCategoryResponse> call=userInterface.getProductCategory();
            call.enqueue(new Callback<AllProductCategoryResponse>() {
                @Override
                public void onResponse(Call<AllProductCategoryResponse> call, Response<AllProductCategoryResponse> response) {
                    if (response.code()==200)
                    {
                       shimmerFrameLayout.stopShimmer();
                       shimmerFrameLayout.setVisibility(View.GONE);
                        allCategory=response.body().getData();
                        productcategory.setLayoutManager(new LinearLayoutManager(ProductCategory.this,LinearLayoutManager.VERTICAL,false));
                        productcategory.setAdapter(new AllProductCategoryAdapter(ProductCategory.this,allCategory));
productcategory.setVisibility(View.VISIBLE);
                    }
                    else if (response.code()!=200){
                        shimmerFrameLayout.stopShimmer();
                        shimmerFrameLayout.setVisibility(View.GONE);
                        productcategory.setVisibility(View.GONE);
                        productcategory.setVisibility(View.VISIBLE);
                        Snackbar.make(ProductCategory.this.getWindow().getDecorView().findViewById(android.R.id.content),"Something Goes Wrong",Snackbar.LENGTH_LONG).show();

                    }
                }

                @Override
                public void onFailure(Call<AllProductCategoryResponse> call, Throwable t) {

                }
            });

        }
        fab=findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(ProductCategory.this,AddProductCategory.class);
                startActivity(intent);

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        shimmerFrameLayout.startShimmer();
    }

    @Override
    protected void onPause() {
        super.onPause();
        shimmerFrameLayout.stopShimmer();
    }
}

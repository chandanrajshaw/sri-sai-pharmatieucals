package kashyap.chandan.medishop.product;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import kashyap.chandan.medishop.Order.OrderProductListResponse;
import kashyap.chandan.medishop.R;
import kashyap.chandan.medishop.admin.AllShopsResponse;
import kashyap.chandan.medishop.admin.AminViewallShopDetail;

class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.MyViewHolder> {
    Context context;
    String productkey;
    List<AllProductResponse.DataBean> allProduct=new ArrayList<AllProductResponse.DataBean>();
    List<OrderProductListResponse.DataBean> categoryproduct;
    String category;
    public ProductAdapter(Context context , String allproduct,List<AllProductResponse.DataBean>allProduct) {
        this.context=context;
        this.allProduct=allProduct;
        this.productkey=allproduct;
    }
    public ProductAdapter(Context context, List<OrderProductListResponse.DataBean> allProduct, String allproduct, String category) {
        this.context=context;
        this.categoryproduct=allProduct;
        this.productkey=allproduct;
        this.category=category;
    }

    @NonNull
    @Override
    public ProductAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.recproductdetail,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ProductAdapter.MyViewHolder holder, int position) {
        if (productkey.equalsIgnoreCase("allproduct")){
            holder.category.setText(allProduct.get(position).getPc_name());
            holder.name.setText(allProduct.get(position).getProduct_name());
            holder.mkt.setText(allProduct.get(position).getMkt_price());
            holder.ptr.setText(allProduct.get(position).getPtr_price());
            Picasso.get().load("http://www.sbpharma.in/admin_assets/uploads/products/"+allProduct.get(position).getImage()).placeholder(R.drawable.loading).error(R.drawable.cross).into(holder.img);
            holder.detail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int pos=holder.getAdapterPosition();
                    AllProductResponse.DataBean data=allProduct.get(pos);
                    Intent intent=new Intent(context, ProductDetail.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    Bundle bundle=new Bundle();
                    bundle.putSerializable("productdetail", (Serializable) data);
                    intent.putExtra("detail",bundle);
                    context.startActivity(intent);
                }
            });
            holder.edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int pos=holder.getAdapterPosition();
                    AllProductResponse.DataBean data=allProduct.get(pos);
                    Intent intent=new Intent(context, EditProduct.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    Bundle bundle=new Bundle();
                    bundle.putSerializable("productdetail", (Serializable) data);
                    intent.putExtra("key","all");
                    intent.putExtra("detail",bundle);
                    context.startActivity(intent);
                }
            });
        }
        else if (productkey.equalsIgnoreCase("category"))
        {

            holder.category.setText(category);
            holder.tvcat.setVisibility(View.VISIBLE);
            holder.name.setText(categoryproduct.get(position).getProduct_name());
            holder.mkt.setText(categoryproduct.get(position).getMkt_price());
            holder.ptr.setText(categoryproduct.get(position).getPtr_price());
            Picasso.get().load("http://www.sbpharma.in/admin_assets/uploads/products/"+categoryproduct.get(position).getImage()).placeholder(R.drawable.loading).error(R.drawable.cross).into(holder.img);
            holder.detail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int pos=holder.getAdapterPosition();
                    OrderProductListResponse.DataBean data=categoryproduct.get(pos);
                    Intent intent=new Intent(context, ProductDetailbyCategory.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    Bundle bundle=new Bundle();
                    bundle.putSerializable("productdetail", (Serializable) data);
                    intent.putExtra("catname",category);
                    intent.putExtra("productbycategorybundle",bundle);
                    context.startActivity(intent);
                }
            });
            holder.edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int pos=holder.getAdapterPosition();
                    OrderProductListResponse.DataBean data=categoryproduct.get(pos);
                    Intent intent=new Intent(context, EditProduct.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    Bundle bundle=new Bundle();
                    bundle.putSerializable("productdetail", (Serializable) data);
                    intent.putExtra("key","cat");
                    intent.putExtra("catname",category);
                    intent.putExtra("detail",bundle);
                    context.startActivity(intent);
                }
            });
        }

    }

    @Override
    public int getItemCount() {
        int size=0;
        if (productkey.equalsIgnoreCase("allproduct"))
        size= allProduct.size();
        else if (productkey.equalsIgnoreCase("category"))
            size= categoryproduct.size();
        return size;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView category,name,mkt,ptr,tvcat;
        ImageView img,edit;
        LinearLayout detail;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            category=itemView.findViewById(R.id.category);
            name=itemView.findViewById(R.id.productname);
            mkt=itemView.findViewById(R.id.mktprice);
            ptr=itemView.findViewById(R.id.ptrprice);
            img=itemView.findViewById(R.id.productImage);
            detail=itemView.findViewById(R.id.detail);
            edit=itemView.findViewById(R.id.edit);
            tvcat=itemView.findViewById(R.id.tvcat);
        }
    }
}

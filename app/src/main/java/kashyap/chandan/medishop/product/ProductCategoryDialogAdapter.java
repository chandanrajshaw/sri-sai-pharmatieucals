package kashyap.chandan.medishop.product;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import kashyap.chandan.medishop.R;


public class ProductCategoryDialogAdapter extends RecyclerView.Adapter<ProductCategoryDialogAdapter.MyViewHolder> {
    String selectedcategory,selectedid;
    Context context;
    List<AllProductCategoryResponse.DataBean> allcategory;
    private int mSelectedItem = -1;
    TextView catName,catId;
    Dialog categorydialog;
    public ProductCategoryDialogAdapter(Context context, List<AllProductCategoryResponse.DataBean> allcategory, TextView catName, Dialog categorydialog, TextView catId) {
    this.context=context;
    this.allcategory=allcategory;
    this.catName=catName;
    this.catId=catId;
    this.categorydialog=categorydialog;
    }

    @NonNull
    @Override
    public ProductCategoryDialogAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.itemlayout,parent,false);
        return new MyViewHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull ProductCategoryDialogAdapter.MyViewHolder holder, final int position) {
        holder.radioselect.setChecked(position == mSelectedItem);
        holder.categoryitem.setText(allcategory.get(position).getPc_name());
        TextView ok= categorydialog.findViewById(R.id.ok);
        TextView dialogheader=categorydialog.findViewById(R.id.dialoghead);
        dialogheader.setText("Select Category");
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                catName.setText(selectedcategory);
                if (catName.getText().toString().isEmpty()){
                    Toast.makeText(context,"Please Select Category",Toast.LENGTH_SHORT).show();
                }
                else {
                    catId.setText(selectedid);
                    categorydialog.dismiss();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return allcategory.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView categoryitem;
        RadioButton radioselect;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            categoryitem=itemView.findViewById(R.id.items);
            radioselect=itemView.findViewById(R.id.selectedItem);


            View.OnClickListener clickListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mSelectedItem = getAdapterPosition();
                    notifyDataSetChanged();
                    selectedcategory=allcategory.get(mSelectedItem).getPc_name();
                    selectedid=allcategory.get(mSelectedItem).getId();
                }
            };
            itemView.setOnClickListener(clickListener);
            radioselect.setOnClickListener(clickListener);
        }
    }
}

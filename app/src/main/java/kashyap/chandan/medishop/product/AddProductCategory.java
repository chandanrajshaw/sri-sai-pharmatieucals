package kashyap.chandan.medishop.product;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.Api;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;

import kashyap.chandan.medishop.ApiClient;
import kashyap.chandan.medishop.ConnectionDetector;
import kashyap.chandan.medishop.R;
import kashyap.chandan.medishop.UserInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddProductCategory extends AppCompatActivity {
TextInputEditText categoryname;
TextView toolheader,addcategory;
ImageView goback;
ConnectionDetector connectionDetector;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_add_product_category);
        connectionDetector=new ConnectionDetector(AddProductCategory.this);
        toolheader=findViewById(R.id.customtoolheader);
        toolheader.setText("Add Category");
        goback=findViewById(R.id.ordergobackarrow);
        goback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                overridePendingTransition(R.anim.transitionrighttoleft,R.anim.transitionrighttoleft);
                finish();
            }
        });
        addcategory=findViewById(R.id.addcategory);
        categoryname=findViewById(R.id.etproductcategory);
        addcategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog progress=new Dialog(AddProductCategory.this);
                progress.setContentView(R.layout.customdialog);
                progress.setCancelable(false);
                progress.show();
                String category=categoryname.getText().toString().trim();
                if (!connectionDetector.isConnectingToInternet()){
                    progress.dismiss();
                    Snackbar.make(AddProductCategory.this.getWindow().getDecorView().findViewById(android.R.id.content),"Internet Not Available",Snackbar.LENGTH_SHORT).show();
                }
                else if (connectionDetector.isConnectingToInternet())
                {
                    UserInterface userInterface= ApiClient.getClient().create(UserInterface.class);
                    Call<ProductCategoryResponse>call=userInterface.addProductCategory(category);
                    call.enqueue(new Callback<ProductCategoryResponse>() {
                        @Override
                        public void onResponse(Call<ProductCategoryResponse> call, Response<ProductCategoryResponse> response) {
                            if (response.code()==200){
                                progress.dismiss();
                                ProductCategoryResponse.DataBean res=response.body().getData();
                                Toast.makeText(AddProductCategory.this, ""+res.getPc_name()+" Added to Product Category", Toast.LENGTH_SHORT).show();
                                Intent intent=new Intent(AddProductCategory.this,ProductCategory.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                overridePendingTransition(R.anim.transitionrighttoleft,R.anim.transitionrighttoleft);
                                startActivity(intent);
                                finish();
                            }
                            else if (response.code()!=200)
                            {
                                progress.dismiss();
                                Toast.makeText(AddProductCategory.this, "Try Again", Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<ProductCategoryResponse> call, Throwable t) {
                            progress.dismiss();
                            Snackbar.make(AddProductCategory.this.getWindow().getDecorView().findViewById(android.R.id.content),""+t.getMessage(),Snackbar.LENGTH_SHORT).show();

                        }
                    });
                }
            }
        });
    }
}

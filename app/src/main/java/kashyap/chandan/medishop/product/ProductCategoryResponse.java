package kashyap.chandan.medishop.product;

import java.io.Serializable;

public class ProductCategoryResponse implements Serializable {


    /**
     * status : {"code":200,"message":"Admin added Product Catagory Successfully"}
     * data : {"pc_name":"Life Saving Drugs"}
     */

    private StatusBean status;
    private DataBean data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class StatusBean implements Serializable{
        /**
         * code : 200
         * message : Admin added Product Catagory Successfully
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean implements Serializable {
        /**
         * pc_name : Life Saving Drugs
         */

        private String pc_name;

        public String getPc_name() {
            return pc_name;
        }

        public void setPc_name(String pc_name) {
            this.pc_name = pc_name;
        }
    }
}

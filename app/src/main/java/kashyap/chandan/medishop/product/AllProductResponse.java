package kashyap.chandan.medishop.product;

import java.io.Serializable;
import java.util.List;

public class AllProductResponse implements Serializable {


    /**
     * status : {"code":200,"message":"Product List"}
     * data : [{"id":"6","product_catagory_id":"10","product_name":"abcd","image":"temp.jpg","mkt_price":"12","ptr_price":"15","date_time":"2020-02-28 11:46:58","status":"1","pc_name":"D Catagories"},{"id":"5","product_catagory_id":"7","product_name":"Paracetamol","image":"logo.jpg","mkt_price":"50","ptr_price":"43","date_time":"2020-02-27 18:50:48","status":"1","pc_name":"Colds"},{"id":"4","product_catagory_id":"2","product_name":"weweewe","image":"water-liquid-gas-solid_1_orig.jpg","mkt_price":"ewewewe","ptr_price":"ewewe","date_time":"2020-02-27 18:17:16","status":"ewew","pc_name":"B Catagory"},{"id":"2","product_catagory_id":"2","product_name":"Tylenol Cold","image":"51-ipW3CGiL.jpg","mkt_price":"250","ptr_price":"200","date_time":"2020-02-26 16:06:26","status":"1","pc_name":"B Catagory"},{"id":"1","product_catagory_id":"1","product_name":"Paracetmol","image":"paracetamol_tablets_500mg.jpg","mkt_price":"200","ptr_price":"150","date_time":"2020-02-25 13:57:46","status":"1","pc_name":"A Catagory"}]
     */

    private StatusBean status;
    private List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean implements Serializable{
        /**
         * code : 200
         * message : Product List
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean implements Serializable {
        /**
         * id : 6
         * product_catagory_id : 10
         * product_name : abcd
         * image : temp.jpg
         * mkt_price : 12
         * ptr_price : 15
         * date_time : 2020-02-28 11:46:58
         * status : 1
         * pc_name : D Catagories
         */

        private String id;
        private String product_catagory_id;
        private String product_name;
        private String image;
        private String mkt_price;
        private String ptr_price;
        private String date_time;
        private String status;
        private String pc_name;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getProduct_catagory_id() {
            return product_catagory_id;
        }

        public void setProduct_catagory_id(String product_catagory_id) {
            this.product_catagory_id = product_catagory_id;
        }

        public String getProduct_name() {
            return product_name;
        }

        public void setProduct_name(String product_name) {
            this.product_name = product_name;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getMkt_price() {
            return mkt_price;
        }

        public void setMkt_price(String mkt_price) {
            this.mkt_price = mkt_price;
        }

        public String getPtr_price() {
            return ptr_price;
        }

        public void setPtr_price(String ptr_price) {
            this.ptr_price = ptr_price;
        }

        public String getDate_time() {
            return date_time;
        }

        public void setDate_time(String date_time) {
            this.date_time = date_time;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getPc_name() {
            return pc_name;
        }

        public void setPc_name(String pc_name) {
            this.pc_name = pc_name;
        }
    }
}

package kashyap.chandan.medishop.product;

import android.Manifest;
import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import kashyap.chandan.medishop.ApiClient;
import kashyap.chandan.medishop.ConnectionDetector;
import kashyap.chandan.medishop.Order.OrderProductListResponse;
import kashyap.chandan.medishop.R;
import kashyap.chandan.medishop.UserInterface;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class EditProduct extends AppCompatActivity {
    String picturePath,imagePic;
TextView addproduct,toolheader;
RelativeLayout category;
ConnectionDetector connectionDetector;
ImageView goback;
RecyclerView categoryRecycler;
TextView catName,catId;
ImageView productImg;
TextInputEditText productName,mktPrice,ptrPrice;
    Bitmap bitmap,converetdImage;
    Uri picUri;
    File image=null;
List<AllProductCategoryResponse.DataBean> allcategory=new ArrayList<AllProductCategoryResponse.DataBean>();
    private ArrayList camerapermissionsToRequest;
    private ArrayList camerapermissionsRejected = new ArrayList();
    private ArrayList camerapermissions = new ArrayList();
    Dialog dialog;
    private final static int CAMERA_ALL_PERMISSIONS_RESULT = 108;
     AllProductResponse.DataBean productdetail;
    OrderProductListResponse.DataBean categoryproduct;
    String productId,status;;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_add_product);
        Intent i=getIntent();
        Bundle bundle=i.getBundleExtra("detail");
        String key=i.getStringExtra("key");


        addproduct=findViewById(R.id.addproduct);
        catId=findViewById(R.id.catid);
        catName=findViewById(R.id.categoryname);
        productName=findViewById(R.id.etproductname);
        mktPrice=findViewById(R.id.etmktprice);
        ptrPrice=findViewById(R.id.etptrprice);
        productImg=findViewById(R.id.productImage);

        if (key.equalsIgnoreCase("all")){

             productdetail= (AllProductResponse.DataBean) bundle.getSerializable("productdetail");
            catId.setText(productdetail.getProduct_catagory_id());
            catName.setText(productdetail.getPc_name());
            productName.setText(productdetail.getProduct_name());
            mktPrice.setText(productdetail.getMkt_price());
            ptrPrice.setText(productdetail.getPtr_price());
            productId=productdetail.getId();
            status=productdetail.getStatus();
            Picasso.get().load("http://www.sbpharma.in/admin_assets/uploads/products/"+productdetail.getImage()).placeholder(R.drawable.loading).error(R.drawable.cross).into(productImg);

        }
        else if (key.equalsIgnoreCase("cat")){
            String catname=i.getStringExtra("catname");
            categoryproduct=(OrderProductListResponse.DataBean) bundle.getSerializable("productdetail");
            catId.setText(categoryproduct.getProduct_catagory_id());
            catName.setText(catname);
            productId=categoryproduct.getId();
            productName.setText(categoryproduct.getProduct_name());
            mktPrice.setText(categoryproduct.getMkt_price());
            ptrPrice.setText(categoryproduct.getPtr_price());
            status=categoryproduct.getStatus();
            Picasso.get().load("http://www.sbpharma.in/admin_assets/uploads/products/"+categoryproduct.getImage()).placeholder(R.drawable.loading).error(R.drawable.cross).into(productImg);

        }

        camerapermissions.add(CAMERA);
        camerapermissions.add(READ_EXTERNAL_STORAGE);
        camerapermissions.add(WRITE_EXTERNAL_STORAGE);

        addproduct.setText("Update");
        toolheader=findViewById(R.id.customtoolheader);
        toolheader.setText("Edit Product");


        connectionDetector=new ConnectionDetector(EditProduct.this);
        goback=findViewById(R.id.ordergobackarrow);
        goback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                overridePendingTransition(R.anim.transitionrighttoleft,R.anim.transitionrighttoleft);
                finish();
            }
        });
        category=findViewById(R.id.layoutcategorydrop);
        category.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog progressDialog = new Dialog(EditProduct.this);
                progressDialog.setContentView(R.layout.customdialog);
                progressDialog.setCancelable(false);
                progressDialog.show();
                if (!connectionDetector.isConnectingToInternet()){
                    progressDialog.dismiss();
                    Snackbar.make(EditProduct.this.getWindow().getDecorView().findViewById(android.R.id.content),"Please connect to Internet and try again",Snackbar.LENGTH_LONG).show();
                }

               else if(connectionDetector.isConnectingToInternet()){

                    UserInterface userInterface= ApiClient.getClient().create(UserInterface.class);
                    Call<AllProductCategoryResponse>call=userInterface.getProductCategory();
                    call.enqueue(new Callback<AllProductCategoryResponse>() {
                        @Override
                        public void onResponse(Call<AllProductCategoryResponse> call, Response<AllProductCategoryResponse> response) {
                            if (response.code()==200){
                                allcategory=response.body().getData();
                                final  Dialog categorydialog=new Dialog(EditProduct.this);
                                categorydialog.setContentView(R.layout.recyclerdialog);
                                DisplayMetrics metrics=getResources().getDisplayMetrics();
                                int width=metrics.widthPixels;
                                categorydialog.getWindow().setLayout(width, LinearLayout.LayoutParams.WRAP_CONTENT);
                                categoryRecycler=categorydialog.findViewById(R.id.dialogRecycler);
                                categoryRecycler.setLayoutManager(new LinearLayoutManager(EditProduct.this,LinearLayoutManager.VERTICAL,false));
                                categoryRecycler.setAdapter(new ProductCategoryDialogAdapter(EditProduct.this,allcategory,catName,categorydialog,catId));
                progressDialog.dismiss();
                                categorydialog.show();
                            }
                            else if (response.code()!=200){
                                progressDialog.dismiss();
                                Snackbar.make(EditProduct.this.getWindow().getDecorView().findViewById(android.R.id.content),"Something Goes Wrong",Snackbar.LENGTH_LONG).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<AllProductCategoryResponse> call, Throwable t) {
                            progressDialog.dismiss();
                            Snackbar.make(EditProduct.this.getWindow().getDecorView().findViewById(android.R.id.content),""+t.getMessage(),Snackbar.LENGTH_LONG).show();

                        }
                    });

//

                }
            }
        });
       productImg.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               if ((ContextCompat.checkSelfPermission(EditProduct.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)) {
                   requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA},
                           CAMERA_ALL_PERMISSIONS_RESULT);
               }
               else {
                   LinearLayout camera, folder;
                   dialog = new Dialog(EditProduct.this);
                   dialog.setContentView(R.layout.dialogboxcamera);
                   DisplayMetrics metrics=getResources().getDisplayMetrics();
                   int width=metrics.widthPixels;
                   dialog.getWindow().setLayout(width, LinearLayout.LayoutParams.MATCH_PARENT);
                   dialog.show();
                   dialog.setCancelable(true);
                   dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                   Window window = dialog.getWindow();
                   window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
                   camera = dialog.findViewById(R.id.camera);
                   folder = dialog.findViewById(R.id.folder);
                   folder.setOnClickListener(new View.OnClickListener() {
                       @Override
                       public void onClick(View view) {

                        /*checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                STORAGE_PERMISSION_CODE);*/
                           Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                           startActivityForResult(i, 100);
                           dialog.dismiss();



                       }
                   });
                   camera.setOnClickListener(new View.OnClickListener() {
                       @Override
                       public void onClick(View view) {

                           //checkPermission(Manifest.permission.CAMERA, CAMERA_PERMISSION_CODE);
                           Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                           startActivityForResult(cameraIntent, 101);
                           dialog.dismiss();


                       }
                   });
               }
           }
       });
        if (imagePic != null && !imagePic.isEmpty() && !imagePic.equals("null")) {

            Picasso.get().load(imagePic).into(productImg);

            bitmap = ((BitmapDrawable) productImg.getDrawable().getCurrent()).getBitmap();
            Log.e("bitmap", "" + bitmap);
            converetdImage = getResizedBitmap(bitmap, 500);

        } else { }

        addproduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String catid=catId.getText().toString();
                String pname=productName.getText().toString();
                String mprice=mktPrice.getText().toString();
                String pprice=ptrPrice.getText().toString();
                if (catid.isEmpty()&& pname.isEmpty()&&mprice.isEmpty()&&pprice.isEmpty())
                    Snackbar.make(EditProduct.this.getWindow().getDecorView().findViewById(android.R.id.content),"Enter All the Fields",Snackbar.LENGTH_LONG).show();
                else if (catid.isEmpty())
                    Snackbar.make(EditProduct.this.getWindow().getDecorView().findViewById(android.R.id.content),"Select Category",Snackbar.LENGTH_LONG).show();
else if (pname.isEmpty())
                    Snackbar.make(EditProduct.this.getWindow().getDecorView().findViewById(android.R.id.content),"Enter Product Name",Snackbar.LENGTH_LONG).show();
else if (mprice.isEmpty())
                    Snackbar.make(EditProduct.this.getWindow().getDecorView().findViewById(android.R.id.content),"Enter MKT Price",Snackbar.LENGTH_LONG).show();
else if (pprice.isEmpty())
                    Snackbar.make(EditProduct.this.getWindow().getDecorView().findViewById(android.R.id.content),"Enter PTR Price",Snackbar.LENGTH_LONG).show();
else {
                    MultipartBody.Part body=null;
                    if (image != null) {
                        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), image);
                        body = MultipartBody.Part.createFormData("image", image.getName(), requestFile);
                    }
                    final Dialog progress=new Dialog(EditProduct.this);
                    progress.setContentView(R.layout.customdialog);
                    progress.setCancelable(false);
                    progress.show();
                    RequestBody productid = RequestBody.create(MediaType.parse("multipart/form-data"),productId);
                    RequestBody cid = RequestBody.create(MediaType.parse("multipart/form-data"), catid);
                    RequestBody name = RequestBody.create(MediaType.parse("multipart/form-data"), pname);
                    RequestBody mp = RequestBody.create(MediaType.parse("multipart/form-data"), mprice);
                    RequestBody pp = RequestBody.create(MediaType.parse("multipart/form-data"), pprice);
                    RequestBody stat = RequestBody.create(MediaType.parse("multipart/form-data"), status);

                    UserInterface userInterface=ApiClient.getClient().create(UserInterface.class);
                    Call<UpdateProductResponse>call=userInterface.updateProduct(productid,cid,name,mp,pp,stat,body);
                    call.enqueue(new Callback<UpdateProductResponse>() {
                        @Override
                        public void onResponse(Call<UpdateProductResponse> call, Response<UpdateProductResponse> response) {
                            if (response.code()==200)
                            {
                                progress.dismiss();
                                Toast.makeText(EditProduct.this, "Product Updated", Toast.LENGTH_SHORT).show();
                                Intent intent=new Intent(EditProduct.this,Products.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                overridePendingTransition(R.anim.transitionrighttoleft,R.anim.transitionrighttoleft);
                                startActivity(intent);
                                finish();

                            }
                            else if (response.code()!=200)
                            {
                                progress.dismiss();
                                Toast.makeText(EditProduct.this, "Product not Updated", Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<UpdateProductResponse> call, Throwable t) {
progress.dismiss();
                            Toast.makeText(EditProduct.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();

                        }
                    });

                }


            }
        });
    }

















    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode)
        {
            case  CAMERA_ALL_PERMISSIONS_RESULT:
                if (grantResults.length <= 0 || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(this, "Permission denied", Toast.LENGTH_SHORT).show();
                }
                else {
//                    startActivityForResult(, 200);
                }

        }
    }
    private Bitmap getResizedBitmap(Bitmap image, int maxSize) {

        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }





    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //  if (requestCode == 100 && resultCode == RESULT_OK && data != null) {

        //     imagepath=selectedImage.getPath();




        if (requestCode == 100 && resultCode == RESULT_OK && data != null) {

//the image URI
            Uri selectedImage = data.getData();

            //     imagepath=selectedImage.getPath();


            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            picturePath = cursor.getString(columnIndex);
            cursor.close();


            if (picturePath != null && !picturePath.equals("")) {
                image = new File(picturePath);
            }

            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);
                converetdImage = getResizedBitmap(bitmap, 500);
                productImg.setImageBitmap(converetdImage);
              productImg.setVisibility(View.VISIBLE);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else if (requestCode == 101 && resultCode == Activity.RESULT_OK) {


            Bitmap converetdImage = (Bitmap) data.getExtras().get("data");
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            converetdImage.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
            productImg.setImageBitmap(converetdImage);
            productImg.setVisibility(View.VISIBLE);
            image = new File(Environment.getExternalStorageDirectory(), "temp.jpg");
            FileOutputStream fo;
            try {
                fo = new FileOutputStream(image);
                fo.write(bytes.toByteArray());
                fo.close();
            } catch (IOException e) {
                e.printStackTrace();
            }


        }
    }
}

package kashyap.chandan.medishop.product;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.shimmer.Shimmer;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.List;

import kashyap.chandan.medishop.ApiClient;
import kashyap.chandan.medishop.ConnectionDetector;
import kashyap.chandan.medishop.R;
import kashyap.chandan.medishop.UserInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Products extends AppCompatActivity {
RecyclerView product_Category,all_products;
TextView toolheader,catnotavail,pronotfound,categoryname;
ImageView goback,fab;
ConnectionDetector connectionDetector;
LinearLayout productscontent;
List<AllProductCategoryResponse.DataBean> allCategory=new ArrayList<AllProductCategoryResponse.DataBean>();
    List<AllProductResponse.DataBean> allProduct=new ArrayList<AllProductResponse.DataBean>();
    ShimmerFrameLayout categoryshimmer,productshimmer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_produsts);
        categoryshimmer=findViewById(R.id.categoryshimmer);
        productshimmer=findViewById(R.id.productshimmer);
        product_Category=findViewById(R.id.categoryrecycler);
        all_products=findViewById(R.id.allproductsrec);
        toolheader=findViewById(R.id.customtoolheader);
        catnotavail=findViewById(R.id.catnotavail);
        toolheader.setText("Products");
        pronotfound=findViewById(R.id.pronotfound);
        connectionDetector=new ConnectionDetector(Products.this);
        fab=findViewById(R.id.fab);
        categoryname=findViewById(R.id.categoryname);
        productscontent=findViewById(R.id.productscontent);
        if (!connectionDetector.isConnectingToInternet()){
            productshimmer.stopShimmer();
            productshimmer.setVisibility(View.GONE);
            categoryshimmer.stopShimmer();
            categoryshimmer.setVisibility(View.GONE);
            productscontent.setVisibility(View.GONE);
            Snackbar.make(Products.this.getWindow().getDecorView().findViewById(android.R.id.content),"Please Connect to internet and try again",Snackbar.LENGTH_LONG).show();
        }
        /*------------------category---------------------------*/
else if (connectionDetector.isConnectingToInternet()){
            UserInterface userInterface= ApiClient.getClient().create(UserInterface.class);
            Call<AllProductCategoryResponse>call=userInterface.getProductCategory();
            call.enqueue(new Callback<AllProductCategoryResponse>() {
                @Override
                public void onResponse(Call<AllProductCategoryResponse> call, Response<AllProductCategoryResponse> response) {
                    if (response.code()==200)
                    {
                        categoryshimmer.stopShimmer();
                        categoryshimmer.setVisibility(View.GONE);
                       allCategory=response.body().getData();
                        product_Category.setLayoutManager(new LinearLayoutManager(Products.this,LinearLayoutManager.HORIZONTAL,false));
                        product_Category.setAdapter(new ProductCategoryAdapter(Products.this,allCategory,all_products,categoryname));
                        product_Category.setVisibility(View.VISIBLE);
                    }
                    else if (response.code()!=200){
                        categoryshimmer.stopShimmer();
                        categoryshimmer.setVisibility(View.GONE);
                        product_Category.setVisibility(View.GONE);
                        catnotavail.setVisibility(View.VISIBLE);
                    }
                }
                @Override
                public void onFailure(Call<AllProductCategoryResponse> call, Throwable t) {
                    Snackbar.make(Products.this.getWindow().getDecorView().findViewById(android.R.id.content),""+t.getMessage(),Snackbar.LENGTH_LONG).show();
                }
            });
           Call<AllProductResponse>allProductResponseCall=userInterface.getAllProducts();
           allProductResponseCall.enqueue(new Callback<AllProductResponse>() {
               @Override
               public void onResponse(Call<AllProductResponse> call, Response<AllProductResponse> response) {
                   if (response.code()==200)
                   {
                       productshimmer.stopShimmer();
                       productshimmer.setVisibility(View.GONE);
                       allProduct=response.body().getData();
                       all_products.setLayoutManager(new LinearLayoutManager(Products.this,LinearLayoutManager.VERTICAL,false));
                       all_products.setAdapter(new ProductAdapter(Products.this,"allproduct",allProduct));
                      all_products.setVisibility(View.VISIBLE);
                   }
                   if (response.code()!=200)
                   {
                       productshimmer.stopShimmer();
                       productshimmer.setVisibility(View.GONE);
                       all_products.setVisibility(View.GONE);
                       pronotfound.setVisibility(View.VISIBLE);
                   }
               }
               @Override
               public void onFailure(Call<AllProductResponse> call, Throwable t) {
                   Snackbar.make(Products.this.getWindow().getDecorView().findViewById(android.R.id.content),""+t.getMessage(),Snackbar.LENGTH_LONG).show();
               }
           });
        }




        goback=findViewById(R.id.ordergobackarrow);
        goback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                overridePendingTransition(R.anim.transitionrighttoleft,R.anim.transitionrighttoleft);
                finish();
            }
        });
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(Products.this,AddProduct.class);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        categoryshimmer.startShimmer();
        productshimmer.startShimmer();
    }

    @Override
    protected void onPause() {
        super.onPause();
        categoryshimmer.stopShimmer();
        productshimmer.stopShimmer();
    }
}

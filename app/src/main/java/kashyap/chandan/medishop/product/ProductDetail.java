package kashyap.chandan.medishop.product;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import kashyap.chandan.medishop.R;
import kashyap.chandan.medishop.admin.AllShopsResponse;

public class ProductDetail extends AppCompatActivity {
ImageView productimg,goback;
TextView category,productname,mktprice,ptrprice,date,qty,avail,toolheader;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_product_detail);
        Intent i=getIntent();
        Bundle bundle=i.getBundleExtra("detail");
        final AllProductResponse.DataBean productdetail= (AllProductResponse.DataBean) bundle.getSerializable("productdetail");
        productimg=findViewById(R.id.proimg);
        toolheader=findViewById(R.id.customtoolheader);
        toolheader.setText("Product Detail");
        category=findViewById(R.id.pcategory);
        productname=findViewById(R.id.pname);
        mktprice=findViewById(R.id.pmktprice);
        ptrprice=findViewById(R.id.pPtrprice);
        date=findViewById(R.id.date);
        qty=findViewById(R.id.qty);
        avail=findViewById(R.id.available);
        goback=findViewById(R.id.ordergobackarrow);
        Picasso.get().load("http://www.sbpharma.in/admin_assets/uploads/products/"+productdetail.getImage()).placeholder(R.drawable.loading).error(R.drawable.cross).into(productimg);
        category.setText(productdetail.getPc_name());
        productname.setText(productdetail.getProduct_name());
        mktprice.setText(productdetail.getMkt_price());
        ptrprice.setText(productdetail.getPtr_price());
        date.setText(productdetail.getDate_time());
        qty.setVisibility(View.GONE);
        avail.setVisibility(View.GONE);
        goback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                overridePendingTransition(R.anim.transitionrighttoleft,R.anim.transitionrighttoleft);
                finish();
            }
        });
    }
}

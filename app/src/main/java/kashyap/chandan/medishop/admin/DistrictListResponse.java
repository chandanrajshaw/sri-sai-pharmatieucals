package kashyap.chandan.medishop.admin;

import java.io.Serializable;
import java.util.List;

public class DistrictListResponse implements Serializable {


    /**
     * status : {"code":200,"message":"District List"}
     * data : [{"id":"6","state_id":"1","district_name":"Maitrivanam","date_time":"2020-08-25 12:12:23","status":"1","state_name":"Andhra Pradesh"},{"id":"1","state_id":"1","district_name":"East Godavari","date_time":"2020-08-17 15:30:02","status":"1","state_name":"Andhra Pradesh"}]
     */

    private StatusBean status;
    private List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean implements Serializable {
        /**
         * code : 200
         * message : District List
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean implements Serializable {
        /**
         * id : 6
         * state_id : 1
         * district_name : Maitrivanam
         * date_time : 2020-08-25 12:12:23
         * status : 1
         * state_name : Andhra Pradesh
         */

        private String id;
        private String state_id;
        private String district_name;
        private String date_time;
        private String status;
        private String state_name;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getState_id() {
            return state_id;
        }

        public void setState_id(String state_id) {
            this.state_id = state_id;
        }

        public String getDistrict_name() {
            return district_name;
        }

        public void setDistrict_name(String district_name) {
            this.district_name = district_name;
        }

        public String getDate_time() {
            return date_time;
        }

        public void setDate_time(String date_time) {
            this.date_time = date_time;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getState_name() {
            return state_name;
        }

        public void setState_name(String state_name) {
            this.state_name = state_name;
        }
    }
}

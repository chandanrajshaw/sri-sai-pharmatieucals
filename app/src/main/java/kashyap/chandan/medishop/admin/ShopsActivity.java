package kashyap.chandan.medishop.admin;

import androidx.appcompat.app.AppCompatActivity;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.shimmer.ShimmerFrameLayout;

import java.util.ArrayList;
import java.util.List;

import kashyap.chandan.medishop.ApiClient;
import kashyap.chandan.medishop.MainActivity;
import kashyap.chandan.medishop.R;
import kashyap.chandan.medishop.UserInterface;
import kashyap.chandan.medishop.agentPannel.AddShopActivity1;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ShopsActivity extends AppCompatActivity {
TextView noshop;
    ShimmerFrameLayout shimmerFrameLayout;
ImageView add,allshopsgobackarrow;
    List<AllShopsResponse.DataBean> dataBeanList=new ArrayList<AllShopsResponse.DataBean>();
    RecyclerView shopactivityrecycler;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
      setContentView(R.layout.activity_shops);
        shopactivityrecycler=findViewById(R.id.shopactivityrecycler);
        shimmerFrameLayout=findViewById(R.id.shimmerview);
        noshop=findViewById(R.id.noshop);
        add=findViewById(R.id.addfab);
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(ShopsActivity.this, AddShopActivity1.class);
                startActivity(intent);
            }
        });
        allshopsgobackarrow=findViewById(R.id.allshopsgobackarrow);
        allshopsgobackarrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent allshopsGobackIntent=new Intent(ShopsActivity.this,DashBoard.class);
                allshopsGobackIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(allshopsGobackIntent);
                overridePendingTransition(R.anim.transitionrighttoleft, R.anim.transitionrighttoleft);
                finish();
            }
        });

        UserInterface userInterface= ApiClient.getClient().create(UserInterface.class);
        Call<AllShopsResponse> call=userInterface.getAllShopAdmin();
        call.enqueue(new Callback<AllShopsResponse>() {
            @Override
            public void onResponse(Call<AllShopsResponse> call, Response<AllShopsResponse> response) {
                if (response.code()==200)
                {
                    shimmerFrameLayout.stopShimmer();
                    shimmerFrameLayout.setVisibility(View.GONE);
                    dataBeanList=response.body().getData();
               shopactivityrecycler.removeAllViews();
              shopactivityrecycler.setLayoutManager(new LinearLayoutManager(ShopsActivity.this, LinearLayoutManager.VERTICAL,false));
              shopactivityrecycler.setAdapter(new ShopListAdapter(ShopsActivity.this,dataBeanList));
shopactivityrecycler.setVisibility(View.VISIBLE);
add.setVisibility(View.VISIBLE);

                }
                else if (response.code()!=200)
                {
                    shimmerFrameLayout.stopShimmer();
                    shimmerFrameLayout.setVisibility(View.GONE);
            shopactivityrecycler.setVisibility(View.GONE);
                 noshop.setVisibility(View.VISIBLE);
                    Toast.makeText(ShopsActivity.this, "Error Occurs!! No Shop Available", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<AllShopsResponse> call, Throwable t) {
                Toast.makeText(ShopsActivity.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }
    @Override
    protected void onResume() {
        super.onResume();
        shimmerFrameLayout.startShimmer();
    }

    @Override
    protected void onPause() {
        super.onPause();
        shimmerFrameLayout.stopShimmer();
    }
}

package kashyap.chandan.medishop.admin;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;

import androidx.drawerlayout.widget.DrawerLayout;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.tabs.TabLayout;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;
import kashyap.chandan.medishop.ApiClient;
import kashyap.chandan.medishop.ConnectionDetector;
import kashyap.chandan.medishop.MainActivity;
import kashyap.chandan.medishop.R;
import kashyap.chandan.medishop.UserInterface;
import kashyap.chandan.medishop.client.Clients;

import kashyap.chandan.medishop.inventory.Inventory;
import kashyap.chandan.medishop.payment.Payment;
import kashyap.chandan.medishop.pojoclasses.AdminDashboardResponse;
import kashyap.chandan.medishop.Order.Orders;
import kashyap.chandan.medishop.product.ProductCategory;
import kashyap.chandan.medishop.product.Products;
import kashyap.chandan.medishop.vendor.CompanyProfile;
import kashyap.chandan.medishop.vendor.Vendors;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DashBoard extends AppCompatActivity {
    boolean doubleBackToExitPressedOnce = false;
    boolean exit=false;
    SwipeRefreshLayout swiperefreshlayout;
    DrawerLayout drawerlayout;
    SharedPreferencesAdmin sharedPreferencesAdmin;
    Toolbar toolbar;
    LinearLayout values;
    TextView textView, logout, adminname, profilename, proPhone;
    RelativeLayout nav_vendorType,allDistrict,valueslay,client,payment,inventory,category,orders,products, add, addUserLayout,addUsers,profile,changePassword,allcity,addCity,addArea,home,users,shops,allArea,testvendor;
    ViewPager viewpager;
    TabLayout tab;
    CircleImageView profileImg;
    ImageView addarrow;
    ConnectionDetector connectionDetector;
     static  int  tabusers,tabshops;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
     setContentView( R.layout.activity_dash_board);
        sharedPreferencesAdmin = new SharedPreferencesAdmin(DashBoard.this);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        allDistrict=findViewById(R.id.allDistrict);
        swiperefreshlayout=findViewById(R.id.swiperefreshlayout);
        drawerlayout=findViewById(R.id.drawerlayout);
        final ActionBar actionBar = getSupportActionBar();
        tab=findViewById(R.id.tab);
        viewpager=findViewById(R.id.viewpager);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.drawable.ic_menu_white_24dp);
        actionBar.setDisplayHomeAsUpEnabled(true);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle
                (
                        this, drawerlayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
   drawerlayout.addDrawerListener(toggle);
        toggle.syncState();
        connectionDetector = new ConnectionDetector(DashBoard.this);
        textView = findViewById(R.id.title_header);
        textView.setText("Welcome");
        logout = findViewById(R.id.logout);
        adminname = findViewById(R.id.adminname);
        adminname.setText(" " + sharedPreferencesAdmin.getAdminNamePreferences());
        profilename = findViewById(R.id.tvproname);
        profilename.setText(sharedPreferencesAdmin.getAdminNamePreferences());
        proPhone = findViewById(R.id.adminphone);
        proPhone.setText(sharedPreferencesAdmin.getAdminPhonePreferences());
        profileImg = findViewById(R.id.navheaderpropic);
        Picasso.get().load(R.drawable.cross).into(profileImg);
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sharedPreferencesAdmin.sessionEnd();
                Intent intent = new Intent(DashBoard.this, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();

            }
        });
        nav_vendorType=findViewById(R.id.nav_vendorType);
        nav_vendorType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
Intent intent=new Intent(DashBoard.this,VendorTypeList.class);
startActivity(intent);
            }
        });
        valueslay=findViewById(R.id.nav_values);
        values=findViewById(R.id.values);

        addarrow = findViewById(R.id.valuearrow);


        valueslay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                values.setVisibility(values.isShown() ? View.GONE : View.VISIBLE);
                if (values.isShown()) {
                    addarrow.setImageResource(R.drawable.up_arrow);
                } else {
                    addarrow.setImageResource(R.drawable.ic_arrowdwn);
                }
            }
        });


        addUsers = findViewById(R.id.adduser);
        addUsers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DashBoard.this
                        , AddUser.class);
                startActivity(intent);
               drawerlayout.closeDrawer(GravityCompat.START);
            }
        });

        if (!connectionDetector.isConnectingToInternet())
            onInternetNotAvailable();

        if (connectionDetector.isConnectingToInternet()) {
            onInternetAvailable();
            UserInterface userInterface = ApiClient.getClient().create(UserInterface.class);
            Call<AdminDashboardResponse> call = userInterface.adminDashboard();
            call.enqueue(new Callback<AdminDashboardResponse>() {
                @Override
                public void onResponse(Call<AdminDashboardResponse> call, Response<AdminDashboardResponse> response) {
                    if (response.code() == 200) {
                        AdminDashboardResponse.DataBean dataBeans = response.body().getData();
                        tabusers = dataBeans.getAgents();
                        tabshops = dataBeans.getStores();
                        System.out.println("tabuser" + tabusers);
                        System.out.println("tabshop" + tabshops);
                        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
                        System.out.println("tabuser" + tabusers);
                        System.out.println("tabshop" + tabshops);
                        adapter.addFragment(new RecentAddedShop(), "SHOPS  " + String.valueOf(tabshops));
                        adapter.addFragment(new RecentAddedUser(), "USERS  " + String.valueOf(tabusers));
            viewpager.setAdapter(adapter);
                 tab.setupWithViewPager(viewpager);
                    } else
                        Toast.makeText(DashBoard.this, "Something Goes wrong", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onFailure(Call<AdminDashboardResponse> call, Throwable t) {
                    Snackbar.make(DashBoard.this.getWindow().getDecorView().findViewById(android.R.id.content), "" + t.getMessage(), Snackbar.LENGTH_SHORT).show();

                }
            });
        }
        ;

     swiperefreshlayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (!connectionDetector.isConnectingToInternet()) {
                    onInternetNotAvailable();

                }
                if (connectionDetector.isConnectingToInternet()) {
                    UserInterface userInterface = ApiClient.getClient().create(UserInterface.class);
                    Call<AdminDashboardResponse> call = userInterface.adminDashboard();
                    call.enqueue(new Callback<AdminDashboardResponse>() {
                        @Override
                        public void onResponse(Call<AdminDashboardResponse> call, Response<AdminDashboardResponse> response) {
                            if (response.code() == 200) {
                                AdminDashboardResponse.DataBean dataBeans = response.body().getData();
                                tabusers = dataBeans.getAgents();
                                tabshops = dataBeans.getStores();
                                onInternetAvailable();
                                ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
                                adapter.addFragment(new RecentAddedShop(), "SHOPS  " + String.valueOf(tabshops));
                                adapter.addFragment(new RecentAddedUser(), "USERS  " + String.valueOf(tabusers));
                         viewpager.setAdapter(adapter);
                       tab.setupWithViewPager(viewpager);
                            } else
                                Toast.makeText(DashBoard.this, "Something goes Wrong", Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onFailure(Call<AdminDashboardResponse> call, Throwable t) {
                            Snackbar.make(DashBoard.this.getWindow().getDecorView().findViewById(android.R.id.content), "" + t.getMessage(), Snackbar.LENGTH_SHORT).show();

                        }
                    });
                    Snackbar.make(DashBoard.this.getWindow().getDecorView().findViewById(android.R.id.content), "Back Online", Snackbar.LENGTH_LONG).show();

                } else
                    Snackbar.make(DashBoard.this.getWindow().getDecorView().findViewById(android.R.id.content), "Internet Not available", Snackbar.LENGTH_LONG).show();

           swiperefreshlayout.setRefreshing(false);
            }
        });

        profile = findViewById(R.id.profile);
        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent profileintent = new Intent(DashBoard.this, ViewProfile.class);
                startActivity(profileintent);
           drawerlayout.closeDrawer(GravityCompat.START);
            }
        });
        changePassword = findViewById(R.id.changePin);
        changePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent changePasswordIntent = new Intent(DashBoard.this, AdminChangePassword.class);
                startActivity(changePasswordIntent);
           drawerlayout.closeDrawer(GravityCompat.START);
            }
        });
        addCity = findViewById(R.id.addcity);
//        addCity.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent addCityIntent=new Intent(DashBoard.this,AddCity.class);
//                startActivity(addCityIntent);
//                add.setVisibility(View.GONE);
//                dashBoardBinding.drawerlayout.closeDrawer(GravityCompat.START);
//            }
//        });
        addArea=findViewById(R.id.addarea);
//        addArea.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//Intent addAreaIntent=new Intent(DashBoard.this,AddArea.class);
//startActivity(addAreaIntent);
//                dashBoardBinding.drawerlayout.closeDrawer(GravityCompat.START);
//
//            }
//        });
       home=findViewById(R.id.nav_home_layout);
       home.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
       drawerlayout.closeDrawer(GravityCompat.START);
           }
       });
       users=findViewById(R.id.nav_user_layout);
       users.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               Intent intent=new Intent(DashBoard.this,UsersActivity.class);
               startActivity(intent);
         drawerlayout.closeDrawer(GravityCompat.START);
           }
       });
       shops=findViewById(R.id.nav_shop_layout);
       shops.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               Intent intent=new Intent(DashBoard.this,ShopsActivity.class);
               startActivity(intent);
            drawerlayout.closeDrawer(GravityCompat.START);
           }
       });
allcity=findViewById(R.id.allcity);
allcity.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        Intent intent=new Intent(DashBoard.this,AdminAllCityList.class);
        startActivity(intent);
drawerlayout.closeDrawer(GravityCompat.START);

    }
});
allArea=findViewById(R.id.allarea);
allArea.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        Intent intent=new Intent(DashBoard.this,AdminAllAreaList.class);
        startActivity(intent);
drawerlayout.closeDrawer(GravityCompat.START);
    }
});
        testvendor=findViewById(R.id.testvendor);
        testvendor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(DashBoard.this, Vendors.class);
                startActivity(intent);
          drawerlayout.closeDrawer(GravityCompat.START);
            }
        });
        orders=findViewById(R.id.nav_orders);
        orders.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(DashBoard.this, Orders.class);
                startActivity(intent);
            drawerlayout.closeDrawer(GravityCompat.START);
            }
        });
        products=findViewById(R.id.nav_products);
        products.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(DashBoard.this, Products.class);
                startActivity(intent);
      drawerlayout.closeDrawer(GravityCompat.START);
            }
        });
        category=findViewById(R.id.nav_productscategory);
        category.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(DashBoard.this, ProductCategory.class);
                startActivity(intent);
          drawerlayout.closeDrawer(GravityCompat.START);

            }
        });
inventory=findViewById(R.id.nav_Inventory);
inventory.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        Intent intent=new Intent(DashBoard.this, Inventory.class);
        startActivity(intent);
       drawerlayout.closeDrawer(GravityCompat.START);
    }
});
payment=findViewById(R.id.nav_payment);
payment.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        Intent intent=new Intent(DashBoard.this, Payment.class);
        startActivity(intent);
   drawerlayout.closeDrawer(GravityCompat.START);
    }
});
client=findViewById(R.id.nav_client);
client.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        Intent intent=new Intent(DashBoard.this, Clients.class);
        startActivity(intent);
drawerlayout.closeDrawer(GravityCompat.START);
    }
});
        allDistrict.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(DashBoard.this,DistrictList.class);
                startActivity(intent);
                drawerlayout.closeDrawer(GravityCompat.START);
            }
        });
    }




public void onInternetNotAvailable() {
tab.setVisibility(View.GONE);
viewpager.setVisibility(View.GONE);
    Snackbar.make(DashBoard.this.getWindow().getDecorView().findViewById(android.R.id.content), "Please Connect to Internet and Swipe", Snackbar.LENGTH_LONG).show();
        }

public void onInternetAvailable() {
       tab.setVisibility(View.VISIBLE);
 viewpager.setVisibility(View.VISIBLE);
       }



    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }
        this.doubleBackToExitPressedOnce = true;

        Snackbar.make(this.getWindow().getDecorView().findViewById(android.R.id.content), "Please click BACK again to exit", Snackbar.LENGTH_SHORT).show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);

    }



}

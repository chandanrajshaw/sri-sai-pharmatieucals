package kashyap.chandan.medishop.admin;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;


import android.Manifest;
import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import kashyap.chandan.medishop.ApiClient;
import kashyap.chandan.medishop.ConnectionDetector;
import kashyap.chandan.medishop.R;
import kashyap.chandan.medishop.UserInterface;
import kashyap.chandan.medishop.pojoclasses.AddUserResponse;
import kashyap.chandan.medishop.product.AddProduct;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class AddUser extends AppCompatActivity {
    String picturePath,imagePic;
    TextInputEditText etusername,etuseremail,etuserphone,etwhatsapp,etuserpassword;
    ImageView usergobackarrow;
    TextView addagent;
    ConnectionDetector connectionDetector;
    private ArrayList camerapermissionsToRequest;
    private ArrayList camerapermissionsRejected = new ArrayList();
    private ArrayList camerapermissions = new ArrayList();
    Dialog dialog;
    private final static int CAMERA_ALL_PERMISSIONS_RESULT = 108;
    Bitmap bitmap,converetdImage;
    Uri picUri;
    File image=null;
    ImageView userImg;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
    setContentView( R.layout.activity_add_user);
       connectionDetector=new ConnectionDetector(AddUser.this);
        camerapermissions.add(CAMERA);
        camerapermissions.add(READ_EXTERNAL_STORAGE);
        camerapermissions.add(WRITE_EXTERNAL_STORAGE);
        etusername=findViewById(R.id.etusername);
        addagent=findViewById(R.id.addagent);
        etwhatsapp=findViewById(R.id.etwhatsapp);
        etuseremail=findViewById(R.id.etuseremail);
        etuserphone=findViewById(R.id.etuserphone);
        userImg=findViewById(R.id.userImage);
        etuserpassword=findViewById(R.id.etuserpassword);
        usergobackarrow=findViewById(R.id.usergobackarrow);

  addagent.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               String name=etusername.getText().toString();
               String email=etuseremail.getText().toString();
               String phone=etuserphone.getText().toString();
               String whatsapp=etwhatsapp.getText().toString();
               String password=etuserpassword.getText().toString();
               if (image==null &&name.isEmpty()&& email.isEmpty()&& phone.isEmpty()&&whatsapp.isEmpty()&& password.isEmpty())
                   Toast.makeText(AddUser.this, "Please fill all the Fields", Toast.LENGTH_SHORT).show();
else if (image==null) Toast.makeText(AddUser.this, "Choose User Image", Toast.LENGTH_SHORT).show();
               else if (name.isEmpty())
                   Toast.makeText(AddUser.this, "Enter Your name", Toast.LENGTH_SHORT).show();
               else if (email.isEmpty())
                   Toast.makeText(AddUser.this, "Enter Your Email", Toast.LENGTH_SHORT).show();
               else if (phone.isEmpty() || phone.length()!=10)
                   Toast.makeText(AddUser.this, "Enter Valid Mobile no", Toast.LENGTH_SHORT).show();
               else if (whatsapp.isEmpty())
                   Toast.makeText(AddUser.this, "Enter Whatsapp No", Toast.LENGTH_SHORT).show();
               else if (password.isEmpty())
                   Toast.makeText(AddUser.this, "Enter Password", Toast.LENGTH_SHORT).show();

                else{
                    if (!connectionDetector.isConnectingToInternet()){
                        Snackbar.make(AddUser.this.getWindow().getDecorView().findViewById(android.R.id.content), "Please Connect to Internet and try again", Snackbar.LENGTH_SHORT).show();

                    }

                    else if (connectionDetector.isConnectingToInternet()){
                        final Dialog progressDialog = new Dialog(AddUser.this);
                        progressDialog.setContentView(R.layout.customdialog);
                        progressDialog.setCancelable(false);
                        progressDialog.show();

                        /*-------------------------------------multipart----------------------------*/
                        MultipartBody.Part body=null;
                        if (image != null) {
                            RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), image);
                            body = MultipartBody.Part.createFormData("image", image.getName(), requestFile);
                        }
                        RequestBody uname = RequestBody.create(MediaType.parse("multipart/form-data"), name);
                        RequestBody umob = RequestBody.create(MediaType.parse("multipart/form-data"), phone);
                        RequestBody uemail = RequestBody.create(MediaType.parse("multipart/form-data"), email);
                        RequestBody uwtsp = RequestBody.create(MediaType.parse("multipart/form-data"), whatsapp);
                        RequestBody upass = RequestBody.create(MediaType.parse("multipart/form-data"), password);

                        /*-------------------------------------multipart----------------------------*/

                        UserInterface userInterface= ApiClient.getClient().create(UserInterface.class);
                        Call<AddUserResponse> call=userInterface.userAdd(uname,uemail,umob,uwtsp,upass,body);
                        call.enqueue(new Callback<AddUserResponse>() {
                            @Override
                            public void onResponse(Call<AddUserResponse> call, Response<AddUserResponse> response) {

                                if (response.code()==200)
                                {
                                    progressDialog.dismiss();
                                    Toast.makeText(AddUser.this,"Added Successfully",Toast.LENGTH_SHORT).show();
                                    Intent intent=new Intent(AddUser.this,UsersActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(intent);
                                    finish();
                                }
                                else if (response.code()==404)
                                    {
                                        progressDialog.dismiss();
                                        Toast.makeText(AddUser.this,"User Already Exists",Toast.LENGTH_SHORT).show();
                                    }
                                else if (response.code()==409)
                                {
                                    progressDialog.dismiss();
                                    Toast.makeText(AddUser.this,"Mobile No. Already Registered",Toast.LENGTH_SHORT).show();
                                }
                                else{progressDialog.dismiss();
                                    Toast.makeText(AddUser.this,"Something Goes Wrong",Toast.LENGTH_SHORT).show();}

                            }

                            @Override
                            public void onFailure(Call<AddUserResponse> call, Throwable t) {
                                Snackbar.make(AddUser.this.getWindow().getDecorView().findViewById(android.R.id.content), ""+t.getMessage(), Snackbar.LENGTH_SHORT).show();

                            }
                        });
                    }
                    else
                        Snackbar.make(AddUser.this.getWindow().getDecorView().findViewById(android.R.id.content), "Something Goes Wrong", Snackbar.LENGTH_SHORT).show();
               }
           }
       });
usergobackarrow.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        overridePendingTransition(R.anim.transitionrighttoleft, R.anim.transitionrighttoleft);
        Intent intent=new Intent(AddUser.this,DashBoard.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }
});
        userImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if ((ContextCompat.checkSelfPermission(AddUser.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)) {
                    requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA},
                            CAMERA_ALL_PERMISSIONS_RESULT);
                }
                else {
                    LinearLayout camera, folder;
                    dialog = new Dialog(AddUser.this);
                    dialog.setContentView(R.layout.dialogboxcamera);
                    DisplayMetrics metrics=getResources().getDisplayMetrics();
                    int width=metrics.widthPixels;
                    dialog.getWindow().setLayout(width, LinearLayout.LayoutParams.MATCH_PARENT);
                    dialog.show();
                    dialog.setCancelable(true);
                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    Window window = dialog.getWindow();
                    window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
                    camera = dialog.findViewById(R.id.camera);
                    folder = dialog.findViewById(R.id.folder);
                    folder.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                        /*checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                STORAGE_PERMISSION_CODE);*/
                            Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            startActivityForResult(i, 100);
                            dialog.dismiss();



                        }
                    });
                    camera.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            //checkPermission(Manifest.permission.CAMERA, CAMERA_PERMISSION_CODE);
                            Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            startActivityForResult(cameraIntent, 101);
                            dialog.dismiss();


                        }
                    });
                }
            }
        });
        if (imagePic != null && !imagePic.isEmpty() && !imagePic.equals("null")) {

            Picasso.get().load(imagePic).into(userImg);

            bitmap = ((BitmapDrawable) userImg.getDrawable().getCurrent()).getBitmap();
            Log.e("bitmap", "" + bitmap);
            converetdImage = getResizedBitmap(bitmap, 500);

        } else { }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode)
        {
            case  CAMERA_ALL_PERMISSIONS_RESULT:
                if (grantResults.length <= 0 || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(this, "Permission denied", Toast.LENGTH_SHORT).show();
                }
                else {
//                    startActivityForResult(, 200);
                }

        }
    }
    private Bitmap getResizedBitmap(Bitmap image, int maxSize) {

        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }





    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 100 && resultCode == RESULT_OK && data != null) {

//the image URI
            Uri selectedImage = data.getData();

            //     imagepath=selectedImage.getPath();


            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            picturePath = cursor.getString(columnIndex);
            cursor.close();


            if (picturePath != null && !picturePath.equals("")) {
                image = new File(picturePath);
            }

            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);
                converetdImage = getResizedBitmap(bitmap, 500);
                userImg.setImageBitmap(converetdImage);
                userImg.setVisibility(View.VISIBLE);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else if (requestCode == 101 && resultCode == Activity.RESULT_OK) {


            Bitmap converetdImage = (Bitmap) data.getExtras().get("data");
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            converetdImage.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
            userImg.setImageBitmap(converetdImage);
            userImg.setVisibility(View.VISIBLE);
            image = new File(Environment.getExternalStorageDirectory(), "temp.jpg");
            FileOutputStream fo;
            try {
                fo = new FileOutputStream(image);
                fo.write(bytes.toByteArray());
                fo.close();
            } catch (IOException e) {
                e.printStackTrace();
            }


        }
    }
}

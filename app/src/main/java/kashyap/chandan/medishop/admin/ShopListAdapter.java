package kashyap.chandan.medishop.admin;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;

import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;



import java.io.Serializable;
import java.time.format.TextStyle;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
;
import kashyap.chandan.medishop.R;

import static android.os.Build.VERSION.SDK;
import static android.view.Gravity.BOTTOM;

public class ShopListAdapter extends RecyclerView.Adapter<ShopListAdapter.MyViewHolder> {

    Context context;
    List<AllShopsResponse.DataBean> dataBeanList;

    public ShopListAdapter(Context context, List<AllShopsResponse.DataBean> dataBeanList) {
   this.context=context;
   this.dataBeanList=dataBeanList;
    }
    @NonNull
    @Override
    public ShopListAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
       View view= LayoutInflater.from(context).inflate(R.layout.shoplist,parent,false);
      return   new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ShopListAdapter.MyViewHolder holder, final int position) {
//        holder.shopcity.setText(dataBeanList.get(position).getCity_name());
        holder.shoparea.setText(dataBeanList.get(position).getArea_name()+","+dataBeanList.get(position).getCity_name());
        holder.shopphone.setText(dataBeanList.get(position).getMobile());
        holder.ownername.setText(dataBeanList.get(position).getPerson_name());
        holder.datetime.setText(dataBeanList.get(position).getDate_time());
        holder.shopname.setText(dataBeanList.get(position).getStore_name());
        holder.agentname.setText(dataBeanList.get(position).getName());
        holder.imgviewmap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String geoUri = "http://maps.google.com/maps?q=loc:" + Double.valueOf(dataBeanList.get(position).getLatitude()) + "," + Double.valueOf(dataBeanList.get(position).getLongitude()) + " Shop";
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(geoUri));
                context.startActivity(intent);
            }
        });



holder.shopname.setText(dataBeanList.get(position).getStore_name());
holder.shopdetail.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        int pos=holder.getAdapterPosition();
        AllShopsResponse.DataBean data=dataBeanList.get(pos);
        Intent intent=new Intent(context, AminViewallShopDetail.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        Bundle bundle=new Bundle();
        bundle.putSerializable("editShop", (Serializable) data);
        intent.putExtra("edit",bundle);
        context.startActivity(intent);



    }
});
holder.call.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        int pos=holder.getAdapterPosition();
        String mob=dataBeanList.get(pos).getMobile();
        Uri u = Uri.fromParts("tel",mob,null);
        Intent i = new Intent(Intent.ACTION_DIAL, u);
        context.startActivity(i);
    }
});

    }

    @Override
    public int getItemCount() {
        return dataBeanList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView shopname,agentname,shopphone,ownername,shoparea,shopdate,datetime;

     LinearLayout shopdetail;
        CircleImageView imgviewmap,call;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            shopname=itemView.findViewById(R.id.shopname);
            datetime=itemView.findViewById(R.id.shopdate);
            shopphone=itemView.findViewById(R.id.shopphone);
            shoparea=itemView.findViewById(R.id.shoparea);
            shopdetail=itemView.findViewById(R.id.detail);
            shopdate=itemView.findViewById(R.id.shopdate);
            agentname=itemView.findViewById(R.id.agentname);
            imgviewmap=itemView.findViewById(R.id.viewinmap);
            ownername=itemView.findViewById(R.id.ownername);
            call=itemView.findViewById(R.id.call);
        }
    }
}

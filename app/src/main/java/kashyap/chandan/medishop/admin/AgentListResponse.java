package kashyap.chandan.medishop.admin;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class AgentListResponse implements Serializable {


    /**
     * status : {"code":200,"message":"Agents List"}
     * data : [{"id":"12","name":"Rishab","email":"rishab@gmail.com","mobile":"8789160444","wa_mobile":"8789160555","password":"81dc9bdb52d04dc20036dbd8313ed055","status":"1","image":"shop.jpg","date_time":"2020-04-09 16:47:32","Stores Count":0},{"id":"11","name":"Rajeev","email":"rajeev.sharma@gmail.com","mobile":"789654123","wa_mobile":"8789160444","password":"81dc9bdb52d04dc20036dbd8313ed055","status":"1","image":"adoption.png","date_time":"2020-04-09 16:27:51","Stores Count":0},{"id":"10","name":"Ramalingesh","email":"ramalingesh.putta@gmail.com","mobile":"9030955577","wa_mobile":"9030955577","password":"5a4ac73f82a227d340a41c8415688ff2","status":"1","image":"","date_time":"2020-04-08 16:51:42","Stores Count":0},{"id":"9","name":"prasad medical","email":"shivsrinadhu@gmail.com","mobile":"8143043430","wa_mobile":"8143043430","password":"81dc9bdb52d04dc20036dbd8313ed055","status":"1","image":"","date_time":"2020-04-01 09:42:09","Stores Count":0},{"id":"8","name":"Chaitanya","email":"chaitanyaa@gmail.com","mobile":"1234567999","wa_mobile":"7286882452","password":"e2fc714c4727ee9395f324cd2e7f331f","status":"1","image":"","date_time":"2020-03-16 18:00:57","Stores Count":0},{"id":"7","name":"Pappu","email":"pappu@gmail.com","mobile":"9135306477","wa_mobile":"8210402087","password":"827ccb0eea8a706c4c34a16891f84e7b","status":"1","image":"","date_time":"2020-03-16 11:05:37","Stores Count":0},{"id":"3","name":"Pankaj","email":"pankaj@gmail.com","mobile":"8210402087","wa_mobile":"9135306477","password":"6c177ccf365121bd69f767ca2808d2bf","status":"1","image":"","date_time":"2020-02-05 15:07:11","Stores Count":0},{"id":"2","name":"vinay","email":"vinaynarendrapeeta@gmail.com","mobile":"9441868259","wa_mobile":"2422312332","password":"e541c8c2e9eb98febb9adf4209047180","status":"1","image":"","date_time":"2020-02-05 15:06:27","Stores Count":1}]
     */

    private StatusBean status;
    private List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean implements Serializable {
        /**
         * code : 200
         * message : Agents List
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean implements Serializable {
        /**
         * id : 12
         * name : Rishab
         * email : rishab@gmail.com
         * mobile : 8789160444
         * wa_mobile : 8789160555
         * password : 81dc9bdb52d04dc20036dbd8313ed055
         * status : 1
         * image : shop.jpg
         * date_time : 2020-04-09 16:47:32
         * Stores Count : 0
         */

        private String id;
        private String name;
        private String email;
        private String mobile;
        private String wa_mobile;
        private String password;
        private String status;
        private String image;
        private String date_time;
        @SerializedName("Stores Count")
        private int _$StoresCount91; // FIXME check this code

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getWa_mobile() {
            return wa_mobile;
        }

        public void setWa_mobile(String wa_mobile) {
            this.wa_mobile = wa_mobile;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getDate_time() {
            return date_time;
        }

        public void setDate_time(String date_time) {
            this.date_time = date_time;
        }

        public int get_$StoresCount91() {
            return _$StoresCount91;
        }

        public void set_$StoresCount91(int _$StoresCount91) {
            this._$StoresCount91 = _$StoresCount91;
        }
    }
}

package kashyap.chandan.medishop.admin;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;

public  class SharedPreferencesAdmin {
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    Context context;
    public SharedPreferencesAdmin(Context context){
        this.context=context;
        this.sharedPreferences= this.context.getSharedPreferences("Login",MODE_PRIVATE);
        this.editor=sharedPreferences.edit();
    }
    public void getSharedPreference(String uname,String pass,String phone,String id,String email)
    {
         editor.putString("username",uname);
        editor.putString("password",pass);
        editor.putString("phone",phone);
        editor.putString("id",id);
        editor.putString("email",email);
         editor.commit();
    }
    public void sessionEnd()
    {
        editor.clear();
        editor.commit();
    }
    public String getAdminNamePreferences()
    {
        String username=sharedPreferences.getString("username",null);
        return username ;
    }
    public String getAdminPhonePreferences()
    {
        String phone=sharedPreferences.getString("phone",null);
        return phone ;
    }
    public String getAdminIdPreferences()
    {
        String id=sharedPreferences.getString("id",null);
        return id ;
    }
    public String getAdminemailPreferences()
    {
        String email=sharedPreferences.getString("email",null);
        return email ;
    }
    public String getAdminPassPreferences()
    {
        String pass=sharedPreferences.getString("password",null);
        return pass ;
    }

}

package kashyap.chandan.medishop.admin;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import kashyap.chandan.medishop.ApiClient;
import kashyap.chandan.medishop.R;
import kashyap.chandan.medishop.UserInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

class EditAreaAdapter extends RecyclerView.Adapter<EditAreaAdapter.MyViewHolder> {
    Context context;
    List<AdminAllAreaResponse.DataBean> allArea;
    public EditAreaAdapter(Context context, List<AdminAllAreaResponse.DataBean> allArea) {
        this.context=context;
        this.allArea=allArea;
    }

    @NonNull
    @Override
    public EditAreaAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.allcity_custom_layout,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final EditAreaAdapter.MyViewHolder holder, int position) {
holder.tvarea.setText(allArea.get(position).getArea_name());
holder.tvcity.setText(allArea.get(position).getCity_name());

holder.deleteArea.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        final int pos=holder.getAdapterPosition();
        final  String areaid=allArea.get(pos).getId();
        final Dialog yesnoDialog=new Dialog(context);
        DisplayMetrics metrics=context.getResources().getDisplayMetrics();
        int width=metrics.widthPixels;

      yesnoDialog.setContentView(R.layout.yesnodialog);
        yesnoDialog.getWindow().setLayout(width, LinearLayout.LayoutParams.WRAP_CONTENT);
      TextView yes=yesnoDialog.findViewById(R.id.yes);
      TextView no=yesnoDialog.findViewById(R.id.no);
      yesnoDialog.show();
      yes.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View view) {
              final Dialog progressDialog=new Dialog(context);
              progressDialog.setContentView(R.layout.customdialog);
              progressDialog.setCancelable(false);
              progressDialog.show();
              UserInterface userInterface= ApiClient.getClient().create(UserInterface.class);
              Call<DeleteAreaResponse>call=userInterface.deletearea(areaid);
              call.enqueue(new Callback<DeleteAreaResponse>() {
                  @Override
                  public void onResponse(Call<DeleteAreaResponse> call, Response<DeleteAreaResponse> response) {
                      if (response.code()==200)
                      {
                          progressDialog.dismiss();
                          DeleteAreaResponse.StatusBean statusBean=response.body().getStatus();
                          Toast.makeText(context, ""+statusBean.getMessage(), Toast.LENGTH_SHORT).show();
                          removeAt(pos);
                          yesnoDialog.dismiss();
                      }
                      else if (response.code()!=200)
                      {
                          Toast.makeText(context, "Area Not Deleted", Toast.LENGTH_SHORT).show();
                          progressDialog.dismiss();
                          yesnoDialog.dismiss();

                      }
                  }

                  @Override
                  public void onFailure(Call<DeleteAreaResponse> call, Throwable t) {
                      Toast.makeText(context, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
                      yesnoDialog.dismiss();
                      progressDialog.dismiss();

                  }
              });
          }
      });
      no.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View view) {
              yesnoDialog.dismiss();
          }
      });
    }
});
holder.editArea.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        int pos=holder.getAdapterPosition();
        final Dialog dialog=new Dialog(context);
        dialog.setContentView(R.layout.dialogcityedit);
        DisplayMetrics metrics=context.getResources().getDisplayMetrics();
        int width = metrics.widthPixels;
        int height = metrics.heightPixels;
        dialog.getWindow().setLayout(width, LinearLayout.LayoutParams.WRAP_CONTENT);
      final   EditText darea= dialog.findViewById(R.id.dcityedit);
        darea.setText(allArea.get(pos).getArea_name());
        dialog.show();
      final   String areaid=allArea.get(pos).getId();
       final String cityId=allArea.get(pos).getCity_id();
       final String status=allArea.get(pos).getStatus();
        ImageView close=dialog.findViewById(R.id.closebtn);
        TextView update=dialog.findViewById(R.id.btnupdate);
update.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        if (darea.getText().toString().isEmpty()){
            Toast.makeText(context,"Enter Area",Toast.LENGTH_SHORT);
        }
        else {
            UserInterface userInterface= ApiClient.getClient().create(UserInterface.class);
            Call<EditAreaResponse>call=userInterface.updateArea(areaid,cityId,darea.getText().toString(),status);
            call.enqueue(new Callback<EditAreaResponse>() {
                @Override
                public void onResponse(Call<EditAreaResponse> call, Response<EditAreaResponse> response) {
                    if (response.code()==200)
                    { dialog.dismiss();
                        EditAreaResponse.StatusBean statusBean=response.body().getStatus();
                        Toast.makeText(context,""+statusBean.getMessage(),Toast.LENGTH_SHORT).show();
                        Intent intent=new Intent(context,AdminAllAreaList.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        context.startActivity(intent);
                        ((AdminAllAreaList)context).finish();
                    }
                    else if (response.code()!=200){
                        Toast.makeText(context,"Area Not Updated",Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<EditAreaResponse> call, Throwable t) {
                    Toast.makeText(context,""+t.getMessage(),Toast.LENGTH_SHORT).show();
                }
            });

        }


    }
});
        close.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               dialog.dismiss();
           }
       });
    }
});
    }

    @Override
    public int getItemCount() {
        return allArea.size();
    }
    private void removeAt(int position) {
        allArea.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, allArea.size());
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvarea,tvcity;
        ImageView editArea,deleteArea ;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            editArea=itemView.findViewById(R.id.editcityorarea);
            deleteArea=itemView.findViewById(R.id.deletecityarea);
            tvarea=itemView.findViewById(R.id.cityforarea);
            tvcity=itemView.findViewById(R.id.cityfromadmin);

        }
    }
}

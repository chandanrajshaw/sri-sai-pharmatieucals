package kashyap.chandan.medishop.admin;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import kashyap.chandan.medishop.CustomItemClickListener;
import kashyap.chandan.medishop.R;
import kashyap.chandan.medishop.pojoclasses.AdminCityList;

public class DistrictListAdapter extends RecyclerView.Adapter<DistrictListAdapter.MyViewHolder>{
    Context context;
    List<DistrictResponse.DataBean> allDistrict;
    private int mSelectedItem = -1;
    Dialog districtDialog;
    CustomItemClickListener listener;
    public DistrictListAdapter(Context context, List<DistrictResponse.DataBean> allDistrict,
                               Dialog districtDialog, CustomItemClickListener listener) {
        this.context=context;
        this.districtDialog=districtDialog;
        this.allDistrict=allDistrict;
        this.listener=listener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.itemlayout,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.radioselect.setChecked(position == mSelectedItem);
        holder.item.setText(allDistrict.get(position).getDistrict_name());
        TextView ok= districtDialog.findViewById(R.id.ok);
        TextView dialogheader=districtDialog.findViewById(R.id.dialoghead);
        dialogheader.setText("District");
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mSelectedItem==-1)
                { Toast.makeText(context, "Select District", Toast.LENGTH_SHORT).show(); }
                else
                { districtDialog.dismiss(); }
            }
        });
    }

    @Override
    public int getItemCount() {
        return allDistrict.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView item;
        RadioButton radioselect;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            item=itemView.findViewById(R.id.items);
            radioselect=itemView.findViewById(R.id.selectedItem);
            View.OnClickListener clickListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mSelectedItem = getAdapterPosition();
                    notifyDataSetChanged();
                    listener.onItemClick(v, Integer.parseInt(allDistrict.get(mSelectedItem).getId()),allDistrict.get(mSelectedItem).getDistrict_name());
                }

            };
            itemView.setOnClickListener(clickListener);
            radioselect.setOnClickListener(clickListener);
        }
        }
    }


package kashyap.chandan.medishop.admin;

import androidx.appcompat.app.AppCompatActivity;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import kashyap.chandan.medishop.ApiClient;
import kashyap.chandan.medishop.ConnectionDetector;
import kashyap.chandan.medishop.R;
import kashyap.chandan.medishop.UserInterface;
import kashyap.chandan.medishop.agentPannel.AgentAllShopResponse;
import kashyap.chandan.medishop.agentPannel.UserAllShopAdapter;
import kashyap.chandan.medishop.agentPannel.UserDashBoard;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AdminUsersDetail extends AppCompatActivity {

ImageView userdetailgobackarrow,user;
TextView tvdagentame,dagentemail,dagentmob,ddate,shopnotfound;
ConnectionDetector connectionDetector;
RecyclerView agentshops;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

   setContentView(R.layout.activity_admin_users_detail);
        Intent i=getIntent();
        Bundle bundle=i.getBundleExtra("detail");
         AgentListResponse.DataBean dataBean = (AgentListResponse.DataBean) bundle.getSerializable("agent");
         user=findViewById(R.id.user);
      connectionDetector=new ConnectionDetector(AdminUsersDetail.this);
        userdetailgobackarrow=findViewById(R.id.userdetailgobackarrow);
      userdetailgobackarrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                overridePendingTransition(R.anim.transitionrighttoleft, R.anim.transitionrighttoleft);
                finish();
            }
        });
        Picasso.get().load("http://www.sbpharma.in/admin_assets/uploads/agents/"+dataBean.getImage()).error(R.drawable.cross).placeholder(R.drawable.loading).into(user);

        tvdagentame=findViewById(R.id.tvdagentame);
     tvdagentame.setText(dataBean.getName());
        dagentemail=findViewById(R.id.dagentemail);
  dagentemail.setText(dataBean.getEmail());
        dagentmob=findViewById(R.id.dagentmob);
       dagentmob.setText(dataBean.getMobile());
        ddate=findViewById(R.id.ddate);
      ddate.setText(dataBean.getDate_time());
        agentshops=findViewById(R.id.agentshops);
        shopnotfound=findViewById(R.id.shopnotfound);
//        adminusersDetailBinding.agentshops
        if (!connectionDetector.isConnectingToInternet()){
      agentshops.setVisibility(View.GONE);
       shopnotfound.setVisibility(View.VISIBLE);
            Toast.makeText(this, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
        else {
            final Dialog progressDialog=new Dialog(AdminUsersDetail.this);
            progressDialog.setContentView(R.layout.customdialog);
            progressDialog.setCancelable(false);
            progressDialog.show();
            UserInterface userInterface= ApiClient.getClient().create(UserInterface.class);
            Call<AgentAllShopResponse>call=userInterface.getAllAgentShop(dataBean.getId());
            call.enqueue(new Callback<AgentAllShopResponse>() {
                @Override
                public void onResponse(Call<AgentAllShopResponse> call, Response<AgentAllShopResponse> response) {
                    if (response.code()==200){

                        List<AgentAllShopResponse.DataBean> dataBeanList=new ArrayList<AgentAllShopResponse.DataBean>();
                        dataBeanList=response.body().getData();
                   agentshops.setLayoutManager(new LinearLayoutManager(AdminUsersDetail.this,LinearLayoutManager.VERTICAL,false));
                  agentshops.setAdapter(new UserAllShopAdapteradmin(AdminUsersDetail.this,dataBeanList));
                        progressDialog.dismiss();
                    }
                    else if (response.code()!=200)
                    {
                        Toast.makeText(AdminUsersDetail.this, "NO Shops found for this user", Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();

                    }
                }

                @Override
                public void onFailure(Call<AgentAllShopResponse> call, Throwable t) {
                    Toast.makeText(AdminUsersDetail.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }
    }
}

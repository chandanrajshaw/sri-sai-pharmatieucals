package kashyap.chandan.medishop.admin;

import androidx.appcompat.app.AppCompatActivity;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.List;

import kashyap.chandan.medishop.ApiClient;
import kashyap.chandan.medishop.CustomItemClickListener;
import kashyap.chandan.medishop.R;
import kashyap.chandan.medishop.UserInterface;

import kashyap.chandan.medishop.pojoclasses.AdminAddAreaResponse;
import kashyap.chandan.medishop.pojoclasses.AdminCityList;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddArea extends AppCompatActivity {

    ImageView addareagobackarrow;
    RelativeLayout areacitylist,stateList,districtList;
    List<AdminCityList.DataBean> allCityList=new ArrayList<AdminCityList.DataBean>();
    List<StateResponse.DataBean>allStateList=new ArrayList<>();
    List<DistrictResponse.DataBean>allDistrict=new ArrayList<>();
    RecyclerView cityRecycler,stateRecycler,districtRecycler;
    TextView areacity,areacityid,btnaddarea,state,district;
    EditText adminaddarea;
    String stateId,districtId;
    private Dialog progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_add_area);
        init();
        addareagobackarrow.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               overridePendingTransition(R.anim.transitionrighttoleft, R.anim.transitionrighttoleft);
               finish();
           }
       });
        stateList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                statelist();
            }
        });
        districtList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (stateId==null||stateId.isEmpty())
                {
                    Toast.makeText(AddArea.this, "Select State", Toast.LENGTH_SHORT).show();
                }
                else {
                    districtlist();
                }
            }
        });
areacitylist.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {

      if (districtId==null||districtId.isEmpty())
      {
          Toast.makeText(AddArea.this, "Select District First", Toast.LENGTH_SHORT).show();
      }
      else
      {
          final Dialog progressDialog = new Dialog(AddArea.this);
          progressDialog.setContentView(R.layout.customdialog);
          progressDialog.setCancelable(false);
          progressDialog.show();
          UserInterface userInterface= ApiClient.getClient().create(UserInterface.class);
          Call<AdminCityList> call=userInterface.getCityList(districtId);
          call.enqueue(new Callback<AdminCityList>() {
              @Override
              public void onResponse(Call<AdminCityList> call, Response<AdminCityList> response) {
                  if (response.code()==200)
                  {
                      allCityList=response.body().getData();
                      final  Dialog citydialog=new Dialog(AddArea.this);
                      citydialog.setContentView(R.layout.recyclerdialog);
                      DisplayMetrics metrics=getResources().getDisplayMetrics();
                      int width=metrics.widthPixels;
                      citydialog.getWindow().setLayout(width, LinearLayout.LayoutParams.WRAP_CONTENT);
                      cityRecycler=citydialog.findViewById(R.id.dialogRecycler);
                      cityRecycler.setLayoutManager(new LinearLayoutManager(AddArea.this,LinearLayoutManager.VERTICAL,false));
                      cityRecycler.setAdapter(new AdminCityListAdapter(AddArea.this,allCityList,areacity,citydialog,areacityid));
                      progressDialog.dismiss();
                      citydialog.show();

                  }
                  else   if (response.code()!=200){
                      progressDialog.dismiss();
                      Toast.makeText(AddArea.this, "Something Goes Wrong", Toast.LENGTH_SHORT).show();
                  }

              }

              @Override
              public void onFailure(Call<AdminCityList> call, Throwable t) {
                  Snackbar.make(AddArea.this.getWindow().getDecorView().findViewById(android.R.id.content), ""+t.getMessage(), Snackbar.LENGTH_SHORT).show();

              }
          });
      }
    }
});
        btnaddarea=findViewById(R.id.btnaddarea);
btnaddarea.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {

        String cityId=areacityid.getText().toString();
        String area=adminaddarea.getText().toString();
        if (cityId.isEmpty()&& area.isEmpty())
            Toast.makeText(AddArea.this, "Select City and Enter Area", Toast.LENGTH_SHORT).show();
       else if (area.isEmpty())
            Toast.makeText(AddArea.this, "Enter Area", Toast.LENGTH_SHORT).show();
            else {
            final Dialog progressDialog = new Dialog(AddArea.this);
            progressDialog.setContentView(R.layout.customdialog);
            progressDialog.setCancelable(false);
            progressDialog.show();
UserInterface userInterface=ApiClient.getClient().create(UserInterface.class);
Call<AdminAddAreaResponse>call=userInterface.addArea(cityId,area);
call.enqueue(new Callback<AdminAddAreaResponse>() {
    @Override
    public void onResponse(Call<AdminAddAreaResponse> call, Response<AdminAddAreaResponse> response) {
        if (response.code()==200){
            progressDialog.dismiss();
AdminAddAreaResponse.DataBean dataBean=response.body().getData();
            Toast.makeText(AddArea.this, dataBean.getArea_name()+"Added Successfully", Toast.LENGTH_SHORT).show();
            Intent intent=new Intent(AddArea.this,AdminAllAreaList.class);
            overridePendingTransition(R.anim.transitionrighttoleft, R.anim.transitionrighttoleft);
            startActivity(intent);

            finish();

        }
       else if (response.code()!=200){
           progressDialog.dismiss();
            Toast.makeText(AddArea.this, "Already Exists", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onFailure(Call<AdminAddAreaResponse> call, Throwable t) {
progressDialog.dismiss();
        Toast.makeText(AddArea.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
    }
});
        }
    }
});
    }
    private void statelist()
    {
        stateId="";
        state.setText("");
        progress = new Dialog(AddArea.this);
        progress.setContentView(R.layout.customdialog);
        progress.setCancelable(false);
        progress.show();
        UserInterface userInterface= ApiClient.getClient().create(UserInterface.class);
        /*--------------------------------------*/
        Call<StateResponse>call=userInterface.stateList();
        call.enqueue(new Callback<StateResponse>() {
            @Override
            public void onResponse(Call<StateResponse> call, Response<StateResponse> response) {
                if (response.code()==200)
                {
                    allStateList=response.body().getData();
                    final  Dialog stateDialog=new Dialog(AddArea.this);
                    stateDialog.setContentView(R.layout.recyclerdialog);
                    DisplayMetrics metrics=getResources().getDisplayMetrics();
                    int width=metrics.widthPixels;
                    stateDialog.getWindow().setLayout(width, LinearLayout.LayoutParams.WRAP_CONTENT);
                    stateRecycler=stateDialog.findViewById(R.id.dialogRecycler);
                    stateRecycler.setLayoutManager(new LinearLayoutManager(AddArea.this,LinearLayoutManager.VERTICAL,false));
                    stateRecycler.setAdapter(new StateListAdapter(AddArea.this,allStateList,stateDialog, new CustomItemClickListener() {
                        @Override
                        public void onItemClick(View v, int position, String value) {
state.setText(value);
stateId=String.valueOf(position);
                        }
                    }));
                    progress.dismiss();
                    stateDialog.show();
                }
                else
                {
                    progress.dismiss();
                    Toast.makeText(AddArea.this, "Something Goes Wrong", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<StateResponse> call, Throwable t) {
                progress.dismiss();
                Toast.makeText(AddArea.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }
    private void districtlist()
    {
        districtId="";
        progress = new Dialog(AddArea.this);
        progress.setContentView(R.layout.customdialog);
        progress.setCancelable(false);
        progress.show();
        UserInterface userInterface= ApiClient.getClient().create(UserInterface.class);
        Call<DistrictResponse>call=userInterface.getDistrict(stateId);
        call.enqueue(new Callback<DistrictResponse>() {
            @Override
            public void onResponse(Call<DistrictResponse> call, Response<DistrictResponse> response) {
                if (response.code()==200)
                {
                    allDistrict=response.body().getData();
                    final  Dialog districtDialog=new Dialog(AddArea.this);
                    districtDialog.setContentView(R.layout.recyclerdialog);
                    DisplayMetrics metrics=getResources().getDisplayMetrics();
                    int width=metrics.widthPixels;
                    districtDialog.getWindow().setLayout(width, LinearLayout.LayoutParams.WRAP_CONTENT);
                    districtRecycler=districtDialog.findViewById(R.id.dialogRecycler);
                    districtRecycler.setLayoutManager(new LinearLayoutManager(AddArea.this,LinearLayoutManager.VERTICAL,false));
        districtRecycler.setAdapter(new DistrictListAdapter(AddArea.this,allDistrict,districtDialog, new CustomItemClickListener() {
            @Override
            public void onItemClick(View v, int position, String value) {
district.setText(value);
districtId=String.valueOf(position);
            }
        }));
                    progress.dismiss();
                    districtDialog.show();
                }
                else {
                    progress.dismiss();
                    Toast.makeText(AddArea.this, "Something Goes Wrong", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<DistrictResponse> call, Throwable t) {
                progress.dismiss();
                Toast.makeText(AddArea.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });




    }
    private void init() {
        addareagobackarrow=findViewById(R.id.addareagobackarrow);
        stateList=findViewById(R.id.stateList);
        districtList=findViewById(R.id.districtList);
        state=findViewById(R.id.state);
        district=findViewById(R.id.district);
        areacity=findViewById(R.id.areacity);
        areacityid=findViewById(R.id.areacityid);
        areacitylist=findViewById(R.id.areacitylist);
        adminaddarea=findViewById(R.id.adminaddarea);
    }
}

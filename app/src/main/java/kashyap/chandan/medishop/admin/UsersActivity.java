package kashyap.chandan.medishop.admin;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.shimmer.ShimmerFrameLayout;

import java.util.ArrayList;
import java.util.List;

import kashyap.chandan.medishop.ApiClient;
import kashyap.chandan.medishop.R;
import kashyap.chandan.medishop.UserInterface;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UsersActivity extends AppCompatActivity {
    ShimmerFrameLayout shimmerFrameLayout;
    ImageView allusergobackarrow,Addusersadmin;
    RecyclerView useractivityrecycler;
    TextView nouser;
    List<AgentListResponse.DataBean> allAgentList=new ArrayList<AgentListResponse.DataBean>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
setContentView(R.layout.activity_users);
        final Dialog progressDialog = new Dialog(UsersActivity.this);
        shimmerFrameLayout=findViewById(R.id.shimmerview);
        useractivityrecycler=findViewById(R.id.useractivityrecycler);
        Addusersadmin=findViewById(R.id.Addusersadmin);
        nouser=findViewById(R.id.nouser);
//        progressDialog.setContentView(R.layout.customdialog);
//        progressDialog.setCancelable(false);
//        progressDialog.show();
        allusergobackarrow=findViewById(R.id.allusergobackarrow);
    useractivityrecycler.removeAllViews();
        UserInterface userInterface= ApiClient.getClient().create(UserInterface.class);
        Call<AgentListResponse> call=userInterface.getAllAgentAdmin();
        call.enqueue(new Callback<AgentListResponse>() {
            @Override
            public void onResponse(Call<AgentListResponse> call, Response<AgentListResponse> response) {
                if (response.code()==200){
//                    progressDialog.dismiss();
                    shimmerFrameLayout.stopShimmer();
                    shimmerFrameLayout.setVisibility(View.GONE);
                    allAgentList=response.body().getData();
                    useractivityrecycler.setLayoutManager(new LinearLayoutManager(UsersActivity.this, LinearLayoutManager.VERTICAL,false));
       useractivityrecycler.setAdapter(new AgentListAdapter(UsersActivity.this,allAgentList));
                  useractivityrecycler.setVisibility(View.VISIBLE);
                }
                else if (response.code()!=200){
                    shimmerFrameLayout.stopShimmer();
                    shimmerFrameLayout.setVisibility(View.GONE);
useractivityrecycler.setVisibility(View.GONE);
nouser.setVisibility(View.VISIBLE);
                    Toast.makeText(UsersActivity.this, "Error Occurs!!No Users Found", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<AgentListResponse> call, Throwable t) {
                Toast.makeText(UsersActivity.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
allusergobackarrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent gobackarrowIntent=new Intent(UsersActivity.this,DashBoard.class);
                gobackarrowIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(gobackarrowIntent);
                overridePendingTransition(R.anim.transitionrighttoleft, R.anim.transitionrighttoleft);
                finish();
            }
        });
Addusersadmin.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        Intent intent=new Intent(UsersActivity.this,AddUser.class);
        startActivity(intent);
    }
});
    }
    @Override
    protected void onResume() {
        super.onResume();
        shimmerFrameLayout.startShimmer();
    }

    @Override
    protected void onPause() {
        super.onPause();
        shimmerFrameLayout.stopShimmer();
    }
}

package kashyap.chandan.medishop.admin;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.List;

import kashyap.chandan.medishop.ApiClient;
import kashyap.chandan.medishop.ConnectionDetector;
import kashyap.chandan.medishop.CustomItemClickListener;
import kashyap.chandan.medishop.R;
import kashyap.chandan.medishop.UserInterface;

import kashyap.chandan.medishop.pojoclasses.AddCityResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddCity extends AppCompatActivity {
ImageView  addcitygobackarrow;
TextView btnaddcity,state,district;
ConnectionDetector connectionDetector;
EditText adminaddcity;
String stateId,districtId;
RelativeLayout stateList,districtList;
Dialog progress;
RecyclerView stateRecycler,districtRecycler;
    List<StateResponse.DataBean> allStateList=new ArrayList<>();
    List<DistrictResponse.DataBean>allDistrict=new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
      setContentView(R.layout.activity_add_city);
        connectionDetector=new ConnectionDetector(AddCity.this);
        addcitygobackarrow=findViewById(R.id.addcitygobackarrow);
        districtList=findViewById(R.id.districtList);
        stateList=findViewById(R.id.stateList);
        stateList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                statelist();
            }
        });
        districtList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (stateId==null||stateId.isEmpty())
                { Toast.makeText(AddCity.this, "Select State First", Toast.LENGTH_SHORT).show(); }
                else {
                    districtlist();
                }
            }
        });
        state=findViewById(R.id.state);
        district=findViewById(R.id.district);
   addcitygobackarrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                overridePendingTransition(R.anim.transitionrighttoleft, R.anim.transitionrighttoleft);
                finish();
            }
        });
        btnaddcity=findViewById(R.id.btnaddcity);
        adminaddcity=findViewById(R.id.adminaddcity);
     btnaddcity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (connectionDetector.isConnectingToInternet()){
                    String cityname=adminaddcity.getText().toString();
                     if (districtId==null&&districtId.isEmpty())
                    {
                        Snackbar.make(AddCity.this.getWindow().getDecorView().findViewById(android.R.id.content), "Please Select District", Snackbar.LENGTH_SHORT).show();
                    }
                 else if (cityname.isEmpty())
                        Snackbar.make(AddCity.this.getWindow().getDecorView().findViewById(android.R.id.content), "Please Enter City Name", Snackbar.LENGTH_SHORT).show();

                    else {
                         final Dialog progressDialog = new Dialog(AddCity.this);
                         progressDialog.setContentView(R.layout.customdialog);
                         progressDialog.setCancelable(false);
                         progressDialog.show();
                        UserInterface userInterface= ApiClient.getClient().create(UserInterface.class);
                        Call<AddCityResponse>call=userInterface.addCity(districtId,cityname);
                        call.enqueue(new Callback<AddCityResponse>() {
                            @Override
                            public void onResponse(Call<AddCityResponse> call, Response<AddCityResponse> response) {
                                if (response.code()==200)
                                {
                                    progressDialog.dismiss();
                                   AddCityResponse.DataBean  dataBean=response.body().getData();
                                    //Snackbar.make(AddCity.this.getWindow().getDecorView().findViewById(android.R.id.content), dataBean.getCity_name()+" is added to city List", Snackbar.LENGTH_SHORT).show();
                                    Toast.makeText(AddCity.this, dataBean.getCity_name()+"Added Successfully", Toast.LENGTH_SHORT).show();
                                    Intent intent=new Intent(AddCity.this,AdminAllCityList.class);
                                    startActivity(intent);
                                }
                          else   if (response.code()!=200){
                              progressDialog.dismiss();
                                Snackbar.make(AddCity.this.getWindow().getDecorView().findViewById(android.R.id.content), "City Already Exists", Snackbar.LENGTH_SHORT).show();
                            }
                            }

                            @Override
                            public void onFailure(Call<AddCityResponse> call, Throwable t) {

                                Snackbar.make(AddCity.this.getWindow().getDecorView().findViewById(android.R.id.content), ""+t.getMessage(), Snackbar.LENGTH_SHORT).show();
                            }
                        });
                    }
                }
               else if (!connectionDetector.isConnectingToInternet()){
                    Snackbar.make(AddCity.this.getWindow().getDecorView().findViewById(android.R.id.content), "Please connect to internet", Snackbar.LENGTH_SHORT).show();

                }
            }
        });
    }
    private void statelist()
    {
        stateId="";
        progress = new Dialog(AddCity.this);
        progress.setContentView(R.layout.customdialog);
        progress.setCancelable(false);
        progress.show();
        UserInterface userInterface= ApiClient.getClient().create(UserInterface.class);
        /*--------------------------------------*/
        Call<StateResponse>call=userInterface.stateList();
        call.enqueue(new Callback<StateResponse>() {
            @Override
            public void onResponse(Call<StateResponse> call, Response<StateResponse> response) {
                if (response.code()==200)
                {
                    allStateList=response.body().getData();
                    final  Dialog stateDialog=new Dialog(AddCity.this);
                    stateDialog.setContentView(R.layout.recyclerdialog);
                    DisplayMetrics metrics=getResources().getDisplayMetrics();
                    int width=metrics.widthPixels;
                    stateDialog.getWindow().setLayout(width, LinearLayout.LayoutParams.WRAP_CONTENT);
                    stateRecycler=stateDialog.findViewById(R.id.dialogRecycler);
                    stateRecycler.setLayoutManager(new LinearLayoutManager(AddCity.this,LinearLayoutManager.VERTICAL,false));
                    stateRecycler.setAdapter(new StateListAdapter(AddCity.this,allStateList,stateDialog, new CustomItemClickListener() {
                        @Override
                        public void onItemClick(View v, int position, String value) {
                            state.setText(value);
                            stateId=String.valueOf(position);
                        }
                    }));
                    progress.dismiss();
                    stateDialog.show();
                }
                else
                {
                    progress.dismiss();
                    Toast.makeText(AddCity.this, "Something Goes Wrong", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<StateResponse> call, Throwable t) {
                progress.dismiss();
                Toast.makeText(AddCity.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }
    private void districtlist()
    {
        districtId="";
        progress = new Dialog(AddCity.this);
        progress.setContentView(R.layout.customdialog);
        progress.setCancelable(false);
        progress.show();
        UserInterface userInterface= ApiClient.getClient().create(UserInterface.class);
        Call<DistrictResponse>call=userInterface.getDistrict(stateId);
        call.enqueue(new Callback<DistrictResponse>() {
            @Override
            public void onResponse(Call<DistrictResponse> call, Response<DistrictResponse> response) {
                if (response.code()==200)
                {
                    allDistrict=response.body().getData();
                    final  Dialog districtDialog=new Dialog(AddCity.this);
                    districtDialog.setContentView(R.layout.recyclerdialog);
                    DisplayMetrics metrics=getResources().getDisplayMetrics();
                    int width=metrics.widthPixels;
                    districtDialog.getWindow().setLayout(width, LinearLayout.LayoutParams.WRAP_CONTENT);
                    districtRecycler=districtDialog.findViewById(R.id.dialogRecycler);
                    districtRecycler.setLayoutManager(new LinearLayoutManager(AddCity.this,LinearLayoutManager.VERTICAL,false));
                    districtRecycler.setAdapter(new DistrictListAdapter(AddCity.this,allDistrict,districtDialog, new CustomItemClickListener() {
                        @Override
                        public void onItemClick(View v, int position, String value) {
                            district.setText(value);
                            districtId=String.valueOf(position);
                        }
                    }));
                    progress.dismiss();
                    districtDialog.show();
                }
                else {
                    progress.dismiss();
                    Toast.makeText(AddCity.this, "Something Goes Wrong", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<DistrictResponse> call, Throwable t) {
                progress.dismiss();
                Toast.makeText(AddCity.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });




    }
}

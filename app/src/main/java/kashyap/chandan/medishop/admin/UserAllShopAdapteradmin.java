package kashyap.chandan.medishop.admin;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.TooltipCompat;
import androidx.recyclerview.widget.RecyclerView;

import java.io.Serializable;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import kashyap.chandan.medishop.R;
import kashyap.chandan.medishop.agentPannel.AgentAllShopResponse;


public class UserAllShopAdapteradmin extends RecyclerView.Adapter<UserAllShopAdapteradmin.MyViewHolder> {
   Context context;
   List<AgentAllShopResponse.DataBean> agentAllShop;
   public UserAllShopAdapteradmin(Context context, List<AgentAllShopResponse.DataBean> agentAllShop) {
       this.context=context;
       this.agentAllShop=agentAllShop;
   }

   @NonNull
   @Override
   public UserAllShopAdapteradmin.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
       View view= LayoutInflater.from(context).inflate(R.layout.shoplist,parent,false);
       return   new MyViewHolder(view);
   }

   @Override
   public void onBindViewHolder(@NonNull final UserAllShopAdapteradmin.MyViewHolder holder, final int position) {
//       holder.shopcity.setText(agentAllShop.get(position).getCity_name());
       holder.shoparea.setText(agentAllShop.get(position).getArea_name()+","+agentAllShop.get(position).getCity_name()+","+agentAllShop.get(position).getDistrict_name()+","+agentAllShop.get(position).getState_name());
       holder.shopphone.setText(agentAllShop.get(position).getMobile());
       holder.shopname.setText(agentAllShop.get(position).getStore_name());
       holder.agentid.setText(agentAllShop.get(position).getAgent_id());
       holder.shopdate.setText(agentAllShop.get(position).getDate_time());
       holder.agentname.setVisibility(View.GONE);
       holder.tielogo.setVisibility(View.GONE);
//       holder.deleteshop.setVisibility(View.GONE);
       //       holder.shopid.setText(agentAllShop.get(position).getId());

       holder.ownername.setText(agentAllShop.get(position).getPerson_name());
       holder.imgviewmap.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               String geoUri = "http://maps.google.com/maps?q=loc:" + Double.valueOf(agentAllShop.get(position).getLatitude()) + "," + Double.valueOf(agentAllShop.get(position).getLongitude()) + " Shop";
               Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(geoUri));
               context.startActivity(intent);
           }
       });
       holder.shopdetail.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               int pos=holder.getAdapterPosition();
               AgentAllShopResponse.DataBean data=agentAllShop.get(pos);
               Intent intent=new Intent(context, AminViewUserShopDetail.class);
               intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
               Bundle bundle=new Bundle();
               bundle.putSerializable("editShop", (Serializable) data);
               intent.putExtra("edit",bundle);
               context.startActivity(intent);

           }
       });
holder.call.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        int pos=holder.getAdapterPosition();
        String mob=agentAllShop.get(pos).getMobile();
        Uri u = Uri.fromParts("tel",mob,null);
        Intent i = new Intent(Intent.ACTION_DIAL, u);
        context.startActivity(i);
    }
});
   }



   private void removeAt(int position) {
       agentAllShop.remove(position);
       notifyItemRemoved(position);
       notifyItemRangeChanged(position, agentAllShop.size());
   }

   @Override
   public int getItemCount() {
       return agentAllShop.size() ;
   }

   public class MyViewHolder extends RecyclerView.ViewHolder {
       TextView shopname,shopdate,agentid,shopphone,agentname,shoparea,ownername;
       CircleImageView imgviewmap,call;
    LinearLayout shopdetail;
    ImageView tielogo;
       public MyViewHolder(@NonNull View itemView) {
           super(itemView);
           shopname=itemView.findViewById(R.id.shopname);
           shopdate=itemView.findViewById(R.id.shopdate);
           agentid=itemView.findViewById(R.id.agentid);
           shopphone=itemView.findViewById(R.id.shopphone);
           imgviewmap=itemView.findViewById(R.id.viewinmap);
           agentname=itemView.findViewById(R.id.agentname);
           shoparea=itemView.findViewById(R.id.shoparea);
           ownername=itemView.findViewById(R.id.ownername);
           shopdetail=itemView.findViewById(R.id.detail);
           call=itemView.findViewById(R.id.call);
           tielogo=itemView.findViewById(R.id.tielogo);
       }
   }
}

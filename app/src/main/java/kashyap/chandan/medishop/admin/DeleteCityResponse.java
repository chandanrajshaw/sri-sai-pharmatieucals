package kashyap.chandan.medishop.admin;

public class DeleteCityResponse {
    /**
     * status : {"code":200,"message":"City Data Deleted Successfully"}
     */

    private StatusBean status;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : City Data Deleted Successfully
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    /**
     * status : 0
     * message : Sorry there is a Dependiencies of this City in Area
     */


}

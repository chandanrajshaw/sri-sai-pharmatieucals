package kashyap.chandan.medishop.admin;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;

import java.util.ArrayList;
import java.util.List;

import kashyap.chandan.medishop.ApiClient;
import kashyap.chandan.medishop.CustomItemClickListener;
import kashyap.chandan.medishop.R;
import kashyap.chandan.medishop.UserInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddDistrict extends AppCompatActivity {
TextView btnAddDistrict,state, stateid;
TextInputEditText etdistrict;
RelativeLayout stateList;
String stateId;
RecyclerView stateRecycler;
List<StateResponse.DataBean>allStateList;
    private Dialog progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_add_district);
        init();
        stateList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Statelist();
            }
        });
        btnAddDistrict.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              if (stateId==null||stateId.isEmpty())
                  Toast.makeText(AddDistrict.this, "Select State", Toast.LENGTH_SHORT).show();
              else
              {
                  AddDistrict();
              }
            }
        });
    }

    private void init() {
        allStateList=new ArrayList<>();
        stateid =findViewById(R.id.stateId);
        state=findViewById(R.id.state);
        btnAddDistrict=findViewById(R.id.btnAddDistrict);
        stateList=findViewById(R.id.stateList);
        etdistrict=findViewById(R.id.etdistrict);
    }
    private void Statelist()
    {
        stateId ="";
        progress = new Dialog(AddDistrict.this);
        progress.setContentView(R.layout.customdialog);
        progress.setCancelable(false);
        progress.show();
        UserInterface userInterface= ApiClient.getClient().create(UserInterface.class);
        /*--------------------------------------*/
        Call<StateResponse> call=userInterface.stateList();
        call.enqueue(new Callback<StateResponse>() {
            @Override
            public void onResponse(Call<StateResponse> call, Response<StateResponse> response) {
                if (response.code()==200)
                {
                    allStateList=response.body().getData();
                    final  Dialog stateDialog=new Dialog(AddDistrict.this);
                    stateDialog.setContentView(R.layout.recyclerdialog);
                    DisplayMetrics metrics=getResources().getDisplayMetrics();
                    int width=metrics.widthPixels;
                    stateDialog.getWindow().setLayout(width, LinearLayout.LayoutParams.WRAP_CONTENT);
                    stateRecycler=stateDialog.findViewById(R.id.dialogRecycler);
                    stateRecycler.setLayoutManager(new LinearLayoutManager(AddDistrict.this,LinearLayoutManager.VERTICAL,false));
                    stateRecycler.setAdapter(new StateListAdapter(AddDistrict.this,allStateList,stateDialog, new CustomItemClickListener() {
                        @Override
                        public void onItemClick(View v, int position, String value) {
                            state.setText(value);
                            stateId =String.valueOf(position);
                        }
                    }));
                    progress.dismiss();
                    stateDialog.show();
                }
                else
                {
                    progress.dismiss();
                    Toast.makeText(AddDistrict.this, "Something Goes Wrong", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<StateResponse> call, Throwable t) {
                progress.dismiss();
                Toast.makeText(AddDistrict.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }
    private  void AddDistrict()
    {
        String districtName=etdistrict.getText().toString();
        if (stateId==null||stateId.isEmpty())
            Toast.makeText(this, "Select State", Toast.LENGTH_SHORT).show();
        else if (districtName.isEmpty())
            Toast.makeText(this, "Enter District Name", Toast.LENGTH_SHORT).show();
        else
        {
            progress = new Dialog(AddDistrict.this);
            progress.setContentView(R.layout.customdialog);
            progress.setCancelable(false);
            progress.show();
            UserInterface userInterface= ApiClient.getClient().create(UserInterface.class);
            /*--------------------------------------*/
            Call<AddDistrictResponse>call=userInterface.addDistrict(stateId,districtName);
            call.enqueue(new Callback<AddDistrictResponse>() {
                @Override
                public void onResponse(Call<AddDistrictResponse> call, Response<AddDistrictResponse> response) {
                    if (response.code()==200)
                    {
                        progress.dismiss();
                        Toast.makeText(AddDistrict.this, "District Added", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                    else if (response.code()==409)
                    {
                        progress.dismiss();
                        Toast.makeText(AddDistrict.this, "District Already Exists", Toast.LENGTH_SHORT).show();
                    }
                    else if (response.code()!=200&&response.code()!=409){
                        progress.dismiss();
                        Toast.makeText(AddDistrict.this, "District Not Added", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<AddDistrictResponse> call, Throwable t) {
progress.dismiss();
                    Toast.makeText(AddDistrict.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }

    }
}
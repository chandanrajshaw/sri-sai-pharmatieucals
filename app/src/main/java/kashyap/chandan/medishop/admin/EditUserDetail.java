package kashyap.chandan.medishop.admin;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;


import android.Manifest;
import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import kashyap.chandan.medishop.ApiClient;
import kashyap.chandan.medishop.LocationTrack;
import kashyap.chandan.medishop.R;
import kashyap.chandan.medishop.UserInterface;

import kashyap.chandan.medishop.client.EditClientProfile;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditUserDetail extends AppCompatActivity {
    private final static int CAMERA_ALL_PERMISSIONS_RESULT = 108;
    Bitmap bitmap,converetdImage;
    Dialog dialog;
    String picturePath,imagePic;
    File image=null;
    LocationTrack locationTrack;
    Uri picUri;
    private ArrayList camerapermissions = new ArrayList();
    public static final int PERMISSION_ID = 44;
TextInputEditText editusermail,editusermob,editusername,etwhatsapp;
ImageView edituserbackarrow,edituserImage;
TextView useredit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
      setContentView(R.layout.activity_edit_user_detail);
        final AgentListResponse.DataBean agentdata= (AgentListResponse.DataBean) getIntent().getBundleExtra("detail").getSerializable("agent");
        edituserbackarrow=findViewById(R.id.edituserbackarrow);
        edituserImage=findViewById(R.id.edituserImage);
        Picasso.get().load("http://www.sbpharma.in/admin_assets/uploads/agents/"+agentdata.getImage()).error(R.mipmap.ic_launcher_launcher_round).placeholder(R.drawable.loading).into(edituserImage);

        edituserbackarrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                overridePendingTransition(R.anim.transitionrighttoleft, R.anim.transitionrighttoleft);
                finish();
            }
        });
        String agentid=agentdata.getId();

        editusermail=findViewById(R.id.editusermail);
    editusermail.setText(agentdata.getEmail());
        editusermob=findViewById(R.id.editusermob);
    editusermob.setText(agentdata.getMobile());
        editusername=findViewById(R.id.editusername);
       editusername.setText(agentdata.getName());
        etwhatsapp=findViewById(R.id.etwhatsapp);
       etwhatsapp.setText(agentdata.getWa_mobile());
        useredit=findViewById(R.id.useredit);
      useredit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name=editusername.getText().toString();
                String mob=editusermob.getText().toString();
                String email=editusermail.getText().toString();
                String wtsp=etwhatsapp.getText().toString();
                if (name.isEmpty()&& email.isEmpty()&& mob.isEmpty() && wtsp.isEmpty())
                Toast.makeText(EditUserDetail.this,"Please Enter All the fields",Toast.LENGTH_SHORT).show();
                else if (name.isEmpty())
                    Toast.makeText(EditUserDetail.this,"Please Enter Agent Name",Toast.LENGTH_SHORT).show();
                else if (mob.isEmpty() || mob.length()!=10)
                    Toast.makeText(EditUserDetail.this,"Please Enter Valid Mobile Number",Toast.LENGTH_SHORT).show();
                else if (email.isEmpty())
                    Toast.makeText(EditUserDetail.this,"Please Enter Email",Toast.LENGTH_SHORT).show();
                else if (wtsp.isEmpty())
                    Toast.makeText(EditUserDetail.this,"Please Enter Whatsapp Contact",Toast.LENGTH_SHORT).show();

                else{
                    final Dialog progress=new Dialog(EditUserDetail.this);
                    progress.setContentView(R.layout.customdialog);
                    progress.setCancelable(false);
                    progress.show();

                    /*-----------------------------------------multipart-------------------*/
                    MultipartBody.Part body=null;
                    if (image != null) {
                        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), image);
                        body = MultipartBody.Part.createFormData("image", image.getName(), requestFile);
                    } else {

                        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), "");
                        body = MultipartBody.Part.createFormData("image", "", requestFile);
                    }
                    RequestBody uid = RequestBody.create(MediaType.parse("multipart/form-data"), agentdata.getId());
                    RequestBody uname = RequestBody.create(MediaType.parse("multipart/form-data"), name);
                    RequestBody umob = RequestBody.create(MediaType.parse("multipart/form-data"), mob);
                    RequestBody uemail = RequestBody.create(MediaType.parse("multipart/form-data"), email);
                    RequestBody uwtsp = RequestBody.create(MediaType.parse("multipart/form-data"), wtsp);
                    RequestBody ustatus = RequestBody.create(MediaType.parse("multipart/form-data"), agentdata.getStatus());
                    /*-----------------------------------------multipart-------------------*/
                    UserInterface userInterface= ApiClient.getClient().create(UserInterface.class);
                    Call<UpdateAgentResponse>call=userInterface.updateAgent(uid,uname,uemail,umob,uwtsp,ustatus,body);
                    call.enqueue(new Callback<UpdateAgentResponse>() {
                        @Override
                        public void onResponse(Call<UpdateAgentResponse> call, Response<UpdateAgentResponse> response) {
                            if (response.code()==200)
                            {
                                progress.dismiss();
                                Toast.makeText(EditUserDetail.this, "Agent Data Updated", Toast.LENGTH_SHORT).show();
                                Intent intent=new Intent(EditUserDetail.this,UsersActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                overridePendingTransition(R.anim.transitionrighttoleft,R.anim.transitionrighttoleft);
                                startActivity(intent);
                                finish();
                            }
                            else if (response.code()!=200)
                            {
                                progress.dismiss();
                                Toast.makeText(EditUserDetail.this, "Agent Data Not Updated!!!", Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<UpdateAgentResponse> call, Throwable t) {

                        }
                    });
                }
            }
        });
        edituserImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LinearLayout camera, folder;
                dialog = new Dialog(EditUserDetail.this);
                dialog.setContentView(R.layout.dialogboxcamera);
                DisplayMetrics metrics=getResources().getDisplayMetrics();
                int width=metrics.widthPixels;
                dialog.getWindow().setLayout(width, LinearLayout.LayoutParams.MATCH_PARENT);
                dialog.show();
                dialog.setCancelable(true);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                Window window = dialog.getWindow();
                window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
                camera = dialog.findViewById(R.id.camera);
                folder = dialog.findViewById(R.id.folder);
                folder.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                     if (ContextCompat.checkSelfPermission(EditUserDetail.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED &&
                             ContextCompat.checkSelfPermission(EditUserDetail.this,Manifest.permission.READ_EXTERNAL_STORAGE)!=PackageManager.PERMISSION_GRANTED&&
                             ContextCompat.checkSelfPermission(EditUserDetail.this,Manifest.permission.CAMERA)!=PackageManager.PERMISSION_GRANTED)
                         requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.CAMERA},PERMISSION_ID);
                        Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(i, 100);
                        dialog.dismiss();



                    }
                });
                camera.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (ContextCompat.checkSelfPermission(EditUserDetail.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED &&
                                ContextCompat.checkSelfPermission(EditUserDetail.this,Manifest.permission.READ_EXTERNAL_STORAGE)!=PackageManager.PERMISSION_GRANTED&&
                                ContextCompat.checkSelfPermission(EditUserDetail.this,Manifest.permission.CAMERA)!=PackageManager.PERMISSION_GRANTED)
                            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.CAMERA},PERMISSION_ID);
                        //checkPermission(Manifest.permission.CAMERA, CAMERA_PERMISSION_CODE);
                        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(cameraIntent, 101);
                        dialog.dismiss();
                        if (picturePath != null && !picturePath.isEmpty() && !picturePath.equals("null")) {

                            Picasso.get().load(imagePic).into(edituserImage);

                            bitmap = ((BitmapDrawable) edituserImage.getDrawable().getCurrent()).getBitmap();
                            Log.e("bitmap", "" + bitmap);
                            converetdImage = getResizedBitmap(bitmap, 500);

                        } else { }

                    }
                });
            }
        });

    }
    private Bitmap getResizedBitmap(Bitmap image, int maxSize) {

        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //  if (requestCode == 100 && resultCode == RESULT_OK && data != null) {

        //     imagepath=selectedImage.getPath();




        if (requestCode == 100 && resultCode == RESULT_OK && data != null) {

//the image URI
            Uri selectedImage = data.getData();

            //     imagepath=selectedImage.getPath();


            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            picturePath = cursor.getString(columnIndex);
            cursor.close();


            if (picturePath != null && !picturePath.equals("")) {
                image = new File(picturePath);
            }

            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);
                converetdImage = getResizedBitmap(bitmap, 500);
                edituserImage.setImageBitmap(converetdImage);
                edituserImage.setVisibility(View.VISIBLE);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else if (requestCode == 101 && resultCode == Activity.RESULT_OK) {


            Bitmap converetdImage = (Bitmap) data.getExtras().get("data");
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            converetdImage.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
            edituserImage.setImageBitmap(converetdImage);
            edituserImage.setVisibility(View.VISIBLE);
            image = new File(Environment.getExternalStorageDirectory(), "shop.jpg");
            FileOutputStream fo;
            try {
                fo = new FileOutputStream(image);
                fo.write(bytes.toByteArray());
                fo.close();
            } catch (IOException e) {
                e.printStackTrace();
            }


        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode)
        {  case PERMISSION_ID :

            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Granted. Start getting the location information
            }
            break;}

    }
}

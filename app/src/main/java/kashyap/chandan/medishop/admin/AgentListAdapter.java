package kashyap.chandan.medishop.admin;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import kashyap.chandan.medishop.ApiClient;
import kashyap.chandan.medishop.R;
import kashyap.chandan.medishop.UserInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

class AgentListAdapter extends RecyclerView.Adapter<AgentListAdapter.MyViewHolder> {
    Context context;
    List<AgentListResponse.DataBean> allAgentList;
    public AgentListAdapter(Context context, List<AgentListResponse.DataBean> allAgentList) {

   this.allAgentList=allAgentList;
    this.context=context;}

    @NonNull
    @Override
    public AgentListAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.recyagentlayout,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final AgentListAdapter.MyViewHolder holder, int position) {
//        holder.userid.setText(allAgentList.get(position).getId());
        holder.name.setText(allAgentList.get(position).getName());
        holder.email.setText(allAgentList.get(position).getEmail());
        holder.phone.setText(allAgentList.get(position).getMobile());
        holder.agentshopcount.setText(String.valueOf(allAgentList.get(position).get_$StoresCount91()));
//        String datetime=allAgentList.get(position).getDate_time();
//        String date=datetime.substring(0,10);
        holder.date.setText(allAgentList.get(position).getDate_time());
//        holder.agentshopcount.setText(position);
        holder.userdetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int pos=holder.getAdapterPosition();
                AgentListResponse.DataBean data=allAgentList.get(pos);
                Intent intent=new Intent(context,AdminUsersDetail.class);
                Bundle bundle=new Bundle();
                bundle.putSerializable("agent", (Serializable) data);
                intent.putExtra("detail",bundle);
                context.startActivity(intent);
            }
        });
        Picasso.get().load("http://www.sbpharma.in/admin_assets/uploads/agents/"+allAgentList.get(position).getImage()).error(R.drawable.cross).placeholder(R.drawable.loading).into(holder.userImg);

        holder.edituser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int pos=holder.getAdapterPosition();
                AgentListResponse.DataBean data=allAgentList.get(pos);
                Intent intent=new Intent(context,EditUserDetail.class);
                Bundle bundle=new Bundle();
                bundle.putSerializable("agent", (Serializable) data);
                intent.putExtra("detail",bundle);
                context.startActivity(intent);
            }
        });
        holder.call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int pos=holder.getAdapterPosition();
                String mob=allAgentList.get(pos).getMobile();
                Uri u = Uri.fromParts("tel",mob,null);
                Intent i = new Intent(Intent.ACTION_DIAL, u);
                context.startActivity(i);
            }
        });
        holder.whatsapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int pos=holder.getAdapterPosition();
                String mob=allAgentList.get(pos).getWa_mobile();
               // use country code with your phone number
                String url = "https://api.whatsapp.com/send?phone=+91"+mob;
                try {
                    PackageManager pm = context.getPackageManager();
                    pm.getPackageInfo("com.whatsapp", PackageManager.GET_ACTIVITIES);
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(url));
                    context.startActivity(i);
                } catch (PackageManager.NameNotFoundException e) {
                    Toast.makeText(context, "Whatsapp app not installed in your phone", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }
        });






    }


    @Override
    public int getItemCount() {
        return allAgentList.size();
    }
    private void removeAt(int position) {
        allAgentList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, allAgentList.size());
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView name,email,phone,date,agentshopcount;
        ImageView edituser,call,whatsapp,userImg;
        LinearLayout userdetail;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
//            userid=itemView.findViewById(R.id.agentide);
            name=itemView.findViewById(R.id.agentname);
            email=itemView.findViewById(R.id.agentemail);
            phone=itemView.findViewById(R.id.agentphone);
            date=itemView.findViewById(R.id.agentdate);
            agentshopcount=itemView.findViewById(R.id.agentshopcount);
edituser=itemView.findViewById(R.id.edituser);
            userdetail=itemView.findViewById(R.id.detail);
            call=itemView.findViewById(R.id.call);
            whatsapp=itemView.findViewById(R.id.whatsapp);
            userImg=itemView.findViewById(R.id.userimg);
//            deleteAgent=itemView.findViewById(R.id.agentdelete);
        }
    }
}

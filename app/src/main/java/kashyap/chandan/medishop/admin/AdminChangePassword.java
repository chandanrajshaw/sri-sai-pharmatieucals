package kashyap.chandan.medishop.admin;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;

import kashyap.chandan.medishop.ApiClient;
import kashyap.chandan.medishop.MainActivity;
import kashyap.chandan.medishop.R;
import kashyap.chandan.medishop.UserInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AdminChangePassword extends AppCompatActivity {

SharedPreferencesAdmin sharedPreferencesAdmin;
ImageView changepasswordgobackarrow;
TextView adminpwdchng;
TextInputEditText changeoldpassword,changenewpassword,changeconfirmpassword;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
      setContentView(R.layout.activity_admin_change_password);
       sharedPreferencesAdmin=new SharedPreferencesAdmin(AdminChangePassword.this);
        changepasswordgobackarrow=findViewById(R.id.changepasswordgobackarrow);
        changepasswordgobackarrow.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               Intent intentgoback=new Intent(AdminChangePassword.this,DashBoard.class);
               intentgoback.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
               startActivity(intentgoback);
               overridePendingTransition(R.anim.transitionrighttoleft, R.anim.transitionrighttoleft);
               finish();
           }
       });
        adminpwdchng=findViewById(R.id.adminpwdchng);
        changeoldpassword=findViewById(R.id.changeoldpassword);
        changenewpassword=findViewById(R.id.changenewpassword);
        changeconfirmpassword=findViewById(R.id.changeconfirmpassword);
  adminpwdchng.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               String oldpwd=changeoldpassword.getText().toString();
               String newpwd=changenewpassword.getText().toString();
               String confpwd=changeconfirmpassword.getText().toString();
               if (oldpwd.isEmpty()&& newpwd.isEmpty()&&confpwd.isEmpty())
                   Toast.makeText(AdminChangePassword.this, "Please fill all the Field", Toast.LENGTH_SHORT).show();
               else if (oldpwd.isEmpty())
                   Toast.makeText(AdminChangePassword.this, "Enter Old Password", Toast.LENGTH_SHORT).show();
               else if (newpwd.isEmpty())
                   Toast.makeText(AdminChangePassword.this, "Enter New Password", Toast.LENGTH_SHORT).show();
               else if (confpwd.isEmpty())
                   Toast.makeText(AdminChangePassword.this, "Enter Confirm Password", Toast.LENGTH_SHORT).show();

               else if (!oldpwd.equals(sharedPreferencesAdmin.getAdminPassPreferences()))
                Toast.makeText(AdminChangePassword.this, "Enter Correct Old Password", Toast.LENGTH_SHORT).show();
               else if (!newpwd.equals(confpwd))
                   Toast.makeText(AdminChangePassword.this, "New Password & Confirm Password Mismatch", Toast.LENGTH_SHORT).show();

               else if (newpwd.equals(sharedPreferencesAdmin.getAdminPassPreferences()))
                   Toast.makeText(AdminChangePassword.this, "Old Password And New Password are same", Toast.LENGTH_SHORT).show();
                else{
                   UserInterface userInterface= ApiClient.getClient().create(UserInterface.class);
                   Call<AdminChangePassResponse> call=userInterface.adminChangePassword(sharedPreferencesAdmin.getAdminemailPreferences(),oldpwd,newpwd);
                   call.enqueue(new Callback<AdminChangePassResponse>() {
                       @Override
                       public void onResponse(Call<AdminChangePassResponse> call, Response<AdminChangePassResponse> response) {
                           if (response.code()==200)
                           {
                               Toast.makeText(AdminChangePassword.this, "Password Changed Successfully", Toast.LENGTH_SHORT).show();
Intent intent=new Intent(AdminChangePassword.this, MainActivity.class);
intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
sharedPreferencesAdmin.sessionEnd();
startActivity(intent);
                           }
                           else if (response.code()!=200){
                               Toast.makeText(AdminChangePassword.this, "Password Not Changed", Toast.LENGTH_SHORT).show();
                           }
                       }

                       @Override
                       public void onFailure(Call<AdminChangePassResponse> call, Throwable t) {

                       }
                   });

               }




           }
       });
    }
}

package kashyap.chandan.medishop.admin;

import androidx.appcompat.app.AppCompatActivity;


import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;

import kashyap.chandan.medishop.R;


public class ViewProfile extends AppCompatActivity {

SharedPreferencesAdmin sharedPreferencesAdmin;
ImageView viewprofilegobackarrow;
TextView submitprofile,editProfile,profilemail,profilename,profilephone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
      setContentView(R.layout.activity_view_profile);
        sharedPreferencesAdmin=new SharedPreferencesAdmin(ViewProfile.this);
        viewprofilegobackarrow=findViewById(R.id.viewprofilegobackarrow);
viewprofilegobackarrow.setOnClickListener(new View.OnClickListener() {
       @Override
       public void onClick(View view) {
           Intent profilebackIntent=new Intent(ViewProfile.this,DashBoard.class);
           profilebackIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
           overridePendingTransition(R.anim.transitionrighttoleft, R.anim.transitionrighttoleft);
           startActivity(profilebackIntent);

           finish();
       }
   });
        submitprofile=findViewById(R.id.submitprofile);
        editProfile=findViewById(R.id.editProfile);
        profilemail=findViewById(R.id.profilemail);
        profilename=findViewById(R.id.profilename);
        profilephone=findViewById(R.id.profilephone);
submitprofile.setVisibility(View.GONE);
editProfile.setVisibility(View.GONE);
  profilemail.setText(sharedPreferencesAdmin.getAdminemailPreferences());
 profilename.setText(sharedPreferencesAdmin.getAdminNamePreferences());
profilephone.setText(sharedPreferencesAdmin.getAdminPhonePreferences());
  editProfile.setOnClickListener(new View.OnClickListener() {
       @Override
       public void onClick(View view) {
     profilename.setEnabled(true);
          profilephone.setEnabled(true);
   submitprofile.setEnabled(true);
      profilename.setFocusable(true);
       }
   });
    }
}

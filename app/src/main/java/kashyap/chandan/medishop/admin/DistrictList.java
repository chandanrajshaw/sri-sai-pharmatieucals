package kashyap.chandan.medishop.admin;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.shimmer.ShimmerFrameLayout;

import java.util.ArrayList;
import java.util.List;

import kashyap.chandan.medishop.ApiClient;
import kashyap.chandan.medishop.ConnectionDetector;
import kashyap.chandan.medishop.CustomItemClickListener;
import kashyap.chandan.medishop.R;
import kashyap.chandan.medishop.UserInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DistrictList extends AppCompatActivity {
    ImageView gobackarrow,Add;
    CardView cardView;
    TextView state,btnGetDistrict;
    RecyclerView allDistrictRecycler,stateRecycler;
    ConnectionDetector connectionDetector;
    RelativeLayout stateList;
    String stateId;
    ShimmerFrameLayout shimmerFrameLayout;
    List<StateResponse.DataBean> allStateList=new ArrayList<>();
    List<DistrictResponse.DataBean> allDistrictList=new ArrayList<>();
    private Dialog progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_district_list);
        Add=findViewById(R.id.Add);
        gobackarrow=findViewById(R.id.gobackarrow);
        cardView=findViewById(R.id.cardView);
        state=findViewById(R.id.state);
        stateList=findViewById(R.id.stateList);
        btnGetDistrict=findViewById(R.id.btnGetDistrict);
        connectionDetector=new ConnectionDetector(DistrictList.this);
        allDistrictRecycler=findViewById(R.id.allDistrictRecycler);
        shimmerFrameLayout=findViewById(R.id.shimmerview);
        shimmerFrameLayout.setVisibility(View.GONE);
        Add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(DistrictList.this,AddDistrict.class);
                startActivity(intent);
            }
        });
        gobackarrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(DistrictList.this,DashBoard.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                overridePendingTransition(R.anim.transitionrighttoleft, R.anim.transitionrighttoleft);
                finish();
            }
        });
        stateList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                statelist();
            }
        });
        btnGetDistrict.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (stateId==null||stateId.isEmpty())
                    Toast.makeText(DistrictList.this, "Select State", Toast.LENGTH_SHORT).show();
                else
                {
                    cardView.setVisibility(View.GONE);
                    shimmerFrameLayout.setVisibility(View.VISIBLE);
                    shimmerFrameLayout.startShimmer();
                    UserInterface userInterface= ApiClient.getClient().create(UserInterface.class);
                    Call<DistrictResponse>call=userInterface.getDistrict(stateId);
                    call.enqueue(new Callback<DistrictResponse>() {
                        @Override
                        public void onResponse(Call<DistrictResponse> call, Response<DistrictResponse> response) {
                            if (response.code()==200)
                            {
                                allDistrictList=response.body().getData();
                                shimmerFrameLayout.stopShimmer();
                                shimmerFrameLayout.setVisibility(View.GONE);
                                allDistrictRecycler.setVisibility(View.VISIBLE);
                                allDistrictRecycler.setLayoutManager(new LinearLayoutManager(DistrictList.this,LinearLayoutManager.VERTICAL,false));
                                allDistrictRecycler.setAdapter(new EditDistrictAdapter(DistrictList.this,allDistrictList));
                            }
                            else
                            {
                                shimmerFrameLayout.setVisibility(View.GONE);
                                shimmerFrameLayout.stopShimmer();
                                Toast.makeText(DistrictList.this, "Something Goes Wrong", Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<DistrictResponse> call, Throwable t) {
                            shimmerFrameLayout.stopShimmer();
                            Toast.makeText(DistrictList.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();

                        }
                    });
                }
            }
        });
    }
    private void statelist()
    {
        stateId="";
        progress = new Dialog(DistrictList.this);
        progress.setContentView(R.layout.customdialog);
        progress.setCancelable(false);
        progress.show();
        UserInterface userInterface= ApiClient.getClient().create(UserInterface.class);
        /*--------------------------------------*/
        Call<StateResponse> call=userInterface.stateList();
        call.enqueue(new Callback<StateResponse>() {
            @Override
            public void onResponse(Call<StateResponse> call, Response<StateResponse> response) {
                if (response.code()==200)
                {
                    allStateList=response.body().getData();
                    final  Dialog stateDialog=new Dialog(DistrictList.this);
                    stateDialog.setContentView(R.layout.recyclerdialog);
                    DisplayMetrics metrics=getResources().getDisplayMetrics();
                    int width=metrics.widthPixels;
                    stateDialog.getWindow().setLayout(width, LinearLayout.LayoutParams.WRAP_CONTENT);
                    stateRecycler=stateDialog.findViewById(R.id.dialogRecycler);
                    stateRecycler.setLayoutManager(new LinearLayoutManager(DistrictList.this,LinearLayoutManager.VERTICAL,false));
                    stateRecycler.setAdapter(new StateListAdapter(DistrictList.this,allStateList,stateDialog, new CustomItemClickListener() {
                        @Override
                        public void onItemClick(View v, int position, String value) {
                            state.setText(value);
                            stateId=String.valueOf(position);
                        }
                    }));
                    progress.dismiss();
                    stateDialog.show();
                }
                else
                {
                    progress.dismiss();
                    Toast.makeText(DistrictList.this, "Something Goes Wrong", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<StateResponse> call, Throwable t) {
                progress.dismiss();
                Toast.makeText(DistrictList.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }
}
package kashyap.chandan.medishop.admin;

import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.lang.annotation.Annotation;

import de.hdodenhof.circleimageview.CircleImageView;
import kashyap.chandan.medishop.ApiClient;
import kashyap.chandan.medishop.ApiError;
import kashyap.chandan.medishop.R;
import kashyap.chandan.medishop.UserInterface;
import kashyap.chandan.medishop.agentPannel.AddShopActivity1;
import kashyap.chandan.medishop.agentPannel.AgentAllShopResponse;
import kashyap.chandan.medishop.client.ClientProfile;
import kashyap.chandan.medishop.makeclient.MakeAsClientResponse;
import kashyap.chandan.medishop.makeclient.MakeClientDocument;
import kashyap.chandan.medishop.makeclient.MakeClientProfile;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;


public class AminViewallShopDetail extends AppCompatActivity {
//ActivityViewShopDetailBinding viewShopDetailBinding;
String shopid,status,agentid,cityid,areaid,eemail;
TextView toolshopname,shopcity,shopname,shopphone,shopemail,ownername,addAsClient;
CircleImageView viewinmap,call;
ImageView viewshopgobackarrow,viewshopImage,whatsapp;
double lat,lng;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

   setContentView(R.layout.activity_view_shop_detail);
        Intent i=getIntent();
        Bundle bundle=i.getBundleExtra("edit");
        final AllShopsResponse.DataBean dataBean= (AllShopsResponse.DataBean) bundle.getSerializable("editShop");
        viewshopImage=findViewById(R.id.viewshopImage);
        Picasso.get().load("http://www.sbpharma.in/admin_assets/uploads/stores/"+dataBean.getPhoto()).error(R.drawable.cross).placeholder(R.drawable.loading).into(viewshopImage);
        toolshopname=findViewById(R.id.toolshopname);

       // viewShopDetailBinding.shoparea.setText(dataBean.getArea_name());
        addAsClient=findViewById(R.id.addAsClient);
        addAsClient.setVisibility(View.VISIBLE);
        shopcity=findViewById(R.id.shopcity);
        shopcity.setText(dataBean.getArea_name()+","+dataBean.getCity_name()+","+dataBean.getDistrict_name()+","+dataBean.getState_name());;
      shopname=findViewById(R.id.shopname);
    shopname.setText(dataBean.getStore_name());
        shopphone=findViewById(R.id.shopphone);
      shopphone.setText(dataBean.getMobile());
        shopemail=findViewById(R.id.shopemail);
        eemail=dataBean.getEmail();
        if (eemail.isEmpty()|| eemail.equalsIgnoreCase(""))
        {
            shopemail.setText("N/A");
        }
        else {
            shopemail.setText(dataBean.getEmail());
        }
        ownername=findViewById(R.id.ownername);
 ownername.setText(dataBean.getPerson_name());
lat=Double.parseDouble(dataBean.getLatitude());
lng=Double.parseDouble(dataBean.getLongitude());
        viewshopgobackarrow=findViewById(R.id.viewshopgobackarrow);
       viewshopgobackarrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                overridePendingTransition(R.anim.transitionrighttoleft, R.anim.transitionrighttoleft);
                finish();
            }
        });
       whatsapp=findViewById(R.id.whatsapp);
       whatsapp.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               if (dataBean.getWa_number()==null||dataBean.getWa_number().equalsIgnoreCase(""))
               {
                   Toast.makeText(AminViewallShopDetail.this, "Whatsapp No Not Available", Toast.LENGTH_SHORT).show();
               }
             else {
                   String url = "https://api.whatsapp.com/send?phone=+91"+dataBean.getWa_number();
                   try {
                       PackageManager pm = getPackageManager();
                       pm.getPackageInfo("com.whatsapp", PackageManager.GET_ACTIVITIES);
                       Intent i = new Intent(Intent.ACTION_VIEW);
                       i.setData(Uri.parse(url));
                       startActivity(i);
                   } catch (PackageManager.NameNotFoundException e) {
                       Toast.makeText(AminViewallShopDetail.this, "Whatsapp app not installed in your phone", Toast.LENGTH_SHORT).show();
                       e.printStackTrace();
                   }
               }
           }
       });
        viewinmap=findViewById(R.id.viewinmap);
        viewinmap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String geoUri = "http://maps.google.com/maps?q=loc:" + lat + "," + lng + dataBean.getStore_name();
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(geoUri));
                startActivity(intent);
            }
        });
        call=findViewById(R.id.call);
  call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri u = Uri.fromParts("tel",dataBean.getMobile(),null);
                Intent i = new Intent(Intent.ACTION_DIAL, u);
                startActivity(i);
            }
        });
        addAsClient.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog progress=new Dialog(AminViewallShopDetail.this);
                progress.setContentView(R.layout.customdialog);
                progress.setCancelable(false);
                progress.show();
                UserInterface userInterface= ApiClient.getClient().create(UserInterface.class);
                Call<MakeAsClientResponse>call=userInterface.makeClient(dataBean.getId());
                call.enqueue(new Callback<MakeAsClientResponse>() {
                    @Override
                    public void onResponse(Call<MakeAsClientResponse> call, Response<MakeAsClientResponse> response) {
                        if (response.code()==200)
                        {
                            Toast.makeText(AminViewallShopDetail.this, "Please Fill Other Detail", Toast.LENGTH_SHORT).show();
                            MakeAsClientResponse.DataBean data=response.body().getData();
                            Intent intent=new Intent(AminViewallShopDetail.this, MakeClientProfile.class);
                            intent.putExtra("shop",data);
                            startActivity(intent);
                            progress.dismiss();
                            finish();
                        }
                        else if (response.code()!=200)
                        {
                            progress.dismiss();
                            Converter<ResponseBody, ApiError> converter =
                                    ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                            ApiError error;
                            try {
                                error = converter.convert(response.errorBody());
                                ApiError.StatusBean status=error.getStatus();
                                Toast.makeText(AminViewallShopDetail.this, ""+status.getMessage(), Toast.LENGTH_LONG).show();
                            } catch (IOException e) { e.printStackTrace(); }


                        }
                    }

                    @Override
                    public void onFailure(Call<MakeAsClientResponse> call, Throwable t) {

                    }
                });

            }
        });

    }
}

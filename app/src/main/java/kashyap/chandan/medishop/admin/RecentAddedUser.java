package kashyap.chandan.medishop.admin;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import kashyap.chandan.medishop.ApiClient;
import kashyap.chandan.medishop.R;
import kashyap.chandan.medishop.UserInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RecentAddedUser extends Fragment {
    RecyclerView recyclerView;
    TextView textView;
    List<AgentListResponse.DataBean> allAgentList=new ArrayList<AgentListResponse.DataBean>();
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.user_list_layout,container,false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerView=view.findViewById(R.id.userrecycler);
        textView=view.findViewById(R.id.textbox);
        recyclerView.removeAllViews();
        UserInterface userInterface= ApiClient.getClient().create(UserInterface.class);
        Call<AgentListResponse>call=userInterface.getAllAgentAdmin();
        call.enqueue(new Callback<AgentListResponse>() {
            @Override
            public void onResponse(Call<AgentListResponse> call, Response<AgentListResponse> response) {
                if (response.code()==200){
                  allAgentList=response.body().getData();
                    recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL,false));
                    recyclerView.setAdapter(new AgentListAdapter(getContext(),allAgentList));
                    recyclerView.setVisibility(View.VISIBLE);

                }
                else if (response.code()!=200){
                    textView.setVisibility(View.VISIBLE);
                   recyclerView.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<AgentListResponse> call, Throwable t) {
                Toast.makeText(getContext(), ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}

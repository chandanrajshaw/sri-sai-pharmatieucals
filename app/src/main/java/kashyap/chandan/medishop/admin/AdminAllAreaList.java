package kashyap.chandan.medishop.admin;

import androidx.appcompat.app.AppCompatActivity;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.List;

import kashyap.chandan.medishop.ApiClient;
import kashyap.chandan.medishop.ConnectionDetector;
import kashyap.chandan.medishop.CustomItemClickListener;
import kashyap.chandan.medishop.R;
import kashyap.chandan.medishop.UserInterface;

import kashyap.chandan.medishop.agentPannel.AddShopActivity1;
import kashyap.chandan.medishop.agentPannel.CityListAdapter;
import kashyap.chandan.medishop.pojoclasses.AdminCityList;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AdminAllAreaList extends AppCompatActivity {

ImageView allarealistgobackarrow,Addareaadmin;
CardView cardView;
TextView btnGetArea,state,district,city,cityId;
RecyclerView allarearec,stateRecycler,districtRecycler,cityRecycler;
ConnectionDetector connectionDetector;
RelativeLayout districtList,stateList,cityList;
String stateId,districtId;
    ShimmerFrameLayout shimmerFrameLayout;
    private Dialog progress;
List<StateResponse.DataBean>allStateList=new ArrayList<>();
List<DistrictResponse.DataBean>allDistrict=new ArrayList<>();
    private List<AdminCityList.DataBean> allCityList=new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
       setContentView(R.layout.activity_admin_all_area_list);
        cityId=findViewById(R.id.cityId);
        cardView=findViewById(R.id.cardView);
        btnGetArea=findViewById(R.id.btnGetArea);
        state=findViewById(R.id.state);
        district=findViewById(R.id.district);
        city=findViewById(R.id.city);
        cityList=findViewById(R.id.cityList);
        districtList=findViewById(R.id.districtList);
        stateList=findViewById(R.id.stateList);
        connectionDetector=new ConnectionDetector(AdminAllAreaList.this);
        shimmerFrameLayout=findViewById(R.id.shimmerview);
        allarealistgobackarrow=findViewById(R.id.allarealistgobackarrow);
        Addareaadmin=findViewById(R.id.Addareaadmin);
        allarearec=findViewById(R.id.allarearec);
        shimmerFrameLayout.setVisibility(View.GONE);
      allarealistgobackarrow.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        Intent intent=new Intent(AdminAllAreaList.this,DashBoard.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        overridePendingTransition(R.anim.transitionrighttoleft, R.anim.transitionrighttoleft);
        finish();
    }
});
stateList.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        statelist();
    }
});
districtList.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        if (stateId==null||stateId.isEmpty())
            Toast.makeText(AdminAllAreaList.this, "Select State", Toast.LENGTH_SHORT).show();
        else
        {
            districtlist();
        }
    }
});
        cityList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (districtId==null||districtId.isEmpty())
                    Toast.makeText(AdminAllAreaList.this, "Select District", Toast.LENGTH_SHORT).show();
                else
                {
                    final Dialog progressDialog = new Dialog(AdminAllAreaList.this);
                    progressDialog.setContentView(R.layout.customdialog);
                    progressDialog.setCancelable(false);
                    progressDialog.show();
                    UserInterface userInterface= ApiClient.getClient().create(UserInterface.class);
                    Call<AdminCityList> call=userInterface.getCityList(districtId);
                    call.enqueue(new Callback<AdminCityList>() {
                        @Override
                        public void onResponse(Call<AdminCityList> call, Response<AdminCityList> response) {
                            if (response.code()==200)
                            {
                                allCityList=response.body().getData();
                                final  Dialog citydialog=new Dialog(AdminAllAreaList.this);
                                citydialog.setContentView(R.layout.recyclerdialog);
                                DisplayMetrics metrics=getResources().getDisplayMetrics();
                                int width=metrics.widthPixels;
                                citydialog.getWindow().setLayout(width, LinearLayout.LayoutParams.MATCH_PARENT);
                                cityRecycler=citydialog.findViewById(R.id.dialogRecycler);
                                cityRecycler.setLayoutManager(new LinearLayoutManager(AdminAllAreaList.this,LinearLayoutManager.VERTICAL,false));
                                cityRecycler.setAdapter(new CityListAdapter(AdminAllAreaList.this,allCityList,city,citydialog,cityId));
                                progressDialog.dismiss();
                                citydialog.show();
                            }
                            else
                            {
                                progressDialog.dismiss();
                                Toast.makeText(AdminAllAreaList.this, "Something Goes Wrong", Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<AdminCityList> call, Throwable t) {
                            Snackbar.make(AdminAllAreaList.this.getWindow().getDecorView().findViewById(android.R.id.content), ""+t.getMessage(), Snackbar.LENGTH_SHORT).show();

                        }
                    });
                }
            }
        });

      btnGetArea.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View view) {
              String id=cityId.getText().toString();
              if (id==null||id.isEmpty()||id.equalsIgnoreCase(""))
                  Toast.makeText(AdminAllAreaList.this, "Select City", Toast.LENGTH_SHORT).show();
              else
              {
                  if (!connectionDetector.isConnectingToInternet()){
                      Toast.makeText(AdminAllAreaList.this, "Please Connect to Internet", Toast.LENGTH_SHORT).show();
                  }
                  else if (connectionDetector.isConnectingToInternet())
                  {
                      cardView.setVisibility(View.GONE);
                      shimmerFrameLayout.setVisibility(View.VISIBLE);
                      UserInterface userInterface= ApiClient.getClient().create(UserInterface.class);
                      Call<AdminAllAreaResponse>call=userInterface.getAllAreaAdmin(id);
                      call.enqueue(new Callback<AdminAllAreaResponse>() {
                          @Override
                          public void onResponse(Call<AdminAllAreaResponse> call, Response<AdminAllAreaResponse> response) {
                              if (response.code()==200)
                              {
                                  shimmerFrameLayout.stopShimmer();
                                  shimmerFrameLayout.setVisibility(View.GONE);
                                  List<AdminAllAreaResponse.DataBean> allArea=new ArrayList<AdminAllAreaResponse.DataBean>();
                                  allArea=response.body().getData();
                                  allarearec.setLayoutManager(new LinearLayoutManager(AdminAllAreaList.this, LinearLayoutManager.VERTICAL,false));
                                  allarearec.setAdapter(new EditAreaAdapter(AdminAllAreaList.this,allArea));
                                  allarearec.setVisibility(View.VISIBLE);
                              }
                              else if (response.code()!=200)
                              {
                                  shimmerFrameLayout.stopShimmer();
                                  shimmerFrameLayout.setVisibility(View.GONE);
                                  Toast.makeText(AdminAllAreaList.this, "No Area Found", Toast.LENGTH_SHORT).show();

                              }
                          }

                          @Override
                          public void onFailure(Call<AdminAllAreaResponse> call, Throwable t) {
                              shimmerFrameLayout.stopShimmer();
                              shimmerFrameLayout.setVisibility(View.GONE);
                              Toast.makeText(AdminAllAreaList.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();

                          }
                      });
                  }
              }
          }
      });
       Addareaadmin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(AdminAllAreaList.this,AddArea.class);
                startActivity(intent);
            }
        });
    }
    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        shimmerFrameLayout.stopShimmer();
    }
    private void statelist()
    {
        stateId="";
        progress = new Dialog(AdminAllAreaList.this);
        progress.setContentView(R.layout.customdialog);
        progress.setCancelable(false);
        progress.show();
        UserInterface userInterface= ApiClient.getClient().create(UserInterface.class);
        /*--------------------------------------*/
        Call<StateResponse>call=userInterface.stateList();
        call.enqueue(new Callback<StateResponse>() {
            @Override
            public void onResponse(Call<StateResponse> call, Response<StateResponse> response) {
                if (response.code()==200)
                {
                    allStateList=response.body().getData();
                    final  Dialog stateDialog=new Dialog(AdminAllAreaList.this);
                    stateDialog.setContentView(R.layout.recyclerdialog);
                    DisplayMetrics metrics=getResources().getDisplayMetrics();
                    int width=metrics.widthPixels;
                    stateDialog.getWindow().setLayout(width, LinearLayout.LayoutParams.WRAP_CONTENT);
                    stateRecycler=stateDialog.findViewById(R.id.dialogRecycler);
                    stateRecycler.setLayoutManager(new LinearLayoutManager(AdminAllAreaList.this,LinearLayoutManager.VERTICAL,false));
                    stateRecycler.setAdapter(new StateListAdapter(AdminAllAreaList.this,allStateList,stateDialog, new CustomItemClickListener() {
                        @Override
                        public void onItemClick(View v, int position, String value) {
                            state.setText(value);
                            stateId=String.valueOf(position);
                        }
                    }));
                    progress.dismiss();
                    stateDialog.show();
                }
                else
                {
                    progress.dismiss();
                    Toast.makeText(AdminAllAreaList.this, "Something Goes Wrong", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<StateResponse> call, Throwable t) {
                progress.dismiss();
                Toast.makeText(AdminAllAreaList.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }
    private void districtlist()
    {
        districtId="";
        progress = new Dialog(AdminAllAreaList.this);
        progress.setContentView(R.layout.customdialog);
        progress.setCancelable(false);
        progress.show();
        UserInterface userInterface= ApiClient.getClient().create(UserInterface.class);
        Call<DistrictResponse>call=userInterface.getDistrict(stateId);
        call.enqueue(new Callback<DistrictResponse>() {
            @Override
            public void onResponse(Call<DistrictResponse> call, Response<DistrictResponse> response) {
                if (response.code()==200)
                {
                    allDistrict=response.body().getData();
                    final  Dialog districtDialog=new Dialog(AdminAllAreaList.this);
                    districtDialog.setContentView(R.layout.recyclerdialog);
                    DisplayMetrics metrics=getResources().getDisplayMetrics();
                    int width=metrics.widthPixels;
                    districtDialog.getWindow().setLayout(width, LinearLayout.LayoutParams.WRAP_CONTENT);
                    districtRecycler=districtDialog.findViewById(R.id.dialogRecycler);
                    districtRecycler.setLayoutManager(new LinearLayoutManager(AdminAllAreaList.this,LinearLayoutManager.VERTICAL,false));
                    districtRecycler.setAdapter(new DistrictListAdapter(AdminAllAreaList.this,allDistrict,districtDialog, new CustomItemClickListener() {
                        @Override
                        public void onItemClick(View v, int position, String value) {
                            district.setText(value);
                            districtId=String.valueOf(position);
                        }
                    }));
                    progress.dismiss();
                    districtDialog.show();
                }
                else {
                    progress.dismiss();
                    Toast.makeText(AdminAllAreaList.this, "Something Goes Wrong", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<DistrictResponse> call, Throwable t) {
                progress.dismiss();
                Toast.makeText(AdminAllAreaList.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });




    }

}

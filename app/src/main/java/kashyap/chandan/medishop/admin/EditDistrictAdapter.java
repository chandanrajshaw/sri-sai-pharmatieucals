package kashyap.chandan.medishop.admin;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import kashyap.chandan.medishop.ApiClient;
import kashyap.chandan.medishop.R;
import kashyap.chandan.medishop.UserInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditDistrictAdapter extends RecyclerView.Adapter<EditDistrictAdapter.MyViewHolder> {
    Context context;
    List<DistrictResponse.DataBean> allDistrictList;
    public EditDistrictAdapter(Context context, List<DistrictResponse.DataBean> allDistrictList) {
        this.context=context;
        this.allDistrictList=allDistrictList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.allcity_custom_layout,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, int position) {
        holder.tvDistrict.setText(allDistrictList.get(position).getDistrict_name());
        holder.tvState.setText(allDistrictList.get(position).getState_name());
        holder.deleteDistrict.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final int pos=holder.getAdapterPosition();
                final  String areaid=allDistrictList.get(pos).getId();
                final Dialog yesnoDialog=new Dialog(context);
                DisplayMetrics metrics=context.getResources().getDisplayMetrics();
                int width=metrics.widthPixels;
                yesnoDialog.setContentView(R.layout.yesnodialog);
                yesnoDialog.getWindow().setLayout(width, LinearLayout.LayoutParams.WRAP_CONTENT);
                TextView yes=yesnoDialog.findViewById(R.id.yes);
                TextView no=yesnoDialog.findViewById(R.id.no);
                yesnoDialog.show();
                yes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        final Dialog progressDialog=new Dialog(context);
                        progressDialog.setContentView(R.layout.customdialog);
                        progressDialog.setCancelable(false);
                        progressDialog.show();
                        UserInterface userInterface= ApiClient.getClient().create(UserInterface.class);
                        Call<DeleteDistrictResponse>call=userInterface.deleteDistrict(allDistrictList.get(pos).getId());
                        call.enqueue(new Callback<DeleteDistrictResponse>() {
                            @Override
                            public void onResponse(Call<DeleteDistrictResponse> call, Response<DeleteDistrictResponse> response) {
                               if (response.code()==200)
                               {
                                   progressDialog.dismiss();
                                   DeleteDistrictResponse.StatusBean statusBean=response.body().getStatus();
                                   Toast.makeText(context, ""+statusBean.getMessage(), Toast.LENGTH_SHORT).show();
                                   removeAt(pos);
                                   yesnoDialog.dismiss();
                               }
                               else
                               {
                                   Toast.makeText(context, "District Not Deleted", Toast.LENGTH_SHORT).show();
                                   progressDialog.dismiss();
                                   yesnoDialog.dismiss();
                               }
                            }

                            @Override
                            public void onFailure(Call<DeleteDistrictResponse> call, Throwable t) {
                                Toast.makeText(context, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
                                progressDialog.dismiss();
                                yesnoDialog.dismiss();
                            }
                        });

                    }
                });

                no.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        yesnoDialog.dismiss();
                    }
                });
            }
        });
        holder.editDistrict.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int pos=holder.getAdapterPosition();
                final Dialog dialog=new Dialog(context);
                dialog.setContentView(R.layout.dialogcityedit);
                DisplayMetrics metrics=context.getResources().getDisplayMetrics();
                int width = metrics.widthPixels;
                int height = metrics.heightPixels;
                dialog.getWindow().setLayout(width, LinearLayout.LayoutParams.WRAP_CONTENT);
                final EditText dDistrict= dialog.findViewById(R.id.dcityedit);
                dDistrict.setText(allDistrictList.get(pos).getDistrict_name());
                dialog.show();
                final   String districtId=allDistrictList.get(pos).getId();
                final String stateId=allDistrictList.get(pos).getState_id();
                final String status=allDistrictList.get(pos).getStatus();
                ImageView close=dialog.findViewById(R.id.closebtn);
                TextView update=dialog.findViewById(R.id.btnupdate);
                update.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        final String district=dDistrict.getText().toString();
                        final Dialog progressDialog=new Dialog(context);
                        progressDialog.setContentView(R.layout.customdialog);
                        progressDialog.setCancelable(false);
                        progressDialog.show();
                        UserInterface userInterface= ApiClient.getClient().create(UserInterface.class);
                        Call<UpdateDistrictResponse>call=userInterface.updateDistrict(stateId,districtId,district,status);
                        call.enqueue(new Callback<UpdateDistrictResponse>() {
                            @Override
                            public void onResponse(Call<UpdateDistrictResponse> call, Response<UpdateDistrictResponse> response) {
                                if (response.code()==200)
                                {
                                    progressDialog.dismiss();
                                    UpdateDistrictResponse.StatusBean statusBean=response.body().getStatus();
                                    Toast.makeText(context,""+statusBean.getMessage(),Toast.LENGTH_SHORT).show();
                                    Intent intent=new Intent(context,DistrictList.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    context.startActivity(intent);
                                    ((DistrictList) context).finish();
                                    dialog.dismiss();
                                    notifyDataSetChanged();
                                }
                                else
                                {
                                    progressDialog.dismiss();
                                    Toast.makeText(context, "District Not Updated", Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onFailure(Call<UpdateDistrictResponse> call, Throwable t) {
                                progressDialog.dismiss();
                                Toast.makeText(context, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                });
                close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });
            }
        });
    }

    @Override
    public int getItemCount() {
        return allDistrictList.size();
    }
    private void removeAt(int position) {
        allDistrictList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, allDistrictList.size());
    }
    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvDistrict, tvState,area,city;
        ImageView editDistrict,deleteDistrict ;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            city=itemView.findViewById(R.id.city);
            area.setText("District :");
            city.setText("State :");
            area=itemView.findViewById(R.id.area);
            editDistrict=itemView.findViewById(R.id.editcityorarea);
            deleteDistrict=itemView.findViewById(R.id.deletecityarea);
            tvDistrict =itemView.findViewById(R.id.cityforarea);
            tvState =itemView.findViewById(R.id.cityfromadmin);
        }
    }
}

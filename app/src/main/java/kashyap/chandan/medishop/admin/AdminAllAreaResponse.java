package kashyap.chandan.medishop.admin;

import java.util.List;

public class AdminAllAreaResponse {


    /**
     * status : {"code":200,"message":"Area List"}
     * data : [{"id":"23","city_id":"1","area_name":"S R NAGAR","date_time":"2020-05-11 10:18:28",
     * "status":"1","city_name":"Hyderabad"},{"id":"22","city_id":"1","area_name":"KPHB","date_time":"2020-05-11 08:56:19","status":"1","city_name":"Hyderabad"},{"id":"21","city_id":"1","area_name":"RAJIVGANDHINAGAR","date_time":"2020-05-03 19:28:13","status":"1","city_name":"Hyderabad"},{"id":"20","city_id":"1","area_name":"PRAGATHINAGAR ","date_time":"2020-05-03 09:46:58","status":"1","city_name":"Hyderabad"},{"id":"19","city_id":"1","area_name":"hafizpet ","date_time":"2020-05-02 12:09:00","status":"1","city_name":"Hyderabad"},{"id":"18","city_id":"1","area_name":"MIYAPUR ","date_time":"2020-05-02 11:33:38","status":"1","city_name":"Hyderabad"},{"id":"17","city_id":"1","area_name":"kousalya nagar","date_time":"2020-04-28 11:16:09","status":"1","city_name":"Hyderabad"},{"id":"16","city_id":"1","area_name":"VENKATAGIRI","date_time":"2020-04-26 16:37:58","status":"1","city_name":"Hyderabad"},{"id":"15","city_id":"1","area_name":"KARMIKANAGAR ","date_time":"2020-04-20 17:33:32","status":"1","city_name":"Hyderabad"},{"id":"14","city_id":"1","area_name":"BORABANDA","date_time":"2020-04-20 17:33:02","status":"1","city_name":"Hyderabad"},{"id":"13","city_id":"1","area_name":"REHMATHNAGAR","date_time":"2020-04-20 17:32:26","status":"1","city_name":"Hyderabad"},{"id":"12","city_id":"1","area_name":"MOTINAGAR ","date_time":"2020-04-20 17:31:39","status":"1","city_name":"Hyderabad"},{"id":"11","city_id":"1","area_name":"JAWAHARNAGAR","date_time":"2020-04-20 17:31:13","status":"1","city_name":"Hyderabad"},{"id":"10","city_id":"1","area_name":"BACHUPALLY ","date_time":"2020-04-20 12:14:43","status":"1","city_name":"Hyderabad"},{"id":"9","city_id":"1","area_name":"Kondapoor Village ","date_time":"2020-04-19 13:49:20","status":"1","city_name":"Hyderabad"},{"id":"8","city_id":"1","area_name":"lal bangula","date_time":"2020-04-19 11:02:02","status":"1","city_name":"Hyderabad"},{"id":"7","city_id":"1","area_name":"Jagatgirigutta ","date_time":"2020-04-19 10:43:35","status":"1","city_name":"Hyderabad"},{"id":"6","city_id":"1","area_name":"bollaram ","date_time":"2020-04-19 10:43:16","status":"1","city_name":"Hyderabad"},{"id":"5","city_id":"1","area_name":"kukatpally ","date_time":"2020-04-19 10:42:57","status":"1","city_name":"Hyderabad"},{"id":"4","city_id":"1","area_name":"DEEPTHISRINAGAR","date_time":"2020-04-18 19:00:48","status":"1","city_name":"Hyderabad"},{"id":"3","city_id":"1","area_name":"Nizampet","date_time":"2020-04-18 18:58:50","status":"1","city_name":"Hyderabad"},{"id":"2","city_id":"1","area_name":"bharath nagar","date_time":"2020-04-18 18:58:36","status":"1","city_name":"Hyderabad"},{"id":"1","city_id":"1","area_name":"moosapet","date_time":"2020-04-18 18:58:20","status":"1","city_name":"Hyderabad"}]
     */

    private StatusBean status;
    private List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : Area List
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean {
        /**
         * id : 23
         * city_id : 1
         * area_name : S R NAGAR
         * date_time : 2020-05-11 10:18:28
         * status : 1
         * city_name : Hyderabad
         */

        private String id;
        private String city_id;
        private String area_name;
        private String date_time;
        private String status;
        private String city_name;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getCity_id() {
            return city_id;
        }

        public void setCity_id(String city_id) {
            this.city_id = city_id;
        }

        public String getArea_name() {
            return area_name;
        }

        public void setArea_name(String area_name) {
            this.area_name = area_name;
        }

        public String getDate_time() {
            return date_time;
        }

        public void setDate_time(String date_time) {
            this.date_time = date_time;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getCity_name() {
            return city_name;
        }

        public void setCity_name(String city_name) {
            this.city_name = city_name;
        }
    }
}

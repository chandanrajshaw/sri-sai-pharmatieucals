package kashyap.chandan.medishop.admin;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.facebook.shimmer.ShimmerFrameLayout;

import kashyap.chandan.medishop.R;

public class VendorTypeList extends AppCompatActivity {
RecyclerView vendorTypeList;
ImageView gobackarrow,add;
ShimmerFrameLayout shimmerview;
Dialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vendor_type_list);
        init();
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(VendorTypeList.this,AddVendorType.class);
                startActivity(intent);
            }
        });
        gobackarrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void init() {
        vendorTypeList=findViewById(R.id.vendorTypeList);
        gobackarrow=findViewById(R.id.gobackarrow);
        shimmerview=findViewById(R.id.shimmerview);
        add=findViewById(R.id.add);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
    private void vendorTypeList()
    {
        progressDialog=new Dialog(VendorTypeList.this);
        progressDialog.setContentView(R.layout.customdialog);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }
}
package kashyap.chandan.medishop.admin;

import java.io.Serializable;
import java.util.List;

public class StateResponse implements Serializable {

    /**
     * status : {"code":200,"message":"State List"}
     * data : [{"id":"6","state_name":"Karnataka","date_time":"2020-08-21 12:48:33","status":"1"},{"id":"3","state_name":" Telangana","date_time":"2020-08-17 15:19:49","status":"1"},{"id":"1","state_name":"Andhra Pradesh","date_time":"2020-08-17 15:18:55","status":"1"}]
     */

    private StatusBean status;
    private List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean implements Serializable {
        /**
         * code : 200
         * message : State List
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean implements Serializable {
        /**
         * id : 6
         * state_name : Karnataka
         * date_time : 2020-08-21 12:48:33
         * status : 1
         */

        private String id;
        private String state_name;
        private String date_time;
        private String status;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getState_name() {
            return state_name;
        }

        public void setState_name(String state_name) {
            this.state_name = state_name;
        }

        public String getDate_time() {
            return date_time;
        }

        public void setDate_time(String date_time) {
            this.date_time = date_time;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
    }
}

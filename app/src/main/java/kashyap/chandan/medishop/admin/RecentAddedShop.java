package kashyap.chandan.medishop.admin;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.facebook.shimmer.ShimmerFrameLayout;

import java.util.ArrayList;
import java.util.List;

import kashyap.chandan.medishop.ApiClient;
import kashyap.chandan.medishop.R;
import kashyap.chandan.medishop.UserInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RecentAddedShop extends Fragment {
    RecyclerView recyclerView;
    TextView textView;
    List<AllShopsResponse.DataBean> dataBeanList=new ArrayList<AllShopsResponse.DataBean>();
    ShimmerFrameLayout shimmerFrameLayout;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.shop_layout,container,false);
        return view;
    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        textView=view.findViewById(R.id.textbox);
shimmerFrameLayout=view.findViewById(R.id.shimmerview);
        recyclerView=view.findViewById(R.id.shoprecycler);
        recyclerView.removeAllViews();
        UserInterface userInterface= ApiClient.getClient().create(UserInterface.class);
        Call<AllShopsResponse>call=userInterface.getAllShopAdmin();
        call.enqueue(new Callback<AllShopsResponse>() {
            @Override
            public void onResponse(Call<AllShopsResponse> call, Response<AllShopsResponse> response) {
if (response.code()==200)
{
    shimmerFrameLayout.stopShimmer();
    shimmerFrameLayout.setVisibility(View.GONE);
    dataBeanList=response.body().getData();
    recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL,false));
    recyclerView.setAdapter(new ShopListAdapter(getContext(),dataBeanList));
    recyclerView.setVisibility(View.VISIBLE);

}


else if (response.code()!=200)
{
    shimmerFrameLayout.stopShimmer();
    shimmerFrameLayout.setVisibility(View.GONE);
    recyclerView.setVisibility(View.GONE);
    textView.setVisibility(View.VISIBLE);
}
            }

            @Override
            public void onFailure(Call<AllShopsResponse> call, Throwable t) {
                Toast.makeText(getContext(), ""+t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });

    }

    @Override
    public void onPause() {
        super.onPause();
        shimmerFrameLayout.stopShimmer();
    }

    @Override
    public void onResume() {
        super.onResume();
        shimmerFrameLayout.startShimmer();
    }
}

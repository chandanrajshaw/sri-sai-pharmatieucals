package kashyap.chandan.medishop.admin;

import androidx.appcompat.app.AppCompatActivity;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.shimmer.ShimmerFrameLayout;

import java.util.ArrayList;
import java.util.List;

import kashyap.chandan.medishop.ApiClient;
import kashyap.chandan.medishop.ConnectionDetector;
import kashyap.chandan.medishop.CustomItemClickListener;
import kashyap.chandan.medishop.R;
import kashyap.chandan.medishop.UserInterface;
import kashyap.chandan.medishop.pojoclasses.AdminCityList;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AdminAllCityList extends AppCompatActivity {
List<AdminCityList.DataBean> allCity=new ArrayList<AdminCityList.DataBean>();
List<StateResponse.DataBean>allStateList=new ArrayList<>();
List<DistrictResponse.DataBean>allDistrict=new ArrayList<>();
String stateId,districtId;
CardView cardView;
TextView state,district,btnGetCity;
ConnectionDetector connectionDetector;
ShimmerFrameLayout shimmerFrameLayout;
RecyclerView allcityrec,stateRecycler,districtRecycler;
ImageView allcitylistgobackarrow,Addcityadmin;
RelativeLayout stateList,districtList;
    private Dialog progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
    setContentView(R.layout.activity_admin_all_city_list);
        cardView=findViewById(R.id.cardView);
    btnGetCity=findViewById(R.id.btnGetCity);
    state=findViewById(R.id.state);
    district=findViewById(R.id.district);
        districtList=findViewById(R.id.districtList);
        stateList=findViewById(R.id.stateList);
        allcityrec=findViewById(R.id.allcityrec);
        allcitylistgobackarrow=findViewById(R.id.allcitylistgobackarrow);
        connectionDetector=new ConnectionDetector(AdminAllCityList.this);
        shimmerFrameLayout=findViewById(R.id.shimmerview);
        shimmerFrameLayout.setVisibility(View.GONE);
        Addcityadmin=findViewById(R.id.Addcityadmin);
        stateList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                statelist();
            }
        });
        districtList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (stateId==null||stateId.isEmpty())
                    Toast.makeText(AdminAllCityList.this, "Select State", Toast.LENGTH_SHORT).show();
                else
                {
                    districtlist();
                }
            }
        });
    allcitylistgobackarrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(AdminAllCityList.this,DashBoard.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                overridePendingTransition(R.anim.transitionrighttoleft, R.anim.transitionrighttoleft);
                finish();
            }
        });
btnGetCity.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        if (!connectionDetector.isConnectingToInternet()){
            shimmerFrameLayout.stopShimmer();
            shimmerFrameLayout.setVisibility(View.GONE);
            Toast.makeText(AdminAllCityList.this, "Please Connect to Internet", Toast.LENGTH_SHORT).show();
        }
        else if (connectionDetector.isConnectingToInternet()){
            cardView.setVisibility(View.GONE);
            shimmerFrameLayout.startShimmer();
            shimmerFrameLayout.setVisibility(View.VISIBLE);
            UserInterface userInterface= ApiClient.getClient().create(UserInterface.class);
            Call<AdminCityList>call=userInterface.getCityList(districtId);
            call.enqueue(new Callback<AdminCityList>() {
                @Override
                public void onResponse(Call<AdminCityList> call, Response<AdminCityList> response) {
                    if (response.code()==200){
                        shimmerFrameLayout.stopShimmer();
                        shimmerFrameLayout.setVisibility(View.GONE);
                        cardView.setVisibility(View.GONE);
                        allCity=response.body().getData();
                        allcityrec.setLayoutManager(new LinearLayoutManager(AdminAllCityList.this, LinearLayoutManager.VERTICAL,false));
                        allcityrec.setAdapter(new EditCityAdapter(AdminAllCityList.this,allCity));
                        allcityrec.setVisibility(View.VISIBLE);
                    }
                    else if (response.code()!=200){
                        shimmerFrameLayout.stopShimmer();
                        shimmerFrameLayout.setVisibility(View.GONE);
                        cardView.setVisibility(View.GONE);
                        Toast.makeText(AdminAllCityList.this, "No City Found", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<AdminCityList> call, Throwable t) {
                    Toast.makeText(AdminAllCityList.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();

                }
            });
        }
    }
});

Addcityadmin.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        Intent intent=new Intent(AdminAllCityList.this,AddCity.class);
        startActivity(intent);
    }
});
    }
    @Override
    protected void onResume() {
        super.onResume();
        allCity.clear();
        allcityrec.removeAllViews();
        cardView.setVisibility(View.VISIBLE);
        allDistrict.clear();
        allStateList.clear();
        stateId="";
        districtId="";
        state.setText("State");
        district.setText("District");
    }

    @Override
    protected void onPause() {
        super.onPause();
        shimmerFrameLayout.stopShimmer();
//        cardView.setVisibility(View.VISIBLE);
    }
    private void statelist()
    {
        stateId="";
        progress = new Dialog(AdminAllCityList.this);
        progress.setContentView(R.layout.customdialog);
        progress.setCancelable(false);
        progress.show();
        UserInterface userInterface= ApiClient.getClient().create(UserInterface.class);
        /*--------------------------------------*/
        Call<StateResponse>call=userInterface.stateList();
        call.enqueue(new Callback<StateResponse>() {
            @Override
            public void onResponse(Call<StateResponse> call, Response<StateResponse> response) {
                if (response.code()==200)
                {
                    allStateList=response.body().getData();
                    final  Dialog stateDialog=new Dialog(AdminAllCityList.this);
                    stateDialog.setContentView(R.layout.recyclerdialog);
                    DisplayMetrics metrics=getResources().getDisplayMetrics();
                    int width=metrics.widthPixels;
                    stateDialog.getWindow().setLayout(width, LinearLayout.LayoutParams.WRAP_CONTENT);
                    stateRecycler=stateDialog.findViewById(R.id.dialogRecycler);
                    stateRecycler.setLayoutManager(new LinearLayoutManager(AdminAllCityList.this,LinearLayoutManager.VERTICAL,false));
                    stateRecycler.setAdapter(new StateListAdapter(AdminAllCityList.this,allStateList,stateDialog, new CustomItemClickListener() {
                        @Override
                        public void onItemClick(View v, int position, String value) {
                            state.setText(value);
                            stateId=String.valueOf(position);
                        }
                    }));
                    progress.dismiss();
                    stateDialog.show();
                }
                else
                {
                    progress.dismiss();
                    Toast.makeText(AdminAllCityList.this, "Something Goes Wrong", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<StateResponse> call, Throwable t) {
                progress.dismiss();
                Toast.makeText(AdminAllCityList.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }
    private void districtlist()
    {
        districtId="";
        progress = new Dialog(AdminAllCityList.this);
        progress.setContentView(R.layout.customdialog);
        progress.setCancelable(false);
        progress.show();
        UserInterface userInterface= ApiClient.getClient().create(UserInterface.class);
        Call<DistrictResponse>call=userInterface.getDistrict(stateId);
        call.enqueue(new Callback<DistrictResponse>() {
            @Override
            public void onResponse(Call<DistrictResponse> call, Response<DistrictResponse> response) {
                if (response.code()==200)
                {
                    allDistrict=response.body().getData();
                    final  Dialog districtDialog=new Dialog(AdminAllCityList.this);
                    districtDialog.setContentView(R.layout.recyclerdialog);
                    DisplayMetrics metrics=getResources().getDisplayMetrics();
                    int width=metrics.widthPixels;
                    districtDialog.getWindow().setLayout(width, LinearLayout.LayoutParams.WRAP_CONTENT);
                    districtRecycler=districtDialog.findViewById(R.id.dialogRecycler);
                    districtRecycler.setLayoutManager(new LinearLayoutManager(AdminAllCityList.this,LinearLayoutManager.VERTICAL,false));
                    districtRecycler.setAdapter(new DistrictListAdapter(AdminAllCityList.this,allDistrict,districtDialog, new CustomItemClickListener() {
                        @Override
                        public void onItemClick(View v, int position, String value) {
                            district.setText(value);
                            districtId=String.valueOf(position);
                        }
                    }));
                    progress.dismiss();
                    districtDialog.show();
                }
                else {
                    progress.dismiss();
                    Toast.makeText(AdminAllCityList.this, "Something Goes Wrong", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<DistrictResponse> call, Throwable t) {
                progress.dismiss();
                Toast.makeText(AdminAllCityList.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });




    }
}

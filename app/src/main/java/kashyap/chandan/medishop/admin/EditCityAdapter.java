package kashyap.chandan.medishop.admin;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import kashyap.chandan.medishop.ApiClient;
import kashyap.chandan.medishop.R;
import kashyap.chandan.medishop.UserInterface;
import kashyap.chandan.medishop.pojoclasses.AdminCityList;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

class EditCityAdapter extends RecyclerView.Adapter<EditCityAdapter.MyViewHolder> {
    Context context;
    List<AdminCityList.DataBean> allCity;
    public EditCityAdapter(Context context, List<AdminCityList.DataBean> allCity) {
        this.allCity=allCity;
        this.context=context;
    }

    @NonNull
    @Override
    public EditCityAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.allcity_custom_layout,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final EditCityAdapter.MyViewHolder holder, int position) {
holder.textView.setText(allCity.get(position).getCity_name());
holder.areaname.setVisibility(View.GONE);
holder.area.setVisibility(View.GONE);
        holder.deletecity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final int pos=holder.getAdapterPosition();
                final  String cityid=allCity.get(pos).getId();
                final Dialog yesnoDialog=new Dialog(context);
                DisplayMetrics metrics=context.getResources().getDisplayMetrics();
                int width=metrics.widthPixels;
                yesnoDialog.setContentView(R.layout.yesnodialog);
                yesnoDialog.getWindow().setLayout(width, LinearLayout.LayoutParams.WRAP_CONTENT);
               final TextView tvsure=yesnoDialog.findViewById(R.id.tvsure);
                tvsure.setText("Are You Sure to delete City");
                TextView yes=yesnoDialog.findViewById(R.id.yes);
                TextView no=yesnoDialog.findViewById(R.id.no);
                yesnoDialog.show();
                yes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        final Dialog progressDialog=new Dialog(context);
                        progressDialog.setContentView(R.layout.customdialog);
                        progressDialog.setCancelable(false);
                        progressDialog.show();
                        UserInterface userInterface= ApiClient.getClient().create(UserInterface.class);
                        Call<DeleteCityResponse>call=userInterface.deletecity(cityid);
                        call.enqueue(new Callback<DeleteCityResponse>() {
                            @Override
                            public void onResponse(Call<DeleteCityResponse> call, Response<DeleteCityResponse> response) {
                                if (response.code()==200)
                                {
                                    progressDialog.dismiss();
                                    yesnoDialog.dismiss();
                                    Toast.makeText(context,"City Deleted Successfully",Toast.LENGTH_SHORT).show();
                                    removeAt(pos);
                                }
                                else if (response.code()!=200){
                                    Toast.makeText(context,"You Can't Delete,There may Some Dependency on City",Toast.LENGTH_SHORT).show();

                                }
                            }

                            @Override
                            public void onFailure(Call<DeleteCityResponse> call, Throwable t) {
                                Toast.makeText(context,""+t.getMessage(),Toast.LENGTH_SHORT).show();
                                progressDialog.dismiss();
                                yesnoDialog.dismiss();

                            }
                        });
                    }
                });
                no.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        yesnoDialog.dismiss();
                    }
                });
            }
        });
holder.editCity.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
int pos=holder.getAdapterPosition();
        final Dialog dialog=new Dialog(context);
        dialog.setContentView(R.layout.dialogcityedit);
        DisplayMetrics metrics=context.getResources().getDisplayMetrics();
        int width = metrics.widthPixels;
        int height = metrics.heightPixels;
        dialog.getWindow().setLayout(width, LinearLayout.LayoutParams.WRAP_CONTENT);
       final EditText dcity= dialog.findViewById(R.id.dcityedit);
        dcity.setText(allCity.get(pos).getCity_name());
       final String id=allCity.get(pos).getId();
       final String status=allCity.get(pos).getStatus();
       final String districtId=allCity.get(pos).getDistrict_id();
        ImageView close=dialog.findViewById(R.id.closebtn);
        TextView update=dialog.findViewById(R.id.btnupdate);
        dialog.show();
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
update.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        String cityname=dcity.getText().toString();
        if (cityname.isEmpty())
            Toast.makeText(context,"Enter City Name",Toast.LENGTH_SHORT).show();
        else {
            final Dialog progressDialog = new Dialog(context);
            progressDialog.setContentView(R.layout.customdialog);
            progressDialog.setCancelable(false);
            progressDialog.show();
            UserInterface userInterface= ApiClient.getClient().create(UserInterface.class);
            Call<AdminUpdateCityResponse>call=userInterface.updateCity(districtId,id,cityname,status);
            call.enqueue(new Callback<AdminUpdateCityResponse>() {
                @Override
                public void onResponse(Call<AdminUpdateCityResponse> call, Response<AdminUpdateCityResponse> response) {
                    if (response.code()==200){
                        progressDialog.dismiss();
                        dialog.dismiss();
                        AdminUpdateCityResponse.StatusBean statusBean=response.body().getStatus();
                        Toast.makeText(context,""+statusBean.getMessage(),Toast.LENGTH_SHORT).show();
                        Intent intent=new Intent(context,AdminAllCityList.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        context.startActivity(intent);
                        ((AdminAllCityList) context).finish();
                    }
                    else
                    {
                        progressDialog.dismiss();
                        Toast.makeText(context, "Something Goes Wrong", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<AdminUpdateCityResponse> call, Throwable t) {
                    progressDialog.dismiss();
                    Toast.makeText(context,""+t.getMessage(),Toast.LENGTH_SHORT).show();

                }
            });

        }

    }
});


    }
});
    }
    private void removeAt(int position) {
        allCity.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, allCity.size());
    }

    @Override
    public int getItemCount() {
        return allCity.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView textView,area,areaname;
        ImageView editCity,deletecity;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            textView=itemView.findViewById(R.id.cityfromadmin);
            editCity=itemView.findViewById(R.id.editcityorarea);
            deletecity=itemView.findViewById(R.id.deletecityarea);
area=itemView.findViewById(R.id.area);
areaname=itemView.findViewById(R.id.cityforarea);
        }
    }
}

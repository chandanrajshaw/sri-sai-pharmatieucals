package kashyap.chandan.medishop.makeclient;

import java.io.Serializable;

public class MakeAsClientResponse implements Serializable {


    /**
     * status : {"code":200,"message":"List of Stores"}
     * data : {"id":"5","agent_id":"3","store_name":"KARUNYA MEDICAL  AND GENERAL STORES ","person_name":"SUMA LATHA","email":"roselenhyra@gmail.com","mobile":"6309702813","wa_number":"","state":"3","district":"5","city":"1","area":"6","photo":"temp4.jpg","latitude":"17.5536775","longitude":"78.3487115","date_time":"2020-04-19 12:35:45","status":"1","state_name":" Telangana","district_name":"Hyderabad","city_name":"Hyderabad","area_name":"bollaram ","name":"Amar"}
     */

    private StatusBean status;
    private DataBean data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class StatusBean implements Serializable {
        /**
         * code : 200
         * message : List of Stores
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean implements Serializable {
        /**
         * id : 5
         * agent_id : 3
         * store_name : KARUNYA MEDICAL  AND GENERAL STORES
         * person_name : SUMA LATHA
         * email : roselenhyra@gmail.com
         * mobile : 6309702813
         * wa_number :
         * state : 3
         * district : 5
         * city : 1
         * area : 6
         * photo : temp4.jpg
         * latitude : 17.5536775
         * longitude : 78.3487115
         * date_time : 2020-04-19 12:35:45
         * status : 1
         * state_name :  Telangana
         * district_name : Hyderabad
         * city_name : Hyderabad
         * area_name : bollaram
         * name : Amar
         */

        private String id;
        private String agent_id;
        private String store_name;
        private String person_name;
        private String email;
        private String mobile;
        private String wa_number;
        private String state;
        private String district;
        private String city;
        private String area;
        private String photo;
        private String latitude;
        private String longitude;
        private String date_time;
        private String status;
        private String state_name;
        private String district_name;
        private String city_name;
        private String area_name;
        private String name;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getAgent_id() {
            return agent_id;
        }

        public void setAgent_id(String agent_id) {
            this.agent_id = agent_id;
        }

        public String getStore_name() {
            return store_name;
        }

        public void setStore_name(String store_name) {
            this.store_name = store_name;
        }

        public String getPerson_name() {
            return person_name;
        }

        public void setPerson_name(String person_name) {
            this.person_name = person_name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getWa_number() {
            return wa_number;
        }

        public void setWa_number(String wa_number) {
            this.wa_number = wa_number;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getDistrict() {
            return district;
        }

        public void setDistrict(String district) {
            this.district = district;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getArea() {
            return area;
        }

        public void setArea(String area) {
            this.area = area;
        }

        public String getPhoto() {
            return photo;
        }

        public void setPhoto(String photo) {
            this.photo = photo;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getDate_time() {
            return date_time;
        }

        public void setDate_time(String date_time) {
            this.date_time = date_time;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getState_name() {
            return state_name;
        }

        public void setState_name(String state_name) {
            this.state_name = state_name;
        }

        public String getDistrict_name() {
            return district_name;
        }

        public void setDistrict_name(String district_name) {
            this.district_name = district_name;
        }

        public String getCity_name() {
            return city_name;
        }

        public void setCity_name(String city_name) {
            this.city_name = city_name;
        }

        public String getArea_name() {
            return area_name;
        }

        public void setArea_name(String area_name) {
            this.area_name = area_name;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
}

package kashyap.chandan.medishop.makeclient;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.annotation.Annotation;

import kashyap.chandan.medishop.ApiClient;
import kashyap.chandan.medishop.ApiError;
import kashyap.chandan.medishop.ConnectionDetector;
import kashyap.chandan.medishop.LocationAddress;
import kashyap.chandan.medishop.R;
import kashyap.chandan.medishop.UserInterface;
import kashyap.chandan.medishop.admin.AminViewallShopDetail;
import kashyap.chandan.medishop.client.AddClientResponse;
import kashyap.chandan.medishop.client.Clients;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

public class MakeClientDocument extends AppCompatActivity {
    TextView submit, skipdoc, toolheader;
    EditText gst, pan;
    Dialog dialog;
    double lat, lng;
    ImageView goback, panImg, gstImg;
    Bitmap gstbitmap, panbitmap, gstconveretdImage, panconvertedImage;
    Uri picUri;
    File shopimage = null, gstCertificate = null, panCertificate = null;
    String ownerName, shopName, mobile, email, address, city, area,id,stateId,districtId;
    String gstpicturePath, panpictutePath, gstimagePic, panimagePic;
    ConnectionDetector connectionDetector;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_company_document);
        connectionDetector = new ConnectionDetector(MakeClientDocument.this);
        toolheader = findViewById(R.id.customtoolheader);
        toolheader.setText("Shop Document");
        goback = findViewById(R.id.ordergobackarrow);
        goback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                overridePendingTransition(R.anim.transitionrighttoleft, R.anim.transitionrighttoleft);
                finish();
            }
        });
        skipdoc = findViewById(R.id.skipdoc);
        skipdoc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MakeClientDocument.this, Clients.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                overridePendingTransition(R.anim.transitionrighttoleft, R.anim.transitionrighttoleft);
                startActivity(intent);
                finish();
            }
        });
       /*---------------File pictureFile = (File)getIntent.getExtras().get("picture");

       ------------------*/
        Intent i = getIntent();
        Bundle bundle = i.getBundleExtra("formdata");
        shopimage = (File) bundle.getSerializable("file");
        ownerName = bundle.getString("ownername");
        shopName = bundle.getString("company");
        mobile = bundle.getString("phone");
        email = bundle.getString("email");
        city = bundle.getString("city");
        address = bundle.getString("address");
        stateId=bundle.getString("state");
        districtId=bundle.getString("district");
        area = bundle.getString("area");
        id=bundle.getString("storeid");
lat=Double.parseDouble(bundle.getString("lat"));
lng=Double.parseDouble(bundle.getString("lng"));

        //editText
        gst = findViewById(R.id.gstno);
        pan = findViewById(R.id.panno);
        //Image view
        gstImg = findViewById(R.id.gstcertificate);
        panImg = findViewById(R.id.panimg);
        gstImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LinearLayout camera, folder;
                dialog = new Dialog(MakeClientDocument.this);
                dialog.setContentView(R.layout.dialogboxcamera);
                DisplayMetrics metrics = getResources().getDisplayMetrics();
                int width = metrics.widthPixels;
                dialog.getWindow().setLayout(width, LinearLayout.LayoutParams.MATCH_PARENT);
                dialog.show();
                dialog.setCancelable(true);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                Window window = dialog.getWindow();
                window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
                camera = dialog.findViewById(R.id.camera);
                folder = dialog.findViewById(R.id.folder);
                folder.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        /*checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                STORAGE_PERMISSION_CODE);*/
                        Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(i, 100);
                        dialog.dismiss();

                    }
                });
                camera.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        //checkPermission(Manifest.permission.CAMERA, CAMERA_PERMISSION_CODE);
                        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(cameraIntent, 101);
                        dialog.dismiss();


                    }
                });
            }
        });
        panImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LinearLayout camera, folder;
                dialog = new Dialog(MakeClientDocument.this);
                dialog.setContentView(R.layout.dialogboxcamera);
                DisplayMetrics metrics = getResources().getDisplayMetrics();
                int width = metrics.widthPixels;
                dialog.getWindow().setLayout(width, LinearLayout.LayoutParams.MATCH_PARENT);
                dialog.show();
                dialog.setCancelable(true);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                Window window = dialog.getWindow();
                window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
                camera = dialog.findViewById(R.id.camera);
                folder = dialog.findViewById(R.id.folder);
                folder.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        /*checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                STORAGE_PERMISSION_CODE);*/
                        Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(i, 102);
                        dialog.dismiss();

                    }
                });
                camera.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        //checkPermission(Manifest.permission.CAMERA, CAMERA_PERMISSION_CODE);
                        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(cameraIntent, 103);
                        dialog.dismiss();


                    }
                });
            }
        });


        if (gstpicturePath != null && !gstpicturePath.isEmpty() && !gstpicturePath.equals("null")) {

            Picasso.get().load(gstpicturePath).into(gstImg);

            gstbitmap = ((BitmapDrawable) gstImg.getDrawable().getCurrent()).getBitmap();
//            Log.e("bitmap", "" + bitmap);
            gstconveretdImage = getResizedBitmap(gstbitmap, 500);

        } else {
        }
        if (panpictutePath != null && !panpictutePath.isEmpty() && !panpictutePath.equals("null")) {

            Picasso.get().load(panpictutePath).into(panImg);

            panbitmap = ((BitmapDrawable) panImg.getDrawable().getCurrent()).getBitmap();
//            Log.e("bitmap", "" + bitmap);
            panconvertedImage = getResizedBitmap(panbitmap, 500);

        } else {
        }

        submit = findViewById(R.id.submitdoc);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String gstno = gst.getText().toString().trim();
                String panno = pan.getText().toString().trim();
                if (shopimage == null && gstCertificate == null && panCertificate == null && gstno.isEmpty() && panno.isEmpty())
                    Snackbar.make(MakeClientDocument.this.getWindow().getDecorView().findViewById(android.R.id.content), "Enter all the fields", Snackbar.LENGTH_SHORT).show();
                else if (shopimage == null)
                    Snackbar.make(MakeClientDocument.this.getWindow().getDecorView().findViewById(android.R.id.content), "Shop Image is not available", Snackbar.LENGTH_SHORT).show();
                else if (gstCertificate == null)
                    Snackbar.make(MakeClientDocument.this.getWindow().getDecorView().findViewById(android.R.id.content), "Please Upload GST Certificate", Snackbar.LENGTH_SHORT).show();
                else if (panCertificate == null)
                    Snackbar.make(MakeClientDocument.this.getWindow().getDecorView().findViewById(android.R.id.content), "Please Upload Pan Card Image", Snackbar.LENGTH_SHORT).show();
                else if (gstno.isEmpty())
                    Snackbar.make(MakeClientDocument.this.getWindow().getDecorView().findViewById(android.R.id.content), "Enter GST No.", Snackbar.LENGTH_SHORT).show();
                else if (panno.isEmpty())
                    Snackbar.make(MakeClientDocument.this.getWindow().getDecorView().findViewById(android.R.id.content), "Enter Pan No.", Snackbar.LENGTH_SHORT).show();
                else {
                    if (!connectionDetector.isConnectingToInternet())
                        Snackbar.make(MakeClientDocument.this.getWindow().getDecorView().findViewById(android.R.id.content), "Everything is fine", Snackbar.LENGTH_SHORT).show();
                    else if (connectionDetector.isConnectingToInternet()) {

                        MultipartBody.Part cimg = null, gstimg = null, panimg = null;
                        if (shopimage != null && gstCertificate != null && panCertificate != null) {
                            RequestBody img1 = RequestBody.create(MediaType.parse("multipart/form-data"), shopimage);
                            cimg = MultipartBody.Part.createFormData("image", shopimage.getName(), img1);
                            RequestBody img2 = RequestBody.create(MediaType.parse("multipart/form-data"), gstCertificate);
                            gstimg = MultipartBody.Part.createFormData("gst_certificate", gstCertificate.getName(), img2);
                            RequestBody img3 = RequestBody.create(MediaType.parse("multipart/form-data"), panCertificate);
                            panimg = MultipartBody.Part.createFormData("pan_card", panCertificate.getName(), img3);
                        }
                        RequestBody sid = RequestBody.create(MediaType.parse("multipart/form-data"), id);
                        RequestBody cname = RequestBody.create(MediaType.parse("multipart/form-data"), shopName);
                        RequestBody owner = RequestBody.create(MediaType.parse("multipart/form-data"), ownerName);
                        RequestBody cemail = RequestBody.create(MediaType.parse("multipart/form-data"), email);
                        RequestBody cmob = RequestBody.create(MediaType.parse("multipart/form-data"), mobile);
                        RequestBody ccity = RequestBody.create(MediaType.parse("multipart/form-data"), city);
                        RequestBody carea = RequestBody.create(MediaType.parse("multipart/form-data"), area);
                        RequestBody cstate = RequestBody.create(MediaType.parse("multipart/form-data"), stateId);
                        RequestBody cdistrict = RequestBody.create(MediaType.parse("multipart/form-data"), districtId);
                        RequestBody clat = RequestBody.create(MediaType.parse("multipart/form-data"), String.valueOf(lat));
                        RequestBody clng = RequestBody.create(MediaType.parse("multipart/form-data"), String.valueOf(lng));
                        RequestBody caddr = RequestBody.create(MediaType.parse("multipart/form-data"), address);
                        RequestBody no_gst = RequestBody.create(MediaType.parse("multipart/form-data"), gstno);
                        RequestBody no_pan = RequestBody.create(MediaType.parse("multipart/form-data"), panno);
                        final Dialog progress = new Dialog(MakeClientDocument.this);
                        progress.setContentView(R.layout.customdialog);
                        progress.setCancelable(false);
                        progress.show();
                        UserInterface userInterface = ApiClient.getClient().create(UserInterface.class);
                        Call<ShopAsClientResponse> call = userInterface.addStoreClient(sid,cname, owner, cemail, cmob, ccity, carea, caddr, clat, clng, no_gst, no_pan, cimg, gstimg, panimg,cstate,cdistrict);
                        call.enqueue(new Callback<ShopAsClientResponse>() {
                            @Override
                            public void onResponse(Call<ShopAsClientResponse> call, Response<ShopAsClientResponse> response) {
                                if (response.code() == 200) {
                                    progress.dismiss();
                                    Toast.makeText(MakeClientDocument.this, "Shop Added As Client", Toast.LENGTH_SHORT).show();
                                    Intent intent = new Intent(MakeClientDocument.this, Clients.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    overridePendingTransition(R.anim.transitionrighttoleft, R.anim.transitionrighttoleft);
                                    startActivity(intent);
                                    finish();
                                } else if (response.code() != 200) {
                                    progress.dismiss();
//                                    Toast.makeText(MakeClientDocument.this, "Client Not Added Try Again", Toast.LENGTH_SHORT).show();
                                    Converter<ResponseBody, ApiError> converter =
                                            ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                                    ApiError error;
                                    try {
                                        error = converter.convert(response.errorBody());
                                        ApiError.StatusBean status=error.getStatus();
                                        Toast.makeText(MakeClientDocument.this, ""+status.getMessage(), Toast.LENGTH_LONG).show();
                                    } catch (IOException e) { e.printStackTrace(); }
                                }
                            }


                            @Override
                            public void onFailure(Call<ShopAsClientResponse> call, Throwable t) {
                                Snackbar.make(MakeClientDocument.this.getWindow().getDecorView().findViewById(android.R.id.content), "" + t.getMessage(), Snackbar.LENGTH_SHORT).show();

                            }
                        });

                    }
                }


            }
        });


    }

    private Bitmap getResizedBitmap(Bitmap image, int maxSize) {

        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //  if (requestCode == 100 && resultCode == RESULT_OK && data != null) {

        //     imagepath=selectedImage.getPath();

        if (requestCode == 100 && resultCode == RESULT_OK && data != null) {

//the image URI
            Uri selectedImage = data.getData();

            //     imagepath=selectedImage.getPath();


            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            gstpicturePath = cursor.getString(columnIndex);
            cursor.close();


            if (gstpicturePath != null && !gstpicturePath.equals("")) {
                gstCertificate = new File(gstpicturePath);
            }

            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);
                gstconveretdImage = getResizedBitmap(bitmap, 500);
                gstImg.setImageBitmap(gstconveretdImage);
                gstImg.setVisibility(View.VISIBLE);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (requestCode == 101 && resultCode == Activity.RESULT_OK) {


            Bitmap converetdImage = (Bitmap) data.getExtras().get("data");
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            converetdImage.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
            gstImg.setImageBitmap(converetdImage);
            gstImg.setVisibility(View.VISIBLE);
            gstCertificate = new File(Environment.getExternalStorageDirectory(), "gst.jpg");
            FileOutputStream fo;
            try {
                fo = new FileOutputStream(gstCertificate);
                fo.write(bytes.toByteArray());
                fo.close();
            } catch (IOException e) {
                e.printStackTrace();
            }


        } else if (requestCode == 102 && resultCode == RESULT_OK && data != null) {

//the image URI
            Uri selectedImage = data.getData();

            //     imagepath=selectedImage.getPath();


            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            panpictutePath = cursor.getString(columnIndex);
            cursor.close();


            if (panpictutePath != null && !panpictutePath.equals("")) {
                panCertificate = new File(panpictutePath);
            }

            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);
                panconvertedImage = getResizedBitmap(bitmap, 500);
                panImg.setImageBitmap(panconvertedImage);
                panImg.setVisibility(View.VISIBLE);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (requestCode == 103 && resultCode == Activity.RESULT_OK) {


            Bitmap converetdImage = (Bitmap) data.getExtras().get("data");
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            converetdImage.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
            panImg.setImageBitmap(converetdImage);
            panImg.setVisibility(View.VISIBLE);
            panCertificate = new File(Environment.getExternalStorageDirectory(), "pan.jpg");
            FileOutputStream fo;
            try {
                fo = new FileOutputStream(panCertificate);
                fo.write(bytes.toByteArray());
                fo.close();
            } catch (IOException e) {
                e.printStackTrace();
            }


        }

    }

    @Override
    public void onResume() {
        super.onResume();
//        if (checkPermissions()) {
//            getLastLocation();
//        }

    }
}
//    @SuppressLint("MissingPermission")
//    private void getLastLocation(){
//        if (checkPermissions()) {
//            if (isLocationEnabled()) {
//                mFusedLocationClient.getLastLocation().addOnCompleteListener(
//                        new OnCompleteListener<Location>() {
//                            @Override
//                            public void onComplete(@NonNull Task<Location> task) {
//                                Location location = task.getResult();
//                                if (location == null) {
//                                    requestNewLocationData();
//                                } else {
//                                    lat=location.getLatitude();
//                                    lng=location.getLongitude();
//                                    Toast.makeText(MakeClientDocument.this, lat+" "+lng, Toast.LENGTH_SHORT).show();
//                                    LocationAddress locationAddress = new LocationAddress();
//                                    locationAddress.getAddressFromLocation(location.getLatitude(), location.getLongitude(),
//                                            getApplicationContext(), new MakeClientDocument.GeocoderHandler());
//                                }
//                            }
//                        }
//                );
//            } else {
//                Toast.makeText(this, "Turn on location", Toast.LENGTH_LONG).show();
//                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
//                startActivity(intent);
//            }
//        } else {
//            requestPermissions();
//        }
//    }
//    private void requestPermissions() {
//        ActivityCompat.requestPermissions(
//                this,
//                new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION},
//                PERMISSION_ID
//        );
//    }
//    @SuppressLint("MissingPermission")
//    private void requestNewLocationData(){
//
//        LocationRequest mLocationRequest = new LocationRequest();
//        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
//        mLocationRequest.setInterval(0);
//        mLocationRequest.setFastestInterval(0);
//        mLocationRequest.setNumUpdates(1);
//
//        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
//        mFusedLocationClient.requestLocationUpdates(
//                mLocationRequest, mLocationCallback,
//                Looper.myLooper()
//        );
//
//    }
//    private LocationCallback mLocationCallback = new LocationCallback() {
//        @Override
//        public void onLocationResult(LocationResult locationResult) {
//            Location mLastLocation = locationResult.getLastLocation();
//            lat=mLastLocation.getLatitude();
//            lng=mLastLocation.getLongitude();
//            Toast.makeText(MakeClientDocument.this, "Lat:"+lat+"\tLng:"+lng, Toast.LENGTH_SHORT).show();
//
//        }
//    };
//
//    private boolean checkPermissions() {
//        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
//                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
//            return true;
//        }
//        return false;
//    }
//    private class GeocoderHandler extends Handler {
//        @Override
//        public void handleMessage(Message message) {
//            String locationAddress;
//            switch (message.what) {
//                case 1:
//                    Bundle bundle = message.getData();
//                    locationAddress = bundle.getString("address");
//                    break;
//                default:
//                    locationAddress = null;
//            }
////            etaddress.setText(locationAddress);
//        }
//    }
//    private boolean isLocationEnabled() {
//        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
//        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(
//                LocationManager.NETWORK_PROVIDER
//        );
//    }
//
//    @Override
//    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
//        switch (requestCode)
//        {
//            case PERMISSION_ID:
//                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                    // Granted. Start getting the location information
//                }
//                break;
//        }
//    }
//}

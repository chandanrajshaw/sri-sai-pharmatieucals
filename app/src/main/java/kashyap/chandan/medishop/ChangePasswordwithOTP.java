package kashyap.chandan.medishop;

import androidx.appcompat.app.AppCompatActivity;


import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.Api;
import com.google.android.material.textfield.TextInputEditText;

import kashyap.chandan.medishop.admin.AdminChangePassword;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChangePasswordwithOTP extends AppCompatActivity {
    ImageView changepwdotpgobackarrow;
    TextView pwdchng;
    TextInputEditText emailcpwo,changeotppassword,confirmotppassword;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
   setContentView(R.layout.activity_change_passwordwith_otp);
        changepwdotpgobackarrow=findViewById(R.id.changepwdotpgobackarrow);
       changepwdotpgobackarrow.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               Intent intent=new Intent(ChangePasswordwithOTP.this,ValidateOTP.class);
               intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
               overridePendingTransition(R.anim.transitionrighttoleft, R.anim.transitionrighttoleft);
               startActivity(intent);
               finish();
           }
       });
        pwdchng=findViewById(R.id.pwdchng);
        emailcpwo=findViewById(R.id.emailcpwo);changeotppassword=findViewById(R.id.changeotppassword);
        confirmotppassword=findViewById(R.id.confirmotppassword);

       pwdchng.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               String email=emailcpwo.getText().toString();
               String newpwd=changeotppassword.getText().toString();
               String confpwd=confirmotppassword.getText().toString();
               if (email.isEmpty()&& newpwd.isEmpty()&&confpwd.isEmpty())
                   Toast.makeText(ChangePasswordwithOTP.this, "Please fill all the Field", Toast.LENGTH_SHORT).show();
               else if (email.isEmpty())
                   Toast.makeText(ChangePasswordwithOTP.this, "Enter Old Password", Toast.LENGTH_SHORT).show();
               else if (newpwd.isEmpty())
                   Toast.makeText(ChangePasswordwithOTP.this, "Enter New Password", Toast.LENGTH_SHORT).show();
               else if (confpwd.isEmpty())
                   Toast.makeText(ChangePasswordwithOTP.this, "Enter Confirm Password", Toast.LENGTH_SHORT).show();

              else if (!newpwd.equals(confpwd))
                   Toast.makeText(ChangePasswordwithOTP.this, "New Password & Confirm Password Mismatch", Toast.LENGTH_SHORT).show();
else
               {
                   final Dialog progressDialog=new Dialog(ChangePasswordwithOTP.this);
                   progressDialog.setContentView(R.layout.customdialog);
                   progressDialog.setCancelable(false);
                   progressDialog.show();
UserInterface userInterface= ApiClient.getClient().create(UserInterface.class);
                   Call<ChangePwdFinalResponse>call=userInterface.changepwd(email,newpwd,confpwd);
                   call.enqueue(new Callback<ChangePwdFinalResponse>() {
                       @Override
                       public void onResponse(Call<ChangePwdFinalResponse> call, Response<ChangePwdFinalResponse> response) {
                           if (response.code()==200)
                           {progressDialog.dismiss();
                               Toast.makeText(ChangePasswordwithOTP.this, "Password Changed Succesfully!! Login", Toast.LENGTH_LONG).show();
                               Intent intent=new Intent(ChangePasswordwithOTP.this,MainActivity.class);
                               intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                               startActivity(intent);
                               finish();

                           }
                           else if (response.code()!=200)
                           {
                               progressDialog.dismiss();
                               Toast.makeText(ChangePasswordwithOTP.this, "Password Not Changed!!!Try Again ", Toast.LENGTH_LONG).show();
                           }
                       }

                       @Override
                       public void onFailure(Call<ChangePwdFinalResponse> call, Throwable t) {
                           progressDialog.dismiss();
                           Toast.makeText(ChangePasswordwithOTP.this, ""+t.getMessage(), Toast.LENGTH_LONG).show();

                       }
                   });
               }

           }
       });
    }
}

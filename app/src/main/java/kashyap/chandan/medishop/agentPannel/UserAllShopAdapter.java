package kashyap.chandan.medishop.agentPannel;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.TooltipCompat;
import androidx.recyclerview.widget.RecyclerView;

import java.io.Serializable;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import kashyap.chandan.medishop.ApiClient;
import kashyap.chandan.medishop.R;
import kashyap.chandan.medishop.UserInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


 public class UserAllShopAdapter extends RecyclerView.Adapter<UserAllShopAdapter.MyViewHolder> {
    Context context;
    List<AgentAllShopResponse.DataBean> agentAllShop;
    public UserAllShopAdapter(Context context, List<AgentAllShopResponse.DataBean> agentAllShop) {
        this.context=context;
        this.agentAllShop=agentAllShop;
    }

    @NonNull
    @Override
    public UserAllShopAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.shoplist,parent,false);
        return   new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final UserAllShopAdapter.MyViewHolder holder, final int position) {
//        holder.shopcity.setText(agentAllShop.get(position).getCity_name());
        holder.shoparea.setText(agentAllShop.get(position).getArea_name()+","+agentAllShop.get(position).getCity_name());
        holder.shopphone.setText(agentAllShop.get(position).getMobile());
        holder.ownername.setText(agentAllShop.get(position).getPerson_name());
        holder.shopdate.setText(agentAllShop.get(position).getDate_time());
        holder.agentname.setVisibility(View.GONE);
//        holder.tvid.setText("Date&Time");
        holder.imgviewmap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String geoUri = "http://maps.google.com/maps?q=loc:" + Double.valueOf(agentAllShop.get(position).getLatitude()) + "," + Double.valueOf(agentAllShop.get(position).getLongitude()) + " Shop";
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(geoUri));
                context.startActivity(intent);
            }
        });
        holder.shopname.setText(agentAllShop.get(position).getStore_name());
       // holder.agentid.setText(agentAllShop.get(position).getAgent_id());
//        holder.agentid.setVisibility(View.GONE);
//        holder.agentidtv.setVisibility(View.GONE);
//        holder.agentid.setVisibility(View.GONE);
//        holder.shopid.setText(agentAllShop.get(position).getDate_time());
        holder.shopdetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int pos=holder.getAdapterPosition();
                AgentAllShopResponse.DataBean data=agentAllShop.get(pos);
                Intent intent=new Intent(context,ViewShopDetail.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                Bundle bundle=new Bundle();
                bundle.putSerializable("editShop", (Serializable) data);
                intent.putExtra("edit",bundle);
                context.startActivity(intent);

            }
        });

        holder.agentlinear.setVisibility(View.GONE);

        holder.call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int pos=holder.getAdapterPosition();
                Uri u = Uri.fromParts("tel",agentAllShop.get(pos).getMobile(),null);
                Intent i = new Intent(Intent.ACTION_DIAL, u);
                context.startActivity(i);
            }
        });
    }



    private void removeAt(int position) {
        agentAllShop.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, agentAllShop.size());
    }

    @Override
    public int getItemCount() {
        return agentAllShop.size() ;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView shopname,shopid,agentid,shopphone,shopcity,ownername,shoparea,tvid,agentidtv,shopdate,agentname;
//        ImageView deleteshop;
//        LinearLayout shopdetail,;
        CircleImageView imgviewmap;
        ImageView call;
        LinearLayout shopdetail,agentlinear;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            shopname=itemView.findViewById(R.id.shopname);
//            shopid=itemView.findViewById(R.id.shopid);
            agentid=itemView.findViewById(R.id.agentid);
//            agentidtv=itemView.findViewById(R.id.agentidtv);
            shopphone=itemView.findViewById(R.id.shopphone);
//            shopcity=itemView.findViewById(R.id.shopcity);
            shoparea=itemView.findViewById(R.id.shoparea);
            shopdetail=itemView.findViewById(R.id.detail);
//            deleteshop=itemView.findViewById(R.id.shopdelete);
//            tvid=itemView.findViewById(R.id.tvid);
            agentlinear=itemView.findViewById(R.id.agentlayout);
            shopdate=itemView.findViewById(R.id.shopdate);
            shopdate=itemView.findViewById(R.id.shopdate);
            agentname=itemView.findViewById(R.id.agentname);
            imgviewmap=itemView.findViewById(R.id.viewinmap);
            ownername=itemView.findViewById(R.id.ownername);
            call=itemView.findViewById(R.id.call);

        }
    }
}

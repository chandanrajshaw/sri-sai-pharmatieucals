package kashyap.chandan.medishop.agentPannel;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import androidx.exifinterface.media.ExifInterface;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.List;

import kashyap.chandan.medishop.ApiClient;
import kashyap.chandan.medishop.ApiError;
import kashyap.chandan.medishop.CustomItemClickListener;
import kashyap.chandan.medishop.LocationAddress;
import kashyap.chandan.medishop.LocationTrack;
import kashyap.chandan.medishop.R;
import kashyap.chandan.medishop.UserInterface;
import kashyap.chandan.medishop.admin.AdminAllAreaResponse;
import kashyap.chandan.medishop.admin.DistrictListAdapter;
import kashyap.chandan.medishop.admin.DistrictResponse;
import kashyap.chandan.medishop.admin.SharedPreferencesAdmin;
import kashyap.chandan.medishop.admin.StateListAdapter;
import kashyap.chandan.medishop.admin.StateResponse;
import kashyap.chandan.medishop.pojoclasses.AdminCityList;
import kashyap.chandan.medishop.pojoclasses.GetAreaResponse;
import kashyap.chandan.medishop.pojoclasses.GetCityResponseUser;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class EditShopDetail extends AppCompatActivity {
    List<StateResponse.DataBean>allStateList=new ArrayList<>();
    List<DistrictResponse.DataBean>allDistrict=new ArrayList<>();
    private ArrayList permissionsToRequest;
    private ArrayList permissionsRejected = new ArrayList();
    private ArrayList permissions = new ArrayList();
    private final static int ALL_PERMISSIONS_RESULT = 101;
    public static final int PERMISSION_ID = 44;
    private ArrayList camerapermissionsToRequest;
    private ArrayList camerapermissionsRejected = new ArrayList();
    private ArrayList camerapermissions = new ArrayList();
    private final static int CAMERA_ALL_PERMISSIONS_RESULT = 108;
    Bitmap bitmap,converetdImage;
    Dialog dialog;
    String picturePath,imagePic;
    File image=null;
    LocationTrack locationTrack;
    Uri picUri;
    FusedLocationProviderClient mFusedLocationClient;
    double longitude,latitude;
    String shopid,agentid,lng,lat,pname,stateId,districtId;
    RecyclerView cityRecycler,areaRecycler,stateRecycler,districtRecycler;
    List<AdminCityList.DataBean> allCityList=new ArrayList<AdminCityList.DataBean>();
    List<AdminAllAreaResponse.DataBean> allAreaList=new ArrayList<AdminAllAreaResponse.DataBean>();
    String status;

    TextInputEditText eteditshopname,eteditshopphone,eteditwtsp,eteditpersonemail,eteditname,eteditwebsite,eteditlandmark,eteditmsg;
    TextView editareaid,editcity,editcityid,editarea,eteditaddress,submiteditshop, state, stateid, districtid,district;
    RelativeLayout editcitylist,editarealist,stateList,districtList;
    ImageView editshopImage,editshopgobackarrow;
    private Dialog progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
               setContentView(R.layout.activity_edit_shop_detail);
        state =findViewById(R.id.state);
        district=findViewById(R.id.district);
        districtid =findViewById(R.id.districtId);
        stateid =findViewById(R.id.stateId);
        districtList=findViewById(R.id.districtList);
        stateList=findViewById(R.id.stateList);
        editcity=findViewById(R.id.editcity);
        editareaid=findViewById(R.id.editareaid);
        eteditaddress=findViewById(R.id.eteditaddress);
        eteditwebsite=findViewById(R.id.eteditwebsite);
        submiteditshop=findViewById(R.id.submiteditshop);
        eteditlandmark=findViewById(R.id.eteditlandmark);
        eteditmsg=findViewById(R.id.eteditmsg);
        camerapermissions.add(CAMERA);
        camerapermissions.add(READ_EXTERNAL_STORAGE);
        camerapermissions.add(WRITE_EXTERNAL_STORAGE);
        camerapermissionsToRequest = findUnAskedCameraPermissions(camerapermissions);
        editshopImage=findViewById(R.id.editshopImage);
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        getLastLocation();
        Intent i=getIntent();
        Bundle bundle=i.getBundleExtra("edit");
        final AgentAllShopResponse.DataBean dataBean = (AgentAllShopResponse.DataBean) bundle.getSerializable("editShop");
       Picasso.get().load("http://www.sbpharma.in/admin_assets/uploads/stores/"+dataBean.getPhoto()).error(R.mipmap.ic_launcher_launcher_round).placeholder(R.drawable.loader).into(editshopImage);
        editcitylist=findViewById(R.id.editcitylist);
        locationTrack=new LocationTrack(EditShopDetail.this);
       editcity.setText(dataBean.getCity_name());
editareaid.setText(dataBean.getArea());
        editcityid=findViewById(R.id.editcityid);
        editarea=findViewById(R.id.editarea);
      editarea.setText(dataBean.getArea_name());
  editcityid.setText(dataBean.getCity());
        eteditshopname=findViewById(R.id.eteditshopname);
        eteditshopphone=findViewById(R.id.eteditshopphone);
  eteditshopname.setText(dataBean.getStore_name());
     eteditshopphone.setText(dataBean.getMobile());
     eteditwtsp=findViewById(R.id.etwtsp);
        eteditpersonemail=findViewById(R.id.eteditpersonemail);
        editshopgobackarrow=findViewById(R.id.editshopgobackarrow);
     eteditpersonemail.setText(dataBean.getEmail());
         shopid=dataBean.getId();
         status=dataBean.getStatus();
         agentid=dataBean.getAgent_id();
        lat=dataBean.getLatitude();
        lng=dataBean.getLongitude();
       getLastLocation();
        eteditname=findViewById(R.id.eteditname);
eteditname.setText(dataBean.getPerson_name());
       editcitylist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog progressDialog = new Dialog(EditShopDetail.this);
                progressDialog.setContentView(R.layout.customdialog);
                progressDialog.setCancelable(false);
                progressDialog.show();
                UserInterface userInterface= ApiClient.getClient().create(UserInterface.class);
                Call<AdminCityList> call=userInterface.getCityList(districtId);
                call.enqueue(new Callback<AdminCityList>() {
                    @Override
                    public void onResponse(Call<AdminCityList> call, Response<AdminCityList> response) {
                        if (response.code()==200)
                        {

                            final  Dialog citydialog=new Dialog(EditShopDetail.this);
                            citydialog.setContentView(R.layout.recyclerdialog);
                            DisplayMetrics metrics=getResources().getDisplayMetrics();
                            int width=metrics.widthPixels;
                            citydialog.getWindow().setLayout(width, LinearLayout.LayoutParams.MATCH_PARENT);
                            allCityList=response.body().getData();
                            cityRecycler=citydialog.findViewById(R.id.dialogRecycler);
                           cityRecycler.setLayoutManager(new LinearLayoutManager(EditShopDetail.this,LinearLayoutManager.VERTICAL,false));
                           cityRecycler.setAdapter(new CityListAdapter(EditShopDetail.this,allCityList,editcity,citydialog,editcityid));
                           citydialog.show();
                            progressDialog.dismiss();
                        }
                        else
                        {
                            progressDialog.dismiss();
                            Toast.makeText(EditShopDetail.this, "Something Goes Wrong", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<AdminCityList> call, Throwable t) {
                        Snackbar.make(EditShopDetail.this.getWindow().getDecorView().findViewById(android.R.id.content), ""+t.getMessage(), Snackbar.LENGTH_SHORT).show();

                    }
                });
            }
        });

        editarealist=findViewById(R.id.editarealist);
      editarealist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String CityId=editcityid.getText().toString();
                final Dialog progressDialog = new Dialog(EditShopDetail.this);
                progressDialog.setContentView(R.layout.customdialog);
                progressDialog.setCancelable(false);
                progressDialog.show();
                UserInterface userInterface= ApiClient.getClient().create(UserInterface.class);
                Call<AdminAllAreaResponse> call=userInterface.getAllAreaAdmin(CityId);
                call.enqueue(new Callback<AdminAllAreaResponse>() {
                    @Override
                    public void onResponse(Call<AdminAllAreaResponse> call, Response<AdminAllAreaResponse> response) {
                        if (response.code()==200)
                        { final  Dialog areadialog=new Dialog(EditShopDetail.this);
                            areadialog.setContentView(R.layout.recyclerdialog);
                            DisplayMetrics metrics=getResources().getDisplayMetrics();
                            int width=metrics.widthPixels;
                            areadialog.getWindow().setLayout(width, LinearLayout.LayoutParams.MATCH_PARENT);
                            areaRecycler=areadialog.findViewById(R.id.dialogRecycler);
                             areaRecycler.removeAllViews();
                            allAreaList=response.body().getData();
                            areaRecycler.setLayoutManager(new LinearLayoutManager(EditShopDetail.this,LinearLayoutManager.VERTICAL,false));
                            areaRecycler.setAdapter(new AreaListAdapter(EditShopDetail.this,allAreaList,editarea,areadialog,editareaid));
                           areadialog.show();
                            progressDialog.dismiss();
                        }
                        else
                        {
                            progressDialog.dismiss();
                            Toast.makeText(EditShopDetail.this, "Something Goes Wrong", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<AdminAllAreaResponse> call, Throwable t) {
                        Snackbar.make(EditShopDetail.this.getWindow().getDecorView().findViewById(android.R.id.content), ""+t.getMessage(), Snackbar.LENGTH_SHORT).show();

                    }
                });

            }
        });

     editshopgobackarrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                overridePendingTransition(R.anim.transitionrighttoleft, R.anim.transitionrighttoleft);
                finish();
            }
        });
       editshopImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if ((ContextCompat.checkSelfPermission(EditShopDetail.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)) {
                    requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA},
                            CAMERA_ALL_PERMISSIONS_RESULT);
                }
                else {
                    LinearLayout camera, folder;
                    dialog = new Dialog(EditShopDetail.this);
                    dialog.setContentView(R.layout.dialogboxcamera);
                    DisplayMetrics metrics=getResources().getDisplayMetrics();
                    int width=metrics.widthPixels;
                    dialog.getWindow().setLayout(width, CardView.LayoutParams.MATCH_PARENT);
                    dialog.show();
                    dialog.setCancelable(true);
                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    Window window = dialog.getWindow();
                    window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
                    camera = dialog.findViewById(R.id.camera);
                    folder = dialog.findViewById(R.id.folder);
                    folder.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                        /*checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                STORAGE_PERMISSION_CODE);*/
                            Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            startActivityForResult(i, 100);
                            dialog.dismiss();



                        }
                    });
                    camera.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            //checkPermission(Manifest.permission.CAMERA, CAMERA_PERMISSION_CODE);
                            Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            startActivityForResult(cameraIntent, 101);
                            dialog.dismiss();


                        }
                    });
                }
            }
        });
        if (imagePic != null && !imagePic.isEmpty() && !imagePic.equals("null")) {

            Picasso.get().load(imagePic).into(editshopImage);

            bitmap = ((BitmapDrawable) editshopImage.getDrawable().getCurrent()).getBitmap();
            Log.e("bitmap", "" + bitmap);
            converetdImage = getResizedBitmap(bitmap, 500);

        } else { }
        submiteditshop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                SharedPreferencesAdmin sharedPreferencesAdmin=new SharedPreferencesAdmin(EditShopDetail.this);
                String pname=eteditname.getText().toString();
                String id=dataBean.getId();
                String cityname=editcityid.getText().toString();
                String areaname=editareaid.getText().toString();
                String shopname=eteditshopname.getText().toString();
                String email=eteditpersonemail.getText().toString();
                String phone=eteditshopphone.getText().toString();
                String web=eteditwebsite.getText().toString();
                String Address=eteditaddress.getText().toString();
                String landmark=eteditlandmark.getText().toString();
                String msg=eteditmsg.getText().toString();
                String wtsp=eteditwtsp.getText().toString();
                if (cityname.equalsIgnoreCase("City")&& areaname.equalsIgnoreCase("Area")&&shopname.isEmpty()
                        && phone.isEmpty()&& Address.isEmpty()&&landmark.isEmpty()&& pname.isEmpty())
                    Snackbar.make(EditShopDetail.this.getWindow().getDecorView().findViewById(android.R.id.content), "Please Enter All Details ", Snackbar.LENGTH_SHORT).show();
                else if(cityname.equalsIgnoreCase("City"))
                    Snackbar.make(EditShopDetail.this.getWindow().getDecorView().findViewById(android.R.id.content), "Please Select City", Snackbar.LENGTH_SHORT).show();
                else if (areaname.equalsIgnoreCase("Area"))
                    Snackbar.make(EditShopDetail.this.getWindow().getDecorView().findViewById(android.R.id.content), "Please Select Area ", Snackbar.LENGTH_SHORT).show();
                else if (shopname.isEmpty())
                    Snackbar.make(EditShopDetail.this.getWindow().getDecorView().findViewById(android.R.id.content), "Please Enter Owner Name ", Snackbar.LENGTH_SHORT).show();
                else if (pname.isEmpty())
                    Snackbar.make(EditShopDetail.this.getWindow().getDecorView().findViewById(android.R.id.content), "Please Enter Shop Name ", Snackbar.LENGTH_SHORT).show();

               else if (phone.isEmpty() ||phone.length()!=10)
                    Snackbar.make(EditShopDetail.this.getWindow().getDecorView().findViewById(android.R.id.content), "Please Enter Valid Mobile No. ", Snackbar.LENGTH_SHORT).show();
                else if (Address.isEmpty())
                    Snackbar.make(EditShopDetail.this.getWindow().getDecorView().findViewById(android.R.id.content), "Please fill Address field Manually", Snackbar.LENGTH_SHORT).show();
                else if (landmark.isEmpty())
                    Snackbar.make(EditShopDetail.this.getWindow().getDecorView().findViewById(android.R.id.content), "Please Enter Landmark near by you", Snackbar.LENGTH_SHORT).show();
                else
                {
                    final Dialog progressDialog=new Dialog(EditShopDetail.this);
                    progressDialog.setContentView(R.layout.customdialog);
                    progressDialog.setCancelable(false);
                    progressDialog.show();
                    MultipartBody.Part body=null;
                    if (image != null) {
                        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), image);
                        body = MultipartBody.Part.createFormData("photo", image.getName(), requestFile);
                    } else {

                        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), "");
                        body = MultipartBody.Part.createFormData("photo", "", requestFile);
                    }
                    RequestBody sid = RequestBody.create(MediaType.parse("multipart/form-data"), id);
                    RequestBody oname = RequestBody.create(MediaType.parse("multipart/form-data"), pname);
                    RequestBody sname = RequestBody.create(MediaType.parse("multipart/form-data"), shopname);
                    RequestBody sarea = RequestBody.create(MediaType.parse("multipart/form-data"), areaname);
                    RequestBody scity = RequestBody.create(MediaType.parse("multipart/form-data"), cityname);
                    RequestBody slat = RequestBody.create(MediaType.parse("multipart/form-data"), String.valueOf(latitude));
                    RequestBody slng = RequestBody.create(MediaType.parse("multipart/form-data"), String.valueOf(longitude));
                    RequestBody smob = RequestBody.create(MediaType.parse("multipart/form-data"), phone);
                    RequestBody swtsp = RequestBody.create(MediaType.parse("multipart/form-data"), wtsp);
                    RequestBody semail = RequestBody.create(MediaType.parse("multipart/form-data"), email);
                    RequestBody sstatus = RequestBody.create(MediaType.parse("multipart/form-data"), status);
                    RequestBody sstate = RequestBody.create(MediaType.parse("multipart/form-data"), stateId);
                    RequestBody sdistrict = RequestBody.create(MediaType.parse("multipart/form-data"), districtId);

                    UserInterface userInterface=ApiClient.getClient().create(UserInterface.class);
                    Call<UpdateShopResponse>call=userInterface.updateShop(sid,sname,oname,semail,smob,swtsp,scity,sarea,slat,slng,sstatus,body);
                    call.enqueue(new Callback<UpdateShopResponse>() {
                        @Override
                        public void onResponse(Call<UpdateShopResponse> call, Response<UpdateShopResponse> response) {
                            if (response.code()==200){
                                progressDialog.dismiss();
                                Toast.makeText(EditShopDetail.this, "Shop Updated Successfully", Toast.LENGTH_SHORT).show();
                                Intent intent=new Intent(EditShopDetail.this,UserDashBoard.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                                finish();
                            }
                            else if (response.code()!=200)
                            {
                                progressDialog.dismiss();
                                Toast.makeText(EditShopDetail.this, "Shop Not Updated", Toast.LENGTH_LONG).show();

                                Converter<ResponseBody, ApiError> converter =
                                        ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                                ApiError error;
                                try {
                                    error = converter.convert(response.errorBody());
                                    ApiError.StatusBean status=error.getStatus();
                                    Toast.makeText(EditShopDetail.this, ""+status.getMessage(), Toast.LENGTH_LONG).show();
                                } catch (IOException e) { e.printStackTrace(); }
                            }
                        }

                        @Override
                        public void onFailure(Call<UpdateShopResponse> call, Throwable t) {

                            Toast.makeText(EditShopDetail.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();

                        }
                    });

                }
            }
        });
    }
    private Bitmap getResizedBitmap(Bitmap image, int maxSize) {

        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }
    public Intent getPickImageChooserIntent() {

        // Determine Uri of camera image to save.
        Uri outputFileUri = getCaptureImageOutputUri();

        List<Intent> allIntents = new ArrayList<Intent>();
        PackageManager packageManager = getPackageManager();

        // collect all camera intents
        Intent captureIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        List listCam = packageManager.queryIntentActivities(captureIntent, 0);
        for (Object res : listCam) {
            Intent intent = new Intent(captureIntent);
            intent.setComponent(new ComponentName(((ResolveInfo)res).activityInfo.packageName, ((ResolveInfo)res).activityInfo.name));
            intent.setPackage(((ResolveInfo)res).activityInfo.packageName);
            if (outputFileUri != null) {
                intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            }
            allIntents.add(intent);
        }

        // collect all gallery intents
        Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");
        List listGallery = packageManager.queryIntentActivities(galleryIntent, 0);
        for (Object res : listGallery) {
            Intent intent = new Intent(galleryIntent);
            intent.setComponent(new ComponentName(((ResolveInfo)res).activityInfo.packageName, ((ResolveInfo)res).activityInfo.name));
            intent.setPackage(((ResolveInfo)res).activityInfo.packageName);
            allIntents.add(intent);
        }

        // the main intent is the last in the list (fucking android) so pickup the useless one
        Intent mainIntent = allIntents.get(allIntents.size() - 1);
        for (Intent intent : allIntents) {
            if (intent.getComponent().getClassName().equals(getClass()))
            {
                mainIntent = intent;
                break;
            }
        }
        allIntents.remove(mainIntent);

        // Create a chooser from the main intent
        Intent chooserIntent = Intent.createChooser(mainIntent, "Select source");

        // Add all other intents
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, allIntents.toArray(new Parcelable[allIntents.size()]));

        return chooserIntent;
    }
    private Uri getCaptureImageOutputUri() {
        Uri outputFileUri = null;
        File getImage = getExternalCacheDir();
        if (getImage != null) {
            outputFileUri = Uri.fromFile(new File(getImage.getPath(), "profile.png"));
        }
        return outputFileUri;
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //  if (requestCode == 100 && resultCode == RESULT_OK && data != null) {

        //     imagepath=selectedImage.getPath();




        if (requestCode == 100 && resultCode == RESULT_OK && data != null) {

//the image URI
            Uri selectedImage = data.getData();

            //     imagepath=selectedImage.getPath();


            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            picturePath = cursor.getString(columnIndex);
            cursor.close();


            if (picturePath != null && !picturePath.equals("")) {
                image = new File(picturePath);
            }

            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);
                converetdImage = getResizedBitmap(bitmap, 500);
                editshopImage.setImageBitmap(converetdImage);
                editshopImage.setVisibility(View.VISIBLE);


            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (requestCode == 101 && resultCode == Activity.RESULT_OK) {


            Bitmap converetdImage = (Bitmap) data.getExtras().get("data");
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            converetdImage.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
            editshopImage.setImageBitmap(converetdImage);
            editshopImage.setVisibility(View.VISIBLE);
            image = new File(Environment.getExternalStorageDirectory(), "temp.jpg");
            FileOutputStream fo;
            try {
                fo = new FileOutputStream(image);
                fo.write(bytes.toByteArray());
                fo.close();
            } catch (IOException e) {
                e.printStackTrace();
            }


        }
    }

    //    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode,resultCode,data);
//        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
//        Bitmap bitmap;
//
//        if (resultCode == Activity.RESULT_OK) {
//
//            ImageView imageView = (ImageView) findViewById(R.id.editshopImage);
//
//            if (getPickImageResultUri(data) != null) {
//                picUri = getPickImageResultUri(data);
//
//                String[] filePathColumn = {MediaStore.Images.Media.DATA};
//                Cursor cursor = getContentResolver().query(picUri,
//                        filePathColumn, null, null, null);
//                cursor.moveToFirst();
//                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
//                picturePath = cursor.getString(columnIndex);
//                cursor.close();
//
//                if (picturePath != null && !picturePath.equals("")) {
//                    image = new File(picturePath);
//                }
//                try {
//                    myBitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), picUri);
//                    // myBitmap = rotateImageIfRequired(myBitmap, picUri);
//                    myBitmap = getResizedBitmap(myBitmap, 500);
//
//                    CircleImageView croppedImageView = (CircleImageView) findViewById(R.id.editshopImage);
//                    croppedImageView.setImageBitmap(myBitmap);
//                    imageView.setImageBitmap(myBitmap);
////                    image = new File(Environment.getExternalStorageDirectory(), "temp.jpg");
////                    FileOutputStream fo;
////                    try {
////                        fo = new FileOutputStream(image);
////                        fo.write(bytes.toByteArray());
////                        fo.close();
////                    } catch (IOException e) {
////                        e.printStackTrace();
////                    }
//
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//
//
//            } else {
//
//
//                bitmap = (Bitmap) data.getExtras().get("data");
//
//                myBitmap = bitmap;
//                CircleImageView croppedImageView = (CircleImageView) findViewById(R.id.editshopImage);
//                if (croppedImageView != null) {
//                    croppedImageView.setImageBitmap(myBitmap);
//                }
//
//                imageView.setImageBitmap(myBitmap);
//
//            }
//
//        }
//
//    }
    private static Bitmap rotateImageIfRequired(Bitmap img, Uri selectedImage) throws IOException {

        ExifInterface ei = new ExifInterface(selectedImage.getPath());
        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                return rotateImage(img, 90);
            case ExifInterface.ORIENTATION_ROTATE_180:
                return rotateImage(img, 180);
            case ExifInterface.ORIENTATION_ROTATE_270:
                return rotateImage(img, 270);
            default:
                return img;
        }
    }
    private static Bitmap rotateImage(Bitmap img, int degree) {
        Matrix matrix = new Matrix();
        matrix.postRotate(degree);
        Bitmap rotatedImg = Bitmap.createBitmap(img, 0, 0, img.getWidth(), img.getHeight(), matrix, true);
        img.recycle();
        return rotatedImg;
    }
//    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
//        int width = image.getWidth();
//        int height = image.getHeight();
//
//        float bitmapRatio = (float) width / (float) height;
//        if (bitmapRatio > 0) {
//            width = maxSize;
//            height = (int) (width / bitmapRatio);
//        } else {
//            height = maxSize;
//            width = (int) (height * bitmapRatio);
//        }
//        return Bitmap.createScaledBitmap(image, width, height, true);
//    }
    public Uri getPickImageResultUri(Intent data) {
        boolean isCamera = true;
        if (data != null) {
            String action = data.getAction();
            isCamera = action != null && action.equals(MediaStore.ACTION_IMAGE_CAPTURE);
        }


        return isCamera ? getCaptureImageOutputUri() : data.getData();
    }



    private boolean hasPermission(String permission) {
        if (canMakeSmores()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                return (checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED);
            }
        }
        return true;
    }
    private boolean canMakeSmores() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }
    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {

            case PERMISSION_ID:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Granted. Start getting the location information
                }
                break;
            case CAMERA_ALL_PERMISSIONS_RESULT:
                if (grantResults.length <= 0 || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(this, "Permission denied", Toast.LENGTH_SHORT).show();
                }
                else {
//                    startActivityForResult(, 200);
                }
                break;

        }

    }
    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(EditShopDetail.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }
    private class GeocoderHandler extends Handler {
        @Override
        public void handleMessage(Message message) {
            String locationAddress;
            switch (message.what) {
                case 1:
                    Bundle bundle = message.getData();
                    locationAddress = bundle.getString("address");
                    break;
                default:
                    locationAddress = null;
            }
            eteditaddress.setText(locationAddress);
        }
    }
    private ArrayList findUnAskedPermissions(ArrayList wanted) {
        ArrayList result = new ArrayList();
        for (Object perm:wanted) {
            if (!hasPermission((String) perm)) {
                result.add(perm);
            }
        }

        return result;
    }
    private ArrayList findUnAskedCameraPermissions(ArrayList wanted) {
        ArrayList result = new ArrayList();

        for (Object perm : wanted) {
            if (!hascameraPermission((String)perm)) {
                result.add(perm);
            }
        }

        return result;
    }
    private boolean hascameraPermission(String permission) {
        if (canMakeSmores()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                return (checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED);
            }
        }
        return true;
    }
    @Override
    public void onResume(){
        super.onResume();
        if (checkPermissions()) {
            getLastLocation();
        }

    }
    @SuppressLint("MissingPermission")
    private void getLastLocation(){
        if (checkPermissions()) {
            if (isLocationEnabled()) {
                mFusedLocationClient.getLastLocation().addOnCompleteListener(
                        new OnCompleteListener<Location>() {
                            @Override
                            public void onComplete(@NonNull Task<Location> task) {
                                Location location = task.getResult();
                                if (location == null) {
                                    requestNewLocationData();
                                } else {
                                    lat=String.valueOf(location.getLatitude());
                                    lng=String.valueOf(location.getLongitude());
                                    Toast.makeText(EditShopDetail.this, lat+" "+lng, Toast.LENGTH_SHORT).show();
                                    LocationAddress locationAddress = new LocationAddress();
                                    locationAddress.getAddressFromLocation(location.getLatitude(), location.getLongitude(),
                                            getApplicationContext(), new GeocoderHandler());
                                }
                            }
                        }
                );
            } else {
                Toast.makeText(this, "Turn on location", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        } else {
            requestPermissions();
        }
    }
    @SuppressLint("MissingPermission")
    private void requestNewLocationData(){

        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(0);
        mLocationRequest.setFastestInterval(0);
        mLocationRequest.setNumUpdates(1);

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mFusedLocationClient.requestLocationUpdates(
                mLocationRequest, mLocationCallback,
                Looper.myLooper()
        );

    }
    private LocationCallback mLocationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            Location mLastLocation = locationResult.getLastLocation();
            lat=String.valueOf(mLastLocation.getLatitude());
            lng=String.valueOf(mLastLocation.getLongitude());

        }
    };

    private boolean checkPermissions() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        return false;
    }

    private void requestPermissions() {
        ActivityCompat.requestPermissions(
                this,
                new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION},
                PERMISSION_ID
        );
    }
    private boolean isLocationEnabled() {
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(
                LocationManager.NETWORK_PROVIDER
        );
    }



    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        // save file url in bundle as it will be null on scren orientation
        // changes
        outState.putParcelable("pic_uri", picUri);
    }
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        // get the file url
        picUri = savedInstanceState.getParcelable("pic_uri");
    }
    private void districtlist()
    {
        districtId="";
        progress = new Dialog(EditShopDetail.this);
        progress.setContentView(R.layout.customdialog);
        progress.setCancelable(false);
        progress.show();
        UserInterface userInterface= ApiClient.getClient().create(UserInterface.class);
        Call<DistrictResponse>call=userInterface.getDistrict(stateId);
        call.enqueue(new Callback<DistrictResponse>() {
            @Override
            public void onResponse(Call<DistrictResponse> call, Response<DistrictResponse> response) {
                if (response.code()==200)
                {
                    allDistrict=response.body().getData();
                    final  Dialog districtDialog=new Dialog(EditShopDetail.this);
                    districtDialog.setContentView(R.layout.recyclerdialog);
                    DisplayMetrics metrics=getResources().getDisplayMetrics();
                    int width=metrics.widthPixels;
                    districtDialog.getWindow().setLayout(width, LinearLayout.LayoutParams.WRAP_CONTENT);
                    districtRecycler=districtDialog.findViewById(R.id.dialogRecycler);
                    districtRecycler.setLayoutManager(new LinearLayoutManager(EditShopDetail.this,LinearLayoutManager.VERTICAL,false));
                    districtRecycler.setAdapter(new DistrictListAdapter(EditShopDetail.this,allDistrict,districtDialog, new CustomItemClickListener() {
                        @Override
                        public void onItemClick(View v, int position, String value) {
                            district.setText(value);
                            districtId=String.valueOf(position);
                        }
                    }));
                    progress.dismiss();
                    districtDialog.show();
                }
                else {
                    progress.dismiss();
                    Toast.makeText(EditShopDetail.this, "Something Goes Wrong", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<DistrictResponse> call, Throwable t) {
                progress.dismiss();
                Toast.makeText(EditShopDetail.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
    private void statelist()
    {
        state.setText("");
        progress = new Dialog(EditShopDetail.this);
        progress.setContentView(R.layout.customdialog);
        progress.setCancelable(false);
        progress.show();
        UserInterface userInterface= ApiClient.getClient().create(UserInterface.class);
        /*--------------------------------------*/
        Call<StateResponse>call=userInterface.stateList();
        call.enqueue(new Callback<StateResponse>() {
            @Override
            public void onResponse(Call<StateResponse> call, Response<StateResponse> response) {
                if (response.code()==200)
                {
                    allStateList=response.body().getData();
                    final  Dialog stateDialog=new Dialog(EditShopDetail.this);
                    stateDialog.setContentView(R.layout.recyclerdialog);
                    DisplayMetrics metrics=getResources().getDisplayMetrics();
                    int width=metrics.widthPixels;
                    stateDialog.getWindow().setLayout(width, LinearLayout.LayoutParams.WRAP_CONTENT);
                    stateRecycler=stateDialog.findViewById(R.id.dialogRecycler);
                    stateRecycler.setLayoutManager(new LinearLayoutManager(EditShopDetail.this,LinearLayoutManager.VERTICAL,false));
                    stateRecycler.setAdapter(new StateListAdapter(EditShopDetail.this,allStateList,stateDialog, new CustomItemClickListener() {
                        @Override
                        public void onItemClick(View v, int position, String value) {
                            state.setText(value);
                            stateId =String.valueOf(position);
                        }
                    }));
                    progress.dismiss();
                    stateDialog.show();
                }
                else
                {
                    progress.dismiss();
                    Toast.makeText(EditShopDetail.this, "Something Goes Wrong", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<StateResponse> call, Throwable t) {
                progress.dismiss();
                Toast.makeText(EditShopDetail.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }
}

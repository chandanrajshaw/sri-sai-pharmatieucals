package kashyap.chandan.medishop.agentPannel;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import kashyap.chandan.medishop.R;
import kashyap.chandan.medishop.pojoclasses.AdminCityList;
import kashyap.chandan.medishop.pojoclasses.GetCityResponseUser;

public class CityListAdapter extends RecyclerView.Adapter<CityListAdapter.MyViewHolder> {
    Context context;
    List<AdminCityList.DataBean> allCityList;
    TextView city,cityid;
    Dialog cityDialog;
    private int mSelectedItem = -1;
    GetCityResponseUser.DataBean item;
   private String selectedCity,selectedid;
    public CityListAdapter(Context context, List<AdminCityList.DataBean> allCityList, TextView city, Dialog cityDialog, TextView cityid) {
        this.allCityList=allCityList;
        this.city=city;
        this.context=context;
        this.cityDialog=cityDialog;
        this.cityid=cityid;
    }

    @NonNull
    @Override
    public CityListAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.itemlayout,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final CityListAdapter.MyViewHolder holder, int position) {
        holder.radioselect.setChecked(position == mSelectedItem);
        holder.cityitem.setText(allCityList.get(position).getCity_name());
         TextView ok= cityDialog.findViewById(R.id.ok);
ok.setOnClickListener(new View.OnClickListener() {
    @Override

    public void onClick(View view) {
        city.setText(selectedCity);
        if (city.getText().toString().isEmpty()){
            Toast.makeText(context, "Select City", Toast.LENGTH_SHORT).show();
        }
        else {
            city.setText(selectedCity);
            cityid.setText(selectedid);
            cityDialog.dismiss();
        }

    }
});


    }

    @Override
    public int getItemCount() {
        return allCityList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView cityitem,dialogheader;
        RadioButton radioselect;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            cityitem=itemView.findViewById(R.id.items);
radioselect=itemView.findViewById(R.id.selectedItem);

            View.OnClickListener clickListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mSelectedItem = getAdapterPosition();
                    notifyDataSetChanged();
                    selectedCity=allCityList.get(mSelectedItem).getCity_name();
                    selectedid=allCityList.get(mSelectedItem).getId();
                }
            };
itemView.setOnClickListener(clickListener);
radioselect.setOnClickListener(clickListener);

        }
    }
}

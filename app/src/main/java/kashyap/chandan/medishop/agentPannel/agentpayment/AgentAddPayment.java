package kashyap.chandan.medishop.agentPannel.agentpayment;

import android.Manifest;
import android.app.ActionBar;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import kashyap.chandan.medishop.ApiClient;
import kashyap.chandan.medishop.ConnectionDetector;
import kashyap.chandan.medishop.Order.OrderAllShopAdapter;
import kashyap.chandan.medishop.Order.OrderShopListResponse;
import kashyap.chandan.medishop.R;
import kashyap.chandan.medishop.UserInterface;
import kashyap.chandan.medishop.admin.SharedPreferencesAdmin;
import kashyap.chandan.medishop.payment.Payment;
import kashyap.chandan.medishop.payment.PtypeAdapter;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AgentAddPayment extends AppCompatActivity {
TextView toolheader,deldate,shopname,ptype,shopid,pid,addPayment,desc;
ImageView billimg;
RelativeLayout shopdrop,datedialog,paytype;
EditText amt,billno;
SharedPreferencesAdmin sharedPreferencesAdmin;
    Bitmap bitmap,converetdImage;
    Uri picUri;
    File image=null;
    String picturePath,imagePic;
ImageView goback;
    Dialog dialog;
RecyclerView shoprec,payrec;
ConnectionDetector connectionDetector;
String[] payment={"Cash","Online","Cheque"};
    private final static int CAMERA_ALL_PERMISSIONS_RESULT = 108;
    Calendar calendar=Calendar.getInstance();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_add_payment);
        toolheader=findViewById(R.id.customtoolheader);
        sharedPreferencesAdmin=new SharedPreferencesAdmin(AgentAddPayment.this);
        connectionDetector=new ConnectionDetector(AgentAddPayment.this);
        toolheader.setText("Add Payment");
        goback=findViewById(R.id.ordergobackarrow);
        billimg=findViewById(R.id.billimg);
        shopdrop=findViewById(R.id.shoplistdrop);
        datedialog=findViewById(R.id.date);
        amt=findViewById(R.id.amt);
        amt.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b)
                amt.setHint("");
                else
                    amt.setHint("Amount");
            }
        });
        desc=findViewById(R.id.desc);
        desc.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b)
                    desc.setHint("");
                else
                    desc.setHint("Description");
            }
        });
        billno=findViewById(R.id.billno);
        billno.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b)
                billno.setHint("");
                else
                    billno.setHint("Bill No");
            }
        });
        paytype=findViewById(R.id.paymenttype);
        deldate=findViewById(R.id.delDate);
        shopname=findViewById(R.id.shopname);
        pid=findViewById(R.id.ptypeid);
        shopid=findViewById(R.id.shopid);
        ptype=findViewById(R.id.ptype);
        addPayment=findViewById(R.id.Submit);
        goback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                overridePendingTransition(R.anim.transitionrighttoleft,R.anim.transitionrighttoleft);
                finish();
            }
        });
        shopdrop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!connectionDetector.isConnectingToInternet()){
                    Snackbar.make(AgentAddPayment.this.getWindow().getDecorView().findViewById(android.R.id.content),"Plzz Connect to internet and try again",Snackbar.LENGTH_SHORT).show();
                }
                else if (connectionDetector.isConnectingToInternet())
                {
                    final Dialog progress=new Dialog(AgentAddPayment.this);
                    progress.setContentView(R.layout.customdialog);
                    progress.setCancelable(false);
                    progress.show();
                    UserInterface userInterface= ApiClient.getClient().create(UserInterface.class);
                    Call<OrderShopListResponse> call=userInterface.getOrderShopList();
                    call.enqueue(new Callback<OrderShopListResponse>() {
                        @Override
                        public void onResponse(Call<OrderShopListResponse> call, Response<OrderShopListResponse> response) {
                            if (response.code()==200)
                            {
                                progress.dismiss();
                                List<OrderShopListResponse.DataBean> allshops=new ArrayList<OrderShopListResponse.DataBean>();
                                allshops=response.body().getData();
                                Dialog shopdialog=new Dialog(AgentAddPayment.this);
                                shopdialog.setContentView(R.layout.recyclerdialog);
                                DisplayMetrics metrics=getResources().getDisplayMetrics();
                                int width=metrics.widthPixels;
                                shopdialog.getWindow().setLayout(width, LinearLayout.LayoutParams.MATCH_PARENT);
                                shopdialog.setCancelable(false);
                                shoprec=shopdialog.findViewById(R.id.dialogRecycler);
                                shoprec.setLayoutManager(new LinearLayoutManager(AgentAddPayment.this,LinearLayoutManager.VERTICAL,false));
                                shoprec.setAdapter(new OrderAllShopAdapter(AgentAddPayment.this,allshops,shopname,shopid,shopdialog));
                                shopdialog.show();
                            }
                            else if (response.code()!=200)
                            {
                                progress.dismiss();
                                Snackbar.make(AgentAddPayment.this.getWindow().getDecorView().findViewById(android.R.id.content),"Error Occurs !!Try again Later",Snackbar.LENGTH_SHORT).show();

                            }
                        }

                        @Override
                        public void onFailure(Call<OrderShopListResponse> call, Throwable t) {
                            progress.dismiss();
                            Snackbar.make(AgentAddPayment.this.getWindow().getDecorView().findViewById(android.R.id.content),""+t.getMessage(),Snackbar.LENGTH_SHORT).show();

                        }
                    });
                }
            }
        });
        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, monthOfYear);
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }
        };
        deldate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(AgentAddPayment.this,R.style.TimePickerTheme,date, calendar
                        .get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
        ptype.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Dialog paydialog=new Dialog(AgentAddPayment.this);
                paydialog.setContentView(R.layout.recyclerdialog);
                paydialog.setCancelable(false);
                DisplayMetrics metrics=getResources().getDisplayMetrics();
                int width=metrics.widthPixels;
                paydialog.getWindow().setLayout(width, LinearLayout.LayoutParams.MATCH_PARENT);
                payrec=paydialog.findViewById(R.id.dialogRecycler);
                payrec.setLayoutManager(new LinearLayoutManager(AgentAddPayment.this,LinearLayoutManager.VERTICAL,false));
                payrec.setAdapter(new PtypeAdapter(AgentAddPayment.this,payment,ptype,pid,paydialog));
paydialog.show();
            }
        });
        billimg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if ((ContextCompat.checkSelfPermission(AgentAddPayment.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)) {
                    requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA},
                            CAMERA_ALL_PERMISSIONS_RESULT);
                }
                else {
                    LinearLayout camera, folder;
                    dialog = new Dialog(AgentAddPayment.this);
                    dialog.setContentView(R.layout.dialogboxcamera);
                    DisplayMetrics metrics=getResources().getDisplayMetrics();
                    int width=metrics.widthPixels;
                    dialog.getWindow().setLayout(width, LinearLayout.LayoutParams.MATCH_PARENT);
                    dialog.show();
                    dialog.setCancelable(true);
                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    Window window = dialog.getWindow();
                    window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
                    camera = dialog.findViewById(R.id.camera);
                    folder = dialog.findViewById(R.id.folder);
                    folder.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                        /*checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                STORAGE_PERMISSION_CODE);*/
                            Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            startActivityForResult(i, 100);
                            dialog.dismiss();



                        }
                    });
                    camera.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            //checkPermission(Manifest.permission.CAMERA, CAMERA_PERMISSION_CODE);
                            Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            startActivityForResult(cameraIntent, 101);
                            dialog.dismiss();


                        }
                    });
                }
            }
        });
        if (imagePic != null && !imagePic.isEmpty() && !imagePic.equals("null")) {

            Picasso.get().load(imagePic).into(billimg);

            bitmap = ((BitmapDrawable) billimg.getDrawable().getCurrent()).getBitmap();
            Log.e("bitmap", "" + bitmap);
            converetdImage = getResizedBitmap(bitmap, 500);

        } else { }

        addPayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String sid=shopid.getText().toString();
//                String pdate=deldate.getText().toString();
                String amount=amt.getText().toString();
                String paymentType=pid.getText().toString();
                String bill=billno.getText().toString();
                String Description=desc.getText().toString();
                if (sid.isEmpty()&&amount.isEmpty()&&paymentType.isEmpty()&&bill.isEmpty()&& image==null&&Description.isEmpty())
                    Snackbar.make(AgentAddPayment.this.getWindow().getDecorView().findViewById(android.R.id.content),"Enter All the Fields",Snackbar.LENGTH_LONG).show();
else if (image==null)
                    Snackbar.make(AgentAddPayment.this.getWindow().getDecorView().findViewById(android.R.id.content),"Select Image",Snackbar.LENGTH_LONG).show();
else if (sid.isEmpty())
                    Snackbar.make(AgentAddPayment.this.getWindow().getDecorView().findViewById(android.R.id.content),"Select Shop",Snackbar.LENGTH_LONG).show();
else if (paymentType.isEmpty())
                    Snackbar.make(AgentAddPayment.this.getWindow().getDecorView().findViewById(android.R.id.content),"Select Payment type",Snackbar.LENGTH_LONG).show();
else if (bill.isEmpty())
                    Snackbar.make(AgentAddPayment.this.getWindow().getDecorView().findViewById(android.R.id.content),"Enter Bill No",Snackbar.LENGTH_LONG).show();
else if (Description.isEmpty())
                    Snackbar.make(AgentAddPayment.this.getWindow().getDecorView().findViewById(android.R.id.content),"Description ",Snackbar.LENGTH_LONG).show();

                else {
                    String ptype=null;
                    if (paymentType.equalsIgnoreCase("Cash")) ptype="1";
                    else if (paymentType.equalsIgnoreCase("Online"))ptype="2";
                    else if (paymentType.equalsIgnoreCase("Cheque"))ptype="3";
                    MultipartBody.Part body=null;
                    if (image != null) {
                        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), image);
                        body = MultipartBody.Part.createFormData("image", image.getName(), requestFile);
                    }
                    RequestBody agentid = RequestBody.create(MediaType.parse("multipart/form-data"), sharedPreferencesAdmin.getAdminIdPreferences());
                    RequestBody id = RequestBody.create(MediaType.parse("multipart/form-data"), sid);
//                    RequestBody date = RequestBody.create(MediaType.parse("multipart/form-data"), pdate);
                    RequestBody type = RequestBody.create(MediaType.parse("multipart/form-data"), ptype);
                    RequestBody amts = RequestBody.create(MediaType.parse("multipart/form-data"), amount);
                    RequestBody bills = RequestBody.create(MediaType.parse("multipart/form-data"), bill);
                    RequestBody des = RequestBody.create(MediaType.parse("multipart/form-data"), Description);

                    final Dialog progress=new Dialog(AgentAddPayment.this);
                    progress.setContentView(R.layout.customdialog);
                    progress.setCancelable(false);
                    progress.show();
                    UserInterface userInterface=ApiClient.getClient().create(UserInterface.class);
                    Call<AgentAddPaymentResponse>call=userInterface.addAgentPayment(body,agentid,id,amts,type,des,bills);
                    call.enqueue(new Callback<AgentAddPaymentResponse>() {
                        @Override
                        public void onResponse(Call<AgentAddPaymentResponse> call, Response<AgentAddPaymentResponse> response) {
                            if (response.code()==200)
                            {
                                progress.dismiss();
                                Toast.makeText(AgentAddPayment.this, "Payment Added", Toast.LENGTH_SHORT).show();
                                Intent intent=new Intent(AgentAddPayment.this,Payment.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                overridePendingTransition(R.anim.transitionrighttoleft,R.anim.transitionrighttoleft);
                                startActivity(intent);
                                finish();

                            }
                            else if (response.code()!=200)
                            {
                                progress.dismiss();
                                Toast.makeText(AgentAddPayment.this, "Payment Not Added", Toast.LENGTH_SHORT).show();

                            }
                        }

                        @Override
                        public void onFailure(Call<AgentAddPaymentResponse> call, Throwable t) {
progress.dismiss();
                            Snackbar.make(AgentAddPayment.this.getWindow().getDecorView().findViewById(android.R.id.content),""+t.getMessage(),Snackbar.LENGTH_LONG).show();


                        }
                    });
                }

            }
        });

    }
    private void updateLabel() {

        String myFormat = "MM-dd-yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        deldate.setText(sdf.format(calendar.getTime()));
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode)
        {
            case  CAMERA_ALL_PERMISSIONS_RESULT:
                if (grantResults.length <= 0 || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(this, "Permission denied", Toast.LENGTH_SHORT).show();
                }
                else {
//                    startActivityForResult(, 200);
                }

        }
    }
    private Bitmap getResizedBitmap(Bitmap image, int maxSize) {

        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }





    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //  if (requestCode == 100 && resultCode == RESULT_OK && data != null) {

        //     imagepath=selectedImage.getPath();




        if (requestCode == 100 && resultCode == RESULT_OK && data != null) {

//the image URI
            Uri selectedImage = data.getData();

            //     imagepath=selectedImage.getPath();


            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            picturePath = cursor.getString(columnIndex);
            cursor.close();


            if (picturePath != null && !picturePath.equals("")) {
                image = new File(picturePath);
            }

            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);
                converetdImage = getResizedBitmap(bitmap, 500);
                billimg.setImageBitmap(converetdImage);
                billimg.setVisibility(View.VISIBLE);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else if (requestCode == 101 && resultCode == Activity.RESULT_OK) {


            Bitmap converetdImage = (Bitmap) data.getExtras().get("data");
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            converetdImage.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
            billimg.setImageBitmap(converetdImage);
            billimg.setVisibility(View.VISIBLE);
            image = new File(Environment.getExternalStorageDirectory(), "temp.jpg");
            FileOutputStream fo;
            try {
                fo = new FileOutputStream(image);
                fo.write(bytes.toByteArray());
                fo.close();
            } catch (IOException e) {
                e.printStackTrace();
            }


        }
    }

}

package kashyap.chandan.medishop.agentPannel;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;


import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.io.Serializable;

import kashyap.chandan.medishop.R;


public class ViewShopDetail extends AppCompatActivity {
String shopid,status,agentid,lat,lng,cityid,areaid;
TextView shopnametool,shopname,shopcity,shopphone,shopemail,ownername,editshop;
String eemail;
ImageView viewshopgobackarrow,viewinmap,call,whatsapp,viewshopImage;
public static final int CALL_PERMISSION=10;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

     setContentView(R.layout.activity_view_shop_detail);
        editshop=findViewById(R.id.editshop);
        viewshopgobackarrow=findViewById(R.id.viewshopgobackarrow);
        viewinmap=findViewById(R.id.viewinmap);
        call=findViewById(R.id.call);
        whatsapp=findViewById(R.id.whatsapp);
        shopcity=findViewById(R.id.shopcity);
        shopphone=findViewById(R.id.shopphone);
        shopemail=findViewById(R.id.shopemail);
        shopname=findViewById(R.id.shopname);
        ownername=findViewById(R.id.ownername);
        viewshopImage=findViewById(R.id.viewshopImage);
        Intent i=getIntent();
        Bundle bundle=i.getBundleExtra("edit");
        final AgentAllShopResponse.DataBean dataBean = (AgentAllShopResponse.DataBean) bundle.getSerializable("editShop");
        Picasso.get().load("http://www.sbpharma.in/admin_assets/uploads/stores/"+dataBean.getPhoto()).error(R.mipmap.ic_launcher_launcher_round).placeholder(R.drawable.loading).into(viewshopImage);
//        viewShopDetailBinding.shoparea.setText();
        shopnametool=findViewById(R.id.toolshopname);
        shopnametool.setText(dataBean.getStore_name());
   shopcity.setText(dataBean.getArea_name()+","+dataBean.getCity_name()+","+dataBean.getDistrict_name()+","+dataBean.getState_name());;
shopname.setText(dataBean.getStore_name());
shopphone.setText(dataBean.getMobile());
eemail=dataBean.getEmail();
if (eemail.isEmpty()|| eemail.equalsIgnoreCase(""))
{
    shopemail.setText("N/A");
}
else {
    shopemail.setText(dataBean.getEmail());
}

ownername.setText(dataBean.getPerson_name());
      editshop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(ViewShopDetail.this,EditShopDetail.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                Bundle bundle=new Bundle();
                bundle.putSerializable("editShop", (Serializable) dataBean);
                intent.putExtra("edit",bundle);
                startActivity(intent);
            }
        });
viewshopgobackarrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                overridePendingTransition(R.anim.transitionrighttoleft, R.anim.transitionrighttoleft);
                finish();
            }
        });
       viewinmap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String geoUri = "http://maps.google.com/maps?q=loc:" + Double.valueOf(dataBean.getLongitude()) + "," + Double.valueOf(dataBean.getLatitude()) + " Shop";
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(geoUri));
                startActivity(intent);
            }
        });
call.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        Uri u = Uri.fromParts("tel",dataBean.getMobile(),null);
        if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.CALL_PHONE},
                    CALL_PERMISSION);



            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        Intent i = new Intent(Intent.ACTION_DIAL, u);
        startActivity(i);
    }
});
whatsapp.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        if (dataBean.getWa_number()==null||dataBean.getWa_number().equalsIgnoreCase("")){
            Toast.makeText(ViewShopDetail.this, "Whatsapp No Not Available", Toast.LENGTH_SHORT).show();
        }
      else
        {
            String url = "https://api.whatsapp.com/send?phone=+91"+dataBean.getWa_number();
            try {
                PackageManager pm = getPackageManager();
                pm.getPackageInfo("com.whatsapp", PackageManager.GET_ACTIVITIES);
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            } catch (PackageManager.NameNotFoundException e) {
                Toast.makeText(ViewShopDetail.this, "Whatsapp app not installed in your phone", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
        }
    }
});
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode)
        {
            case CALL_PERMISSION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Granted. Start getting the location information
                }
        }

    }


}

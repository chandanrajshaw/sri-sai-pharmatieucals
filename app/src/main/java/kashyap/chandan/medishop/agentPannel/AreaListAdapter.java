package kashyap.chandan.medishop.agentPannel;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import kashyap.chandan.medishop.R;
import kashyap.chandan.medishop.admin.AdminAllAreaResponse;
import kashyap.chandan.medishop.pojoclasses.GetAreaResponse;

public class AreaListAdapter extends RecyclerView.Adapter<AreaListAdapter.MyViewHolder> {
    List<AdminAllAreaResponse.DataBean> allAreaList;
    TextView area,areaid;
    Context context;
 Dialog areaDialog;
    private int SelectedItem = -1;
    String selectedArea,selectedid;


    public AreaListAdapter(Context context, List<AdminAllAreaResponse.DataBean> allAreaList, TextView area, Dialog areaDialog, TextView areaid ) {

        this.allAreaList=allAreaList;
        this.area=area;
        this.context=context;
        this.areaDialog=areaDialog;
        this.areaid=areaid;
    }

    @NonNull
    @Override
    public AreaListAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.itemlayout,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final AreaListAdapter.MyViewHolder holder, int position)
    {
        holder.radioselect.setChecked(position == SelectedItem);
        holder.areaitem.setText(allAreaList.get(position).getArea_name());
        TextView head=areaDialog.findViewById(R.id.dialoghead);
        head.setText("Select Area");
        TextView ok=areaDialog.findViewById(R.id.ok);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                area.setText(selectedArea);
                if (area.getText().toString().isEmpty()){
                    Toast.makeText(context,"Select Area",Toast.LENGTH_SHORT).show();
                }
                else {

                    areaid.setText(selectedid);
                    areaDialog.dismiss();
                }

            }
        });

    }

    @Override
    public int getItemCount() {
        return allAreaList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView areaitem,dialogheader;
        RadioButton radioselect;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            View.OnClickListener clickListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SelectedItem =getAdapterPosition();
                    notifyDataSetChanged();
                    selectedArea=allAreaList.get(SelectedItem).getArea_name();
                    selectedid=allAreaList.get(SelectedItem).getId();
                }
            };
            areaitem=itemView.findViewById(R.id.items);
            radioselect=itemView.findViewById(R.id.selectedItem);
itemView.setOnClickListener(clickListener);
radioselect.setOnClickListener(clickListener);
        }
    }
}

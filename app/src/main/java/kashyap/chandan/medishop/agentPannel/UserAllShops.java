package kashyap.chandan.medishop.agentPannel;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.List;

import kashyap.chandan.medishop.ApiClient;
import kashyap.chandan.medishop.ConnectionDetector;
import kashyap.chandan.medishop.R;
import kashyap.chandan.medishop.UserInterface;
import kashyap.chandan.medishop.admin.SharedPreferencesAdmin;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserAllShops extends AppCompatActivity {
ConnectionDetector connectionDetector;
RecyclerView usershoprecycler;
TextView shopnotfound;
SharedPreferencesAdmin sharedPreferencesAdmin;
ImageView gobackarrow;
    ShimmerFrameLayout shimmerFrameLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_user_all_shops);
        connectionDetector=new ConnectionDetector(UserAllShops.this);
        usershoprecycler=findViewById(R.id.usershoprecycler);
        shimmerFrameLayout=findViewById(R.id.shimmerview);
        shopnotfound=findViewById(R.id.shopnotfound);
        sharedPreferencesAdmin=new SharedPreferencesAdmin(UserAllShops.this);
        if (!connectionDetector.isConnectingToInternet())
        {
            shimmerFrameLayout.stopShimmer();
            shimmerFrameLayout.setVisibility(View.GONE);
           usershoprecycler.setVisibility(View.GONE);
        shopnotfound.setVisibility(View.VISIBLE);
            Snackbar.make(UserAllShops.this.getWindow().getDecorView().findViewById(android.R.id.content
            ),"Internet Is not available, First Connect to internet",Snackbar.LENGTH_LONG).show();
        }
        else {
            String agent_id=sharedPreferencesAdmin.getAdminIdPreferences();
//            final Dialog progressDialog=new Dialog(UserAllShops.this);
//            progressDialog.setContentView(R.layout.customdialog);
//            progressDialog.setCancelable(false);
//            progressDialog.show();
            UserInterface userInterface= ApiClient.getClient().create(UserInterface.class);
            Call<AgentAllShopResponse> call=userInterface.getAllAgentShop(agent_id);
            call.enqueue(new Callback<AgentAllShopResponse>() {
                @Override
                public void onResponse(Call<AgentAllShopResponse> call, Response<AgentAllShopResponse> response) {
                    if (response.code() == 200) {
                        shimmerFrameLayout.stopShimmer();
                        shimmerFrameLayout.setVisibility(View.GONE);
                        List<AgentAllShopResponse.DataBean> agentAllShop=new ArrayList<AgentAllShopResponse.DataBean>();
                        agentAllShop=response.body().getData();

                       usershoprecycler.setLayoutManager(new LinearLayoutManager(UserAllShops.this,LinearLayoutManager.VERTICAL,false));
                      usershoprecycler.setAdapter(new UserAllShopAdapter(UserAllShops.this,agentAllShop));
//                        progressDialog.dismiss();
                        usershoprecycler.setVisibility(View.VISIBLE);
                    }
                    else if (response.code() != 200)
                    {
                        shimmerFrameLayout.stopShimmer();
                        shimmerFrameLayout.setVisibility(View.GONE);
                        shopnotfound.setVisibility(View.VISIBLE);
//                        progressDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<AgentAllShopResponse> call, Throwable t) {

                }
            });
        }
        gobackarrow=findViewById(R.id.userallshopgobackarrow);
        gobackarrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                overridePendingTransition(R.anim.transitionrighttoleft, R.anim.transitionlefttoright);
                finish();
            }
        });
    }
    @Override
    protected void onResume() {
        super.onResume();
        shimmerFrameLayout.startShimmer();
    }

    @Override
    protected void onPause() {
        super.onPause();
        shimmerFrameLayout.stopShimmer();
    }
}

package kashyap.chandan.medishop.agentPannel.agentpayment;

import java.io.Serializable;
import java.util.List;

public class AgentPaymentListResponse implements Serializable {

    /**
     * status : {"code":200,"message":"Payments List(Here product_catagories belongs to pc_name)"}
     * data : [{"id":"1","unique_id":"SBPAY1","agent_id":"2","shop_id":"1","paid_amount":"200","payment_type":"1","description":"Paid","bill_no":"1234","image":"donation.png","date_time":"2020-04-06 14:29:08","status":"1","shop_name":"Shakti medical"}]
     */

    private StatusBean status;
    private List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean implements Serializable{
        /**
         * code : 200
         * message : Payments List(Here product_catagories belongs to pc_name)
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean implements Serializable {
        /**
         * id : 1
         * unique_id : SBPAY1
         * agent_id : 2
         * shop_id : 1
         * paid_amount : 200
         * payment_type : 1
         * description : Paid
         * bill_no : 1234
         * image : donation.png
         * date_time : 2020-04-06 14:29:08
         * status : 1
         * shop_name : Shakti medical
         */

        private String id;
        private String unique_id;
        private String agent_id;
        private String shop_id;
        private String paid_amount;
        private String payment_type;
        private String description;
        private String bill_no;
        private String image;
        private String date_time;
        private String status;
        private String shop_name;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getUnique_id() {
            return unique_id;
        }

        public void setUnique_id(String unique_id) {
            this.unique_id = unique_id;
        }

        public String getAgent_id() {
            return agent_id;
        }

        public void setAgent_id(String agent_id) {
            this.agent_id = agent_id;
        }

        public String getShop_id() {
            return shop_id;
        }

        public void setShop_id(String shop_id) {
            this.shop_id = shop_id;
        }

        public String getPaid_amount() {
            return paid_amount;
        }

        public void setPaid_amount(String paid_amount) {
            this.paid_amount = paid_amount;
        }

        public String getPayment_type() {
            return payment_type;
        }

        public void setPayment_type(String payment_type) {
            this.payment_type = payment_type;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getBill_no() {
            return bill_no;
        }

        public void setBill_no(String bill_no) {
            this.bill_no = bill_no;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getDate_time() {
            return date_time;
        }

        public void setDate_time(String date_time) {
            this.date_time = date_time;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getShop_name() {
            return shop_name;
        }

        public void setShop_name(String shop_name) {
            this.shop_name = shop_name;
        }
    }
}

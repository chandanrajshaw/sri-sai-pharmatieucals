package kashyap.chandan.medishop.agentPannel;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;

import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.List;

import kashyap.chandan.medishop.ApiClient;
import kashyap.chandan.medishop.ConnectionDetector;
import kashyap.chandan.medishop.LocationTrack;
import kashyap.chandan.medishop.MainActivity;
import kashyap.chandan.medishop.R;
import kashyap.chandan.medishop.UserInterface;
import kashyap.chandan.medishop.admin.SharedPreferencesAdmin;
import kashyap.chandan.medishop.agentPannel.agentorder.AgentOrders;
import kashyap.chandan.medishop.agentPannel.agentpayment.AgentPayment;
import kashyap.chandan.medishop.vendor.CompanyProfile;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserDashBoard extends AppCompatActivity {
    boolean doubleBackToExitPressedOnce = false;
double longitude,latitude;
    Toolbar toolbar;
    TextView userlogout,toobarname,navphone,navname,shopnotfound;
    RelativeLayout home,addshop,changepin,profile,viewshop,paymenytLayout,orderLayout;
    DrawerLayout drawerLayout;
    ConnectionDetector connectionDetector;
    ShimmerFrameLayout shimmerFrameLayout;
    RecyclerView usershoprecycler;
SharedPreferencesAdmin sharedPreferencesAdmin;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
      setContentView(R.layout.activity_user_dash_board);
        toolbar = findViewById(R.id.toolbar);
        usershoprecycler=findViewById(R.id.usershoprecycler);
        shopnotfound=findViewById(R.id.shopnotfound);
        shimmerFrameLayout=findViewById(R.id.shimmerview);
        connectionDetector=new ConnectionDetector(UserDashBoard.this);
        setSupportActionBar(toolbar);
        final ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.drawable.ic_menu_white_24dp);
        actionBar.setDisplayHomeAsUpEnabled(true);
        drawerLayout=findViewById(R.id.userdrawer);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle
                (
                        this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();
       sharedPreferencesAdmin=new SharedPreferencesAdmin(UserDashBoard.this);
       toobarname=findViewById(R.id.adminname);
       navphone=findViewById(R.id.adminphone);
       navphone.setText(sharedPreferencesAdmin.getAdminPhonePreferences());
       toobarname.setText(sharedPreferencesAdmin.getAdminNamePreferences());
       navname=findViewById(R.id.tvproname);
       navname.setText(sharedPreferencesAdmin.getAdminNamePreferences());
        LocationTrack locationTrack=new LocationTrack(UserDashBoard.this);
       userlogout=findViewById(R.id.userlogout);
    userlogout.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               sharedPreferencesAdmin.sessionEnd();
               Intent intent=new Intent(UserDashBoard.this, MainActivity.class);
               intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
               startActivity(intent);
               finish();
           }
       });
    home=findViewById(R.id.nav_home_layout);
    home.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            drawerLayout.closeDrawer(GravityCompat.START);
        }
    });
    addshop=findViewById(R.id.nav_home_addshop);
    addshop.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent addShopIntent=new Intent(UserDashBoard.this,AddShopActivity1.class);
            startActivity(addShopIntent);
            drawerLayout.closeDrawer(GravityCompat.START);
        }
    });
    paymenytLayout=findViewById(R.id.nav_payment);
    paymenytLayout.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent intent=new Intent(UserDashBoard.this, AgentPayment.class);
            startActivity(intent);
            drawerLayout.closeDrawer(GravityCompat.START);
        }
    });
    orderLayout=findViewById(R.id.nav_orders);
    orderLayout.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent intent=new Intent(UserDashBoard.this, AgentOrders.class);
            startActivity(intent);
            drawerLayout.closeDrawer(GravityCompat.START);
        }
    });
        changepin=findViewById(R.id.userchangePin);
        changepin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent changePinIntent=new Intent(UserDashBoard.this,UserChangePin.class);
                startActivity(changePinIntent);
                drawerLayout.closeDrawer(GravityCompat.START);
            }
        });
        if (!connectionDetector.isConnectingToInternet())
        {
            shimmerFrameLayout.stopShimmer();
            shimmerFrameLayout.setVisibility(View.GONE);
          usershoprecycler.setVisibility(View.GONE);
       shopnotfound.setVisibility(View.VISIBLE);
            Snackbar.make(drawerLayout,"Internet Is not available, First Connect to internet",Snackbar.LENGTH_LONG).show();
        }
        else {

String agent_id=sharedPreferencesAdmin.getAdminIdPreferences();
            UserInterface userInterface= ApiClient.getClient().create(UserInterface.class);
            Call<AgentAllShopResponse>call=userInterface.getAllAgentShop(agent_id);
            call.enqueue(new Callback<AgentAllShopResponse>() {
                @Override
                public void onResponse(Call<AgentAllShopResponse> call, Response<AgentAllShopResponse> response) {
                    if (response.code() == 200) {
                        shimmerFrameLayout.stopShimmer();
                        shimmerFrameLayout.setVisibility(View.GONE);
                        List<AgentAllShopResponse.DataBean> agentAllShop=new ArrayList<AgentAllShopResponse.DataBean>();
                        agentAllShop=response.body().getData();
                      usershoprecycler.setLayoutManager(new LinearLayoutManager(UserDashBoard.this,LinearLayoutManager.VERTICAL,false));
                       usershoprecycler.setAdapter(new UserAllShopAdapter(UserDashBoard.this,agentAllShop));
                usershoprecycler.setVisibility(View.VISIBLE);

                    }
                    else if (response.code() != 200)
                    {
                        shimmerFrameLayout.stopShimmer();
                        shimmerFrameLayout.setVisibility(View.GONE);
                     shopnotfound.setVisibility(View.VISIBLE);
                    }
                }
                @Override
                public void onFailure(Call<AgentAllShopResponse> call, Throwable t) {
                }
            });
        }
profile=findViewById(R.id.profile);
        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(UserDashBoard.this,AgentViewProfile.class);
                startActivity(intent);
                drawerLayout.closeDrawer(GravityCompat.START);
            }
        });



        //---------------------------------Testing vendor----------------------------------------

viewshop=findViewById(R.id.nav_home_view_shop);
viewshop.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        Intent intent=new Intent(UserDashBoard.this,UserAllShops.class);
        overridePendingTransition(R.anim.transitionrighttoleft, R.anim.transitionrighttoleft);
        startActivity(intent);
        drawerLayout.closeDrawer(GravityCompat.START);
    }
});
    }
    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }
        this.doubleBackToExitPressedOnce = true;
        Snackbar.make(this.getWindow().getDecorView().findViewById(android.R.id.content), "Please click BACK again to exit", Snackbar.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);

    }
    @Override
    protected void onResume() {
        super.onResume();
        shimmerFrameLayout.startShimmer();
    }

    @Override
    protected void onPause() {
        super.onPause();
        shimmerFrameLayout.stopShimmer();
    }
}

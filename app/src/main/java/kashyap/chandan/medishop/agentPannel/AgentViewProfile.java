package kashyap.chandan.medishop.agentPannel;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import kashyap.chandan.medishop.R;
import kashyap.chandan.medishop.admin.DashBoard;
import kashyap.chandan.medishop.admin.SharedPreferencesAdmin;


public class AgentViewProfile extends AppCompatActivity {

SharedPreferencesAdmin sharedPreferencesAdmin;
ImageView viewprofilegobackarrow;
    TextView submitprofile,editProfile,profilemail,profilename,profilephone;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
     setContentView(R.layout.activity_view_profile);
        sharedPreferencesAdmin=new SharedPreferencesAdmin(AgentViewProfile.this);
        viewprofilegobackarrow=findViewById(R.id.viewprofilegobackarrow);
viewprofilegobackarrow.setOnClickListener(new View.OnClickListener() {
       @Override
       public void onClick(View view) {
           Intent profilebackIntent=new Intent(AgentViewProfile.this,UserDashBoard.class);
           profilebackIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
           overridePendingTransition(R.anim.transitionrighttoleft, R.anim.transitionrighttoleft);
           startActivity(profilebackIntent);
           finish();
       }
   });
        submitprofile=findViewById(R.id.submitprofile);
        editProfile=findViewById(R.id.editProfile);
        profilemail=findViewById(R.id.profilemail);
        profilename=findViewById(R.id.profilename);
        profilephone=findViewById(R.id.profilephone);
submitprofile.setVisibility(View.GONE);
 editProfile.setVisibility(View.GONE);
profilemail.setText(sharedPreferencesAdmin.getAdminemailPreferences());
profilename.setText(sharedPreferencesAdmin.getAdminNamePreferences());
  profilephone.setText(sharedPreferencesAdmin.getAdminPhonePreferences());
editProfile.setOnClickListener(new View.OnClickListener() {
       @Override
       public void onClick(View view) {
profilename.setEnabled(true);
        profilephone.setEnabled(true);
          submitprofile.setEnabled(true);
  profilename.setFocusable(true);
       }
   });
submitprofile.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
       String name=profilename.getText().toString();
       String phone=profilephone.getText().toString();
       if (name.isEmpty() && phone.isEmpty())
           Toast.makeText(AgentViewProfile.this, "Please Fill all Fields", Toast.LENGTH_SHORT).show();
       else if (name.isEmpty())
           Toast.makeText(AgentViewProfile.this, "Please Enter Profile Name", Toast.LENGTH_SHORT).show();
       else if (phone.isEmpty())
           Toast.makeText(AgentViewProfile.this, "Please Enter valid Mobile No.", Toast.LENGTH_SHORT).show();
       else
           Toast.makeText(AgentViewProfile.this, "Everything is fine", Toast.LENGTH_SHORT).show();



    }
});


    }
}

package kashyap.chandan.medishop.agentPannel.agentorder;

public class AgentAddOrderResponse {

    /**
     * status : {"code":200,"message":"Admin added Orders Successfully"}
     * data : {"shop_id":"1","unique_id":"SBORD2","agent_id":"2","product_catagories":"2","product_id":"1","quantity":"2","price":"100","total":"200"}
     */

    private StatusBean status;
    private DataBean data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : Admin added Orders Successfully
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean {
        /**
         * shop_id : 1
         * unique_id : SBORD2
         * agent_id : 2
         * product_catagories : 2
         * product_id : 1
         * quantity : 2
         * price : 100
         * total : 200
         */

        private String shop_id;
        private String unique_id;
        private String agent_id;
        private String product_catagories;
        private String product_id;
        private String quantity;
        private String price;
        private String total;

        public String getShop_id() {
            return shop_id;
        }

        public void setShop_id(String shop_id) {
            this.shop_id = shop_id;
        }

        public String getUnique_id() {
            return unique_id;
        }

        public void setUnique_id(String unique_id) {
            this.unique_id = unique_id;
        }

        public String getAgent_id() {
            return agent_id;
        }

        public void setAgent_id(String agent_id) {
            this.agent_id = agent_id;
        }

        public String getProduct_catagories() {
            return product_catagories;
        }

        public void setProduct_catagories(String product_catagories) {
            this.product_catagories = product_catagories;
        }

        public String getProduct_id() {
            return product_id;
        }

        public void setProduct_id(String product_id) {
            this.product_id = product_id;
        }

        public String getQuantity() {
            return quantity;
        }

        public void setQuantity(String quantity) {
            this.quantity = quantity;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getTotal() {
            return total;
        }

        public void setTotal(String total) {
            this.total = total;
        }
    }
}

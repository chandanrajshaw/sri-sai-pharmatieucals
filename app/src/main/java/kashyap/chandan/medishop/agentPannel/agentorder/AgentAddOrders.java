package kashyap.chandan.medishop.agentPannel.agentorder;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.List;

import kashyap.chandan.medishop.ApiClient;
import kashyap.chandan.medishop.ConnectionDetector;
import kashyap.chandan.medishop.Order.OrderAllCategoryAdapter;
import kashyap.chandan.medishop.Order.OrderAllProductAdapter;
import kashyap.chandan.medishop.Order.OrderAllShopAdapter;
import kashyap.chandan.medishop.Order.OrderCategoryListResponse;
import kashyap.chandan.medishop.Order.OrderProductListResponse;
import kashyap.chandan.medishop.Order.OrderShopListResponse;
import kashyap.chandan.medishop.Order.Orders;
import kashyap.chandan.medishop.R;
import kashyap.chandan.medishop.UserInterface;
import kashyap.chandan.medishop.admin.SharedPreferencesAdmin;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AgentAddOrders extends AppCompatActivity {
ImageView goback;
TextView order, toolheader,shopname,shopid,makeorder;
RelativeLayout shopdrop,categorydrop,productdrop;
EditText qty,price;
int itemCount=0;
long grandtotal=0;
ConnectionDetector connectionDetector;
TableLayout table;
RecyclerView shoprec,productrec,categoryrec,orderlist;
String[] tot=new String[10];
    String[] cat=new String[10];
    String[] product=new String[10];
    String[] qunty=new String[10];
    String[] amt=new String[10];
    String[] productnam=new String[10];
    String sid;
    SharedPreferencesAdmin sharedPreferencesAdmin;
Dialog orderDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_add_orders);
        toolheader=findViewById(R.id.customtoolheader);
        sharedPreferencesAdmin=new SharedPreferencesAdmin(AgentAddOrders.this);
        toolheader.setText("Add Order");
        table=findViewById(R.id.table);
        orderlist=findViewById(R.id.orderlist);
        goback=findViewById(R.id.ordergobackarrow);
        shopdrop=findViewById(R.id.shoplistdrop);
        order=findViewById(R.id.order);
        shopname=findViewById(R.id.shopname);
        shopid=findViewById(R.id.shopid);
        makeorder=findViewById(R.id.placeorder);

        connectionDetector=new ConnectionDetector(AgentAddOrders.this);
        if (itemCount==0)
            makeorder.setVisibility(View.GONE);
        goback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                overridePendingTransition(R.anim.transitionrighttoleft,R.anim.transitionrighttoleft);
                finish();
            }
        });
        shopdrop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!connectionDetector.isConnectingToInternet()){
                    Snackbar.make(AgentAddOrders.this.getWindow().getDecorView().findViewById(android.R.id.content),"Plzz Connect to internet and try again",Snackbar.LENGTH_SHORT).show();
                }
                else if (connectionDetector.isConnectingToInternet())
                {
                    final Dialog progress=new Dialog(AgentAddOrders.this);
                    progress.setContentView(R.layout.customdialog);
                    progress.setCancelable(false);
                    progress.show();
                    UserInterface userInterface= ApiClient.getClient().create(UserInterface.class);
                    Call<OrderShopListResponse> call=userInterface.getOrderShopList();
                    call.enqueue(new Callback<OrderShopListResponse>() {
                        @Override
                        public void onResponse(Call<OrderShopListResponse> call, Response<OrderShopListResponse> response) {
                            if (response.code()==200)
                            {
                                progress.dismiss();
                                List<OrderShopListResponse.DataBean> allshops=new ArrayList<OrderShopListResponse.DataBean>();
                                allshops=response.body().getData();
                                Dialog shopdialog=new Dialog(AgentAddOrders.this);
                                shopdialog.setContentView(R.layout.recyclerdialog);
                                DisplayMetrics metrics=getResources().getDisplayMetrics();
                                int width=metrics.widthPixels;
                                shopdialog.getWindow().setLayout(width, LinearLayout.LayoutParams.MATCH_PARENT);
                                shopdialog.setCancelable(false);
                                shoprec=shopdialog.findViewById(R.id.dialogRecycler);
                                shoprec.setLayoutManager(new LinearLayoutManager(AgentAddOrders.this,LinearLayoutManager.VERTICAL,false));
                                    shoprec.setAdapter(new OrderAllShopAdapter(AgentAddOrders.this,allshops,shopname,shopid,shopdialog));
                           shopdialog.show();
                            }
                            else if (response.code()!=200)
                            {
                                progress.dismiss();
                                Snackbar.make(AgentAddOrders.this.getWindow().getDecorView().findViewById(android.R.id.content),"Error Occurs !!Try again Later",Snackbar.LENGTH_SHORT).show();

                            }
                        }

                        @Override
                        public void onFailure(Call<OrderShopListResponse> call, Throwable t) {
                            progress.dismiss();
                            Snackbar.make(AgentAddOrders.this.getWindow().getDecorView().findViewById(android.R.id.content),""+t.getMessage(),Snackbar.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        });



        order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
        if (shopid.getText().toString().trim().isEmpty())
        {
            Toast.makeText(AgentAddOrders.this, "Please select Shop First", Toast.LENGTH_SHORT).show();
        }else {
            final  TextView categoryname,categoryid,productname,productid,addorder,total;
            final EditText qty,price;
            orderDialog=new Dialog(AgentAddOrders.this);
            orderDialog.setContentView(R.layout.orderdialog);
            DisplayMetrics metrics=getResources().getDisplayMetrics();
            int width=metrics.widthPixels;
            orderDialog.getWindow().setLayout(width, LinearLayout.LayoutParams.MATCH_PARENT);
            orderDialog.setCancelable(false);
            orderDialog.show();
            total=orderDialog.findViewById(R.id.total);
            qty=orderDialog.findViewById(R.id.qty);
            qty.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View view, boolean b) {
                    if (b)
                    qty.setHint("");
                    else
                        qty.setHint("Quantity");
                }
            });
            price=orderDialog.findViewById(R.id.price);
            price.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View view, boolean b) {
                    if (b)
                    price.setHint("");
                    else
                        price.setHint("Price");
                }
            });
            TextWatcher inputTextWatcher = new TextWatcher() {
                public void afterTextChanged(Editable s) {
                    if (qty.getText().toString().isEmpty() && price.getText().toString().isEmpty()){
                        total.setText("");
                    }
                    else if (qty.getText().toString().isEmpty())
                        total.setText("");
                    else if (price.getText().toString().isEmpty())
                        total.setText("");
                    else {
                        int quantity=Integer.valueOf(qty.getText().toString());
                        long amt= Long.valueOf(s.toString());
                        long tot=quantity*amt;
                        total.setText(String.valueOf(tot));}

                }
                public void beforeTextChanged(CharSequence s, int start, int count, int after){
                }
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                }
            };


            price.addTextChangedListener(inputTextWatcher);
qty.addTextChangedListener(inputTextWatcher);
            categoryname=orderDialog.findViewById(R.id.categoryname);
            categoryid=orderDialog.findViewById(R.id.categoryid);
            productname=orderDialog.findViewById(R.id.productname);
            productid=orderDialog.findViewById(R.id.productid);
            addorder=orderDialog.findViewById(R.id.addorder);
            categorydrop=orderDialog.findViewById(R.id.productcategorydrop);
            categorydrop.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!connectionDetector.isConnectingToInternet()){
                        Snackbar.make(AgentAddOrders.this.getWindow().getDecorView().findViewById(android.R.id.content),"Plzz Connect to internet and try again",Snackbar.LENGTH_SHORT).show();
                    }
                    else if (connectionDetector.isConnectingToInternet())
                    {
                        final Dialog progress=new Dialog(AgentAddOrders.this);
                        progress.setContentView(R.layout.customdialog);
                        progress.setCancelable(false);
                        progress.show();
                        UserInterface userInterface= ApiClient.getClient().create(UserInterface.class);
                        Call<OrderCategoryListResponse>call=userInterface.getOrderCategoryList();
                        call.enqueue(new Callback<OrderCategoryListResponse>() {
                            @Override
                            public void onResponse(Call<OrderCategoryListResponse> call, Response<OrderCategoryListResponse> response) {
                                if (response.code()==200)
                                {
                                    progress.dismiss();
                                    List<OrderCategoryListResponse.DataBean> allCategory=new ArrayList<OrderCategoryListResponse.DataBean>();
                                    allCategory=response.body().getData();
                                    Dialog catdialog=new Dialog(AgentAddOrders.this);
                                    catdialog.setContentView(R.layout.recyclerdialog);
                                    DisplayMetrics metrics=getResources().getDisplayMetrics();
                                    int width=metrics.widthPixels;
                                    catdialog.getWindow().setLayout(width, LinearLayout.LayoutParams.MATCH_PARENT);
                                    catdialog.setCancelable(false);
                                    categoryrec=catdialog.findViewById(R.id.dialogRecycler);
                                    categoryrec.setLayoutManager(new LinearLayoutManager(AgentAddOrders.this,LinearLayoutManager.VERTICAL,false));
                                    categoryrec.setAdapter(new OrderAllCategoryAdapter(AgentAddOrders.this,allCategory,categoryname,categoryid,catdialog));
                                    catdialog.show();
                                }
                                else if (response.code()!=200)
                                {
                                    progress.dismiss();
                                    Snackbar.make(AgentAddOrders.this.getWindow().getDecorView().findViewById(android.R.id.content),"Error Occurs !!Try again Later",Snackbar.LENGTH_SHORT).show();

                                }
                            }

                            @Override
                            public void onFailure(Call<OrderCategoryListResponse> call, Throwable t) {
                                progress.dismiss();
                                Snackbar.make(AgentAddOrders.this.getWindow().getDecorView().findViewById(android.R.id.content),""+t.getMessage(),Snackbar.LENGTH_SHORT).show();

                            }
                        });
                    }
                }
            });
            productdrop=orderDialog.findViewById(R.id.itemdrop);
            productdrop.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String catid=categoryid.getText().toString().trim();
                    if (catid.isEmpty()){
                        Toast.makeText(AgentAddOrders.this, "Select Poduct Category First", Toast.LENGTH_SHORT).show();
                    }
                    else {
                        if (!connectionDetector.isConnectingToInternet()){
                            Snackbar.make(AgentAddOrders.this.getWindow().getDecorView().findViewById(android.R.id.content),"Plzz Connect to internet and try again",Snackbar.LENGTH_SHORT).show();

                        }
                        else if (connectionDetector.isConnectingToInternet())
                        {
                            final Dialog progress=new Dialog(AgentAddOrders.this);
                            progress.setContentView(R.layout.customdialog);
                            progress.setCancelable(false);
                            progress.show();
                            UserInterface userInterface= ApiClient.getClient().create(UserInterface.class);
                            Call<OrderProductListResponse>call=userInterface.orderProductList(catid);
                            call.enqueue(new Callback<OrderProductListResponse>() {
                                @Override
                                public void onResponse(Call<OrderProductListResponse> call, Response<OrderProductListResponse> response) {
                                    if (response.code()==200)
                                    {
                                        progress.dismiss();
                                        List<OrderProductListResponse.DataBean> allproducts=new ArrayList<OrderProductListResponse.DataBean>();
                                        allproducts=response.body().getData();
                                        Dialog productdialog=new Dialog(AgentAddOrders.this);
                                        productdialog.setContentView(R.layout.recyclerdialog);
                                        DisplayMetrics metrics=getResources().getDisplayMetrics();
                                        int width=metrics.widthPixels;
                                        productdialog.getWindow().setLayout(width, LinearLayout.LayoutParams.MATCH_PARENT);
                                        productdialog.setCancelable(false);
                                        productrec=productdialog.findViewById(R.id.dialogRecycler);
                                        productrec.setLayoutManager(new LinearLayoutManager(AgentAddOrders.this,LinearLayoutManager.VERTICAL,false));
                                        productrec.setAdapter(new OrderAllProductAdapter(AgentAddOrders.this,allproducts,productname,productid,productdialog));
                                        productdialog.show();
                                    }
                                    else if (response.code()!=200)
                                    {
                                        progress.dismiss();
                                        Toast.makeText(AgentAddOrders.this, "No Products Found For this Category", Toast.LENGTH_SHORT).show();

                                    }
                                }

                                @Override
                                public void onFailure(Call<OrderProductListResponse> call, Throwable t) {
                                    progress.dismiss();
                                    Snackbar.make(AgentAddOrders.this.getWindow().getDecorView().findViewById(android.R.id.content),""+t.getMessage(),Snackbar.LENGTH_SHORT).show();

                                }
                            });
                        }
                    }
                }
            });
            addorder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String qnty=qty.getText().toString().trim();
                    sid=shopid.getText().toString().trim();
                    String pname=productname.getText().toString().trim();
                    String catid=categoryid.getText().toString().trim();
                    String pid=productid.getText().toString().trim();
                    String amount=price.getText().toString().trim();
                    String totalamt=total.getText().toString();

                    if (qnty.isEmpty()&& sid.isEmpty()&&catid.isEmpty()&&pid.isEmpty()&&amount.isEmpty())
                        Toast.makeText(AgentAddOrders.this,"Plzz Fill all the fields",Toast.LENGTH_SHORT).show();
                    else if (sid.isEmpty())
                        Toast.makeText(AgentAddOrders.this,"Select Shop",Toast.LENGTH_SHORT).show();
                    else if (catid.isEmpty())
                        Toast.makeText(AgentAddOrders.this,"Select Product Category",Toast.LENGTH_SHORT).show();
                    else if (pid.isEmpty())
                        Snackbar.make(AgentAddOrders.this.getWindow().getDecorView().findViewById(android.R.id.content),"Select Product",Snackbar.LENGTH_SHORT).show();
                    else if (qnty.isEmpty())
                        Toast.makeText(AgentAddOrders.this,"Enter Quantity",Toast.LENGTH_SHORT).show();
                    else if (amount.isEmpty())
                        Toast.makeText(AgentAddOrders.this,"Enter Price",Toast.LENGTH_SHORT).show();
                    else {
                        grandtotal=grandtotal+Long.parseLong(totalamt);
                        itemCount++;
                        if (itemCount==1) addHeaders();
                        if (itemCount==0){
                            makeorder.setVisibility(View.GONE);
                        }


                        if (itemCount!=0)
                        {
                            makeorder.setVisibility(View.VISIBLE);
                            shopdrop.setEnabled(false);
                        }
                        if (itemCount==10)
                        {
                            order.setVisibility(View.GONE);
                            addorder.setVisibility(View.GONE);
                        }
                        tot[itemCount-1]=totalamt;//item*priceperunit
                        cat[itemCount-1]=catid;
                        product[itemCount-1]=pid;//product name
                        qunty[itemCount-1]=qnty;
                        amt[itemCount-1]=amount;//price perunit
                        productnam[itemCount-1]=pname;
                        makeorder.setText("Make Order : Rs "+String.valueOf(grandtotal));
                        qty.setText("");
                        total.setText("");
                        price.setText("");
                        productid.setText("");
                        productname.setText("");
                        categoryid.setText("");
                        categoryname.setText("");
                        orderDialog.dismiss();
//                        addHeaders();
                            addData(String.valueOf(itemCount),pname,qnty,totalamt);
                    }


                }
            });
        }



            }
        });

        makeorder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (itemCount==0)
                {
                    Snackbar.make(AgentAddOrders.this.getWindow().getDecorView().findViewById(android.R.id.content),"First Select",Snackbar.LENGTH_SHORT).show();

                }
                else {
                    if (!connectionDetector.isConnectingToInternet()){
                        Snackbar.make(AgentAddOrders.this.getWindow().getDecorView().findViewById(android.R.id.content),"Internet Is Not Available!!",Snackbar.LENGTH_SHORT).show();

                    }
                    else if (connectionDetector.isConnectingToInternet()){
                        final Dialog progress=new Dialog(AgentAddOrders.this);
                        progress.setContentView(R.layout.customdialog);
                        progress.setCancelable(false);
                        progress.show();
                        UserInterface userInterface= ApiClient.getClient().create(UserInterface.class);
                        Call<AgentAddOrderResponse>call=userInterface.addAgentOrder(sid,sharedPreferencesAdmin.getAdminIdPreferences(),cat,product,qunty,amt,tot);
                        call.enqueue(new Callback<AgentAddOrderResponse>() {
                            @Override
                            public void onResponse(Call<AgentAddOrderResponse> call, Response<AgentAddOrderResponse> response) {
                                if (response.code()==200)
                                {
                                    progress.dismiss();
                                    Toast.makeText(AgentAddOrders.this, "Order Added", Toast.LENGTH_SHORT).show();
                                    Intent intent=new Intent(AgentAddOrders.this,Orders.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    overridePendingTransition(R.anim.transitionrighttoleft,R.anim.transitionrighttoleft);
                                    startActivity(intent);
                                    finish();
                                }
                                else if(response.code()!=200){
                                    progress.dismiss();
                                    Snackbar.make(AgentAddOrders.this.getWindow().getDecorView().findViewById(android.R.id.content),"Order Not Added",Snackbar.LENGTH_SHORT).show();

                                }
                            }

                            @Override
                            public void onFailure(Call<AgentAddOrderResponse> call, Throwable t) {
                                progress.dismiss();
                                Snackbar.make(AgentAddOrders.this.getWindow().getDecorView().findViewById(android.R.id.content),""+t.getMessage(),Snackbar.LENGTH_SHORT).show();

                            }
                        });

                    }
                }
            }
        });
    }
    public void addHeaders() {
        TableLayout tl = findViewById(R.id.table);
        TableRow tr = new TableRow(this);
        tr.setLayoutParams(getLayoutParams());
        tr.addView(getTextView(0, "Sl.No", ContextCompat.getColor(this, R.color.textcolor), Typeface.BOLD, ContextCompat.getColor(this, R.color.background_color)));
        tr.addView(getTextView(0, "Product",  ContextCompat.getColor(this, R.color.textcolor), Typeface.BOLD,ContextCompat.getColor(this, R.color.background_color)));
        tr.addView(getTextView(0, "Quantity", ContextCompat.getColor(this, R.color.textcolor), Typeface.BOLD,ContextCompat.getColor(this, R.color.background_color)));
        tr.addView(getTextView(0, "Amount", ContextCompat.getColor(this, R.color.textcolor), Typeface.BOLD,ContextCompat.getColor(this, R.color.background_color)));
        tl.addView(tr, getTblLayoutParams());
    }
    public void addData(String sno,String productname,String quantity,String total) {
        // int numCompanies = companies.length;
        TableLayout tl = findViewById(R.id.table);
        TableRow tr = new TableRow(this);
        tr.setLayoutParams(getLayoutParams());

            tr.addView(getTextView( 1,String.valueOf(sno), ContextCompat.getColor(this, R.color.background_color), Typeface.NORMAL,  ContextCompat.getColor(this, R.color.textcolor)));
            tr.addView(getTextView(1, productname,ContextCompat.getColor(this, R.color.background_color), Typeface.NORMAL,  ContextCompat.getColor(this, R.color.textcolor)));
            tr.addView(getTextView(1,quantity, ContextCompat.getColor(this, R.color.background_color), Typeface.NORMAL,  ContextCompat.getColor(this, R.color.textcolor)));
            tr.addView(getTextView(1, total, ContextCompat.getColor(this, R.color.background_color), Typeface.NORMAL, ContextCompat.getColor(this, R.color.textcolor)));
            tl.addView(tr, getTblLayoutParams());


    }
    private TextView getTextView ( int id, String title,int color, int typeface, int bgColor)
    {
        TextView tv = new TextView(AgentAddOrders.this);
        tv.setId(id);
        tv.setText(title.toUpperCase());
        tv.setTextColor(color);
        tv.setPadding(5, 5, 5, 5);
        tv.setTypeface(Typeface.DEFAULT, typeface);
        tv.setBackgroundColor(bgColor);
        tv.setLayoutParams(getLayoutParams());

        return tv;
    }
    @NonNull
    private TableLayout.LayoutParams getTblLayoutParams () {
        return new TableLayout.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT);
    }
    private TableRow.LayoutParams getLayoutParams ()
    {
        TableRow.LayoutParams params = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT);
        params.setMargins(2, 0, 0, 2);
        return params;
    }
}

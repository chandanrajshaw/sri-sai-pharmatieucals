package kashyap.chandan.medishop.agentPannel;

import java.io.Serializable;
import java.util.List;

public class CityForAreaResponse implements Serializable {

    /**
     * status : {"code":200,"message":"City List in Add Areas in the form of  Dropdown"}
     * data : [{"id":"8","city_name":"Tadepalligudem","date_time":"2020-03-07 15:52:59","status":"1"},{"id":"5","city_name":"Vizag","date_time":"2020-02-14 12:56:19","status":"1"},{"id":"4","city_name":"Bhimavaram","date_time":"2020-02-14 12:56:14","status":"1"},{"id":"3","city_name":"Rajamundry","date_time":"2020-02-14 12:56:09","status":"1"},{"id":"2","city_name":"Vijayawada","date_time":"2020-02-14 12:56:03","status":"1"},{"id":"1","city_name":"Hyderabad","date_time":"2020-02-14 12:55:58","status":"1"}]
     */

    private StatusBean status;
    private List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean implements Serializable {
        /**
         * code : 200
         * message : City List in Add Areas in the form of  Dropdown
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean implements Serializable{
        /**
         * id : 8
         * city_name : Tadepalligudem
         * date_time : 2020-03-07 15:52:59
         * status : 1
         */

        private String id;
        private String city_name;
        private String date_time;
        private String status;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getCity_name() {
            return city_name;
        }

        public void setCity_name(String city_name) {
            this.city_name = city_name;
        }

        public String getDate_time() {
            return date_time;
        }

        public void setDate_time(String date_time) {
            this.date_time = date_time;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
    }
}

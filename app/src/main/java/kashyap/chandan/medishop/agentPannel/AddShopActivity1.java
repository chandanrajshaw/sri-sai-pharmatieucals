package kashyap.chandan.medishop.agentPannel;

import android.Manifest;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.List;

import kashyap.chandan.medishop.ApiClient;
import kashyap.chandan.medishop.ApiError;
import kashyap.chandan.medishop.CustomItemClickListener;
import kashyap.chandan.medishop.LocationAddress;
import kashyap.chandan.medishop.R;
import kashyap.chandan.medishop.UserInterface;
import kashyap.chandan.medishop.admin.AdminAllAreaList;
import kashyap.chandan.medishop.admin.AdminAllAreaResponse;
import kashyap.chandan.medishop.admin.AdminAllCityList;
import kashyap.chandan.medishop.admin.DistrictListAdapter;
import kashyap.chandan.medishop.admin.DistrictResponse;
import kashyap.chandan.medishop.admin.SharedPreferencesAdmin;
import kashyap.chandan.medishop.admin.ShopsActivity;
import kashyap.chandan.medishop.admin.StateListAdapter;
import kashyap.chandan.medishop.admin.StateResponse;
import kashyap.chandan.medishop.pojoclasses.AddShopResponse;
import kashyap.chandan.medishop.pojoclasses.AdminCityList;
import kashyap.chandan.medishop.pojoclasses.GetAreaResponse;
import kashyap.chandan.medishop.pojoclasses.GetCityResponseUser;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class AddShopActivity1 extends AppCompatActivity {
    String picturePath,imagePic,stateId,districtId,CityId;
    FusedLocationProviderClient mFusedLocationClient;
   public static final int PERMISSION_ID = 44;
    double lat,lng;
    Dialog dialog;
RecyclerView cityRecycler,areaRecycler,stateRecycler,districtRecycler;
RadioButton select;
    private ArrayList camerapermissionsToRequest;
    private ArrayList camerapermissionsRejected = new ArrayList();
    private ArrayList camerapermissions = new ArrayList();

    private final static int CAMERA_ALL_PERMISSIONS_RESULT = 108;

    File image=null;
List<AdminCityList.DataBean> allCityList=new ArrayList<AdminCityList.DataBean>();
    List<AdminAllAreaResponse.DataBean> allAreaList=new ArrayList<AdminAllAreaResponse.DataBean>();
    List<StateResponse.DataBean>allStateList=new ArrayList<>();
    List<DistrictResponse.DataBean>allDistrict=new ArrayList<>();
Bitmap bitmap,converetdImage;
Uri picUri;
TextInputEditText etaddress,etpname,etshopname,etpersonemail,etshopphone,etwtsp,etwebsite,etmsg,etlandmark;
RelativeLayout citylist,arealist,stateList,districtList;
TextView cityid,city,submitshop,areaid,areaname, state, stateid, districtid,district;
ImageView addshopgobackarrow,shopImage;
    private Dialog progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
       setContentView(R.layout.activity_add_shop);
       init();
        camerapermissions.add(CAMERA);
        camerapermissions.add(READ_EXTERNAL_STORAGE);
        camerapermissions.add(WRITE_EXTERNAL_STORAGE);

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        getLastLocation();
        camerapermissionsToRequest = findUnAskedCameraPermissions(camerapermissions);
        stateList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                statelist();
            }
        });
        districtList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (stateId==null||stateId.isEmpty())
                    Toast.makeText(AddShopActivity1.this, "Select State", Toast.LENGTH_SHORT).show();
                else
                {
                    districtlist();
                }
            }

        });
      citylist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            if (districtId==null||districtId.isEmpty())
                Toast.makeText(AddShopActivity1.this, "Select District", Toast.LENGTH_SHORT).show();
            else
            {
                final Dialog progressDialog = new Dialog(AddShopActivity1.this);
                progressDialog.setContentView(R.layout.customdialog);
                progressDialog.setCancelable(false);
                progressDialog.show();
                UserInterface userInterface= ApiClient.getClient().create(UserInterface.class);
                Call<AdminCityList> call=userInterface.getCityList(districtId);
                call.enqueue(new Callback<AdminCityList>() {
                    @Override
                    public void onResponse(Call<AdminCityList> call, Response<AdminCityList> response) {
                        if (response.code()==200)
                        {
                            allCityList=response.body().getData();
                            final  Dialog citydialog=new Dialog(AddShopActivity1.this);
                            citydialog.setContentView(R.layout.recyclerdialog);
                            DisplayMetrics metrics=getResources().getDisplayMetrics();
                            int width=metrics.widthPixels;
                            citydialog.getWindow().setLayout(width, LinearLayout.LayoutParams.MATCH_PARENT);
                            cityRecycler=citydialog.findViewById(R.id.dialogRecycler);
                            cityRecycler.setLayoutManager(new LinearLayoutManager(AddShopActivity1.this,LinearLayoutManager.VERTICAL,false));
                            cityRecycler.setAdapter(new CityListAdapter(AddShopActivity1.this,allCityList,city,citydialog,cityid));
                            progressDialog.dismiss();
                            citydialog.show();
                        }
                        else
                        {
                            progressDialog.dismiss();
                            Toast.makeText(AddShopActivity1.this, "Something Goes Wrong", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<AdminCityList> call, Throwable t) {
                        Snackbar.make(AddShopActivity1.this.getWindow().getDecorView().findViewById(android.R.id.content), ""+t.getMessage(), Snackbar.LENGTH_SHORT).show();

                    }
                });
            }
            }
        });


        arealist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CityId=cityid.getText().toString();
              if (CityId==null||CityId.isEmpty())
                  Toast.makeText(AddShopActivity1.this, "Select City", Toast.LENGTH_SHORT).show();
              else
              {
                  final   Dialog progressDialog = new Dialog(AddShopActivity1.this);
                  progressDialog.setContentView(R.layout.customdialog);
                  progressDialog.setCancelable(false);
                  progressDialog.show();
                  UserInterface userInterface=ApiClient.getClient().create(UserInterface.class);
                  Call<AdminAllAreaResponse> call=userInterface.getAllAreaAdmin(CityId);
                  call.enqueue(new Callback<AdminAllAreaResponse>()
                  {
                      @Override
                      public void onResponse(Call<AdminAllAreaResponse> call, Response<AdminAllAreaResponse> response) {
                          if (response.code()==200)
                          {
                              Dialog areadialog=new Dialog(AddShopActivity1.this);
                              areadialog.setContentView(R.layout.recyclerdialog);
                              DisplayMetrics metrics=getResources().getDisplayMetrics();
                              int width=metrics.widthPixels;
                              areadialog.getWindow().setLayout(width, LinearLayout.LayoutParams.MATCH_PARENT);
                              allAreaList=response.body().getData();
                              areaRecycler=areadialog.findViewById(R.id.dialogRecycler);
                              areaRecycler.removeAllViews();
                              areaRecycler.setLayoutManager(new LinearLayoutManager(AddShopActivity1.this,LinearLayoutManager.VERTICAL,false));
                              areaRecycler.setAdapter(new AreaListAdapter(AddShopActivity1.this,allAreaList,areaname,areadialog,areaid));
                              areadialog.show();
                              progressDialog.dismiss();
                          }
                          else
                          {
                              progressDialog.dismiss();
                              Toast.makeText(AddShopActivity1.this, "Something Goes Wrong", Toast.LENGTH_SHORT).show();
                          }
                      }

                      @Override
                      public void onFailure(Call<AdminAllAreaResponse> call, Throwable t) {
                          Snackbar.make(AddShopActivity1.this.getWindow().getDecorView().findViewById(android.R.id.content), ""+t.getMessage(), Snackbar.LENGTH_SHORT).show();

                      }
                  });
              }


            }
        });


        submitshop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                SharedPreferencesAdmin sharedPreferencesAdmin=new SharedPreferencesAdmin(AddShopActivity1.this);
                String pname=etpname.getText().toString();
                final String id=sharedPreferencesAdmin.getAdminIdPreferences();
                String citynameid=cityid.getText().toString();
                String areaname=areaid.getText().toString();
                String shopname=etshopname.getText().toString();
                String email=etpersonemail.getText().toString();
                String phone=etshopphone.getText().toString();
                String web=etwebsite.getText().toString();
                String Address=etaddress.getText().toString();
                String landmark=etlandmark.getText().toString();
                String msg=etmsg.getText().toString();
                String wtsp=etwtsp.getText().toString().trim();
                if ((stateId==null ||stateId.isEmpty())&&(districtId==null||districtId.isEmpty())&&citynameid.equalsIgnoreCase("City")&& areaname.equalsIgnoreCase("Area")
                        &&shopname.isEmpty()
                && phone.isEmpty()&& Address.isEmpty()&&landmark.isEmpty()&& pname.isEmpty())
                    Snackbar.make(AddShopActivity1.this.getWindow().getDecorView().findViewById(android.R.id.content), "Please Enter All Details ", Snackbar.LENGTH_SHORT).show();
             else if (stateId==null||stateId.isEmpty())
                    Snackbar.make(AddShopActivity1.this.getWindow().getDecorView().findViewById(android.R.id.content), "Select State ", Snackbar.LENGTH_SHORT).show();
else if (districtId==null||districtId.isEmpty())
                    Snackbar.make(AddShopActivity1.this.getWindow().getDecorView().findViewById(android.R.id.content), "Select District", Snackbar.LENGTH_SHORT).show();

                else if(citynameid.equalsIgnoreCase("City"))
                    Snackbar.make(AddShopActivity1.this.getWindow().getDecorView().findViewById(android.R.id.content), "Please Select City", Snackbar.LENGTH_SHORT).show();
else if (areaname.equalsIgnoreCase("Area")|| areaname.isEmpty())
                    Snackbar.make(AddShopActivity1.this.getWindow().getDecorView().findViewById(android.R.id.content), "Please Select Area ", Snackbar.LENGTH_SHORT).show();
else if (shopname.isEmpty())
                    Snackbar.make(AddShopActivity1.this.getWindow().getDecorView().findViewById(android.R.id.content), "Please Enter Owner Name ", Snackbar.LENGTH_SHORT).show();
else if (pname.isEmpty())
                    Snackbar.make(AddShopActivity1.this.getWindow().getDecorView().findViewById(android.R.id.content), "Please Enter Shop Name ", Snackbar.LENGTH_SHORT).show();

              else if (phone.isEmpty() ||phone.length()!=10)
                    Snackbar.make(AddShopActivity1.this.getWindow().getDecorView().findViewById(android.R.id.content), "Please Enter Valid Mobile No. ", Snackbar.LENGTH_SHORT).show();
else if (Address.isEmpty())
                    Snackbar.make(AddShopActivity1.this.getWindow().getDecorView().findViewById(android.R.id.content), "Please fill Address field Manually", Snackbar.LENGTH_SHORT).show();
else if (landmark.isEmpty())
                    Snackbar.make(AddShopActivity1.this.getWindow().getDecorView().findViewById(android.R.id.content), "Please Enter Landmark near by you", Snackbar.LENGTH_SHORT).show();
else if (image==null){
                    Snackbar.make(AddShopActivity1.this.getWindow().getDecorView().findViewById(android.R.id.content), "Please choose An Image to Upload", Snackbar.LENGTH_SHORT).show();

                }

else
    {
        MultipartBody.Part body=null;
        if (image != null) {
            RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), image);
            body = MultipartBody.Part.createFormData("photo", image.getName(), requestFile);
        }
        final Dialog progress=new Dialog(AddShopActivity1.this);
        progress.setContentView(R.layout.customdialog);
        progress.setCancelable(false);
        progress.show();
        RequestBody sid = RequestBody.create(MediaType.parse("multipart/form-data"), id);
        RequestBody oname = RequestBody.create(MediaType.parse("multipart/form-data"), pname);
        RequestBody sname = RequestBody.create(MediaType.parse("multipart/form-data"), shopname);
            RequestBody sarea = RequestBody.create(MediaType.parse("multipart/form-data"), areaname);
            RequestBody scity = RequestBody.create(MediaType.parse("multipart/form-data"), citynameid);
            RequestBody slat = RequestBody.create(MediaType.parse("multipart/form-data"), String.valueOf(lat));
            RequestBody slng = RequestBody.create(MediaType.parse("multipart/form-data"), String.valueOf(lng));
            RequestBody smob = RequestBody.create(MediaType.parse("multipart/form-data"), phone);
        RequestBody swtsp = RequestBody.create(MediaType.parse("multipart/form-data"), wtsp);
            RequestBody semail = RequestBody.create(MediaType.parse("multipart/form-data"), email);
            RequestBody sadd = RequestBody.create(MediaType.parse("multipart/form-data"), Address);
            RequestBody sland = RequestBody.create(MediaType.parse("multipart/form-data"), landmark);
            RequestBody smsg = RequestBody.create(MediaType.parse("multipart/form-data"),msg);
            RequestBody sweb = RequestBody.create(MediaType.parse("multipart/form-data"), web);
        RequestBody sstate = RequestBody.create(MediaType.parse("multipart/form-data"),stateId);
        RequestBody sdistrict = RequestBody.create(MediaType.parse("multipart/form-data"),districtId);
UserInterface userInterface=ApiClient.getClient().create(UserInterface.class);
Call<AddShopResponse>call=userInterface.addShop(sid,sname,oname,semail,smob,swtsp,scity,sarea,slat,slng,body,sstate,sdistrict);
call.enqueue(new Callback<AddShopResponse>() {
    @Override
    public void onResponse(Call<AddShopResponse> call, Response<AddShopResponse> response) {
        if (response.code()==200){
            progress.dismiss();
            Toast.makeText(AddShopActivity1.this, "Shop Added Successfully", Toast.LENGTH_SHORT).show();
            if (id.equalsIgnoreCase("1"))
            {
             Intent intent=new Intent(AddShopActivity1.this, ShopsActivity.class);
             intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
             startActivity(intent);
             finish();
            }
            else {
                Intent intent=new Intent(AddShopActivity1.this,UserDashBoard.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            }

        }
        else if (response.code()!=200)
        {
            progress.dismiss();
//            Toast.makeText(AddShopActivity1.this, "Shop Not Added,Try With other Phone No.!!Try Again", Toast.LENGTH_LONG).show();

            Converter<ResponseBody, ApiError> converter =
                    ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
            ApiError error;
            try {
                error = converter.convert(response.errorBody());
                ApiError.StatusBean status=error.getStatus();
                Toast.makeText(AddShopActivity1.this, ""+status.getMessage(), Toast.LENGTH_LONG).show();
            } catch (IOException e) { e.printStackTrace(); }


        }
    }

    @Override
    public void onFailure(Call<AddShopResponse> call, Throwable t) {
        Toast.makeText(AddShopActivity1.this, " "+t.getMessage(), Toast.LENGTH_SHORT).show();
progress.dismiss();
    }
});
    }
            }
        });
  addshopgobackarrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                overridePendingTransition(R.anim.transitionrighttoleft, R.anim.transitionlefttoright);
                finish();
            }
        });
     shopImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if ((ContextCompat.checkSelfPermission(AddShopActivity1.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)) {
                    requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA},
                            CAMERA_ALL_PERMISSIONS_RESULT);
                }
                else {
                    LinearLayout camera, folder;
                    dialog = new Dialog(AddShopActivity1.this);
                    dialog.setContentView(R.layout.dialogboxcamera);
                    DisplayMetrics metrics=getResources().getDisplayMetrics();
                    int width=metrics.widthPixels;
                    dialog.getWindow().setLayout(width, LinearLayout.LayoutParams.MATCH_PARENT);
                    dialog.show();
                    dialog.setCancelable(true);
                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    Window window = dialog.getWindow();
                    window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
                    camera = dialog.findViewById(R.id.camera);
                    folder = dialog.findViewById(R.id.folder);
                    folder.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                        /*checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                STORAGE_PERMISSION_CODE);*/
                            Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                           // i.putExtra(Intent.EXTRA_ALLOW_MULTIPLE,true);
                            startActivityForResult(i, 100);
                            dialog.dismiss();



                        }
                    });
                    camera.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            //checkPermission(Manifest.permission.CAMERA, CAMERA_PERMISSION_CODE);
                            Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            startActivityForResult(cameraIntent, 101);
                            dialog.dismiss();


                        }
                    });
                }



            }
        });
        if (picturePath != null && !picturePath.isEmpty() && !picturePath.equals("null")) {

            Picasso.get().load(imagePic).into(shopImage);

            bitmap = ((BitmapDrawable) shopImage.getDrawable().getCurrent()).getBitmap();
            Log.e("bitmap", "" + bitmap);
            converetdImage = getResizedBitmap(bitmap, 500);

        } else { }

    }

    private void init() {
        state =findViewById(R.id.state);
        district=findViewById(R.id.district);
        districtid =findViewById(R.id.districtId);
        stateid =findViewById(R.id.stateId);
        districtList=findViewById(R.id.districtList);
        stateList=findViewById(R.id.stateList);
        citylist=findViewById(R.id.citylist);
        arealist=findViewById(R.id.arealist);
        cityid=findViewById(R.id.cityid);
        city=findViewById(R.id.city);
        addshopgobackarrow=findViewById(R.id.addshopgobackarrow);
        shopImage=findViewById(R.id.shopImage);
        submitshop=findViewById(R.id.submitshop);
        areaid=findViewById(R.id.areaid);
        areaname=findViewById(R.id.areaname);
        etaddress=findViewById(R.id.etaddress);
        etpname=findViewById(R.id.etpname);
        etshopname=findViewById(R.id.etshopname);
        etpersonemail=findViewById(R.id.etpersonemail);
        etshopphone=findViewById(R.id.etshopphone);
        etwtsp=findViewById(R.id.etwtsp);
        etwebsite=findViewById(R.id.etwebsite);
        etmsg=findViewById(R.id.etmsg);
        etlandmark=findViewById(R.id.etlandmark);
    }

    private Bitmap getResizedBitmap(Bitmap image, int maxSize) {

        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }





    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 100 && resultCode == RESULT_OK && data != null) {

//the image URI
            Uri selectedImage = data.getData();

            //     imagepath=selectedImage.getPath();


            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            picturePath = cursor.getString(columnIndex);
            cursor.close();


            if (picturePath != null && !picturePath.equals("")) {
                image = new File(picturePath);
            }

            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);
                converetdImage = getResizedBitmap(bitmap, 500);
                shopImage.setImageBitmap(converetdImage);
                shopImage.setVisibility(View.VISIBLE);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else if (requestCode == 101 && resultCode == Activity.RESULT_OK) {


            Bitmap converetdImage = (Bitmap) data.getExtras().get("data");
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            converetdImage.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
            shopImage.setImageBitmap(converetdImage);
            shopImage.setVisibility(View.VISIBLE);
            image = new File(Environment.getExternalStorageDirectory(), "shopImage.jpg");
            FileOutputStream fo;
            try {
                fo = new FileOutputStream(image);
                fo.write(bytes.toByteArray());
                fo.close();
            } catch (IOException e) {
                e.printStackTrace();
            }


        }
    }


    @Override
    public void onResume(){
        super.onResume();
        if (checkPermissions()) {
            getLastLocation();
        }

    }
        @SuppressLint("MissingPermission")
        private void getLastLocation(){
            if (checkPermissions()) {
                if (isLocationEnabled()) {
                    mFusedLocationClient.getLastLocation().addOnCompleteListener(
                            new OnCompleteListener<Location>() {
                                @Override
                                public void onComplete(@NonNull Task<Location> task) {
                                    Location location = task.getResult();
                                    if (location == null) {
                                        requestNewLocationData();
                                    } else {
                                        lat=location.getLatitude();
                                        lng=location.getLongitude();
                                        Toast.makeText(AddShopActivity1.this, lat+" "+lng, Toast.LENGTH_SHORT).show();
                                        LocationAddress locationAddress = new LocationAddress();
                                        locationAddress.getAddressFromLocation(location.getLatitude(), location.getLongitude(),
                                                getApplicationContext(), new GeocoderHandler());
                                    }
                                }
                            }
                    );
                } else {
                    Toast.makeText(this, "Turn on location", Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(intent);
                }
            } else {
                requestPermissions();
            }
        }
        @SuppressLint("MissingPermission")
        private void requestNewLocationData(){

            LocationRequest mLocationRequest = new LocationRequest();
            mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            mLocationRequest.setInterval(0);
            mLocationRequest.setFastestInterval(0);
            mLocationRequest.setNumUpdates(1);

            mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
            mFusedLocationClient.requestLocationUpdates(
                    mLocationRequest, mLocationCallback,
                    Looper.myLooper()
            );

        }
        private LocationCallback mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                Location mLastLocation = locationResult.getLastLocation();
               lat=mLastLocation.getLatitude();
               lng=mLastLocation.getLongitude();

            }
        };

    private boolean checkPermissions() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        return false;
    }

    private void requestPermissions() {
        ActivityCompat.requestPermissions(
                this,
                new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION},
                PERMISSION_ID
        );
    }
    private boolean isLocationEnabled() {
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(
                LocationManager.NETWORK_PROVIDER
        );
    }



    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        // save file url in bundle as it will be null on scren orientation
        // changes
        outState.putParcelable("pic_uri", picUri);
    }
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        // get the file url
        picUri = savedInstanceState.getParcelable("pic_uri");
    }

    private boolean hasPermission(String permission) {
        if (canMakeSmores()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                return (checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED);
            }
        }
        return true;
    }
    private boolean canMakeSmores() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }
    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {

            case PERMISSION_ID:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Granted. Start getting the location information
                }
                break;
            case CAMERA_ALL_PERMISSIONS_RESULT:
                if (grantResults.length <= 0 || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(this, "Permission denied", Toast.LENGTH_SHORT).show();
                }
                else {
//                    startActivityForResult(, 200);
                }


        }

    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(AddShopActivity1.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }
    private class GeocoderHandler extends Handler {
        @Override
        public void handleMessage(Message message) {
            String locationAddress;
            switch (message.what) {
                case 1:
                    Bundle bundle = message.getData();
                    locationAddress = bundle.getString("address");
                    break;
                default:
                    locationAddress = null;
            }
         etaddress.setText(locationAddress);
        }
    }

    private ArrayList findUnAskedCameraPermissions(ArrayList wanted) {
        ArrayList result = new ArrayList();

        for (Object perm : wanted) {
            if (!hascameraPermission((String)perm)) {
                result.add(perm);
            }
        }

        return result;
    }
    private boolean hascameraPermission(String permission) {
        if (canMakeSmores()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                return (checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED);
            }
        }
        return true;
    }
    private void districtlist()
    {
        districtId="";
        progress = new Dialog(AddShopActivity1.this);
        progress.setContentView(R.layout.customdialog);
        progress.setCancelable(false);
        progress.show();
        UserInterface userInterface= ApiClient.getClient().create(UserInterface.class);
        Call<DistrictResponse>call=userInterface.getDistrict(stateId);
        call.enqueue(new Callback<DistrictResponse>() {
            @Override
            public void onResponse(Call<DistrictResponse> call, Response<DistrictResponse> response) {
                if (response.code()==200)
                {
                    allDistrict=response.body().getData();
                    final  Dialog districtDialog=new Dialog(AddShopActivity1.this);
                    districtDialog.setContentView(R.layout.recyclerdialog);
                    DisplayMetrics metrics=getResources().getDisplayMetrics();
                    int width=metrics.widthPixels;
                    districtDialog.getWindow().setLayout(width, LinearLayout.LayoutParams.WRAP_CONTENT);
                    districtRecycler=districtDialog.findViewById(R.id.dialogRecycler);
                    districtRecycler.setLayoutManager(new LinearLayoutManager(AddShopActivity1.this,LinearLayoutManager.VERTICAL,false));
                    districtRecycler.setAdapter(new DistrictListAdapter(AddShopActivity1.this,allDistrict,districtDialog, new CustomItemClickListener() {
                        @Override
                        public void onItemClick(View v, int position, String value) {
                            district.setText(value);
                            districtId=String.valueOf(position);
                        }
                    }));
                    progress.dismiss();
                    districtDialog.show();
                }
                else {
                    progress.dismiss();
                    Toast.makeText(AddShopActivity1.this, "Something Goes Wrong", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<DistrictResponse> call, Throwable t) {
                progress.dismiss();
                Toast.makeText(AddShopActivity1.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
    private void statelist()
    {
        state.setText("");
        progress = new Dialog(AddShopActivity1.this);
        progress.setContentView(R.layout.customdialog);
        progress.setCancelable(false);
        progress.show();
        UserInterface userInterface= ApiClient.getClient().create(UserInterface.class);
        /*--------------------------------------*/
        Call<StateResponse>call=userInterface.stateList();
        call.enqueue(new Callback<StateResponse>() {
            @Override
            public void onResponse(Call<StateResponse> call, Response<StateResponse> response) {
                if (response.code()==200)
                {
                    allStateList=response.body().getData();
                    final  Dialog stateDialog=new Dialog(AddShopActivity1.this);
                    stateDialog.setContentView(R.layout.recyclerdialog);
                    DisplayMetrics metrics=getResources().getDisplayMetrics();
                    int width=metrics.widthPixels;
                    stateDialog.getWindow().setLayout(width, LinearLayout.LayoutParams.WRAP_CONTENT);
                    stateRecycler=stateDialog.findViewById(R.id.dialogRecycler);
                    stateRecycler.setLayoutManager(new LinearLayoutManager(AddShopActivity1.this,LinearLayoutManager.VERTICAL,false));
                    stateRecycler.setAdapter(new StateListAdapter(AddShopActivity1.this,allStateList,stateDialog, new CustomItemClickListener() {
                        @Override
                        public void onItemClick(View v, int position, String value) {
                            state.setText(value);
                            stateId =String.valueOf(position);
                        }
                    }));
                    progress.dismiss();
                    stateDialog.show();
                }
                else
                {
                    progress.dismiss();
                    Toast.makeText(AddShopActivity1.this, "Something Goes Wrong", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<StateResponse> call, Throwable t) {
                progress.dismiss();
                Toast.makeText(AddShopActivity1.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

}

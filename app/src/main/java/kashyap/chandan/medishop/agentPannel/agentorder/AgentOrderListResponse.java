package kashyap.chandan.medishop.agentPannel.agentorder;

import java.io.Serializable;
import java.util.List;

public class AgentOrderListResponse implements Serializable {

    /**
     * status : {"code":200,"message":"Orders List(Here product_catagories belongs to pc_name)"}
     * data : [{"unique_id":"SBORD7","total":"300","date_time":"2020-04-06 13:32:18","shop_name":"Shakti medical","no_of_items":2,"details":[{"id":"14","unique_id":"SBORD7","agent_id":"2","shop_id":"1","product_catagories":"2","product_id":"1","quantity":"2","price":"100","total":"200","date_time":"2020-04-06 13:32:18","status":"1","product_name":"Paracetmol","pc_name":"B Catagory"},{"id":"13","unique_id":"SBORD7","agent_id":"2","shop_id":"1","product_catagories":"1","product_id":"1","quantity":"2","price":"50","total":"100","date_time":"2020-04-06 13:32:18","status":"1","product_name":"Paracetmol","pc_name":"A Catagory"}]},{"unique_id":"SBORD6","total":"300","date_time":"2020-04-06 13:19:55","shop_name":"Shakti medical","no_of_items":2,"details":[{"id":"12","unique_id":"SBORD6","agent_id":"2","shop_id":"1","product_catagories":"2","product_id":"1","quantity":"2","price":"100","total":"200","date_time":"2020-04-06 13:19:55","status":"1","product_name":"Paracetmol","pc_name":"B Catagory"},{"id":"11","unique_id":"SBORD6","agent_id":"2","shop_id":"1","product_catagories":"1","product_id":"1","quantity":"2","price":"50","total":"100","date_time":"2020-04-06 13:19:55","status":"1","product_name":"Paracetmol","pc_name":"A Catagory"}]},{"unique_id":"SBORD5","total":"300","date_time":"2020-04-06 13:16:30","shop_name":"Shakti medical","no_of_items":2,"details":[{"id":"10","unique_id":"SBORD5","agent_id":"2","shop_id":"1","product_catagories":"2","product_id":"1","quantity":"2","price":"100","total":"200","date_time":"2020-04-06 13:16:30","status":"1","product_name":"Paracetmol","pc_name":"B Catagory"},{"id":"9","unique_id":"SBORD5","agent_id":"2","shop_id":"1","product_catagories":"1","product_id":"1","quantity":"2","price":"50","total":"100","date_time":"2020-04-06 13:16:30","status":"1","product_name":"Paracetmol","pc_name":"A Catagory"}]},{"unique_id":"SBORD2","total":"300","date_time":"2020-04-06 13:03:49","shop_name":"Shakti medical","no_of_items":2,"details":[{"id":"4","unique_id":"SBORD2","agent_id":"2","shop_id":"1","product_catagories":"2","product_id":"1","quantity":"2","price":"100","total":"200","date_time":"2020-04-06 13:03:49","status":"1","product_name":"Paracetmol","pc_name":"B Catagory"},{"id":"3","unique_id":"SBORD2","agent_id":"2","shop_id":"1","product_catagories":"1","product_id":"1","quantity":"2","price":"50","total":"100","date_time":"2020-04-06 13:03:49","status":"1","product_name":"Paracetmol","pc_name":"A Catagory"}]},{"unique_id":"SBORD1","total":"300","date_time":"2020-04-06 12:30:13","shop_name":"Shakti medical","no_of_items":2,"details":[{"id":"2","unique_id":"SBORD1","agent_id":"2","shop_id":"1","product_catagories":"2","product_id":"1","quantity":"2","price":"100","total":"200","date_time":"2020-04-06 12:30:13","status":"1","product_name":"Paracetmol","pc_name":"B Catagory"},{"id":"1","unique_id":"SBORD1","agent_id":"2","shop_id":"1","product_catagories":"1","product_id":"1","quantity":"2","price":"50","total":"100","date_time":"2020-04-06 12:30:13","status":"1","product_name":"Paracetmol","pc_name":"A Catagory"}]}]
     */

    private StatusBean status;
    private List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean implements Serializable {
        /**
         * code : 200
         * message : Orders List(Here product_catagories belongs to pc_name)
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean implements Serializable {
        /**
         * unique_id : SBORD7
         * total : 300
         * date_time : 2020-04-06 13:32:18
         * shop_name : Shakti medical
         * no_of_items : 2
         * details : [{"id":"14","unique_id":"SBORD7","agent_id":"2","shop_id":"1","product_catagories":"2","product_id":"1","quantity":"2","price":"100","total":"200","date_time":"2020-04-06 13:32:18","status":"1","product_name":"Paracetmol","pc_name":"B Catagory"},{"id":"13","unique_id":"SBORD7","agent_id":"2","shop_id":"1","product_catagories":"1","product_id":"1","quantity":"2","price":"50","total":"100","date_time":"2020-04-06 13:32:18","status":"1","product_name":"Paracetmol","pc_name":"A Catagory"}]
         */

        private String unique_id;
        private String total;
        private String date_time;
        private String shop_name;
        private int no_of_items;
        private List<DetailsBean> details;

        public String getUnique_id() {
            return unique_id;
        }

        public void setUnique_id(String unique_id) {
            this.unique_id = unique_id;
        }

        public String getTotal() {
            return total;
        }

        public void setTotal(String total) {
            this.total = total;
        }

        public String getDate_time() {
            return date_time;
        }

        public void setDate_time(String date_time) {
            this.date_time = date_time;
        }

        public String getShop_name() {
            return shop_name;
        }

        public void setShop_name(String shop_name) {
            this.shop_name = shop_name;
        }

        public int getNo_of_items() {
            return no_of_items;
        }

        public void setNo_of_items(int no_of_items) {
            this.no_of_items = no_of_items;
        }

        public List<DetailsBean> getDetails() {
            return details;
        }

        public void setDetails(List<DetailsBean> details) {
            this.details = details;
        }

        public static class DetailsBean implements Serializable {
            /**
             * id : 14
             * unique_id : SBORD7
             * agent_id : 2
             * shop_id : 1
             * product_catagories : 2
             * product_id : 1
             * quantity : 2
             * price : 100
             * total : 200
             * date_time : 2020-04-06 13:32:18
             * status : 1
             * product_name : Paracetmol
             * pc_name : B Catagory
             */

            private String id;
            private String unique_id;
            private String agent_id;
            private String shop_id;
            private String product_catagories;
            private String product_id;
            private String quantity;
            private String price;
            private String total;
            private String date_time;
            private String status;
            private String product_name;
            private String pc_name;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getUnique_id() {
                return unique_id;
            }

            public void setUnique_id(String unique_id) {
                this.unique_id = unique_id;
            }

            public String getAgent_id() {
                return agent_id;
            }

            public void setAgent_id(String agent_id) {
                this.agent_id = agent_id;
            }

            public String getShop_id() {
                return shop_id;
            }

            public void setShop_id(String shop_id) {
                this.shop_id = shop_id;
            }

            public String getProduct_catagories() {
                return product_catagories;
            }

            public void setProduct_catagories(String product_catagories) {
                this.product_catagories = product_catagories;
            }

            public String getProduct_id() {
                return product_id;
            }

            public void setProduct_id(String product_id) {
                this.product_id = product_id;
            }

            public String getQuantity() {
                return quantity;
            }

            public void setQuantity(String quantity) {
                this.quantity = quantity;
            }

            public String getPrice() {
                return price;
            }

            public void setPrice(String price) {
                this.price = price;
            }

            public String getTotal() {
                return total;
            }

            public void setTotal(String total) {
                this.total = total;
            }

            public String getDate_time() {
                return date_time;
            }

            public void setDate_time(String date_time) {
                this.date_time = date_time;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public String getProduct_name() {
                return product_name;
            }

            public void setProduct_name(String product_name) {
                this.product_name = product_name;
            }

            public String getPc_name() {
                return pc_name;
            }

            public void setPc_name(String pc_name) {
                this.pc_name = pc_name;
            }
        }
    }
}

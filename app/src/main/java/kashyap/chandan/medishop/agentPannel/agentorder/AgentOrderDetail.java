package kashyap.chandan.medishop.agentPannel.agentorder;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import java.util.List;

import kashyap.chandan.medishop.Order.OrderResponse;
import kashyap.chandan.medishop.R;

public class AgentOrderDetail extends AppCompatActivity {
ImageView goback;
TextView toolheader,orderno,shopname,personname,contact,total,date;
List<AgentOrderListResponse.DataBean.DetailsBean> order;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_order_detail);
        Intent i=getIntent();
        Bundle b=i.getBundleExtra("bundle");
        order= (List<AgentOrderListResponse.DataBean.DetailsBean>) b.getSerializable("orders");
        System.out.println(""+order.size());
        toolheader=findViewById(R.id.customtoolheader);
        toolheader.setText("Order Detail");
        orderno=findViewById(R.id.orderno);
        orderno.setText(i.getStringExtra("orderid"));
        shopname=findViewById(R.id.shopname);
        shopname.setText(i.getStringExtra("shopname"));
        total=findViewById(R.id.total);
     total.setText(i.getStringExtra("total"));
        date=findViewById(R.id.date);
        date.setText(i.getStringExtra("date"));

        goback=findViewById(R.id.ordergobackarrow);

        addHeaders();
        addData(order);
        goback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                overridePendingTransition(R.anim.transitionrighttoleft,R.anim.transitionrighttoleft);
                finish();
            }
        });
    }
    public void addHeaders() {
        TableLayout tl = findViewById(R.id.table);
        TableRow tr = new TableRow(this);
        tr.setLayoutParams(getLayoutParams());

        tr.addView(getTextView(0, "Sl.No", ContextCompat.getColor(this, R.color.textcolor), Typeface.BOLD, ContextCompat.getColor(this, R.color.background_color)));
        tr.addView(getTextView(0, "Category",  ContextCompat.getColor(this, R.color.textcolor), Typeface.BOLD,ContextCompat.getColor(this, R.color.background_color)));
        tr.addView(getTextView(0, "Product",  ContextCompat.getColor(this, R.color.textcolor), Typeface.BOLD,ContextCompat.getColor(this, R.color.background_color)));
        tr.addView(getTextView(0, "Quantity", ContextCompat.getColor(this, R.color.textcolor), Typeface.BOLD,ContextCompat.getColor(this, R.color.background_color)));
        tr.addView(getTextView(0, "Unit Price", ContextCompat.getColor(this, R.color.textcolor), Typeface.BOLD,ContextCompat.getColor(this, R.color.background_color)));
        tr.addView(getTextView(0, "Amount", ContextCompat.getColor(this, R.color.textcolor), Typeface.BOLD,ContextCompat.getColor(this, R.color.background_color)));
        tl.addView(tr, getTblLayoutParams());
    }
    public void addData(List<AgentOrderListResponse.DataBean.DetailsBean> order) {
       // int numCompanies = companies.length;
        TableLayout tl = findViewById(R.id.table);
        List<AgentOrderListResponse.DataBean.DetailsBean> orders=order;
        for (int i=0;i<orders.size();i++){
            AgentOrderListResponse.DataBean.DetailsBean detail=orders.get(i);
            TableRow tr = new TableRow(this);
            tr.setLayoutParams(getLayoutParams());
            tr.addView(getTextView( 1,""+(i+1), ContextCompat.getColor(this, R.color.background_color), Typeface.NORMAL,  ContextCompat.getColor(this, R.color.textcolor)));
            tr.addView(getTextView(1, detail.getPc_name(),ContextCompat.getColor(this, R.color.background_color), Typeface.NORMAL,  ContextCompat.getColor(this, R.color.textcolor)));
            tr.addView(getTextView(1,detail.getProduct_name(), ContextCompat.getColor(this, R.color.background_color), Typeface.NORMAL,  ContextCompat.getColor(this, R.color.textcolor)));
            tr.addView(getTextView(1, detail.getQuantity(), ContextCompat.getColor(this, R.color.background_color), Typeface.NORMAL, ContextCompat.getColor(this, R.color.textcolor)));
            tr.addView(getTextView(1, detail.getPrice(), ContextCompat.getColor(this, R.color.background_color), Typeface.NORMAL, ContextCompat.getColor(this, R.color.textcolor)));
            tr.addView(getTextView(1,detail.getTotal(), ContextCompat.getColor(this, R.color.background_color), Typeface.NORMAL,  ContextCompat.getColor(this, R.color.textcolor)));
            tl.addView(tr, getTblLayoutParams());
        }

        }
    private TextView getTextView ( int id, String title,int color, int typeface, int bgColor)
    {
        TextView tv = new TextView(this);
        tv.setId(id);
        tv.setText(title.toUpperCase());
        tv.setTextColor(color);
        tv.setPadding(5, 5, 5, 5);
        tv.setTypeface(Typeface.DEFAULT, typeface);
        tv.setBackgroundColor(bgColor);
        tv.setLayoutParams(getLayoutParams());

        return tv;
    }
    @NonNull
    private TableLayout.LayoutParams getTblLayoutParams () {
        return new TableLayout.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT);
    }
    private TableRow.LayoutParams getLayoutParams ()
    {
        TableRow.LayoutParams params = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT);
        params.setMargins(2, 0, 0, 2);
        return params;
    }
}

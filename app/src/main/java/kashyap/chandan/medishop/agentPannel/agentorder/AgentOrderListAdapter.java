package kashyap.chandan.medishop.agentPannel.agentorder;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import kashyap.chandan.medishop.R;
public class AgentOrderListAdapter extends RecyclerView.Adapter<AgentOrderListAdapter.MyViewHolder> {
    Context context;
    AgentOrderListResponse allorders;
    List<AgentOrderListResponse.DataBean> order=new ArrayList<>();
    public AgentOrderListAdapter(Context context, AgentOrderListResponse allorders) {
        this.context=context;
        this.allorders=allorders;
        order=allorders.getData();
    }
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.rec_order_list,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, int position) {
        holder.orderno.setText(order.get(position).getUnique_id());
        holder.date.setText(order.get(position).getDate_time());
        holder.qty.setText(String.valueOf(order.get(position).getNo_of_items()));
        holder.status.setVisibility(View.GONE);
        holder.shopname.setText(order.get(position).getShop_name());
        holder.amt.setText(order.get(position).getTotal());
        holder.detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int pos=holder.getAdapterPosition();
                Intent intent=new Intent(context, AgentOrderDetail.class);
                Bundle bundle=new Bundle();
                List<AgentOrderListResponse.DataBean.DetailsBean> detail=new ArrayList<>();
                detail=order.get(pos).getDetails();
                bundle.putSerializable("orders", (Serializable) detail);
                intent.putExtra("bundle",bundle);
                intent.putExtra("orderid",order.get(pos).getUnique_id());
                intent.putExtra("items",order.get(pos).getNo_of_items());
                intent.putExtra("shopname",order.get(pos).getShop_name());
                intent.putExtra("date",order.get(pos).getDate_time());
                intent.putExtra("total",order.get(pos).getTotal());
                context.startActivity(intent);
            }
        });
    }
    @Override
    public int getItemCount() {
        return order.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        LinearLayout detail,statuslay;
        TextView orderno,qty,amt,date,shopname,status;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            detail=itemView.findViewById(R.id.detail);
            orderno=itemView.findViewById(R.id.orderno);
            qty=itemView.findViewById(R.id.quantity);
            amt=itemView.findViewById(R.id.amount);
            date=itemView.findViewById(R.id.date);
            shopname=itemView.findViewById(R.id.shopname);
            status=itemView.findViewById(R.id.status);
            statuslay=itemView.findViewById(R.id.statuslay);
            statuslay.setVisibility(View.GONE);
        }
    }
}

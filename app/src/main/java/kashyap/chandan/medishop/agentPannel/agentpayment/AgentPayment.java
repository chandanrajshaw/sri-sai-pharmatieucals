package kashyap.chandan.medishop.agentPannel.agentpayment;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.material.snackbar.Snackbar;

import java.util.List;

import kashyap.chandan.medishop.ApiClient;
import kashyap.chandan.medishop.ConnectionDetector;
import kashyap.chandan.medishop.R;
import kashyap.chandan.medishop.UserInterface;
import kashyap.chandan.medishop.admin.SharedPreferencesAdmin;
import kashyap.chandan.medishop.payment.AddPayment;
import kashyap.chandan.medishop.payment.PaymentAdapter;
import kashyap.chandan.medishop.payment.PaymentResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AgentPayment extends AppCompatActivity {
ImageView goback;
TextView toolheader;
ConnectionDetector connectionDetector;
ImageView addpay;
    ShimmerFrameLayout shimmerFrameLayout;
SharedPreferencesAdmin sharedPreferencesAdmin;
    RecyclerView paymentrec;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_payment);
        connectionDetector=new ConnectionDetector(AgentPayment.this);
        shimmerFrameLayout=findViewById(R.id.shimmerview);
sharedPreferencesAdmin=new SharedPreferencesAdmin(AgentPayment.this);
        toolheader=findViewById(R.id.customtoolheader);
        toolheader.setText("Payment");
        goback=findViewById(R.id.ordergobackarrow);
        goback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                overridePendingTransition(R.anim.transitionrighttoleft,R.anim.transitionrighttoleft);
                finish();
            }
        });

        paymentrec=findViewById(R.id.paymentlist);
        if (!connectionDetector.isConnectingToInternet()){
            shimmerFrameLayout.stopShimmer();
            shimmerFrameLayout.setVisibility(View.GONE);
            paymentrec.setVisibility(View.GONE);
            Snackbar.make(AgentPayment.this.getWindow().getDecorView().findViewById(android.R.id.content),"Plzz Connect to internet And Try Again",Snackbar.LENGTH_LONG).show();
        }
        else if (connectionDetector.isConnectingToInternet()){
            UserInterface userInterface= ApiClient.getClient().create(UserInterface.class);
            Call<AgentPaymentListResponse>call=userInterface.getAgentPayment(sharedPreferencesAdmin.getAdminIdPreferences());
            call.enqueue(new Callback<AgentPaymentListResponse>() {
                @Override
                public void onResponse(Call<AgentPaymentListResponse> call, Response<AgentPaymentListResponse> response) {
                    if (response.code()==200)
                    {
                        shimmerFrameLayout.stopShimmer();
                        shimmerFrameLayout.setVisibility(View.GONE);
                        List<AgentPaymentListResponse.DataBean>allpayment=response.body().getData();
                        paymentrec.setLayoutManager(new LinearLayoutManager(AgentPayment.this,LinearLayoutManager.VERTICAL,false));
                        paymentrec.setAdapter(new AgentPaymentAdapter(AgentPayment.this,allpayment));
                       paymentrec.setVisibility(View.VISIBLE);
                    }
                    else if (response.code()!=200)
                    {
                        shimmerFrameLayout.stopShimmer();
                        shimmerFrameLayout.setVisibility(View.GONE);
                        Snackbar.make(AgentPayment.this.getWindow().getDecorView().findViewById(android.R.id.content),"Error Occurs!! Try Again Later",Snackbar.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<AgentPaymentListResponse> call, Throwable t) {
                    Snackbar.make(AgentPayment.this.getWindow().getDecorView().findViewById(android.R.id.content),""+t.getMessage(),Snackbar.LENGTH_LONG).show();
                }
            });
        }

addpay=findViewById(R.id.addfab);
addpay.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        Intent intent=new Intent(AgentPayment.this,AddPayment.class);
        startActivity(intent);
    }
});
    }
    @Override
    protected void onResume() {
        super.onResume();
        shimmerFrameLayout.startShimmer();
    }

    @Override
    protected void onPause() {
        super.onPause();
        shimmerFrameLayout.stopShimmer();
    }
}

package kashyap.chandan.medishop.agentPannel.agentorder;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.material.snackbar.Snackbar;
import kashyap.chandan.medishop.ApiClient;
import kashyap.chandan.medishop.ConnectionDetector;
import kashyap.chandan.medishop.Order.AddOrders;
import kashyap.chandan.medishop.Order.OrderListAdapter;
import kashyap.chandan.medishop.Order.OrderResponse;
import kashyap.chandan.medishop.R;
import kashyap.chandan.medishop.UserInterface;
import kashyap.chandan.medishop.admin.SharedPreferencesAdmin;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class AgentOrders extends AppCompatActivity {
    SharedPreferencesAdmin sharedPreferencesAdmin;
RecyclerView orderrecycler;
ShimmerFrameLayout shimmerFrameLayout;
//TextView tv;
ImageView addFab,gobackarrow;
ConnectionDetector connectionDetector;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_orders);
orderrecycler=findViewById(R.id.orderlist);
shimmerFrameLayout=findViewById(R.id.shimmerview);
addFab=findViewById(R.id.addfab);
        sharedPreferencesAdmin=new SharedPreferencesAdmin(AgentOrders.this);
//tv=findViewById(R.id.customtoolheader);
//tv.setText("No Order");
        connectionDetector=new ConnectionDetector(AgentOrders.this);
gobackarrow=findViewById(R.id.ordergobackarrow);
addFab.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        Intent intent=new Intent(AgentOrders.this,AddOrders.class);
        overridePendingTransition(R.anim.transitionrighttoleft,R.anim.transitionrighttoleft);
        startActivity(intent);
    }
});
if (!connectionDetector.isConnectingToInternet()){
    Snackbar.make(AgentOrders.this.getWindow().getDecorView().findViewById(android.R.id.content),"Plzz Connect to internet And Tryu Again",Snackbar.LENGTH_SHORT).show();

}
else if (connectionDetector.isConnectingToInternet()){

    UserInterface userInterface= ApiClient.getClient().create(UserInterface.class);
    Call<AgentOrderListResponse>call=userInterface.getAgentOrders(sharedPreferencesAdmin.getAdminIdPreferences());
    call.enqueue(new Callback<AgentOrderListResponse>() {
        @Override
        public void onResponse(Call<AgentOrderListResponse> call, Response<AgentOrderListResponse> response) {
            if (response.code()==200){
                AgentOrderListResponse allorders=new AgentOrderListResponse();
                allorders=response.body();
                orderrecycler.setLayoutManager(new LinearLayoutManager(AgentOrders.this,LinearLayoutManager.VERTICAL,false));
                orderrecycler.setAdapter(new AgentOrderListAdapter(AgentOrders.this,allorders));
                shimmerFrameLayout.stopShimmer();
                shimmerFrameLayout.setVisibility(View.GONE);
                orderrecycler.setVisibility(View.VISIBLE);
//                progress.dismiss();
            }
            else if (response.code()!=200)
            {
//                progress.dismiss();
                shimmerFrameLayout.stopShimmer();
                shimmerFrameLayout.setVisibility(View.GONE);
                Snackbar.make(AgentOrders.this.getWindow().getDecorView().findViewById(android.R.id.content),"Error Occurs!!Try Again",Snackbar.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onFailure(Call<AgentOrderListResponse> call, Throwable t) {
//            progress.dismiss();
            Snackbar.make(AgentOrders.this.getWindow().getDecorView().findViewById(android.R.id.content),""+t.getMessage(),Snackbar.LENGTH_SHORT).show();

        }
    });
}
gobackarrow.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        overridePendingTransition(R.anim.transitionrighttoleft,R.anim.transitionrighttoleft);
        finish();
    }
});

    }

    @Override
    protected void onResume() {
        super.onResume();
        shimmerFrameLayout.startShimmer();
    }

    @Override
    protected void onPause() {
        super.onPause();
        shimmerFrameLayout.stopShimmer();
    }
}

package kashyap.chandan.medishop.agentPannel;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;

import kashyap.chandan.medishop.ApiClient;
import kashyap.chandan.medishop.MainActivity;
import kashyap.chandan.medishop.R;
import kashyap.chandan.medishop.UserInterface;
import kashyap.chandan.medishop.admin.AdminChangePassword;
import kashyap.chandan.medishop.admin.SharedPreferencesAdmin;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserChangePin extends AppCompatActivity {
    SharedPreferencesAdmin sharedPreferencesAdmin;
ImageView userchangepasswordgobackarrow;
TextView userpwdchng;
TextInputEditText userchangeoldpassword,userchangenewpassword,userchangeconfirmpassword;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
  setContentView(R.layout.activity_user_change_pin);
        sharedPreferencesAdmin=new SharedPreferencesAdmin(UserChangePin.this);
        userchangepasswordgobackarrow=findViewById(R.id.userchangepasswordgobackarrow);
        userpwdchng=findViewById(R.id.userpwdchng);
        userchangeoldpassword=findViewById(R.id.userchangeoldpassword);
        userchangenewpassword=findViewById(R.id.userchangenewpassword);
        userchangeconfirmpassword=findViewById(R.id.userchangeconfirmpassword);
        userchangepasswordgobackarrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(UserChangePin.this,UserDashBoard.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                overridePendingTransition(R.anim.transitionrighttoleft, R.anim.transitionrighttoleft);
                finish();
            }
        });
        userpwdchng.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String oldpwd=userchangeoldpassword.getText().toString();
                String newpwd=userchangenewpassword.getText().toString();
                String confpwd=userchangeconfirmpassword.getText().toString();
                if (oldpwd.isEmpty()&& newpwd.isEmpty()&&confpwd.isEmpty())
                    Toast.makeText(UserChangePin.this, "Please fill all the Field", Toast.LENGTH_SHORT).show();
                else if (oldpwd.isEmpty())
                    Toast.makeText(UserChangePin.this, "Enter Old Password", Toast.LENGTH_SHORT).show();
                else if (newpwd.isEmpty())
                    Toast.makeText(UserChangePin.this, "Enter New Password", Toast.LENGTH_SHORT).show();
                else if (confpwd.isEmpty())
                    Toast.makeText(UserChangePin.this, "Enter Confirm Password", Toast.LENGTH_SHORT).show();
                else if (!oldpwd.equals(sharedPreferencesAdmin.getAdminPassPreferences()))
                    Toast.makeText(UserChangePin.this, "Enter Correct Old Password", Toast.LENGTH_SHORT).show();
                else if (!newpwd.equals(confpwd))
                    Toast.makeText(UserChangePin.this, "New Password & Confirm Password Mismatch", Toast.LENGTH_SHORT).show();

                else if (newpwd.equals(sharedPreferencesAdmin.getAdminPassPreferences()))
                    Toast.makeText(UserChangePin.this, "Old Password And New Password are same", Toast.LENGTH_SHORT).show();
                else
                {
                    final Dialog progress=new Dialog(UserChangePin.this);
                    progress.setContentView(R.layout.customdialog);
                    progress.setCancelable(false);
                    progress.show();
                    UserInterface userInterface= ApiClient.getClient().create(UserInterface.class);
                    Call<AgentChangePwdResponse>call=userInterface.agentChangePassword(sharedPreferencesAdmin.getAdminemailPreferences(),oldpwd,newpwd);
               call.enqueue(new Callback<AgentChangePwdResponse>() {
                   @Override
                   public void onResponse(Call<AgentChangePwdResponse> call, Response<AgentChangePwdResponse> response) {
                       if (response.code()==200){
                         progress.dismiss();
                         Intent intent=new Intent(UserChangePin.this, MainActivity.class);
                         sharedPreferencesAdmin.sessionEnd();
                         intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                         startActivity(intent);
                         finish();
                       }
                       else if (response.code()!=200)
                       {
                           progress.dismiss();
                           Toast.makeText(UserChangePin.this, "Password Not Changed", Toast.LENGTH_SHORT).show();

                       }
                   }

                   @Override
                   public void onFailure(Call<AgentChangePwdResponse> call, Throwable t) {
                       Toast.makeText(UserChangePin.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();

                   }
               });

                }
            }
        });
    }
}
